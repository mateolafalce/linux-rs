/**************************************************************************
 *   elevator.rs  --  This file is part of linux-rs.                      *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::{
    include::linux::{
        kobject::Kobject,
        module::Module,
        mutex_types::Mutex,
        sysfs::Attribute,
        types::{ListHead, SizeT},
    },
    mm::slab::KmemCache,
};

const ELV_NAME_MAX: usize = 16;

/// each queue has an elevator_queue associated with it
pub struct ElevatorQueue {
    type_: ElevatorType,
    elevator_data: *mut u32,
    kobj: Kobject,
    sysfs_lock: Mutex,
    flags: u64,
    // LOOK macro -> DECLARE_HASHTABLE(hash, ELV_HASH_BITS);
}

/// identifies an elevator type, such as AS or deadline
struct ElevatorType {
    /// managed by elevator core
    icq_cache: *mut KmemCache,
    /// see iocontext.rs
    icq_size: SizeT,
    /// ditto
    icq_align: SizeT,
    elevator_attrs: *mut ElvFsEntry,
    elevator_name: *const char,
    elevator_alias: *const char,
    elevator_features: u32,
    elevator_owner: *mut Module,
    #[cfg(feature = "CONFIG_BLK_DEBUG_FS")]
    queue_debugfs_attrs: *const BlkMqDebugfsAttr,
    #[cfg(feature = "CONFIG_BLK_DEBUG_FS")]
    hctx_debugfs_attrs: *const BlkMqDebugfsAttr,
    /// elvname + "_io_cq"
    icq_cache_name: [char; ELV_NAME_MAX + 6],
    list: ListHead,
}

/*   TODO: impl fns
    /// fields provided by elevator implementation
    struct elevator_mq_ops ops;
};*/

struct ElvFsEntry {
    attr: Attribute,
}

/*impl fns
    ssize_t (*show)(struct elevator_queue *, char *);
    ssize_t (*store)(struct elevator_queue *, const char *, size_t);
};*/
