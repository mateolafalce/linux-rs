/**************************************************************************
 *   blk_mq.rs  --  This file is part of linux-rs.                        *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{
    blk_mq::BlkMqHwCtx, blkdev::RequestQueue, kobject::Kobject, spinlock_types::SpinlockT,
    types::ListHead,
};

/// @HCTX_TYPE_DEFAULT:	All I/O not otherwise accounted for.
const HCTX_TYPE_DEFAULT: usize = 0;
/// @HCTX_TYPE_READ:	Just for READ I/O.
const HCTX_TYPE_READ: usize = 1;
/// @HCTX_TYPE_POLL:	Polled I/O of any kind.
const HCTX_TYPE_POLL: usize = 2;
/// @HCTX_MAX_TYPES:	Number of types of hctx.
pub const HCTX_MAX_TYPES: usize = 3;

/// struct blk_mq_ctx - State for a software queue facing the submitting CPUs
pub struct BlkMqCtx {
    lock: SpinlockT,
    rq_lists: [ListHead; HCTX_MAX_TYPES],
    cpu: u32,
    index_hw: [u16; HCTX_MAX_TYPES],
    hctxs: [BlkMqHwCtx; HCTX_MAX_TYPES],
    queue: *mut RequestQueue,
    ctxs: *mut BlkMqCtxs,
    kobj: Kobject,
}

struct BlkMqCtxs {
    kobj: Kobject,
    queue_ctx: BlkMqCtx,
}
