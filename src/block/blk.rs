/**************************************************************************
 *   blk.rs  --  This file is part of linux-rs.                           *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{
    blk_mq::Request, blk_types::BlkStatusT, spinlock_types::SpinlockT, types::ListHead,
};

pub struct BlkFlushQueue {
    mq_flush_lock: SpinlockT,
    flush_pending_idx: bool,
    flush_running_idx: bool,
    rq_status: BlkStatusT,
    flush_pending_since: u64,
    flush_queue: [ListHead; 2],
    flush_data_in_flight: u64,
    flush_rq: *mut Request,
}
