/**************************************************************************
 *   uprobes.rs  --  This file is part of linux-rs.                       *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{mm_types::Page, types::AtomicT, wait::WaitQueueHeadT};

/// Execute out of line area: anonymous executable mapping installed
/// by the probed task to execute the copy of the original instruction
/// mangled by set_swbp().
///
/// On a breakpoint hit, thread contests for a slot.  It frees the
/// slot after singlestep. Currently a fixed number of slots are
/// allocated.
pub struct XolArea {
    /// if all slots are busy
    wq: WaitQueueHeadT,
    /// number of in-use slots
    slot_count: AtomicT,
    /// 0 = free slot
    bitmap: *mut u64,
    xol_mapping: VmSpecialMapping,
    pages: [*mut Page; 2],
    /// We keep the vma's vm_start rather than a pointer to the vma
    /// itself.  The probed process or a naughty kernel module could make
    /// the vma go away, and we must handle that reasonably gracefully.
    /// Page(s) of instruction slots
    vaddr: u64,
}
