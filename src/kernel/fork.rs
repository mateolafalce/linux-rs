/**************************************************************************
 *   fork.rs  --  This file is part of linux-rs.                          *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::{
    linux::{sched::TaskStruct, sched_::task_stack::end_of_stack},
    uapi::linux::magic::STACK_END_MAGIC,
};

pub fn set_task_stack_end_magic(tsk: *mut TaskStruct) {
    let stackend: *mut u64 = end_of_stack(tsk);
    unsafe {
        //for overflow detection
        *stackend = STACK_END_MAGIC;
    }
}
