/**************************************************************************
 *   sched.rs  --  This file is part of linux-rs.                         *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/
#[cfg(feature = "CONFIG_RT_GROUP_SCHED")]
use crate::include::linux::{
    sched::{HighestPrio, RtPrioArray, RtRqCRGS, RtRqConfigSmp},
    spinlock_types_raw::RawSpinlockT,
};

use crate::include::linux::{
    rbtree_types::RbRootCached,
    sched::{LoadWeight, SchedEntity},
};

///Real-Time classes' related field in a runqueue:
#[cfg(feature = "CONFIG_RT_GROUP_SCHED")]
pub struct RtRq {
    active: RtPrioArray,
    rt_nr_running: u32,
    rr_nr_running: u32,
    highest_prio: HighestPrio,
    #[cfg(feature = "CONFIG_SMP")]
    rt_rq_config_smp: RtRqConfigSmp,
    rt_queued: i32,
    rt_throttled: i32,
    rt_time: u64,
    rt_runtime: u64,
    /// Nests inside the rq lock:
    rt_runtime_lock: RawSpinlockT,
    #[cfg(feature = "CONFIG_RT_GROUP_SCHED")]
    rt_rq_config_rt_group_sched: RtRqCRGS,
}

/// CFS-related fields in a runqueue
pub struct CfsRq {
    load: LoadWeight,
    nr_running: u32,
    ///SCHED_{NORMAL,BATCH,IDLE}
    h_nr_running: u32,
    idle_nr_running: u32,
    idle_h_nr_running: u32,
    avg_vruntime: i64,
    avg_load: u64,
    exec_clock: u64,
    min_vruntime: u64,
    #[cfg(feature = "CONFIG_SCHED_CORE")]
    config_sched_core: ConfigSchedCore,
    #[cfg(feature = "CONFIG_64BIT")]
    min_vruntime_copy: u64,
    tasks_timeline: RbRootCached,
    /// 'curr' points to currently running entity on this cfs_rq.
    /// It is set to NULL otherwise (i.e when none are currently running).
    curr: *mut SchedEntity,
    next: *mut SchedEntity,
    #[cfg(feature = "CONFIG_SCHED_DEBUG")]
    nr_spread_over: u32,
    #[cfg(feature = "CONFIG_SMP")]
    config_smp: ConfigSmp,
}

#[cfg(feature = "CONFIG_SCHED_CORE")]
struct ConfigSchedCore {
    forceidle_seq: u32,
    min_vruntime_fi: u64,
}

#[cfg(feature = "CONFIG_SMP")]
struct ConfigSmp {
    /// CFS load tracking
    avg: crate::include::linux::sched::SchedAvg,
    #[cfg(not(feature = "CONFIG_64BIT"))]
    last_update_time_copy: u64,
    removed: Removed,
}

#[cfg(feature = "CONFIG_SMP")]
struct Removed {
    lock: RawSpinlockT,
    nr: i32,
    load_avg: u64,
    util_avg: u64,
    runnable_avg: u64,
}

/*

    struct {
        raw_spinlock_t	lock ____cacheline_aligned;
        int		nr;
        unsigned long	load_avg;
        unsigned long	util_avg;
        unsigned long	runnable_avg;
    } removed;

#ifdef CONFIG_FAIR_GROUP_SCHED
    u64			last_update_tg_load_avg;
    unsigned long		tg_load_avg_contrib;
    long			propagate;
    long			prop_runnable_sum;

    /*
     *   h_load = weight * f(tg)
     *
     * Where f(tg) is the recursive weight fraction assigned to
     * this group.
     */
    unsigned long		h_load;
    u64			last_h_load_update;
    struct sched_entity	*h_load_next;
#endif /* CONFIG_FAIR_GROUP_SCHED */
#endif /* CONFIG_SMP */


#ifdef CONFIG_FAIR_GROUP_SCHED
    struct rq		*rq;	/* CPU runqueue to which this cfs_rq is attached */

    /*
     * leaf cfs_rqs are those that hold tasks (lowest schedulable entity in
     * a hierarchy). Non-leaf lrqs hold other higher schedulable entities
     * (like users, containers etc.)
     *
     * leaf_cfs_rq_list ties together list of leaf cfs_rq's in a CPU.
     * This list is used during load balance.
     */
    int			on_list;
    struct list_head	leaf_cfs_rq_list;
    struct task_group	*tg;	/* group that "owns" this runqueue */

    /* Locally cached copy of our task_group's idle value */
    int			idle;

#ifdef CONFIG_CFS_BANDWIDTH
    int			runtime_enabled;
    s64			runtime_remaining;

    u64			throttled_pelt_idle;
#ifndef CONFIG_64BIT
    u64                     throttled_pelt_idle_copy;
#endif
    u64			throttled_clock;
    u64			throttled_clock_pelt;
    u64			throttled_clock_pelt_time;
    u64			throttled_clock_self;
    u64			throttled_clock_self_time;
    int			throttled;
    int			throttle_count;
    struct list_head	throttled_list;
    struct list_head	throttled_csd_list;
#endif /* CONFIG_CFS_BANDWIDTH */
#endif /* CONFIG_FAIR_GROUP_SCHED */
};
 */
