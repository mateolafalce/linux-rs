/**************************************************************************
 *   workqueue.rs  --  This file is part of linux-rs.                     *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{
    mutex_types::Mutex,
    types::{AtomicT, ListHead, RcuHead},
};

/// The externally visible workqueue.  It relays the issued work items to
/// the appropriate worker_pool through its pool_workqueues.
pub struct WorkqueueStruct {
    /// WR: all pwqs of this wq
    pwqs: ListHead,
    /// PR: list of all workqueues
    list: ListHead,
    /// protects this wq
    mutex: Mutex,
    /// WQ: current work color
    work_color: i32,
    /// WQ: current flush color
    flush_color: i32,
    /// flush in progress
    nr_pwqs_to_flush: AtomicT,
    /// WQ: first flusher
    first_flusher: *mut WqFlusher,
    /// WQ: flush waiters
    flusher_queue: ListHead,
    /// WQ: flush overflow list
    flusher_overflow: ListHead,
    /// MD: pwqs requesting rescue
    maydays: ListHead,
    /// MD: rescue worker
    rescuer: *mut Worker,
    /// WQ: drain in progress
    nr_drainers: i32,
    /// WQ: saved pwq max_active
    saved_max_active: i32,
    /// PW: only for unbound wqs
    unbound_attrs: *mut WorkqueueAttrs,
    /// PW: only for unbound wqs
    dfl_pwq: *mut PoolWorkqueue,
    /// I: for sysfs interface
    #[cfg(feature = "CONFIG_SYSFS")]
    wq_dev: *mut WqDevice,
    #[cfg(feature = "CONFIG_LOCKDEP")]
    lock_name: *mut char,
    #[cfg(feature = "CONFIG_LOCKDEP")]
    key: LockClassKey,
    #[cfg(feature = "CONFIG_LOCKDEP")]
    lockdep_map: LockdepMap,
    /// I: workqueue name
    name: [char; WQ_NAME_LEN],
    /// Destruction of workqueue_struct is RCU protected to allow walking
    /// the workqueues list without grabbing wq_pool_mutex.
    /// This is used to dump all workqueues from sysrq.
    rcu: RcuHead,
    /// hot fields used during command issue, aligned to cacheline
    /// WQ: WQ_* flags
    flags: u32,
    /// I: per-cpu pwqs
    cpu_pwq: *mut PoolWorkqueue,
}
