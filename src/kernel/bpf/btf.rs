/**************************************************************************
 *   bpf.rs  --  This file is part of linux-rs.                           *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{refcount::RefCountT, types::RcuHead};

pub struct Btf {
    data: *mut u32,
    types: *mut BtfType,
    resolved_ids: *mut u32,
    resolved_sizes: *mut u32,
    strings: *const char,
    nohdr_data: *mut u32,
    hdr: BtfHeader,
    /// includes VOID for base BTF
    nr_types: u32,
    types_size: u32,
    data_size: u32,
    refcnt: RefCountT,
    id: u32,
    rcu: RcuHead,
    // TODO: impl fns kfunc_set_tab: *mut BtfKfuncSetTab,
    // dtor_kfunc_tab: *mut BtfIdDtorKfuncTab
    struct_meta_tab: *mut BtfStructMetas,
    /// split BTF support
    base_btf: *mut Btf,
    /// first type ID in this BTF (0 for base BTF)
    start_id: u32,
    /// first string offset (0 for base BTF)
    start_str_off: u32,
    name: [char; MODULE_NAME_LEN],
    kernel_btf: bool,
}
