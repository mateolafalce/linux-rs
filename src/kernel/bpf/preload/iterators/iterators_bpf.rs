/**************************************************************************
 *   iterators_bpf.rs  --  This file is part of linux-rs.                 *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::{include::uapi::linux::bpf::BpfFuncInfo, kernel::bpf::btf::Btf};

struct BpfProgAux {
    id: u32,
    name: [char; 16],
    attach_func_name: *const char,
    dst_prog: *mut BpfProg,
    func_info: *mut BpfFuncInfo,
    btf: *mut Btf,
}

pub struct BpfProg {
    aux: *mut BpfProgAux,
}

#[cfg(all(feature = "CONFIG_RT_GROUP_SCHED", feature = "CONFIG_CGROUP_BPF"))]
struct Btf {
    strings: [char; 20],
    types: *mut BtfType,
    hdr: BtfHeader,
}

#[cfg(all(feature = "CONFIG_RT_GROUP_SCHED", feature = "CONFIG_CGROUP_BPF"))]
struct BtfType {
    name_off: u32,
}

#[cfg(all(feature = "CONFIG_RT_GROUP_SCHED", feature = "CONFIG_CGROUP_BPF"))]
struct BtfHeader {
    str_len: u32,
}
