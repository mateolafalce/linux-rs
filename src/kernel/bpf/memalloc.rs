/**************************************************************************
 *   memalloc.rs  --  This file is part of linux-rs.                      *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::{
    arch::x86::include::asm::local::LocalT,
    include::linux::{
        llist::{LlistHead, LlistNode},
        memcontrol::ObjCgroup,
        types::{AtomicT, RcuHead},
    },
};

pub struct BpfMemCache {
    /// per-cpu list of free objects of size 'unit_size'.
    /// All accesses are done with interrupts disabled and 'active' counter
    /// protection with __llist_add() and __llist_del_first().
    free_llist: LlistHead,
    active: LocalT,
    /// Operations on the free_list from unit_alloc/unit_free/bpf_mem_refill
    /// are sequenced by per-cpu 'active' counter. But unit_free() cannot
    /// fail. When 'active' is busy the unit_free() will add an object to
    /// free_llist_extra.
    free_llist_extra: LlistHead,
    refill_work: IrqWork,
    objcg: *mut ObjCgroup,
    unit_size: i32,
    /// count of objects in free_llist
    free_cnt: i32,
    low_watermark: i32,
    high_watermark: i32,
    batch: i32,
    percpu_size: i32,
    draining: bool,
    tgt: *mut BpfMemCache,
    /// list of objects to be freed after RCU GP
    free_by_rcu: LlistHead,
    free_by_rcu_tail: *mut LlistNode,
    waiting_for_gp: LlistHead,
    waiting_for_gp_tail: *mut LlistNode,
    rcu: RcuHead,
    call_rcu_in_progress: AtomicT,
    free_llist_extra_rcu: LlistHead,
    /// list of objects to be freed after RCU tasks trace GP
    free_by_rcu_ttrace: LlistHead,
    waiting_for_gp_ttrace: LlistHead,
    rcu_ttrace: RcuHead,
    call_rcu_ttrace_in_progress: AtomicT,
}

pub struct BpfMemCaches {
    cache: [BpfMemCache; NUM_CACHES],
}
