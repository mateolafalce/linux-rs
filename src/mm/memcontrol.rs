/**************************************************************************
 *   memcontrol.rs  --  This file is part of linux-rs.                    *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{memcontrol::MEMCG_NR_STAT, types::Atomic64T};

pub struct MemcgVmstats {
    /// Aggregated (CPU and subtree) page state & events
    state: [i64; MEMCG_NR_STAT],
    events: [u64; NR_MEMCG_EVENTS],
    /// Non-hierarchical (CPU aggregated) page state & events
    state_local: [i64; MEMCG_NR_STAT],
    events_local: [u64; NR_MEMCG_EVENTS],
    /// Pending child counts during tree propagation
    state_pending: [i64; MEMCG_NR_STAT],
    events_pending: [u64; NR_MEMCG_EVENTS],
    /// Stats updates since the last flush
    stats_updates: Atomic64T,
}

pub struct MemcgVmstatsPercpu {
    /// Local (CPU and cgroup) page state & events
    state: [i64; MEMCG_NR_STAT],
    events: [u64; NR_MEMCG_EVENTS],
    /// Delta calculation for lockless upward propagation
    state_prev: [i64; MEMCG_NR_STAT],
    events_prev: [u64; NR_MEMCG_EVENTS],
    /// Cgroup1: threshold notifications & softlimit tree updates
    nr_page_events: u64,
    targets: [u64; MEM_CGROUP_NTARGETS],
    /// Stats updates since the last flush
    stats_updates: u64,
}
