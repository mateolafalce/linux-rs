/**************************************************************************
 *   slab.rs  --  This file is part of linux-rs.                          *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{
    gfp_types::GfpT,
    reciprocal_div::ReciprocalValue,
    types::{ListHead, SlabFlagsT},
};

///  Word size structure that can be atomically updated or read and that
///  contains both the order and the number of objects that a slab of the
///  given order would contain.
struct KmemCacheOrderObjects {
    x: u32,
}

/// Slab cache management.
pub struct KmemCache {
    flags: SlabFlagsT,
    min_partial: u64,
    size: u32,
    object_size: u32,
    reciprocal_size: ReciprocalValue,
    offset: u32,
    oo: KmemCacheOrderObjects,
    min: KmemCacheOrderObjects,
    allocflags: GfpT,
    refcount: i32,
    inuse: u32,
    align: u32,
    red_left_pad: u32,
    name: *const char,
    list: ListHead,
    node: *mut KmemCacheNode,
}
