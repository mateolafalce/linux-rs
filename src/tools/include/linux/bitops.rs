/**************************************************************************
 *   bitops.rs  --  This file is part of linux-rs.                        *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{bits::BITS_PER_BYTE, math::div_round_up};

pub fn bits_to_longs(nr: i32) -> usize {
    div_round_up(nr, bits_per_type(64))
}

// define type
fn bits_per_type(type_: u8) -> i32 {
    match type_ {
        8 => return 8 * BITS_PER_BYTE,
        16 => return 16 * BITS_PER_BYTE,
        32 => return 32 * BITS_PER_BYTE,
        64 => return 64 * BITS_PER_BYTE,
        _ => BITS_PER_BYTE,
    }
}
