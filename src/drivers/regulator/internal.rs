/**************************************************************************
 *   internal.rs  --  This file is part of linux-rs.                      *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{
    dcache::Dentry, device::Device, device::DeviceAttribute, regulator::driver::RegulatorDev,
    suspend::PM_SUSPEND_MAX, types::ListHead,
};

const REGULATOR_STATES_NUM: usize = PM_SUSPEND_MAX + 1;

///  struct regulator
///
///  One for each consumer device.
///  @voltage - a voltage array for each state of runtime, i.e.:
///             PM_SUSPEND_ON
///             PM_SUSPEND_TO_IDLE
///             PM_SUSPEND_STANDBY
///             PM_SUSPEND_MEM
///             PM_SUSPEND_MAX
pub struct Regulator {
    dev: *mut Device,
    list: ListHead,
    always_on: bool,
    bypass: bool,
    device_link: bool,
    uA_load: i32,
    enable_count: u32,
    deferred_disables: u32,
    voltage: [RegulatorVoltage; REGULATOR_STATES_NUM],
    supply_name: *const char,
    dev_attr: DeviceAttribute,
    rdev: *mut RegulatorDev,
    debugfs: *mut Dentry,
}

pub struct RegulatorVoltage {
    min_uV: i32,
    max_uV: i32,
}
