/**************************************************************************
 *   core.rs  --  This file is part of linux-rs.                          *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{device::Device, kref::Kref, types::ListHead};

/// struct pinctrl - per-device pin control state holder
/// @node: global list node
/// @dev: the device using this pin control handle
/// @states: a list of states for this device
/// @state: the current state
/// @dt_maps: the mapping table chunks dynamically parsed from device tree for
///	this device, if any
/// @users: reference count
pub struct Pinctrl {
    node: ListHead,
    dev: *mut Device,
    states: ListHead,
    state: PinctrlState,
    dt_maps: ListHead,
    users: Kref,
}

///  struct pinctrl_state - a pinctrl state for a device
///  @node: list node for struct pinctrl's @states field
///  @name: the name of this state
///  @settings: a list of settings for this state
pub struct PinctrlState {
    node: ListHead,
    name: *const char,
    settings: ListHead,
}
