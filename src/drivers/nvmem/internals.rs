/**************************************************************************
 *   internals.rs  --  This file is part of linux-rs.                     *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::{
    linux::{
        device::Device,
        kref::Kref,
        module::Module,
        nvmem_provider::{NvmemKeepout, NvmemLayout},
        sysfs::BinAttribute,
        types::{ListHead, SizeT},
    },
    uapi::linux::synclink::GpioDesc,
};

pub struct NvmemDevice {
    owner: *mut Module,
    dev: Device,
    node: ListHead,
    stride: i32,
    word_size: i32,
    id: i32,
    refcnt: Kref,
    size: SizeT,
    read_only: bool,
    root_only: bool,
    flags: i32,
    eeprom: BinAttribute,
    base_dev: *mut Device,
    cells: ListHead,
    //TODO: impl fn void (*fixup_dt_cell_info)(struct nvmem_device *nvmem,struct nvmem_cell_info *cell);
    keepout: *const NvmemKeepout,
    nkeepout: u32,
    // TODO impl fn reg_read: NvmemRegReadT,
    // TODO impl fn reg_write: NvmemRegWriteT,
    wp_gpio: *mut GpioDesc,
    layout: *mut NvmemLayout,
    priv_: *mut u32,
    sysfs_cells_populated: bool,
}

impl NvmemDevice {
    const NVMEM_TYPE_UNKNOWN: usize = 0;
    const NVMEM_TYPE_EEPROM: usize = 1;
    const NVMEM_TYPE_OTP: usize = 2;
    const NVMEM_TYPE_BATTERY_BACKED: usize = 3;
    const NVMEM_TYPE_FRAM: usize = 4;
}
