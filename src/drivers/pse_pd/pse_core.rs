/**************************************************************************
 *   pse_core.rs  --  This file is part of linux-rs.                      *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::{
    drivers::regulator::internal::Regulator,
    include::linux::{kref::Kref, pse_pd::pse::PseControllerDev, types::ListHead},
};

///  struct pse_control - a PSE control
///  @pcdev: a pointer to the PSE controller device
///          this PSE control belongs to
///  @ps: PSE PI supply of the PSE control
///  @list: list entry for the pcdev's PSE controller list
///  @id: ID of the PSE line in the PSE controller device
///  @refcnt: Number of gets of this pse_control
pub struct PseControl {
    pcdev: *mut PseControllerDev,
    ps: *mut Regulator,
    list: ListHead,
    id: u32,
    refcnt: Kref,
}
