/**************************************************************************
 *   base.rs  --  This file is part of linux-rs.                          *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{
    device::Device,
    device_::driver::DeviceDriver,
    klist::{Klist, KlistNode},
    kobject::Kobject,
    module::ModuleKobject,
    types::ListHead,
};

/// struct device_private - structure to hold the private to the driver core portions of the device structure.
///
/// @klist_children - klist containing all children of this device
/// @knode_parent - node in sibling list
/// @knode_driver - node in driver list
/// @knode_bus - node in bus list
/// @knode_class - node in class list
/// @deferred_probe - entry in deferred_probe_list which is used to retry the
/// binding of drivers which were unable to get all the resources needed by
/// the device; typically because it depends on another driver getting
/// probed first.
/// @async_driver - pointer to device driver awaiting probe via async_probe
/// @device - pointer back to the struct device that this structure is
/// associated with.
/// @dead - This device is currently either in the process of or has been
/// removed from the system. Any asynchronous events scheduled for this
/// device should exit without taking any action.
///
/// Nothing outside of the driver core should ever touch these fields.
pub struct DevicePrivate {
    klist_children: Klist,
    knode_parent: KlistNode,
    knode_driver: KlistNode,
    knode_bus: KlistNode,
    knode_class: KlistNode,
    deferred_probe: ListHead,
    async_driver: *mut DeviceDriver,
    deferred_probe_reason: *mut char,
    device: *mut Device,
    dead: u8,
}

pub struct DriverPrivate {
    kobj: Kobject,
    klist_devices: Klist,
    knode_bus: KlistNode,
    mkobj: *mut ModuleKobject,
    driver: *mut DeviceDriver,
}
