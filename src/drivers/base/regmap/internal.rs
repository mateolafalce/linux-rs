/**************************************************************************
 *   internal.rs  --  This file is part of linux-rs.                      *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::{
    drivers::hwspinlock::hwspinlock_internal::Hwspinlock,
    include::linux::{
        device::Device,
        gfp_types::GfpT,
        mutex_types::Mutex,
        rbtree_types::RbRoot,
        regmap::{RegDefault, RegSequence, RegmapAccessTable, RegmapBus, RegmapLock, RegmapUnlock},
        spinlock_types::SpinlockT,
        spinlock_types_raw::RawSpinlockT,
        types::{ListHead, SizeT},
        wait::WaitQueueHeadT,
    },
};

pub struct Regmap {
    mutex: Mutex,
    spinlock: SpinlockT,
    spinlock_flags: u64,
    raw_spinlock: RawSpinlockT,
    raw_spinlock_flags: u64,
    lock: RegmapLock,
    unlock: RegmapUnlock,
    lock_arg: *mut u32,
    alloc_flags: GfpT,
    reg_base: u32,
    dev: *mut Device,
    work_buf: *mut u32,
    format: RegmapFormat,
    bus: *const RegmapBus,
    bus_context: *mut u32,
    name: *const char,
    _async: bool,
    async_lock: SpinlockT,
    async_waitq: WaitQueueHeadT,
    async_list: ListHead,
    async_free: ListHead,
    async_ret: i32,
    max_register: u32,
    max_register_is_set: bool,
    wr_table: *const RegmapAccessTable,
    rd_table: *const RegmapAccessTable,
    volatile_table: *const RegmapAccessTable,
    precious_table: *const RegmapAccessTable,
    wr_noinc_table: *const RegmapAccessTable,
    rd_noinc_table: *const RegmapAccessTable,
    defer_caching: bool,
    read_flag_mask: u64,
    write_flag_mask: u64,
    reg_shift: i32,
    reg_stride: i32,
    reg_stride_order: i32,
    force_write_field: bool,
    // TODO cache_ops: *const RegcacheOps,
    // TODO impl enum regcache_type
    cache_size_raw: u32,
    cache_word_size: u32,
    num_reg_defaults: u32,
    num_reg_defaults_raw: u32,
    cache_only: bool,
    cache_bypass: bool,
    cache_free: bool,
    reg_defaults: *mut RegDefault,
    reg_defaults_raw: *const *mut u32,
    cache: *mut u32,
    cache_dirty: bool,
    no_sync_defaults: bool,
    patch: *mut RegSequence,
    patch_regs: i32,
    use_single_read: bool,
    use_single_write: bool,
    can_multi_write: bool,
    max_raw_read: SizeT,
    max_raw_write: SizeT,
    range_tree: RbRoot,
    selector_work_buf: *mut u32,
    hwlock: *mut Hwspinlock,
    can_sleep: bool,
}
/* TODO
#ifdef CONFIG_DEBUG_FS
    bool debugfs_disable;
    struct dentry *debugfs;
    const char *debugfs_name;

    unsigned int debugfs_reg_len;
    unsigned int debugfs_val_len;
    unsigned int debugfs_tot_len;

    struct list_head debugfs_off_cache;
    struct mutex cache_lock;
#endif

    bool (*writeable_reg)(struct device *dev, unsigned int reg);
    bool (*readable_reg)(struct device *dev, unsigned int reg);
    bool (*volatile_reg)(struct device *dev, unsigned int reg);
    bool (*precious_reg)(struct device *dev, unsigned int reg);
    bool (*writeable_noinc_reg)(struct device *dev, unsigned int reg);
    bool (*readable_noinc_reg)(struct device *dev, unsigned int reg);
    int (*reg_read)(void *context, unsigned int reg, unsigned int *val);
    int (*reg_write)(void *context, unsigned int reg, unsigned int val);
    int (*reg_update_bits)(void *context, unsigned int reg, unsigned int mask, unsigned int val);
    int (*read)(void *context, const void *reg_buf, size_t reg_size,
            void *val_buf, size_t val_size);
    int (*write)(void *context, const void *data, size_t count);
    */

struct RegmapFormat {
    buf_size: SizeT,
    reg_bytes: SizeT,
    pad_bytes: SizeT,
    val_bytes: SizeT,
    reg_shift: i8,
}
/*
void (*format_write)(struct regmap *map,
             unsigned int reg, unsigned int val);
void (*format_reg)(void *buf, unsigned int reg, unsigned int shift);
void (*format_val)(void *buf, unsigned int val, unsigned int shift);
unsigned int (*parse_val)(const void *buf);
void (*parse_inplace)(void *buf);*/
