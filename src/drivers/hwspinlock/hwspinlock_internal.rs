/**************************************************************************
 *   hwspinlock_internal.rs  --  This file is part of linux-rs.           *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{device::Device, spinlock_types::SpinlockT};

///  struct hwspinlock - this struct represents a single hwspinlock instance
///  @bank: the hwspinlock_device structure which owns this lock
///  @lock: initialized and used by hwspinlock core
///  @priv: private data, owned by the underlying platform-specific hwspinlock drv
pub struct Hwspinlock {
    bank: *mut HwspinlockDevice,
    lock: SpinlockT,
    priv_: *mut u32,
}

///  struct hwspinlock_device - a device which usually spans numerous hwspinlocks
///  @dev: underlying device, will be used to invoke runtime PM api
///  @ops: platform-specific hwspinlock handlers
///  @base_id: id index of the first lock in this device
///  @num_locks: number of locks in this device
///  @lock: dynamically allocated array of 'struct hwspinlock'
struct HwspinlockDevice {
    dev: *mut Device,
    // TODO Impl fns ops: *const HwspinlockOps,
    base_id: i32,
    num_locks: i32,
    // TODO look for vec struct hwspinlock lock[];
}
