/**************************************************************************
 *   phylink.rs  --  This file is part of linux-rs.                       *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::{
    drivers::net::phy::sfp_bus::SfpBus,
    include::{
        linux::{
            device::Device,
            mutex_types::Mutex,
            netdevice::NetDevice,
            phy::PhyDevice,
            phylink::{PhylinkConfig, PhylinkLinkState, PhylinkPcs},
            timer::TimerList,
            workqueue::WorkStruct,
        },
        uapi::linux::synclink::GpioDesc,
    },
};

/// struct phylink - internal data type for phylink
pub struct Phylink {
    netdev: *mut NetDevice,
    //const struct phylink_mac_ops *mac_ops;
    config: *mut PhylinkConfig,
    pcs: PhylinkPcs,
    dev: *mut Device,
    old_link_state: bool,
    phylink_disable_state: u64,
    phydev: *mut PhyDevice,
    //link_interface: PhyInterfaceT,
    cfg_link_an_mode: u8,
    cur_link_an_mode: u8,
    link_port: u8,
    link_config: PhylinkLinkState,
    //cur_interface: PhyInterfaceT,
    link_gpio: *mut GpioDesc,
    link_irq: u32,
    link_poll: TimerList,
    // void (*get_fixed_state)(struct net_device *dev, struct phylink_link_state *s);
    state_mutex: Mutex,
    phy_state: PhylinkLinkState,
    resolve: WorkStruct,
    pcs_neg_mode: u32,
    pcs_state: u32,
    mac_link_dropped: bool,
    using_mac_select_pcs: bool,
    sfp_bus: *mut SfpBus,
    sfp_may_have_phy: bool,
    sfp_port: u8,
}
