/**************************************************************************
 *   sfp.rs  --  This file is part of linux-rs.                           *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::{
    drivers::net::phy::sfp_bus::SfpBus,
    include::{
        linux::{
            device::Device,
            i2c::I2cAdapter,
            mutex_types::Mutex,
            phy::{MiiBus, PhyDevice},
            sfp::SfpEepromId,
            types::SizeT,
            workqueue::DelayedWork,
        },
        uapi::linux::synclink::GpioDesc,
    },
};

const GPIO_MODDEF0: usize = 0;
const GPIO_LOS: usize = 1;
const GPIO_TX_FAULT: usize = 2;
const GPIO_TX_DISABLE: usize = 3;
const GPIO_RS0: usize = 4;
const GPIO_RS1: usize = 5;
const GPIO_MAX: usize = 6;

const SFP_E_INSERT: usize = 0;
const SFP_E_REMOVE: usize = 1;
const SFP_E_DEV_ATTACH: usize = 2;
const SFP_E_DEV_DETACH: usize = 3;
const SFP_E_DEV_DOWN: usize = 4;
const SFP_E_DEV_UP: usize = 5;
const SFP_E_TX_FAULT: usize = 6;
const SFP_E_TX_CLEAR: usize = 7;
const SFP_E_LOS_HIGH: usize = 8;
const SFP_E_LOS_LOW: usize = 9;
const SFP_E_TIMEOUT: usize = 10;

const SFP_MOD_EMPTY: usize = 0;
const SFP_MOD_ERROR: usize = 1;
const SFP_MOD_PROBE: usize = 2;
const SFP_MOD_WAITDEV: usize = 3;
const SFP_MOD_HPOWER: usize = 4;
const SFP_MOD_WAITPWR: usize = 5;
const SFP_MOD_PRESENT: usize = 6;

const SFP_DEV_DETACHED: usize = 0;
const SFP_DEV_DOWN: usize = 1;
const SFP_DEV_UP: usize = 2;

const SFP_S_DOWN: usize = 0;
const SFP_S_FAIL: usize = 1;
const SFP_S_WAIT: usize = 2;
const SFP_S_INIT: usize = 3;
const SFP_S_INIT_PHY: usize = 4;
const SFP_S_INIT_TX_FAULT: usize = 5;
const SFP_S_WAIT_LOS: usize = 6;
const SFP_S_LINK_UP: usize = 7;
const SFP_S_TX_FAULT: usize = 8;
const SFP_S_REINIT: usize = 9;
const SFP_S_TX_DISABLE: usize = 10;

/*LOOK enum {
    SFP_F_PRESENT = BIT(GPIO_MODDEF0),
    SFP_F_LOS = BIT(GPIO_LOS),
    SFP_F_TX_FAULT = BIT(GPIO_TX_FAULT),
    SFP_F_TX_DISABLE = BIT(GPIO_TX_DISABLE),
    SFP_F_RS0 = BIT(GPIO_RS0),
    SFP_F_RS1 = BIT(GPIO_RS1),
    SFP_F_OUTPUTS = SFP_F_TX_DISABLE | SFP_F_RS0 | SFP_F_RS1,
*/

pub struct Sfp {
    dev: *mut Device,
    i2c: *mut I2cAdapter,
    i2c_mii: *mut MiiBus,
    sfp_bus: *mut SfpBus,
    mod_phy: *mut PhyDevice,
    // TODO const struct sff_data *type;
    i2c_block_size: SizeT,
    max_power_mW: u32,
    //TODO unsigned int (*get_state)(struct sfp *);
    //TODO void (*set_state)(struct sfp *, unsigned int);
    //TODO int (*read)(struct sfp *, bool, u8, void *, size_t);
    //TODO int (*write)(struct sfp *, bool, u8, void *, size_t);
    gpio: [GpioDesc; GPIO_MAX],
    gpio_irq: [i32; GPIO_MAX],
    need_poll: bool,
    /// Access rules:
    /// state_hw_drive: st_mutex held
    /// state_hw_mask: st_mutex held
    /// state_soft_mask: st_mutex held
    /// state: st_mutex held unless reading input bits
    /// Protects state
    st_mutex: Mutex,
    state_hw_drive: u32,
    state_hw_mask: u32,
    state_soft_mask: u32,
    state_ignore_mask: u32,
    state: u32,
    poll: DelayedWork,
    timeout: DelayedWork,
    /// Protects state machine
    sm_mutex: Mutex,
    sm_mod_state: u8,
    sm_mod_tries_init: u8,
    sm_mod_tries: u8,
    sm_dev_state: u8,
    sm_state: u16,
    sm_fault_retries: u8,
    sm_phy_retries: u8,
    id: SfpEepromId,
    module_power_mW: u32,
    module_t_start_up: u32,
    module_t_wait: u32,
    phy_t_retry: u32,
    rate_kbd: u32,
    rs_threshold_kbd: u32,
    rs_state_mask: u32,
    have_a2: bool,
    // TODO const struct sfp_quirk *quirk;
    #[cfg(feature = "CONFIG_HWMON")]
    diag: SfpDiag,
    #[cfg(feature = "CONFIG_HWMON")]
    hwmon_probe: DelayedWork,
    #[cfg(feature = "CONFIG_HWMON")]
    hwmon_tries: u32,
    #[cfg(feature = "CONFIG_HWMON")]
    hwmon_dev: *mut Device,
    #[cfg(feature = "CONFIG_HWMON")]
    hwmon_name: *mut char,
    #[cfg(feature = "CONFIG_DEBUG_FS")]
    debugfs_dir: Dentry,
}

impl Sfp {
    const MDIO_I2C_NONE: usize = 0;
    const MDIO_I2C_MARVELL_C22: usize = 1;
    const MDIO_I2C_C45: usize = 2;
    const MDIO_I2C_ROLLBALL: usize = 3;
}
