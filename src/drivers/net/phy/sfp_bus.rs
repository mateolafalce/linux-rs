/**************************************************************************
 *   sfp_bus.rs  --  This file is part of linux-rs.                       *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::{
    drivers::net::phy::sfp::Sfp,
    include::linux::{device::Device, kref::Kref, phy::PhyDevice, types::ListHead},
};

/// struct sfp_bus - internal representation of a sfp bus
pub struct SfpBus {
    /// private:
    kref: Kref,
    node: ListHead,
    // TODO const struct fwnode_handle *fwnode;
    // TODO const struct sfp_socket_ops *socket_ops;
    sfp_dev: *mut Device,
    sfp: *mut Sfp,
    // TODO const struct sfp_quirk *sfp_quirk;
    // TODO const struct sfp_upstream_ops *upstream_ops;
    upstream: *mut u32,
    phydev: *mut PhyDevice,
    registered: bool,
    started: bool,
}
