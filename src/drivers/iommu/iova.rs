/**************************************************************************
 *   iova.rs  --  This file is part of linux-rs.                          *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{iova::IovaDomain, spinlock_types::SpinlockT, workqueue::DelayedWork};

/// Magazine caches for IOVA ranges.  For an introduction to magazines,
/// see the USENIX 2001 paper "Magazines and Vmem: Extending the Slab
/// Allocator to Many CPUs and Arbitrary Resources" by Bonwick and Adams.
/// For simplicity, we use a static magazine size and don't implement the
/// dynamic size tuning described in the paper.
/// As kmalloc's buffer size is fixed to power of 2, 127 is chosen to
/// assure size of 'iova_magazine' to be 1024 bytes, so that no memory
/// will be wasted. Since only full magazines are inserted into the depot,
/// we don't need to waste PFN capacity on a separate list head either.
const IOVA_MAG_SIZE: usize = 127;

pub struct IovaRcache {
    lock: SpinlockT,
    depot_size: u32,
    depot: *mut IovaMagazine,
    cpu_rcaches: *mut IovaCpuRcache,
    iovad: *mut IovaDomain,
    work: DelayedWork,
}

struct IovaMagazine {
    size: u64,
    next: *mut IovaMagazine,
    pfns: [u64; IOVA_MAG_SIZE],
}

struct IovaCpuRcache {
    lock: SpinlockT,
    loaded: *mut IovaMagazine,
    prev: *mut IovaMagazine,
}
