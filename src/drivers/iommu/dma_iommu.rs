/**************************************************************************
 *   dma_iommu.rs  --  This file is part of linux-rs.                     *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::{
    drivers::iommu::iommu::IommuDomain,
    include::linux::{
        iova::IovaDomain,
        mutex_types::Mutex,
        spinlock_types::SpinlockT,
        timer::TimerList,
        types::{Atomic64T, AtomicT, ListHead, SizeT},
    },
};

pub struct IommuDmaCookie {
    msi_page_list: ListHead,
    /// Domain for flush queue callback;
    /// NULL if flush queue not in use
    fq_domain: *mut IommuDomain,
    /// Options for dma-iommu use
    options: IommuDmaOptions,
    /// Full allocator for IOMMU_DMA_IOVA_COOKIE
    iovad: IovaDomain,
    /// Flush queue
    single_fq: *mut IovaFq,
    percpu_fq: *mut IovaFq,
    /// Number of TLB flushes that have been started
    fq_flush_start_cnt: Atomic64T,
    /// Number of TLB flushes that have been finished
    fq_flush_finish_cnt: Atomic64T,
    /// Timer to regularily empty the flush queues
    fq_timer: TimerList,
    /// 1 when timer is active, 0 when not *
    fq_timer_on: AtomicT,
    /// Trivial linear page allocator for IOMMU_DMA_MSI_COOKIE
    msi_iova: u64,
    mutex: Mutex,
}

struct IommuDmaOptions {
    fq_size: SizeT,
    fq_timeout: u32,
}

/// Per-CPU flush queue structure
struct IovaFq {
    lock: SpinlockT,
    head: u32,
    tail: u32,
    mod_mask: u32,
    entries: [IovaFqEntry; 10],
}

/// Flush queue entry for deferred flushing
struct IovaFqEntry {
    iova_pfn: u64,
    pages: u64,
    freelist: ListHead,
    /// Flush counter when this entry was added
    counter: u64,
}

impl IommuDmaOptions {
    const IOMMU_DMA_OPTS_PER_CPU_QUEUE: usize = 0;
    const IOMMU_DMA_OPTS_SINGLE_QUEUE: usize = 1;
}

impl IommuDmaCookie {
    const IOMMU_DMA_IOVA_COOKIE: usize = 0;
    const IOMMU_DMA_MSI_COOKIE: usize = 1;
}
