/**************************************************************************
 *   io_pgfault.rs  --  This file is part of linux-rs.                    *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::{
    include::linux::{device::Device, mutex_types::Mutex, types::ListHead},
    kernel::workqueue::WorkqueueStruct,
};

/// struct iopf_queue - IO Page Fault queue
/// @wq: the fault workqueue
/// @devices: devices attached to this queue
/// @lock: protects the device list
struct IopfQueue {
    wq: *mut WorkqueueStruct,
    devices: ListHead,
    lock: Mutex,
}

/// struct iopf_device_param - IO Page Fault data attached to a device
/// @dev: the device that owns this param
/// @queue: IOPF queue
/// @queue_list: index into queue->devices
/// @partial: faults that are part of a Page Request Group for which the last
/// request hasn't been submitted yet.
pub struct IopfDeviceParam {
    dev: *mut Device,
    queue: *mut IopfQueue,
    queue_list: ListHead,
    partial: ListHead,
}
