/**************************************************************************
 *   iommu.rs  --  This file is part of linux-rs.                         *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::{
    drivers::iommu::dma_iommu::IommuDmaCookie,
    include::linux::{
        kobject::Kobject, mm_types::MmStruct, mutex_types::Mutex, types::ListHead, xarray::Xarray,
    },
};

pub struct IommuGroup {
    kobj: Kobject,
    devices_kobj: *mut Kobject,
    devices: ListHead,
    pasid_array: Xarray,
    mutex: Mutex,
    iommu_data: *mut u32,
    name: *mut char,
    id: i32,
    default_domain: *mut IommuDomain,
    blocking_domain: *mut IommuDomain,
    domain: *mut IommuDomain,
    entry: ListHead,
    owner_cnt: u32,
    owner: *mut u32,
}

/*
TODO: impl fn
    void (*iommu_data_release)(void *iommu_data);
*/

pub struct IommuDomain {
    type_: u32,
    /// Bitmap of page sizes in use
    pgsize_bitmap: u64,
    geometry: IommuDomainGeometry,
    iova_cookie: *mut IommuDmaCookie,
    fault_data: *mut *mut u32,
    // TODO: impl fn handler: IommuFaultHandlerT,
    handler_token: *mut *mut u32,
    /// IOMMU_DOMAIN_SVA
    mm: *mut MmStruct,
    users: i32,
}
/*
TODO: impl fns
    const struct iommu_domain_ops *ops;
    const struct iommu_dirty_ops *dirty_ops;
    enum iommu_page_response_code (*iopf_handler)(struct iommu_fault *fault,void *data);
};
*/

struct IommuDomainGeometry {
    /// First address that can be mapped
    aperture_start: u64,
    /// Last address that can be mapped
    aperture_end: u64,
    /// DMA only allowed in mappable range?
    force_aperture: bool,
}
