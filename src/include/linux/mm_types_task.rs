/**************************************************************************
 *   mm_types_task.rs  --  This file is part of linux-rs.                 *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::mm_types::Page;

/// When updating this, please also update struct resident_page_types[] in
/// kernel/fork.c
/// Resident file mapping pages
const MM_FILEPAGES: usize = 0;
/// Resident anonymous pages
const MM_ANONPAGES: usize = 1;
/// Anonymous swap entries
const MM_SWAPENTS: usize = 2;
/// Resident shared memory pages
const MM_SHMEMPAGES: usize = 3;
pub const NR_MM_COUNTERS: usize = 4;

pub struct PageFrag {
    page: *mut Page,
    offset: u32,
    size: u32,
}
