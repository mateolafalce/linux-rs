/**************************************************************************
 *   reciprocal_div.rs  --  This file is part of linux-rs.                *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

///  This algorithm is based on the paper "Division by Invariant
///  Integers Using Multiplication" by Torbjörn Granlund and Peter
///  L. Montgomery.
///
///  The assembler implementation from Agner Fog, which this code is
///  based on, can be found here:
///  http://www.agner.org/optimize/asmlib.zip
///
///  This optimization for A/B is helpful if the divisor B is mostly
///  runtime invariant. The reciprocal of B is calculated in the
///  slow-path with reciprocal_value(). The fast-path can then just use
///  a much faster multiplication operation with a variable dividend A
///  to calculate the division A/B.
pub struct ReciprocalValue {
    m: u32,
    sh1: u8,
    sh2: u8,
}
