/**************************************************************************
 *   ptr_ring.rs  --  This file is part of linux-rs.                      *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::spinlock_types::SpinlockT;

pub struct PtrRing {
    producer: i32,
    producer_lock: SpinlockT,
    /// next valid entry
    consumer_head: i32,
    /// next entry to invalidate
    consumer_tail: i32,
    consumer_lock: SpinlockT,
    /// Shared consumer/producer data
    /// Read-only by both the producer and the consumer
    /// max entries in queue
    size: i32,
    /// number of entries to consume in a batch
    batch: i32,
    queue: *mut u32,
}
