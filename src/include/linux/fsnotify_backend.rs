/**************************************************************************
 *   fsnotify_backend.rs  --  This file is part of linux-rs.              *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

#[cfg(all(feature = "CONFIG_RT_GROUP_SCHED", feature = "CONFIG_FSNOTIFY"))]
use crate::include::{
    linux::{spinlock_types::SpinlockT, types::HlistHead},
    uapi::asm_generic::posix_types::__KernelFsidT,
};

/// Inode/vfsmount/sb point to this structure which tracks all marks attached to
/// the inode/vfsmount/sb. The reference to inode/vfsmount/sb is held by this
/// structure. We destroy this structure when there are no more marks attached
/// to it. The structure is protected by fsnotify_mark_srcu.
#[cfg(all(feature = "CONFIG_RT_GROUP_SCHED", feature = "CONFIG_FSNOTIFY"))]
pub struct FsnotifyMarkConnector {
    lock: SpinlockT,
    /// Type of object [lock]
    type_: u16,
    /// flags [lock]
    flags: u16,
    /// fsid of filesystem containing object
    fsid: __KernelFsidT,
    /// Object pointer [lock]
    obj: *mut FsnotifyConnpT,
    /// Used listing heads to free after srcu period expires
    destroy_next: *mut FsnotifyMarkConnector,
    list: HlistHead,
}

#[cfg(all(feature = "CONFIG_RT_GROUP_SCHED", feature = "CONFIG_FSNOTIFY"))]
const FSNOTIFY_CONN_FLAG_HAS_FSID: i32 = 0x01;
#[cfg(all(feature = "CONFIG_RT_GROUP_SCHED", feature = "CONFIG_FSNOTIFY"))]
const FSNOTIFY_CONN_FLAG_HAS_IREF: i32 = 0x02;

#[cfg(all(feature = "CONFIG_RT_GROUP_SCHED", feature = "CONFIG_FSNOTIFY"))]
type FsnotifyConnpT = FsnotifyMarkConnector;
