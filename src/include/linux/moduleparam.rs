/**************************************************************************
 *   moduleparam.rs  --  This file is part of linux-rs.                   *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::module::Module;

/// Chosen so that structs with an unsigned long line up.
pub const MAX_PARAM_PREFIX_LEN: usize = 64 - core::mem::size_of::<u64>();

pub struct KernelParam {
    name: *const char,
    mod_: *mut Module,
    //TODO: impl fn const struct kernel_param_ops *ops;
    perm: *const u16,
    level: i8,
    flags: u8,
    arg: *mut u32,
    str_: *const KparamString,
    arr: *const KparamArray,
}

/// Special one for strings we want to copy into
struct KparamString {
    maxlen: u32,
    string: *mut char,
}

/// Special one for arrays
struct KparamArray {
    max: u32,
    elemsize: u32,
    num: *mut u32,
    // TODO: impl fn const struct kernel_param_ops *ops;
    elem: *mut u32,
}
