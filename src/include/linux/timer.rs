/**************************************************************************
 *   timer.rs  --  This file is part of linux-rs.                         *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{lockdep_types::LockdepMap, types::HlistNode};

pub struct TimerList {
    /// All fields that change during normal runtime grouped to the
    /// same cacheline
    entry: HlistNode,
    expires: u64,
    flags: u32,
    // TODO impl fn function: Option<fn(&mut TimerList)>,
    #[cfg(feature = "CONFIG_LOCKDEP")]
    lockdep_map: LockdepMap,
}

impl TimerList {
    pub fn new() -> Self {
        Self {
            entry: HlistNode::new(),
            expires: 0,
            function: None,
            flags: 0,
            #[cfg(feature = "CONFIG_LOCKDEP")]
            lockdep_map: LockdepMap::new(),
        }
    }
}
