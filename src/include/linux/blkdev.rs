/**************************************************************************
 *   blkdev.rs  --  This file is part of linux-rs.                        *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::{
    block::{
        blk::BlkFlushQueue, blk_mq::BlkMqCtx, blk_rq_qos::RqQos, blk_stat::BlkQueueStats,
        disk_events::DiskEvents, elevator::ElevatorQueue,
    },
    drivers::char::random::TimerRandState,
    include::linux::{
        backing_dev_defs::BackingDevInfo,
        badblocks::Badblocks,
        bio::BioSet,
        blk_mq::{BlkMqTagSet, BlkMqTags, Request},
        blk_types::BlockDevice,
        dcache::Dentry,
        kobject::Kobject,
        lockdep_types::LockdepMap,
        mutex_types::Mutex,
        percpu_refcount::PercpuRef,
        refcount::RefCountT,
        spinlock_types::SpinlockT,
        timer::TimerList,
        types::{AtomicT, ListHead, RcuHead, SectorT},
        uuid::UUID_STRING_LEN,
        wait::WaitQueueHeadT,
        workqueue::{DelayedWork, WorkStruct},
        xarray::Xarray,
    },
};
const DISK_NAME_LEN: usize = 32;
const PARTITION_META_INFO_VOLNAMELTH: usize = 64;
/// Enough for the string representation of any kind of UUID plus NULL.
/// EFI UUID is 36 characters. MSDOS UUID is 11 characters.
const PARTITION_META_INFO_UUIDLTH: usize = UUID_STRING_LEN + 1;

type BlkModeT = u32;

pub struct Gendisk {
    /// major/first_minor/minors should not be set by any new driver, the
    /// block core will take care of allocating them automatically.
    major: i32,
    first_minor: i32,
    minors: i32,
    /// name of major driver
    disk_name: [char; DISK_NAME_LEN],
    /// supported events
    events: u16,
    /// flags related to event processing
    event_flags: u16,
    part_tbl: Xarray,
    part0: *mut BlockDevice,
    queue: *mut RequestQueue,
    private_data: *mut *mut u32,
    bio_split: BioSet,
    state: u64,
    /// open/close mutex
    open_mutex: Mutex,
    /// number of open partitions
    open_partitions: u32,
    bdi: BackingDevInfo,
    /// the queue/ directory
    queue_kobj: Kobject,
    slave_dir: *mut Kobject,
    #[cfg(feature = "CONFIG_BLOCK_HOLDER_DEPRECATED")]
    slave_bdevs: ListHead,
    random: *mut TimerRandState,
    /// RAID
    sync_io: AtomicT,
    ev: *mut DiskEvents,
    /// Zoned block device information for request dispatch control.
    /// nr_zones is the total number of zones of the device. This is always
    /// 0 for regular block devices. conv_zones_bitmap is a bitmap of nr_zones
    /// bits which indicates if a zone is conventional (bit set) or
    /// sequential (bit clear). seq_zones_wlock is a bitmap of nr_zones
    /// bits which indicates if a zone is write locked, that is, if a write
    /// request targeting the zone was dispatched.
    ///
    /// Reads of this information must be protected with blk_queue_enter() /
    /// blk_queue_exit(). Modifying this information is only allowed while
    /// no requests are being processed. See also blk_mq_freeze_queue() and
    /// blk_mq_unfreeze_queue().
    #[cfg(feature = "CONFIG_BLK_DEV_ZONED")]
    nr_zones: u32,
    #[cfg(feature = "CONFIG_BLK_DEV_ZONED")]
    max_open_zones: u32,
    #[cfg(feature = "CONFIG_BLK_DEV_ZONED")]
    max_active_zones: u32,
    #[cfg(feature = "CONFIG_BLK_DEV_ZONED")]
    conv_zones_bitmap: *mut u64,
    #[cfg(feature = "CONFIG_BLK_DEV_ZONED")]
    seq_zones_wlock: *mut u64,
    #[cfg(feature = "CONFIG_CDROM")]
    cdi: *mut CdromDeviceInfo,
    node_id: i32,
    bb: *mut Badblocks,
    lockdep_map: LockdepMap,
    diskseq: u64,
    open_mode: BlkModeT,
    /// Independent sector access ranges. This is always NULL for
    /// devices that do not have multiple independent access ranges.
    ia_ranges: *mut BlkIndependentAccessRanges,
}

/*
TODO impl fns
    const struct block_device_operations *fops;
*/

pub struct RequestQueue {
    /// The queue owner gets to use this for whatever they like.
    /// ll_rw_blk doesn't touch it.
    queuedata: *mut *mut u32,
    elevator: *mut ElevatorQueue,
    /// sw queues
    queue_ctx: *mut BlkMqCtx,
    /// various queue flags, see QUEUE_* below
    queue_flags: u64,
    rq_timeout: u32,
    queue_depth: u32,
    refs: RefCountT,
    /// hw dispatch queues
    nr_hw_queues: u32,
    hctx_table: Xarray,
    q_usage_counter: PercpuRef,
    last_merge: *mut Request,
    queue_lock: SpinlockT,
    quiesce_depth: i32,
    disk: *mut Gendisk,
    /// mq queue kobject
    mq_kobj: Kobject,
    limits: QueueLimits,
    #[cfg(feature = "CONFIG_BLK_DEV_INTEGRITY")]
    integrity: BlkIntegrity,
    #[cfg(feature = "CONFIG_PM")]
    dev: *mut Device,
    #[cfg(feature = "CONFIG_PM")]
    rpm_status: RpmStatus,
    /// Number of contexts that have called blk_set_pm_only(). If this
    /// counter is above zero then only RQF_PM requests are processed.
    pm_only: AtomicT,
    stats: *mut BlkQueueStats,
    rq_qos: *mut RqQos,
    rq_qos_mutex: Mutex,
    /// ida allocated id for this queue.  
    /// Used to index queues from ioctx.
    id: i32,
    dma_pad_mask: u32,
    /// queue settings
    /// Max # of requests
    nr_requests: u64,
    #[cfg(feature = "CONFIG_BLK_INLINE_ENCRYPTION")]
    crypto_profile: *mut BlkCryptoProfile,
    #[cfg(feature = "CONFIG_BLK_INLINE_ENCRYPTION")]
    crypto_kobject: *mut Kobject,
    timeout: TimerList,
    timeout_work: WorkStruct,
    nr_active_requests_shared_tags: AtomicT,
    required_elevator_features: u32,
    sched_shared_tags: *mut BlkMqTags,
    icq_list: ListHead,
    // #[cfg(feature = "CONFIG_BLK_CGROUP")]
    // Look for DECLARE_BITMAP      (blkcg_pols, BLKCG_MAX_POLS);
    #[cfg(feature = "CONFIG_BLK_CGROUP")]
    root_blkg: *mut BlkcgGq,
    #[cfg(feature = "CONFIG_BLK_CGROUP")]
    blkg_list: ListHead,
    #[cfg(feature = "CONFIG_BLK_CGROUP")]
    blkcg_mutex: Mutex,
    node: i32,
    requeue_lock: SpinlockT,
    requeue_list: ListHead,
    requeue_work: DelayedWork,
    #[cfg(feature = "CONFIG_BLK_DEV_IO_TRACE")]
    blk_trace: *mut BlkTrace,
    /// for flush operations
    fq: *mut BlkFlushQueue,
    flush_list: ListHead,
    sysfs_lock: Mutex,
    sysfs_dir_lock: Mutex,
    /// for reusing dead hctx instance in
    /// case of updating nr_hw_queues
    unused_hctx_list: ListHead,
    unused_hctx_lock: SpinlockT,
    mq_freeze_depth: i32,
    /// Throttle data
    #[cfg(feature = "CONFIG_BLK_DEV_THROTTLING")]
    td: *mut ThrotlData,
    rcu_head: RcuHead,
    mq_freeze_wq: WaitQueueHeadT,
    /// Protect concurrent access to q_usage_counter by
    /// percpu_ref_kill() and percpu_ref_reinit().
    mq_freeze_lock: Mutex,
    tag_set: *mut BlkMqTagSet,
    tag_set_list: ListHead,
    debugfs_dir: *mut Dentry,
    sched_debugfs_dir: *mut Dentry,
    rqos_debugfs_dir: *mut Dentry,
    /// Serializes all debugfs metadata operations using the above dentries.
    debugfs_mutex: Mutex,
    mq_sysfs_init_done: bool,
}
/*
TODO: impl fns
    const struct blk_mq_ops *mq_ops;
*/

pub struct PartitionMetaInfo {
    uuid: [char; PARTITION_META_INFO_UUIDLTH],
    volname: [u8; PARTITION_META_INFO_VOLNAMELTH],
}

/// Independent access ranges: struct blk_independent_access_range describes
/// a range of contiguous sectors that can be accessed using device command
/// execution resources that are independent from the resources used for
/// other access ranges. This is typically found with single-LUN multi-actuator
/// HDDs where each access range is served by a different set of heads.
/// The set of independent ranges supported by the device is defined using
/// struct blk_independent_access_ranges. The independent ranges must not overlap
/// and must include all sectors within the disk capacity (no sector holes
/// allowed).
/// For a device with multiple ranges, requests targeting sectors in different
/// ranges can be executed in parallel. A request can straddle an access range
/// boundary.
struct BlkIndependentAccessRange {
    kobj: Kobject,
    sector: SectorT,
    nr_sectors: SectorT,
}

struct BlkIndependentAccessRanges {
    kobj: Kobject,
    sysfs_registered: bool,
    nr_ia_ranges: u32,
    ia_range: [BlkIndependentAccessRange; 10],
}

struct QueueLimits {
    seg_boundary_mask: u64,
    virt_boundary_mask: u64,
    max_hw_sectors: u32,
    max_dev_sectors: u32,
    chunk_sectors: u32,
    max_sectors: u32,
    max_user_sectors: u32,
    max_segment_size: u32,
    physical_block_size: u32,
    logical_block_size: u32,
    alignment_offset: u32,
    io_min: u32,
    io_opt: u32,
    max_discard_sectors: u32,
    max_hw_discard_sectors: u32,
    max_secure_erase_sectors: u32,
    max_write_zeroes_sectors: u32,
    max_zone_append_sectors: u32,
    discard_granularity: u32,
    discard_alignment: u32,
    zone_write_granularity: u32,
    max_segments: u16,
    max_integrity_segments: u16,
    max_discard_segments: u16,
    misaligned: u8,
    discard_misaligned: u8,
    raid_partial_stripes_expensive: u8,
    zoned: bool,
    /// Drivers that set dma_alignment to less than 511 must be prepared to
    /// handle individual bvec's that are not a multiple of a SECTOR_SIZE
    /// due to possible offsets.
    dma_alignment: u32,
}

pub struct BdevHandle {
    bdev: *mut BlockDevice,
    holder: *mut *mut u32,
    mode: BlkModeT,
}

impl Gendisk {
    const GD_NEED_PART_SCAN: usize = 0;
    const GD_READ_ONLY: usize = 1;
    const GD_DEAD: usize = 2;
    const GD_NATIVE_CAPACITY: usize = 3;
    const GD_ADDED: usize = 4;
    const GD_SUPPRESS_PART_SCAN: usize = 5;
    const GD_OWNS_QUEUE: usize = 6;
}

impl QueueLimits {
    /// BLK_BOUNCE_NONE: never bounce (default)
    const BLK_BOUNCE_NONE: usize = 0;
    /// BLK_BOUNCE_HIGH: bounce all highmem pages
    const BLK_BOUNCE_HIGH: usize = 1;
}
