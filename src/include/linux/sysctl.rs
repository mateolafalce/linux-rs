/**************************************************************************
 *   sysctl.rs  --  This file is part of linux-rs.                        *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{
    completion::Completion,
    rbtree_types::{RbNode, RbRoot},
    types::{AtomicT, HlistHead, RcuHead, UmodeT},
    wait::WaitQueueHeadT,
};

///  struct ctl_table_header - maintains dynamic lists of struct ctl_table trees
///  @ctl_table: pointer to the first element in ctl_table array
///  @ctl_table_size: number of elements pointed by @ctl_table
///  @used: The entry will never be touched when equal to 0.
///  @count: Upped every time something is added to @inodes and downed every time
///          something is removed from inodes
///  @nreg: When nreg drops to 0 the ctl_table_header will be unregistered.
///  @rcu: Delays the freeing of the inode. Introduced with "unfuck proc_sysctl ->d_compare()"
pub struct CtlTableHeader {
    ctl_table: *const CtlTable,
    ctl_table_size: i32,
    used: i32,
    count: i32,
    nreg: i32,
    rcu: RcuHead,
    unregistering: *mut Completion,
    ctl_table_arg: *const CtlTable,
    root: *mut CtlTableRoot,
    set: *mut CtlTableSet,
    parent: *mut CtlDir,
    node: *mut CtlNode,
    inodes: HlistHead,
}

///  enum type - Enumeration to differentiate between ctl target types
///  @SYSCTL_TABLE_TYPE_DEFAULT: ctl target with no special considerations
///  @SYSCTL_TABLE_TYPE_PERMANENTLY_EMPTY: Used to identify a permanently
///                                        empty directory target to serve
///                                        as mount point.
impl CtlTableHeader {
    const SYSCTL_TABLE_TYPE_DEFAULT: usize = 0;
    const SYSCTL_TABLE_TYPE_PERMANENTLY_EMPTY: usize = 1;
}

///  A sysctl table is an array of struct ctl_table:
struct CtlTable {
    procname: *const char,
    data: *mut u32,
    maxlen: i32,
    mode: UmodeT,
    //proc_handler: *mut ProcHandler,
    poll: *mut CtlTablePoll,
    extra1: *mut u32,
    extra2: *mut u32,
}

struct CtlTableRoot {
    default_set: CtlTableSet,
    /*TODO impl struct ctl_table_set *(*lookup)(struct ctl_table_root *root);
        void (*set_ownership)(struct ctl_table_header *head,
                      kuid_t *uid, kgid_t *gid);
        int (*permissions)(struct ctl_table_header *head, const struct ctl_table *table);
    */
}

struct CtlTableSet {
    dir: CtlDir,
    // TODO: int (*is_seen)(struct ctl_table_set *);
}

struct CtlDir {
    header: CtlTableHeader,
    root: RbRoot,
}

struct CtlNode {
    node: RbNode,
    header: *mut CtlTableHeader,
}

///  Register a set of sysctl names by calling register_sysctl
///  with an initialised array of struct ctl_table's.
///
///  sysctl names can be mirrored automatically under /proc/sys.  The
///  procname supplied controls /proc naming.
///
///  The table's mode will be honoured for proc-fs access.
///
///  Leaf nodes in the sysctl tree will be represented by a single file
///  under /proc non-leaf nodes will be represented by directories.  A
///  null procname disables /proc mirroring at this node.
///
///  The data and maxlen fields of the ctl_table
///  struct enable minimal validation of the values being written to be
///  performed, and the mode field allows minimal authentication.
///  
///  There must be a proc_handler routine for any terminal nodes
///  mirrored under /proc/sys (non-terminals are handled by a built-in
///  directory handler).  Several default handlers are available to
///  cover common cases.
///  Support for userspace poll() to watch for changes
struct CtlTablePoll {
    event: AtomicT,
    wait: WaitQueueHeadT,
}
