/**************************************************************************
 *   mutex_types.rs  --  This file is part of linux-rs.                   *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

#[cfg(feature = "CONFIG_MUTEX_SPIN_ON_OWNER")]
use crate::include::linux::osq_lock::OptimisticSpinQueue;

use crate::include::linux::{
    spinlock_types_raw::RawSpinlockT,
    types::{AtomicLongT, ListHead},
};

#[cfg(feature = "CONFIG_DEBUG_LOCK_ALLOC")]
use crate::include::linux::lockdep_types::LockdepMap;

/// Simple, straightforward mutexes with strict semantics:
///
/// - only one task can hold the mutex at a time
/// - only the owner can unlock the mutex
/// - multiple unlocks are not permitted
/// - recursive locking is not permitted
/// - a mutex object must be initialized via the API
/// - a mutex object must not be initialized via memset or copying
/// - task may not exit with mutex held
/// - memory areas where held locks reside must not be freed
/// - held mutexes must not be reinitialized
/// - mutexes may not be used in hardware or software interrupt
///   contexts such as tasklets and timers
///
/// These semantics are fully enforced when DEBUG_MUTEXES is
/// enabled. Furthermore, besides enforcing the above rules, the mutex
/// debugging code also implements a number of additional features
/// that make lock debugging easier and faster:
///
/// - uses symbolic names of mutexes, whenever they are printed in debug output
/// - point-of-acquire tracking, symbolic lookup of function names
/// - list of all locks held in the system, printout of them
/// - owner tracking
/// - detects self-recursing locks and prints out all relevant info
/// - detects multi-task circular deadlocks and prints out all affected
///   locks and tasks (and only those tasks)
#[cfg(not(feature = "CONFIG_PREEMPT_RT"))]
pub struct Mutex {
    owner: AtomicLongT,
    wait_lock: RawSpinlockT,
    /// Spinner MCS lock
    #[cfg(feature = "CONFIG_MUTEX_SPIN_ON_OWNER")]
    osq: OptimisticSpinQueue,
    wait_list: ListHead,
    #[cfg(feature = "CONFIG_DEBUG_MUTEXES")]
    magic: *mut u32,
    #[cfg(feature = "CONFIG_DEBUG_LOCK_ALLOC")]
    dep_map: LockdepMap,
}

#[cfg(feature = "CONFIG_PREEMPT_RT")]
pub struct Mutex {
    rtmutex: RtMutexBase,
    #[cfg(feature = "CONFIG_DEBUG_LOCK_ALLOC")]
    dep_map: LockdepMap,
}
