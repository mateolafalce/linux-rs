/**************************************************************************
 *   nvmem_provider.rs  --  This file is part of linux-rs.                *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::{drivers::nvmem::internals::NvmemDevice, include::linux::device::Device};

/// struct nvmem_keepout - NVMEM register keepout range.
///
/// @start: The first byte offset to avoid.
/// @end: One beyond the last byte offset to avoid.
/// @value: The byte to fill reads with for this region.
pub struct NvmemKeepout {
    start: u32,
    end: u32,
    value: u8,
}

/// struct nvmem_layout - NVMEM layout definitions
///
/// @dev:        Device-model layout device.
/// @nvmem:      The underlying NVMEM device
/// @add_cells:      Will be called if a nvmem device is found which
///          has this layout. The function will add layout
///          specific cells with nvmem_add_one_cell().
///
/// A nvmem device can hold a well defined structure which can just be
/// evaluated during runtime. For example a TLV list, or a list of "name=val"
/// pairs. A nvmem layout can parse the nvmem device and add appropriate
/// cells.
pub struct NvmemLayout {
    dev: Device,
    nvmem: *mut NvmemDevice,
    // TODO: impl fn int (*add_cells)(struct nvmem_layout *layout);
}
