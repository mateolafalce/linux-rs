/**************************************************************************
 *   blk_types.rs  --  This file is part of linux-rs.                     *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{
    bio::BioSet,
    blkdev::{Gendisk, PartitionMetaInfo, RequestQueue},
    bvec::{BioVec, BvecIter},
    device::Device,
    fs::Inode,
    kobject::Kobject,
    mutex_types::Mutex,
    part_stat::DiskStats,
    spinlock_types::SpinlockT,
    types::{AtomicT, SectorT},
};

const STAT_READ: usize = 0;
const STAT_WRITE: usize = 1;
const STAT_DISCARD: usize = 2;
const STAT_FLUSH: usize = 3;
pub const NR_STAT_GROUPS: usize = 4;

pub type BlkOpfT = u32;
pub type BlkStatusT = u32;
type BlkQcT = u32;

pub struct BlockDevice {
    bd_start_sect: SectorT,
    bd_nr_sectors: SectorT,
    bd_disk: *mut Gendisk,
    bd_queue: *mut RequestQueue,
    bd_stats: *mut DiskStats,
    bd_stamp: u64,
    ///  read-only policy
    bd_read_only: bool,
    bd_partno: u8,
    bd_write_holder: bool,
    bd_has_submit_bio: bool,
    bd_dev: u32,
    /// will die
    bd_inode: *mut Inode,
    bd_openers: AtomicT,
    /// for bd_inode->i_size updates
    bd_size_lock: SpinlockT,
    bd_claiming: *mut u32,
    bd_holder: *mut u32,
    bd_holder_lock: Mutex,
    bd_holders: i32,
    bd_holder_dir: *mut Kobject,
    /// number of freeze requests
    bd_fsfreeze_count: AtomicT,
    /// serialize freeze/thaw
    bd_fsfreeze_mutex: Mutex,
    bd_meta_info: *mut PartitionMetaInfo,
    #[cfg(feature = "CONFIG_FAIL_MAKE_REQUEST")]
    bd_make_it_fail: bool,
    bd_ro_warned: bool,
    bd_writers: i32,
    /// keep this out-of-line as it's both big
    /// and not needed in the fast path
    bd_device: Device,
}

/*
TODO: impl fns
    const struct blk_holder_ops *bd_holder_ops;
*/

/// main unit of I/O for the block layer and lower layers
/// (ie drivers and stacking drivers)
pub struct Bio {
    /// request queue link
    bi_next: *mut Bio,
    bi_bdev: *mut BlockDevice,
    /// bottom bits REQ_OP, top bits req_flags
    bi_opf: BlkOpfT,
    /// BIO_* below
    bi_flags: u16,
    bi_ioprio: u16,
    bi_status: BlkStatusT,
    __bi_remaining: AtomicT,
    bi_iter: BvecIter,
    bi_cookie: BlkQcT,
    ///LOOK this struct bi_end_io: *mut BioEndIoT,
    bi_private: *mut u32,
    /// Represents the association of the css and request_queue for the bio.
    /// If a bio goes direct to device, it will not have a blkg as it will
    /// not have a request_queue associated with it.  The reference is put
    /// on release of the bio.
    #[cfg(feature = "CONFIG_BLK_CGROUP")]
    bi_blkg: *mut BlkcgGq,
    #[cfg(feature = "CONFIG_BLK_CGROUP")]
    bi_issue: BioIssue,
    #[cfg(all(feature = "CONFIG_BLK_CGROUP", feature = "CONFIG_BLK_CGROUP_IOCOST"))]
    bi_iocost_cost: u64,
    #[cfg(feature = "CONFIG_BLK_INLINE_ENCRYPTION")]
    bi_crypt_context: *mut BioCryptCtx,
    /// data integrity
    #[cfg(feature = "CONFIG_BLK_DEV_INTEGRITY")]
    bi_integrity: *mut BioIntegrityPayload,
    /// how many bio_vec's *
    bi_vcnt: u16,
    /// Everything starting with bi_max_vecs will be preserved by bio_reset()
    /// max bvl_vecs we can hold
    bi_max_vecs: u16,
    /// pin count
    __bi_cnt: AtomicT,
    /// the actual vec list
    bi_io_vec: *mut BioVec,
    bi_pool: *mut BioSet,
    /// We can inline a number of vecs at the end of the bio, to avoid
    /// double allocations for a small number of bio_vecs. This member
    /// MUST obviously be kept at the very end of the bio.
    bi_inline_vecs: [BioVec; 10],
}
