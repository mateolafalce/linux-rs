/**************************************************************************
 *   irqdomain.rs  --  This file is part of linux-rs.                     *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{
    device::Device,
    fwnode::FwnodeHandle,
    irq::{IrqData, IrqDomainChipGeneric},
    mutex_types::Mutex,
    radix_tree::RadixTreeRoot,
    types::ListHead,
};

/// struct irq_domain - Hardware interrupt number translation object
/// @link: Element in global irq_domain list.
/// @name: Name of interrupt domain
/// @ops: Pointer to irq_domain methods
/// @host_data: Private data pointer for use by owner.  Not touched by irq_domain
///	core code.
/// @flags: Per irq_domain flags
/// @mapcount: The number of mapped interrupts
/// @mutex: Domain lock, hierarchical domains use root domain's lock
/// @root: Pointer to root domain, or containing structure if non-hierarchical
///
/// Optional elements:
/// @fwnode: Pointer to firmware node associated with the irq_domain. Pretty easy
/// to swap it for the of_node via the irq_domain_get_of_node accessor
/// @gc: Pointer to a list of generic chips. There is a helper function for
///	setting up one or more generic chips for interrupt controllers
///	drivers using the generic chip library which uses this pointer.
/// @dev: Pointer to the device which instantiated the irqdomain
///	With per device irq domains this is not necessarily the same
/// as @pm_dev.
/// @pm_dev: Pointer to a device that can be utilized for power management
///	purposes related to the irq domain.
/// @parent: Pointer to parent irq_domain to support hierarchy irq_domains
/// @msi_parent_ops: Pointer to MSI parent domain methods for per device domain init
///
/// Revmap data, used internally by the irq domain code:
/// @revmap_size: Size of the linear map table @revmap[]
/// @revmap_tree: Radix map tree for hwirqs that don't fit in the linear map
/// @revmap: Linear table of irq_data pointers
pub struct IrqDomain {
    link: ListHead,
    name: *const char,
    // TODO: const struct irq_domain_ops	*ops;
    host_data: *mut u32,
    flags: u32,
    mapcount: u32,
    mutex: Mutex,
    root: *mut IrqDomain,
    /// Optional data
    fwnode: *mut FwnodeHandle,
    gc: *mut IrqDomainChipGeneric,
    dev: *mut Device,
    pm_dev: *mut Device,
    #[cfg(feature = "CONFIG_IRQ_DOMAIN_HIERARCHY")]
    parent: *mut IrqDomain,
    #[cfg(feature = "CONFIG_GENERIC_MSI_IRQ")]
    msi_parent_ops: *const MsiParentOps,
    /// reverse map data. The linear map gets appended to the irq_domain
    hwirq_max: u64,
    revmap_size: u32,
    revmap_tree: RadixTreeRoot,
    revmap: [IrqData; 10],
}

/// Should several domains have the same device node, but serve
/// different purposes (for example one domain is for PCI/MSI, and the
/// other for wired IRQs), they can be distinguished using a
/// bus-specific token. Most domains are expected to only carry
/// DOMAIN_BUS_ANY.
impl IrqDomain {
    const DOMAIN_BUS_ANY: usize = 0;
    const DOMAIN_BUS_WIRED: usize = 1;
    const DOMAIN_BUS_GENERIC_MSI: usize = 2;
    const DOMAIN_BUS_PCI_MSI: usize = 3;
    const DOMAIN_BUS_PLATFORM_MSI: usize = 4;
    const DOMAIN_BUS_NEXUS: usize = 5;
    const DOMAIN_BUS_IPI: usize = 6;
    const DOMAIN_BUS_FSL_MC_MSI: usize = 7;
    const DOMAIN_BUS_TI_SCI_INTA_MSI: usize = 8;
    const DOMAIN_BUS_WAKEUP: usize = 9;
    const DOMAIN_BUS_VMD_MSI: usize = 10;
    const DOMAIN_BUS_PCI_DEVICE_MSI: usize = 11;
    const DOMAIN_BUS_PCI_DEVICE_MSIX: usize = 12;
    const DOMAIN_BUS_DMAR: usize = 13;
    const DOMAIN_BUS_AMDVI: usize = 14;
    const DOMAIN_BUS_PCI_DEVICE_IMS: usize = 14;
}
