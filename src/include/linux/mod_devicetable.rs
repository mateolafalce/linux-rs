/**************************************************************************
 *   mod_devicetable.rs  --  This file is part of linux-rs.               *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

pub const MDIO_NAME_SIZE: usize = 32;
const ACPI_ID_LEN: usize = 16;

pub struct AcpiDeviceId {
    id: [u8; ACPI_ID_LEN],
    driver_data: u64,
    cls: u32,
    cls_msk: u32,
}

/// Struct used for matching a device
pub struct OfDeviceId {
    name: [char; 32],
    type_: [char; 32],
    compatible: [char; 128],
    data: *const u32,
}
