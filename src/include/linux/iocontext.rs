/**************************************************************************
 *   iocontext.rs  --  This file is part of linux-rs.                     *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::{
    include::linux::{
        blkdev::RequestQueue,
        types::{AtomicLongT, AtomicT, HlistNode, ListHead, RcuHead},
    },
    mm::slab::KmemCache,
};

/// An io_cq (icq) is association between an io_context (ioc) and a
/// request_queue (q).  This is used by elevators which need to track
/// information per ioc - q pair.
///
/// Elevator can request use of icq by setting elevator_type->icq_size and
/// ->icq_align.  Both size and align must be larger than that of struct
/// io_cq and elevator can use the tail area for private information.  The
/// recommended way to do this is defining a struct which contains io_cq as
/// the first member followed by private members and using its size and
/// align.  For example,
///
/// struct snail_io_cq {
///     struct io_cq icq;
///     int poke_snail;
///     int feed_snail;
/// };
///
/// struct elevator_type snail_elv_type {
///     .ops = { ... },
///     .icq_size = sizeof(struct snail_io_cq),
///     .icq_align = __alignof__(struct snail_io_cq),
///     ...
/// };
///
/// If icq_size is set, block core will manage icq's.  All requests will
/// have its ->elv.icq field set before elevator_ops->elevator_set_req_fn()
/// is called and be holding a reference to the associated io_context.
///
/// Whenever a new icq is created, elevator_ops->elevator_init_icq_fn() is
/// called and, on destruction, ->elevator_exit_icq_fn().  Both functions
/// are called with both the associated io_context and queue locks held.
///
/// Elevator is allowed to lookup icq using ioc_lookup_icq() while holding
/// queue lock but the returned icq is valid only until the queue lock is
/// released.  Elevators can not and should not try to create or destroy
/// icq's.
///
/// As icq's are linked from both ioc and q, the locking rules are a bit
/// complex.
///
/// - ioc lock nests inside q lock.
///
/// - ioc->icq_list and icq->ioc_node are protected by ioc lock.
///   q->icq_list and icq->q_node by q lock.
///
/// - ioc->icq_tree and ioc->icq_hint are protected by ioc lock, while icq
///   itself is protected by q lock.  However, both the indexes and icq
///   itself are also RCU managed and lookup can be performed holding only
///   the q lock.
///
/// - icq's are not reference counted.  They are destroyed when either the
///   ioc or q goes away.  Each request with icq set holds an extra
///   reference to ioc to ensure it stays until the request is completed.
///
/// - Linking and unlinking icq's are performed while holding both ioc and q
///   locks.  Due to the lock ordering, q exit is simple but ioc exit
///   requires reverse-order double lock dance.
pub struct IoCq {
    q: *mut RequestQueue,
    ioc: *mut IoContext,
    /// q_node and ioc_node link io_cq through icq_list of q and ioc
    /// respectively.  Both fields are unused once ioc_exit_icq() is
    /// called and shared with __rcu_icq_cache and __rcu_head which are
    /// used for RCU free of io_cq.
    q_node: ListHead,
    __rcu_icq_cache: *mut KmemCache,
    ioc_node: HlistNode,
    __rcu_head: RcuHead,
    flags: u32,
}

/// I/O subsystem state of the associated processes.  It is refcounted
/// and kmalloc'ed. These could be shared between processes.
struct IoContext {
    refcount: AtomicLongT,
    active_ref: AtomicT,
    ioprio: u16,
    #[cfg(feature = "CONFIG_BLK_ICQ")]
    /// all the fields below are protected by this lock
    lock: SpinlockT,
    #[cfg(feature = "CONFIG_BLK_ICQ")]
    icq_tree: RadixTreeRoot,
    #[cfg(feature = "CONFIG_BLK_ICQ")]
    icq_hint: *mut IoCq,
    #[cfg(feature = "CONFIG_BLK_ICQ")]
    icq_list: HlistHead,
    #[cfg(feature = "CONFIG_BLK_ICQ")]
    release_work: WorkStruct,
}
