/**************************************************************************
 *   kobject.rs  --  This file is part of linux-rs.                       *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{
    kernfs::KernfsNode, kref::Kref, spinlock_types::SpinlockT, types::ListHead,
};

pub struct Kobject {
    name: *const char,
    entry: ListHead,
    parent: *mut Kobject,
    kset: *mut Kset,
    // TODO: impl fns void (*release)(struct kobject *kobj);
    // const struct sysfs_ops *sysfs_ops;
    // const struct attribute_group **default_groups;
    // const struct kobj_ns_type_operations *(*child_ns_type)(const struct kobject *kobj);
    // const void *(*namespace)(const struct kobject *kobj);
    // void (*get_ownership)(const struct kobject *kobj, kuid_t *uid, kgid_t *gid);
    /// sysfs directory entry
    sd: *mut KernfsNode,
    kref: Kref,
    state_initialized: bool,
    state_in_sysfs: bool,
    state_add_uevent_sent: bool,
    state_remove_uevent_sent: bool,
    uevent_suppress: bool,
    #[cfg(feature = "CONFIG_DEBUG_KOBJECT_RELEASE")]
    release: DelayedWork,
}

/// struct kset - a set of kobjects of a specific type, belonging to a specific subsystem.
///
/// A kset defines a group of kobjects.  They can be individually
/// different "types" but overall these kobjects all want to be grouped
/// together and operated on in the same manner.  ksets are used to
/// define the attribute callbacks and other common events that happen to
/// a kobject.
///
/// @list: the list of all kobjects for this kset
/// @list_lock: a lock for iterating over the kobjects
/// @kobj: the embedded kobject for this kset (recursion, isn't it fun...)
/// @uevent_ops: the set of uevent operations for this kset.  These are
/// called whenever a kobject has something happen to it so that the kset
/// can add new environment variables, or filter out the uevents if so
/// desired.
struct Kset {
    list: ListHead,
    list_lock: SpinlockT,
    kobj: Kobject,
    // TODO impl fns const struct kset_uevent_ops *uevent_ops;
}
