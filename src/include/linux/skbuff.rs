/**************************************************************************
 *   skbuff.rs  --  This file is part of linux-rs.                        *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::{
    include::{
        linux::{
            ktime::KtimeT, llist::LlistNode, netdevice::NetDevice, rbtree_types::RbNode,
            refcount::RefCountT, spinlock_types::SpinlockT, types::ListHead,
        },
        net::sock::Sock,
    },
    tools::virtio::linux::kernel::__Wsum,
};

pub type SkBuffDataT = u32;

/// struct sk_buff - socket buffer
/// @next: Next buffer in list
/// @prev: Previous buffer in list
/// @tstamp: Time we arrived/left
/// @skb_mstamp_ns: (aka @tstamp) earliest departure time; start point
///     for retransmit timer
/// @rbnode: RB tree node, alternative to next/prev for netem/tcp
/// @list: queue head
/// @ll_node: anchor in an llist (eg socket defer_list)
/// @sk: Socket we are owned by
/// @ip_defrag_offset: (aka @sk) alternate use of @sk, used in
///     fragmentation management
/// @dev: Device we arrived on/are leaving by
/// @dev_scratch: (aka @dev) alternate use of @dev when @dev would be %NULL
/// @cb: Control buffer. Free for use by every layer. Put private vars here
/// @_skb_refdst: destination entry (with norefcount bit)
/// @sp: the security path, used for xfrm
/// @len: Length of actual data
/// @data_len: Data length
/// @mac_len: Length of link layer header
/// @hdr_len: writable header length of cloned skb
/// @csum: Checksum (must include start/offset pair)
/// @csum_start: Offset from skb->head where checksumming should start
/// @csum_offset: Offset from csum_start where checksum should be stored
/// @priority: Packet queueing priority
/// @ignore_df: allow local fragmentation
/// @cloned: Head may be cloned (check refcnt to be sure)
/// @ip_summed: Driver fed us an IP checksum
/// @nohdr: Payload reference only, must not modify header
/// @pkt_type: Packet class
/// @fclone: skbuff clone status
/// @ipvs_property: skbuff is owned by ipvs
/// @inner_protocol_type: whether the inner protocol is
///     ENCAP_TYPE_ETHER or ENCAP_TYPE_IPPROTO
/// @remcsum_offload: remote checksum offload is enabled
/// @offload_fwd_mark: Packet was L2-forwarded in hardware
/// @offload_l3_fwd_mark: Packet was L3-forwarded in hardware
/// @tc_skip_classify: do not classify packet. set by IFB device
/// @tc_at_ingress: used within tc_classify to distinguish in/egress
/// @redirected: packet was redirected by packet classifier
/// @from_ingress: packet was redirected from the ingress path
/// @nf_skip_egress: packet shall skip nf egress - see netfilter_netdev.h
/// @peeked: this packet has been seen already, so stats have been
///     done for it, don't do them again
/// @nf_trace: netfilter packet trace flag
/// @protocol: Packet protocol from driver
/// @destructor: Destruct function
/// @tcp_tsorted_anchor: list structure for TCP (tp->tsorted_sent_queue)
/// @_sk_redir: socket redirection information for skmsg
/// @_nfct: Associated connection, if any (with nfctinfo bits)
/// @nf_bridge: Saved data about a bridged frame - see br_netfilter.c
/// @skb_iif: ifindex of device we arrived on
/// @tc_index: Traffic control index
/// @hash: the packet hash
/// @queue_mapping: Queue mapping for multiqueue devices
/// @head_frag: skb was allocated from page fragments,
///     not allocated by kmalloc() or vmalloc().
/// @pfmemalloc: skbuff was allocated from PFMEMALLOC reserves
/// @pp_recycle: mark the packet for recycling instead of freeing (implies
///     page_pool support on driver)
/// @active_extensions: active extensions (skb_ext_id types)
/// @ndisc_nodetype: router type (from link layer)
/// @ooo_okay: allow the mapping of a socket to a queue to be changed
/// @l4_hash: indicate hash is a canonical 4-tuple hash over transport
///     ports.
/// @sw_hash: indicates hash was computed in software stack
/// @wifi_acked_valid: wifi_acked was set
/// @wifi_acked: whether frame was acked on wifi or not
/// @no_fcs:  Request NIC to treat last 4 bytes as Ethernet FCS
/// @encapsulation: indicates the inner headers in the skbuff are valid
/// @encap_hdr_csum: software checksum is needed
/// @csum_valid: checksum is already valid
/// @csum_not_inet: use CRC32c to resolve CHECKSUM_PARTIAL
/// @csum_complete_sw: checksum was completed by software
/// @csum_level: indicates the number of consecutive checksums found in
///     the packet minus one that have been verified as
///     CHECKSUM_UNNECESSARY (max 3)
/// @dst_pending_confirm: need to confirm neighbour
/// @decrypted: Decrypted SKB
/// @slow_gro: state present at GRO time, slower prepare step required
/// @mono_delivery_time: When set, skb->tstamp has the
///     delivery_time in mono clock base (i.e. EDT).  Otherwise, the
///     skb->tstamp has the (rcv) timestamp at ingress and
///     delivery_time at egress.
/// @napi_id: id of the NAPI struct this skb came from
/// @sender_cpu: (aka @napi_id) source CPU in XPS
/// @alloc_cpu: CPU which did the skb allocation.
/// @secmark: security marking
/// @mark: Generic packet mark
/// @reserved_tailroom: (aka @mark) number of bytes of free space available
///     at the tail of an sk_buff
/// @vlan_all: vlan fields (proto & tci)
/// @vlan_proto: vlan encapsulation protocol
/// @vlan_tci: vlan tag control information
/// @inner_protocol: Protocol (encapsulation)
/// @inner_ipproto: (aka @inner_protocol) stores ipproto when
///     skb->inner_protocol_type == ENCAP_TYPE_IPPROTO;
/// @inner_transport_header: Inner transport layer header (encapsulation)
/// @inner_network_header: Network layer header (encapsulation)
/// @inner_mac_header: Link layer header (encapsulation)
/// @transport_header: Transport layer header
/// @network_header: Network layer header
/// @mac_header: Link layer header
/// @kcov_handle: KCOV remote handle for remote coverage collection
/// @tail: Tail pointer
/// @end: End pointer
/// @head: Head of buffer
/// @data: Data head pointer
/// @truesize: Buffer size
/// @users: User count - see {datagram,tcp}.rs
/// @extensions: allocated extensions, valid if active_extensions is nonzero
pub struct SkBuff {
    /// These two members must be first to match sk_buff_head.
    next: *mut SkBuff,
    prev: *mut SkBuff,
    dev: *mut NetDevice,
    /// Some protocols might use this space to store information,
    /// while device pointer would be NULL.
    /// UDP receive path is one user.
    dev_scratch: u64,
    /// used in netem, ip4 defrag, and tcp stack
    rbnode: RbNode,
    list: ListHead,
    ll_node: LlistNode,
    sk: *mut Sock,
    ip_defrag_offset: i32,
    tstamp: KtimeT,
    /// earliest departure time
    skb_mstamp_ns: u64,
    /// This is the control buffer. It is free to use for every
    /// layer. Please put your private variables there. If you
    /// want to keep them across layers you have to do a skb_clone()
    /// first. This is owned by whoever has the skb queued ATM.
    cb: [char; 48],
    _skb_refdst: u64,
    // TODO: void	(*destructor)(struct sk_buff *skb) impl
    tcp_tsorted_anchor: ListHead,
    #[cfg(feature = "CONFIG_NET_SOCK_MSG")]
    _sk_redir: u64,
    #[cfg(any(
        feature = "CONFIG_NF_CONNTRACK",
        feature = "CONFIG_NF_CONNTRACK_MODULE"
    ))]
    _nfct: u64,
    len: u32,
    data_len: u32,
    mac_len: u16,
    hdr_len: u16,
    /// Following fields are _not_ copied in __copy_skb_header()
    /// Note that queue_mapping is here mostly to fill a hole.
    queue_mapping: u16,
    cloned: u8,
    nohdr: u8,
    fclone: u8,
    peeked: u8,
    head_frag: u8,
    pfmemalloc: u8,
    ///  page_pool recycle indicator
    pp_recycle: u8,
    #[cfg(feature = "CONFIG_SKB_EXTENSIONS")]
    active_extensions: u8,
    /// Fields enclosed in headers group are copied
    /// using a single memcpy() in __copy_skb_header()
    __pkt_type_offset: u8,
    /// public
    /// see PKT_TYPE_MAX
    pkt_type: u8,
    ignore_df: u8,
    dst_pending_confirm: u8,
    ip_summed: u8,
    ooo_okay: u8,
    /// private
    __mono_tc_offset: u8,
    /// public
    /// See SKB_MONO_DELIVERY_TIME_MASK
    mono_delivery_time: u8,
    /// See TC_AT_INGRESS_MASK
    #[cfg(feature = "CONFIG_NET_XGRESS")]
    tc_at_ingress: u8,
    #[cfg(feature = "CONFIG_NET_XGRESS")]
    tc_skip_classify: u8,
    remcsum_offload: u8,
    csum_complete_sw: u8,
    csum_level: u8,
    inner_protocol_type: u8,
    l4_hash: u8,
    sw_hash: u8,
    #[cfg(feature = "CONFIG_WIRELESS")]
    wifi_acked_valid: u8,
    #[cfg(feature = "CONFIG_WIRELESS")]
    wifi_acked: u8,
    no_fcs: u8,
    // Indicates the inner headers are valid in the skbuff.
    encapsulation: u8,
    encap_hdr_csum: u8,
    csum_valid: u8,
    #[cfg(feature = "CONFIG_IPV6_NDISC_NODETYPE")]
    ndisc_nodetype: u8,
    #[cfg(feature = "CONFIG_IP_VS")]
    ipvs_property: u8,
    #[cfg(any(
        feature = "CONFIG_NETFILTER_XT_TARGET_TRACE",
        feature = "CONFIG_NF_TABLES"
    ))]
    nf_trace: u8,
    #[cfg(feature = "CONFIG_NET_SWITCHDEV")]
    offload_fwd_mark: u8,
    #[cfg(feature = "CONFIG_NET_SWITCHDEV")]
    offload_l3_fwd_mark: u8,
    redirected: u8,
    #[cfg(feature = "CONFIG_NET_REDIRECT")]
    from_ingress: u8,
    #[cfg(feature = "CONFIG_NETFILTER_SKIP_EGRESS")]
    nf_skip_egress: u8,
    #[cfg(feature = "CONFIG_TLS_DEVICE")]
    decrypted: u8,
    slow_gro: u8,
    #[cfg(feature = "CONFIG_IP_SCTP")]
    csum_not_inet: u8,
    /// traffic control index
    #[cfg(any(feature = "CONFIG_NET_SCHED", feature = "CONFIG_NET_XGRESS"))]
    tc_index: u16,
    alloc_cpu: u16,
    csum: __Wsum,
    csum_start: u16,
    csum_offset: u16,
    priority: u32,
    skb_iif: i32,
    hash: u32,
    vlan_all: u32,
    vlan_proto: u16,
    vlan_tci: u16,
    #[cfg(any(feature = "CONFIG_NET_RX_BUSY_POLL", feature = "CONFIG_XPS"))]
    napi_id: u32,
    #[cfg(any(feature = "CONFIG_NET_RX_BUSY_POLL", feature = "CONFIG_XPS"))]
    sender_cpu: u32,
    #[cfg(feature = "CONFIG_NETWORK_SECMARK")]
    secmark: u32,
    mark: u32,
    reserved_tailroom: u32,
    inner_protocol: u16,
    inner_ipproto: u8,
    inner_transport_header: u16,
    inner_network_header: u16,
    inner_mac_header: u16,
    protocol: u16,
    transport_header: u16,
    network_header: u16,
    mac_header: u16,
    #[cfg(feature = "CONFIG_KCOV")]
    kcov_handle: u64,
    /// These elements must be at the end, see alloc_skb() for details.
    tail: SkBuffDataT,
    end: SkBuffDataT,
    head: *mut u8,
    data: *mut u8,
    truesize: u32,
    users: RefCountT,
    /// only useable after checking ->active_extensions != 0
    #[cfg(feature = "CONFIG_SKB_EXTENSIONS")]
    extensions: *mut SkbExt,
}

pub struct SkBuffHead {
    /// These two members must be first to match sk_buff.
    next: *mut SkBuff,
    prev: *mut SkBuff,
    qlen: u32,
    lock: SpinlockT,
}

///  struct skb_ext - sk_buff extensions
///  @refcnt: 1 on allocation, deallocated on 0
///  @offset: offset to add to @data to obtain extension address
///  @chunks: size currently allocated, stored in SKB_EXT_ALIGN_SHIFT units
///  @data: start of extension data, variable sized
///
///  Note: offsets/lengths are stored in chunks of 8 bytes, this allows
///  to use 'u8' types while allowing up to 2kb worth of extension data.
#[cfg(all(feature = "CONFIG_RT_GROUP_SCHED", feature = "CONFIG_WATCH_QUEUE"))]
struct SkbExt {
    refcnt: RefCountT,
    /// in chunks of 8 bytes
    offset: [u8; SkBuff::SKB_EXT_NUM],
    /// same
    chunks: u8,
    data: [char; 10],
}

#[cfg(all(
    feature = "CONFIG_RT_GROUP_SCHED",
    feature = "CONFIG_WATCH_QUEUE",
    feature = "__BIG_ENDIAN_BITFIELD"
))]
impl SkBuff {
    /// if you move cloned around you also must adapt those constants
    const CLONED_MASK: u8 = 1 << 7;
}

#[cfg(all(feature = "CONFIG_RT_GROUP_SCHED", feature = "CONFIG_WATCH_QUEUE",))]
#[cfg(not(feature = "__BIG_ENDIAN_BITFIELD"))]
impl SkBuff {
    const CLONED_MASK: u8 = 1;
}
