/**************************************************************************
 *   enum_skb.rs  --  This file is part of linux-rs.                      *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::skbuff::SkBuff;

// TODO: make all convinations
/*
enum skb_ext_id {

#if IS_ENABLED(CONFIG_BRIDGE_NETFILTER)
    SKB_EXT_BRIDGE_NF,
#endif

#ifdef CONFIG_XFRM
    SKB_EXT_SEC_PATH,
#endif

#if IS_ENABLED(CONFIG_NET_TC_SKB_EXT)
    TC_SKB_EXT,
#endif

#if IS_ENABLED(CONFIG_MPTCP)
    SKB_EXT_MPTCP,
#endif

#if IS_ENABLED(CONFIG_MCTP_FLOWS)
    SKB_EXT_MCTP,
#endif

    SKB_EXT_NUM, /* must be last */
};
*/

impl SkBuff {
    pub const SKB_EXT_NUM: usize = 0;
}

#[cfg(all(
    feature = "CONFIG_RT_GROUP_SCHED",
    feature = "CONFIG_WATCH_QUEUE",
    feature = "CONFIG_BRIDGE_NETFILTER",
    feature = "CONFIG_XFRM",
    feature = "CONFIG_NET_TC_SKB_EXT",
    feature = "CONFIG_MPTCP",
    feature = "CONFIG_MCTP_FLOWS",
))]
impl SkBuff {
    pub const SKB_EXT_BRIDGE_NF: usize = 0;
    pub const SKB_EXT_SEC_PATH: usize = 1;
    pub const TC_SKB_EXT: usize = 2;
    pub const SKB_EXT_MPTCP: usize = 3;
    pub const SKB_EXT_MCTP: usize = 4;
    pub const SKB_EXT_NUM: usize = 5;
}
