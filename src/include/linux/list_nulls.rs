/**************************************************************************
 *   list_nulls.rs  --  This file is part of linux-rs.                    *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

/// Special version of lists, where end of list is not a NULL pointer,
/// but a 'nulls' marker, which can have many different values.
/// (up to 2^31 different values guaranteed on all platforms)
///
/// In the standard hlist, termination of a list is the NULL pointer.
/// In this special 'nulls' variant, we use the fact that objects stored in
/// a list are aligned on a word (4 or 8 bytes alignment).
/// We therefore use the last significant bit of 'ptr' :
/// Set to 1 : This is a 'nulls' end-of-list marker (ptr >> 1)
/// Set to 0 : This is a pointer to some object (ptr)
pub struct HlistNullsNode {
    next: HlistNullsNode,
    pprev: HlistNullsNode,
}

pub struct HlistNullsHead {
    first: HlistNullsNode,
}
