/**************************************************************************
 *   backing_dev_defs.rs  --  This file is part of linux-rs.              *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{
    device::Device,
    flex_proportions::FpropLocalPercpu,
    kref::Kref,
    percpu_counter::PercpuCounter,
    rbtree_types::RbNode,
    spinlock_types::SpinlockT,
    timer::TimerList,
    types::{AtomicLongT, AtomicT, ListHead},
    wait::WaitQueueHeadT,
    workqueue::DelayedWork,
};

#[cfg(feature = "CONFIG_CGROUP_WRITEBACK")]
use crate::include::linux::{
    cgroup_defs::CgroupSubsysState, percpu_refcount::PercpuRef, types::RcuHead,
    workqueue::WorkStruct,
};

const WB_RECLAIMABLE: usize = 0;
const WB_WRITEBACK: usize = 1;
const WB_DIRTIED: usize = 2;
const WB_WRITTEN: usize = 3;
const NR_WB_STAT_ITEMS: usize = 4;

/// Each wb (bdi_writeback) can perform writeback operations, is measured
/// and throttled, independently.  Without cgroup writeback, each bdi
/// (bdi_writeback) is served by its embedded bdi->wb.
///
/// On the default hierarchy, blkcg implicitly enables memcg.  This allows
/// using memcg's page ownership for attributing writeback IOs, and every
/// memcg - blkcg combination can be served by its own wb by assigning a
/// dedicated wb to each memcg, which enables isolation across different
/// cgroups and propagation of IO back pressure down from the IO layer upto
/// the tasks which are generating the dirty pages to be written back.
///
/// A cgroup wb is indexed on its bdi by the ID of the associated memcg,
/// refcounted with the number of inodes attached to it, and pins the memcg
/// and the corresponding blkcg.  As the corresponding blkcg for a memcg may
/// change as blkcg is disabled and enabled higher up in the hierarchy, a wb
/// is tested for blkcg after lookup and removed from index on mismatch so
/// that a new wb for the combination can be created.
///
/// Each bdi_writeback that is not embedded into the backing_dev_info must hold
/// a reference to the parent backing_dev_info.  See cgwb_create() for details.
pub struct BdiWriteback {
    ///  our parent bdi
    bdi: BackingDevInfo,
    /// Always use atomic bitops on this
    state: u64,
    /// last old data flush
    last_old_flush: u64,
    /// dirty inodes
    b_dirty: ListHead,
    /// parked for writeback
    b_io: ListHead,
    /// parked for more writeback
    b_more_io: ListHead,
    /// time stamps are dirty
    b_dirty_time: ListHead,
    /// protects the b_* lists
    list_lock: SpinlockT,
    /// number of inodes under writeback
    writeback_inodes: AtomicT,
    stat: [PercpuCounter; NR_WB_STAT_ITEMS],
    /// last time write bw is updated
    bw_time_stamp: u64,
    dirtied_stamp: u64,
    /// pages written at bw_time_stamp
    written_stamp: u64,
    /// the estimated write bandwidth
    write_bandwidth: u64,
    /// further smoothed write bw, > 0
    avg_write_bandwidth: u64,
    /// The base dirty throttle rate, re-calculated on every 200ms.
    /// All the bdi tasks' dirty rate will be curbed under it.
    /// @dirty_ratelimit tracks the estimated @balanced_dirty_ratelimit
    /// in small steps and is much more smooth/stable than the latter.
    dirty_ratelimit: u64,
    balanced_dirty_ratelimit: u64,
    completions: FpropLocalPercpu,
    dirty_exceeded: i32,
    /// protects work_list & dwork scheduling
    work_lock: SpinlockT,
    work_list: ListHead,
    /// work item used for writeback
    dwork: DelayedWork,
    /// work item used for bandwidth estimate
    bw_dwork: DelayedWork,
    /// last wait
    dirty_sleep: u64,
    /// anchored at bdi -> wb_list
    bdi_node: ListHead,
    #[cfg(feature = "CONFIG_CGROUP_WRITEBACK")]
    c_c_w: ConfigCgroupWriteback,
}

#[cfg(feature = "CONFIG_CGROUP_WRITEBACK")]
struct ConfigCgroupWriteback {
    /// used only for !root wb's
    refcnt: PercpuRef,
    memcg_completions: FpropLocalPercpu,
    /// the associated memcg
    memcg_css: *mut CgroupSubsysState,
    /// and blkcg
    blkcg_css: *mut CgroupSubsysState,
    /// anchored at memcg -> cgwb_list
    memcg_node: ListHead,
    /// anchored at blkcg -> cgwb_list
    blkcg_node: ListHead,
    /// attached inodes, protected by list_lock
    b_attached: ListHead,
    /// anchored at offline_cgwbs
    offline_node: ListHead,
    release_work: WorkStruct,
    rcu: RcuHead,
}

pub struct BackingDevInfo {
    id: u64,
    /// keyed by ->id
    rb_node: RbNode,
    bdi_list: ListHead,
    /// max readahead in PAGE_SIZE units
    ra_pages: u64,
    /// max allowed IO siz
    io_pages: u64,
    /// Reference counter for the structure
    refcnt: Kref,
    /// Device capabilities
    capabilities: u32,
    min_ratio: u32,
    max_ratio: u32,
    max_prop_frac: u32,
    /// Sum of avg_write_bw of wbs with dirty inodes.  > 0 if there are
    /// any dirty wbs, which is depended upon by bdi_has_dirty().
    tot_write_bandwidth: AtomicLongT,
    /// the root writeback info for this bdi
    wb: BdiWriteback,
    /// list of all wbs
    wb_list: ListHead,
    /// radix tree of active cgroup wbs
    #[cfg(feature = "CONFIG_CGROUP_WRITEBACK")]
    cgwb_tree: RadixTreeRoot,
    /// protect shutdown of wb structs
    #[cfg(feature = "CONFIG_CGROUP_WRITEBACK")]
    cgwb_release_mutex: Mutex,
    /// no cgwb switch while syncing
    #[cfg(feature = "CONFIG_CGROUP_WRITEBACK")]
    wb_switch_rwsem: RwSemaphore,
    wb_waitq: WaitQueueHeadT,
    dev: *mut Device,
    dev_name: [char; 64],
    owner: *mut Device,
    laptop_mode_wb_timer: TimerList,
    #[cfg(feature = "CONFIG_DEBUG_FS")]
    debug_dir: *mut Dentry,
}

impl BdiWriteback {
    /// why some writeback work was initiated
    const WB_REASON_BACKGROUND: usize = 0;
    const WB_REASON_VMSCAN: usize = 1;
    const WB_REASON_SYNC: usize = 2;
    const WB_REASON_PERIODIC: usize = 3;
    const WB_REASON_LAPTOP_TIMER: usize = 4;
    const WB_REASON_FS_FREE_SPACE: usize = 5;
    /// There is no bdi forker thread any more and works are done
    /// by emergency worker, however, this is TPs userland visible
    /// and we'll be exposing exactly the same information,
    /// so it has a mismatch name.
    const WB_REASON_FORKER_THREAD: usize = 6;
    const WB_REASON_FOREIGN_FLUSH: usize = 7;
    const WB_REASON_MAX: usize = 8;
}
