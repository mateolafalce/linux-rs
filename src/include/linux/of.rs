/**************************************************************************
 *   of.rs  --  This file is part of linux-rs.                            *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::fwnode::FwnodeHandle;

pub struct DeviceNode {
    name: *const char,
    phandle: u32,
    full_name: *const char,
    fwnode: FwnodeHandle,
    properties: *mut Property,
    /// removed properties
    deadprops: *mut Property,
    parent: *mut DeviceNode,
    child: *mut DeviceNode,
    sibling: *mut DeviceNode,
    #[cfg(feature = "CONFIG_OF_KOBJ")]
    kobj: Kobject,
    _flags: u64,
    data: *mut u32,
    #[cfg(feature = "CONFIG_SPARC")]
    unique_id: u32,
    #[cfg(feature = "CONFIG_SPARC")]
    irq_trans: *mut OfIrqController,
}

struct Property {
    name: *const char,
    length: i32,
    value: *mut u32,
    next: *mut Property,
    #[cfg(any(feature = "CONFIG_OF_DYNAMIC", feature = "CONFIG_SPARC"))]
    _flags: u64,
    #[cfg(feature = "CONFIG_OF_PROMTREE")]
    unique_id: u32,
    #[cfg(feature = "CONFIG_OF_KOBJ")]
    attr: BinAttribute,
}
