/**************************************************************************
 *   percpu_refcount.rs  --  This file is part of linux-rs.               *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::types::{AtomicLongT, RcuHead};

pub struct PercpuRef {
    /// The low bit of the pointer indicates whether the ref is in percpu
    /// mode; if set, then get/put will manipulate the atomic_t.
    percpu_count_ptr: u64,
    /// 'percpu_ref' is often embedded into user structure, and only
    /// 'percpu_count_ptr' is required in fast path, move other fields
    /// into 'percpu_ref_data', so we can reduce memory footprint in
    /// fast path.
    data: *mut PercpuRefData,
}

struct PercpuRefData {
    count: AtomicLongT,
    //TODO: impl fn release: *mut PercpuRefFuncT,
    //confirm_switch: *mut PercpuRefFuncT,
    force_atomic: bool,
    allow_reinit: bool,
    rcu: RcuHead,
    ref_: *mut PercpuRef,
}
