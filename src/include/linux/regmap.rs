/**************************************************************************
 *   regmap.rs  --  This file is part of linux-rs.                        *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

pub type RegmapLock = fn() -> ();
pub type RegmapUnlock = fn() -> ();

///  struct regmap_bus - Description of a hardware bus for the register map
///                      infrastructure.
///
///  @fast_io: Register IO is fast. Use a spinlock instead of a mutex
///          to perform locking. This field is ignored if custom lock/unlock
///          functions are used (see fields lock/unlock of
///          struct regmap_config).
///  @free_on_exit: kfree this on exit of regmap
///  @write: Write operation.
///  @gather_write: Write operation with split register/value, return -ENOTSUPP
///                 if not implemented  on a given device.
///  @async_write: Write operation which completes asynchronously, optional and
///                must serialise with respect to non-async I/O.
///  @reg_write: Write a single register value to the given register address. This
///              write operation has to complete when returning from the function.
///  @reg_write_noinc: Write multiple register value to the same register. This
///              write operation has to complete when returning from the function.
///  @reg_update_bits: Update bits operation to be used against volatile
///                    registers, intended for devices supporting some mechanism
///                    for setting clearing bits without having to
///                    read/modify/write.
///  @read: Read operation.  Data is returned in the buffer used to transmit
///          data.
///  @reg_read: Read a single register value from a given register address.
///  @free_context: Free context.
///  @async_alloc: Allocate a regmap_async() structure.
///  @read_flag_mask: Mask to be set in the top byte of the register when doing
///                   a read.
///  @reg_format_endian_default: Default endianness for formatted register
///      addresses. Used when the regmap_config specifies DEFAULT. If this is
///      DEFAULT, BIG is assumed.
///  @val_format_endian_default: Default endianness for formatted register
///      values. Used when the regmap_config specifies DEFAULT. If this is
///      DEFAULT, BIG is assumed.
///  @max_raw_read: Max raw read size that can be used on the bus.
///  @max_raw_write: Max raw write size that can be used on the bus.
pub struct RegmapBus {
    fast_io: bool,
    free_on_exit: bool,
    //TODO imp fn write: RegmapHwWrite,
    //TODO imp fn gather_write: RegmapHwGatherWrite,
    //TODO imp fn async_write: RegmapHwAsyncWrite,
    //TODO imp fn reg_write: RegmapHwRegWrite,
    //TODO imp fn reg_noinc_write: RegmapHwRegNoincWrite,
    //TODO imp fn reg_update_bits: RegmapHwRegUpdateBits,
    //TODO imp fn read: RegmapHwRead,
    //TODO imp fn reg_read: RegmapHwRegRead,
    //TODO imp fn reg_noinc_read: RegmapHwRegNoincRead,
    //TODO imp fn free_context: RegmapHwFreeContext,
    //TODO imp fn async_alloc: RegmapHwAsyncAlloc,
    read_flag_mask: u8,
    // TODO impl enum regmap_endian
    // TODO impl enum regmap_endian
    max_raw_read: u64,
    max_raw_write: u64,
}

///  struct regmap_access_table - A table of register ranges for access checks
///
///  @yes_ranges : pointer to an array of regmap ranges used as "yes ranges"
///  @n_yes_ranges: size of the above array
///  @no_ranges: pointer to an array of regmap ranges used as "no ranges"
///  @n_no_ranges: size of the above array
///
///  A table of ranges including some yes ranges and some no ranges.
///  If a register belongs to a no_range, the corresponding check function
///  will return false. If a register belongs to a yes range, the corresponding
///  check function will return true. "no_ranges" are searched first.
pub struct RegmapAccessTable {
    yes_ranges: *const RegmapRange,
    n_yes_ranges: u32,
    no_ranges: *const RegmapRange,
    n_no_ranges: u32,
}

///  struct reg_default - Default value for a register.
///
///  @reg: Register address.
///  @def: Register default value.
///
///  We use an array of structs rather than a simple array as many modern devices
///  have very sparse register maps.
pub struct RegDefault {
    reg: u32,
    def: u32,
}

///  struct reg_sequence - An individual write from a sequence of writes.
///
///  @reg: Register address.
///  @def: Register value.
///  @delay_us: Delay to be applied after the register write in microseconds
///
///  Register/value pairs for sequences of writes with an optional delay in
///  microseconds to be applied after each write.
pub struct RegSequence {
    reg: u32,
    def: u32,
    delay_us: u32,
}

///  struct regmap_range - A register range, used for access related checks
///                        (readable/writeable/volatile/precious checks)
///
///  @range_min: address of first register
///  @range_max: address of last register
struct RegmapRange {
    range_min: u32,
    range_max: u32,
}
