/**************************************************************************
 *   completion.rs  --  This file is part of linux-rs.                    *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::swait::SwaitQueueHead;

/// struct completion - structure used to maintain state for a "completion"
///
/// This is the opaque structure used to maintain the state for a "completion".
/// Completions currently use a FIFO to queue threads that have to wait for
/// the "completion" event.
///
/// See also:  complete(), wait_for_completion() (and friends _timeout,
/// _interruptible, _interruptible_timeout, and _killable), init_completion(),
/// reinit_completion(), and macros DECLARE_COMPLETION(),
/// DECLARE_COMPLETION_ONSTACK().
pub struct Completion {
    done: u32,
    wait: SwaitQueueHead,
}
