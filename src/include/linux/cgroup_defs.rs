/**************************************************************************
 *   cgroup_defs.rs  --  This file is part of linux-rs.                   *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::{
    fs::kernfs::kernfs_internal::KernfsRoot,
    include::{
        linux::{
            bpf_cgroup_defs::CgroupBpf,
            idr::Idr,
            kernfs::KernfsNode,
            mutex_types::Mutex,
            percpu_refcount::PercpuRef,
            psi_types::{PsiGroup, PsiGroupCpu},
            sched::PrevCputime,
            sched_::types::TaskCputime,
            timer::TimerList,
            types::{AtomicT, ListHead, SizeT},
            u64_stats_sync::U64StatsSync,
            wait::WaitQueueHeadT,
            workqueue::{RcuWork, WorkStruct},
        },
        uapi::linux::limits::PATH_MAX,
    },
};

const MAX_CGROUP_TYPE_NAMELEN: usize = 32;
const MAX_CGROUP_ROOT_NAMELEN: usize = 64;
const MAX_CFTYPE_NAME: usize = 64;
const CGROUP_SUBSYS_COUNT: usize = 0;

struct Cgroup {
    /// self css with NULL ->ss, points back to this cgroup
    self_: CgroupSubsysState,
    /// "unsigned long" so bitops work
    flags: u64,
    /// The depth this cgroup is at.  The root is at depth zero and each
    /// step down the hierarchy increments the level.  This along with
    /// ancestors[] can determine whether a given cgroup is a
    /// descendant of another without traversing the hierarchy.
    level: i32,
    /// Maximum allowed descent tree depth
    max_depth: i32,
    /// Keep track of total numbers of visible and dying descent cgroups.
    /// Dying cgroups are cgroups which were deleted by a user,
    /// but are still existing because someone else is holding a reference.
    /// max_descendants is a maximum allowed number of descent cgroups.
    ///
    /// nr_descendants and nr_dying_descendants are protected
    /// by cgroup_mutex and css_set_lock. It's fine to read them holding
    /// any of cgroup_mutex and css_set_lock; for writing both locks
    /// should be held.
    nr_descendants: i32,
    nr_dying_descendants: i32,
    max_descendants: i32,
    /// Each non-empty css_set associated with this cgroup contributes
    /// one to nr_populated_csets.  The counter is zero iff this cgroup
    /// doesn't have any tasks.
    ///
    /// All children which have non-zero nr_populated_csets and/or
    /// nr_populated_children of their own contribute one to either
    /// nr_populated_domain_children or nr_populated_threaded_children
    /// depending on their type.  Each counter is zero iff all cgroups
    /// of the type in the subtree proper don't have any tasks.
    nr_populated_csets: i32,
    nr_populated_domain_children: i32,
    nr_populated_threaded_children: i32,
    /// # of live threaded child cgroups
    nr_threaded_children: i32,
    /// cgroup kernfs entry
    kn: *mut KernfsNode,
    /// handle for "cgroup.procs"
    procs_file: CgroupFile,
    /// handle for "cgroup.events"
    events_file: CgroupFile,
    /// handles for "{cpu,memory,io,irq}.pressure"
    psi_files: [CgroupFile; PsiGroupCpu::NR_PSI_STATES],
    /// The bitmask of subsystems enabled on the child cgroups.
    /// ->subtree_control is the one configured through
    /// "cgroup.subtree_control" while ->subtree_ss_mask is the effective
    /// one which may have more subsystems enabled.  Controller knobs
    /// are made available iff it's enabled in ->subtree_control.
    subtree_control: u16,
    subtree_ss_mask: u16,
    old_subtree_control: u16,
    old_subtree_ss_mask: u16,
    /// Private pointers for each registered subsystem
    subsys: [*mut CgroupSubsysState; CGROUP_SUBSYS_COUNT],
    root: *mut CgroupRoot,
    /// List of cgrp_cset_links pointing at css_sets with tasks in this
    /// cgroup.  Protected by css_set_lock.
    cset_links: ListHead,
    /// On the default hierarchy, a css_set for a cgroup with some
    /// susbsys disabled will point to css's which are associated with
    /// the closest ancestor which has the subsys enabled.  The
    /// following lists all css_sets which point to this cgroup's css
    /// for the given subsystem.
    e_csets: [ListHead; CGROUP_SUBSYS_COUNT],
    /// If !threaded, self.  If threaded, it points to the nearest
    /// domain ancestor.  Inside a threaded subtree, cgroups are exempt
    /// from process granularity and no-internal-task constraint.
    /// Domain level resource consumptions which aren't tied to a
    /// specific task are charged to the dom_cgrp.
    dom_cgrp: *mut Cgroup,
    /// used while enabling threaded
    old_dom_cgrp: *mut Cgroup,
    /// per-cpu recursive resource statistics
    rstat_cpu: *mut CgroupRstatCpu,
    rstat_css_list: ListHead,
    /// A singly-linked list of cgroup structures to be rstat flushed.
    /// This is a scratch field to be used exclusively by
    /// cgroup_rstat_flush_locked() and protected by cgroup_rstat_lock.
    rstat_flush_next: *mut Cgroup,
    /// cgroup basic resource statistics
    last_bstat: CgroupBaseStat,
    bstat: CgroupBaseStat,
    /// for printing out cputime
    prev_cputime: PrevCputime,
    /// list of pidlists, up to two for each namespace (one for procs, one
    /// for tasks); created on demand.
    pidlists: ListHead,
    pidlist_mutex: Mutex,
    /// used to wait for offlining of csses
    offline_waitq: WaitQueueHeadT,
    /// used to schedule release agent
    release_agent_work: WorkStruct,
    /// used to track pressure stalls
    psi: *mut PsiGroup,
    /// used to store eBPF programs
    bpf: CgroupBpf,
    /// If there is block congestion on this cgroup.
    congestion_count: AtomicT,
    /// Used to store internal freezer state
    freezer: CgroupFreezerState,
    #[cfg(feature = "CONFIG_BPF_SYSCALL")]
    bpf_cgrp_storage: BpfLocalStorage,
    /// All ancestors including self
    ancestors: [*mut Cgroup; 10],
}

/// Per-subsystem/per-cgroup state maintained by the system.  This is the
/// fundamental structural building block that controllers deal with.
///
/// Fields marked with "PI:" are public and immutable and may be accessed
/// directly without synchronization.
pub struct CgroupSubsysState {
    /// PI: the cgroup that this css is attached to
    cgroup: *mut Cgroup,
    /// PI: the cgroup subsystem that this css is attached to
    ss: *mut CgroupSubsys,
    /// reference count - access via css_[try]get() and css_put()
    refcnt: PercpuRef,
    /// siblings list anchored at the parent's ->children
    sibling: ListHead,
    children: ListHead,
    /// flush target list anchored at cgrp->rstat_css_list
    rstat_css_node: ListHead,
    /// PI: Subsys-unique ID.  0 is unused and root is always 1.  The
    /// matching css can be looked up using css_from_id().
    id: i32,
    flags: u32,
    /// Monotonically increasing unique serial number which defines a
    /// uniform order among all csses.  It's guaranteed that all
    /// ->children lists are in the ascending order of ->serial_nr and
    /// used to allow interrupting and resuming iterations.
    serial_nr: u64,
    /// Incremented by online self and children.  Used to guarantee that
    /// parents are not offlined before their children.
    online_cnt: AtomicT,
    /// percpu_ref killing and RCU release
    destroy_work: WorkStruct,
    destroy_rwork: RcuWork,
    /// PI: the parent css.  Placed here for cache proximity to following
    /// fields of the containing structure.
    parent: *mut CgroupSubsysState,
}

/// CgroupFile is the handle for a file instance created in a cgroup which
/// is used, for example, to generate file changed notifications.  This can
/// be obtained by setting cftype->file_offset.
pub struct CgroupFile {
    /// do not access any fields from outside cgroup core
    kn: *mut KernfsNode,
    notified_at: u64,
    notify_timer: TimerList,
}

/// A cgroup_root represents the root of a cgroup hierarchy, and may be
/// associated with a kernfs_root to form an active hierarchy.  This is
/// internal to cgroup core.  Don't access directly from controllers.
struct CgroupRoot {
    kf_root: *mut KernfsRoot,
    /// The bitmask of subsystems attached to this hierarchy
    subsys_mask: u32,
    /// Unique id for this hierarchy.
    hierarchy_id: i32,
    /// The root cgroup. The containing cgroup_root will be destroyed on its
    /// release. cgrp->ancestors[0] will be used overflowing into the
    /// following field. cgrp_ancestor_storage must immediately follow.
    cgrp: Cgroup,
    /// must follow cgrp for cgrp->ancestors[0], see above
    cgrp_ancestor_storage: *mut Cgroup,
    /// Number of cgroups in the hierarchy, used only for /proc/cgroups
    nr_cgrps: AtomicT,
    /// A list running through the active hierarchies
    root_list: ListHead,
    /// Hierarchy-specific flags
    flags: u32,
    /// The path to use for release notifications.
    release_agent_path: [char; PATH_MAX],
    /// The name for this hierarchy - may be empty
    name: [char; MAX_CGROUP_ROOT_NAMELEN],
}

/// rstat - cgroup scalable recursive statistics.  Accounting is done
/// per-cpu in cgroup_rstat_cpu which is then lazily propagated up the
/// hierarchy on reads.
///
/// When a stat gets updated, the cgroup_rstat_cpu and its ancestors are
/// linked into the updated tree.  On the following read, propagation only
/// considers and consumes the updated tree.  This makes reading O(the
/// number of descendants which have been active since last read) instead of
/// O(the total number of descendants).
///
/// This is important because there can be a lot of (draining) cgroups which
/// aren't active and stat may be read frequently.  The combination can
/// become very expensive.  By propagating selectively, increasing reading
/// frequency decreases the cost of each read.
///
/// This struct hosts both the fields which implement the above -
/// updated_children and updated_next - and the fields which track basic
/// resource statistics on top of it - bsync, bstat and last_bstat.
struct CgroupRstatCpu {
    /// -> bsync protects -> bstat.  These are the only fields which get
    /// updated in the hot path.
    bsync: U64StatsSync,
    bstat: CgroupBaseStat,
    /// Snapshots at the last reading.  These are used to calculate the
    /// deltas to propagate to the global counters.
    last_bstat: CgroupBaseStat,
    /// This field is used to record the cumulative per-cpu time of
    /// the cgroup and its descendants. Currently it can be read via
    /// eBPF/drgn etc, and we are still trying to determine how to
    /// expose it in the cgroupfs interface.
    subtree_bstat: CgroupBaseStat,
    /// Snapshots at the last reading. These are used to calculate the
    /// deltas to propagate to the per-cpu subtree_bstat.
    last_subtree_bstat: CgroupBaseStat,
    /// Child cgroups with stat updates on this cpu since the last read
    /// are linked on the parent's ->updated_children through
    /// ->updated_next.
    ///
    /// In addition to being more compact, singly-linked list pointing
    /// to the cgroup makes it unnecessary for each per-cpu struct to
    /// point back to the associated cgroup.
    ///
    /// Protected by per-cpu cgroup_rstat_cpu_lock.
    /// terminated by self cgroup
    updated_children: *mut Cgroup,
    /// NULL iff not on the list
    updated_next: *mut Cgroup,
}

struct CgroupBaseStat {
    cputime: TaskCputime,
    #[cfg(feature = "CONFIG_SCHED_CORE")]
    forceidle_sum: u64,
}

struct CgroupFreezerState {
    /// Should the cgroup and its descendants be frozen.
    freeze: bool,
    /// Should the cgroup actually be frozen?
    e_freeze: i32,
    /// Fields below are protected by css_set_lock
    /// Number of frozen descendant cgroups
    nr_frozen_descendants: i32,
    /// Number of tasks, which are counted as frozen:
    /// frozen, SIGSTOPped, and PTRACEd.
    nr_frozen_tasks: i32,
}

/// sock_cgroup_data is embedded at sock->sk_cgrp_data and contains
/// per-socket cgroup information except for memcg association.
///
/// On legacy hierarchies, net_prio and net_cls controllers directly
/// set attributes on each sock which can then be tested by the network
/// layer. On the default hierarchy, each sock is associated with the
/// cgroup it was created in and the networking layer can match the
/// cgroup directly.
pub struct SockCgroupData {
    /// v2
    cgroup: *mut Cgroup,
    /// v1
    #[cfg(feature = "CONFIG_CGROUP_NET_CLASSID")]
    classid: u32,
    /// v1
    #[cfg(feature = "CONFIG_CGROUP_NET_PRIO")]
    prioidx: u16,
}

/// Control Group subsystem type.
/// See Documentation/admin-guide/cgroup-v1/cgroups.rst for details
struct CgroupSubsys {
    early_init: bool,
    /// If %true, the controller, on the default hierarchy, doesn't show
    /// up in "cgroup.controllers" or "cgroup.subtree_control", is
    /// implicitly enabled on all cgroups on the default hierarchy, and
    /// bypasses the "no internal process" constraint.  This is for
    /// utility type controllers which is transparent to userland.
    ///
    /// An implicit controller can be stolen from the default hierarchy
    /// anytime and thus must be okay with offline csses from previous
    /// hierarchies coexisting with csses for the current one.
    implicit_on_dfl: bool,
    /// If %true, the controller, supports threaded mode on the default
    /// hierarchy.  In a threaded subtree, both process granularity and
    /// no-internal-process constraint are ignored and a threaded
    /// controllers should be able to handle that.
    ///
    /// Note that as an implicit controller is automatically enabled on
    /// all cgroups on the default hierarchy, it should also be
    /// threaded.  implicit && !threaded is not supported.
    threaded: bool,
    /// the following two fields are initialized automatically during boot
    id: i32,
    name: *const char,
    /// optional, initialized automatically during boot if not set
    legacy_name: *const char,
    /// link to parent, protected by cgroup_lock()
    root: *mut CgroupRoot,
    /// idr for css->id
    css_idr: Idr,
    /// List of cftypes.  Each entry is the first entry of an array
    /// terminated by zero length name.
    cfts: ListHead,
    /// Base cftypes which are automatically registered.  The two can
    /// point to the same array.
    /// for the default hierarchy
    dfl_cftypes: *mut Cftype,
    /// for the legacy hierarchies
    legacy_cftypes: *mut Cftype,
    /// A subsystem may depend on other subsystems.  When such subsystem
    /// is enabled on a cgroup, the depended-upon subsystems are enabled
    /// together if available.  Subsystems enabled due to dependency are
    /// not visible to userland until explicitly enabled.  The following
    /// specifies the mask of subsystems that this one depends on.
    depends_on: u32,
}

/*
TODO: impl fns
struct cgroup_subsys {
    struct cgroup_subsys_state *(*css_alloc)(struct cgroup_subsys_state *parent_css);
    int (*css_online)(struct cgroup_subsys_state *css);
    void (*css_offline)(struct cgroup_subsys_state *css);
    void (*css_released)(struct cgroup_subsys_state *css);
    void (*css_free)(struct cgroup_subsys_state *css);
    void (*css_reset)(struct cgroup_subsys_state *css);
    void (*css_rstat_flush)(struct cgroup_subsys_state *css, int cpu);
    int (*css_extra_stat_show)(struct seq_file *seq,
                   struct cgroup_subsys_state *css);
    int (*css_local_stat_show)(struct seq_file *seq,
                   struct cgroup_subsys_state *css);

    int (*can_attach)(struct cgroup_taskset *tset);
    void (*cancel_attach)(struct cgroup_taskset *tset);
    void (*attach)(struct cgroup_taskset *tset);
    void (*post_attach)(void);
    int (*can_fork)(struct task_struct *task,
            struct css_set *cset);
    void (*cancel_fork)(struct task_struct *task, struct css_set *cset);
    void (*fork)(struct task_struct *task);
    void (*exit)(struct task_struct *task);
    void (*release)(struct task_struct *task);
    void (*bind)(struct cgroup_subsys_state *root_css);
};
*/

/// struct cftype: handler definitions for cgroup control files
///
/// When reading/writing to a file:
///  - the cgroup to use is file->f_path.dentry->d_parent->d_fsdata
///  - the 'cftype' of the file is file->f_path.dentry->d_fsdata
struct Cftype {
    /// By convention, the name should begin with the name of the
    /// subsystem, followed by a period.  Zero length string indicates
    /// end of cftype array.
    name: [char; MAX_CFTYPE_NAME],
    private: u64,
    /// The maximum length of string, excluding trailing nul, that can
    /// be passed to write.  If < PAGE_SIZE-1, PAGE_SIZE-1 is assumed.
    max_write_len: SizeT,
    /// CFTYPE_* flags
    flags: u32,
    /// If non-zero, should contain the offset from the start of css to
    /// a struct cgroup_file field.  cgroup will record the handle of
    /// the created file into it.  The recorded handle can be used as
    /// long as the containing css remains accessible.
    file_offset: u32,
    /// Fields used for internal bookkeeping.  Initialized automatically
    /// during registration.
    /// NULL for cgroup core files
    ss: *mut CgroupSubsys,
    /// anchored at ss->cfts
    node: ListHead,
    #[cfg(feature = "CONFIG_DEBUG_LOCK_ALLOC")]
    lockdep_key: LockClassKey,
}
/*
struct cftype {
    // TODO: impl fn kf_ops: *mut KernfsOps,
    //int (*open)(struct kernfs_open_file *of);
    //void (*release)(struct kernfs_open_file *of)
    /*
     * read_u64() is a shortcut for the common case of returning a
     * single integer. Use it in place of read()
     */
    u64 (*read_u64)(struct cgroup_subsys_state *css, struct cftype *cft);
    /*
     * read_s64() is a signed version of read_u64()
     */
    s64 (*read_s64)(struct cgroup_subsys_state *css, struct cftype *cft);

    /* generic seq_file read interface */
    int (*seq_show)(struct seq_file *sf, void *v);

    /* optional ops, implement all or none */
    void *(*seq_start)(struct seq_file *sf, loff_t *ppos);
    void *(*seq_next)(struct seq_file *sf, void *v, loff_t *ppos);
    void (*seq_stop)(struct seq_file *sf, void *v);

    /*
     * write_u64() is a shortcut for the common case of accepting
     * a single integer (as parsed by simple_strtoull) from
     * userspace. Use in place of write(); return 0 or error.
     */
    int (*write_u64)(struct cgroup_subsys_state *css, struct cftype *cft,
             u64 val);
    /*
     * write_s64() is a signed version of write_u64()
     */
    int (*write_s64)(struct cgroup_subsys_state *css, struct cftype *cft,
             s64 val);

    /*
     * write() is the generic write callback which maps directly to
     * kernfs write operation and overrides all other operations.
     * Maximum write size is determined by ->max_write_len.  Use
     * of_css/cft() to access the associated css and cft.
     */
    ssize_t (*write)(struct kernfs_open_file *of,
             char *buf, size_t nbytes, loff_t off);

    __poll_t (*poll)(struct kernfs_open_file *of,
             struct poll_table_struct *pt);

};
*/
