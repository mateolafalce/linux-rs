/**************************************************************************
 *   xarray.rs  --  This file is part of linux-rs.                        *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{gfp_types::GfpT, spinlock_types::SpinlockT};

/// struct XArray - The anchor of the XArray.
/// @xa_lock: Lock that protects the contents of the XArray.
///
/// To use the XArray, define it statically or embed it in your data structure.
/// It is a very small data structure, so it does not usually make sense to
/// allocate it separately and keep a pointer to it in your data structure.
///
/// You may use the xa_lock to protect your own data structures as well.
/// If all of the entries in the array are NULL, @xa_head is a NULL pointer.
/// If the only non-NULL entry in the array is at index 0, @xa_head is that
/// entry.  If any other entry in the array is non-NULL, @xa_head points
/// to an @xa_node.
pub struct Xarray {
    xa_lock: SpinlockT,
    /// private: The rest of the data structure is not to be used directly.
    xa_flags: GfpT,
    xa_head: *mut u32,
}
