/**************************************************************************
 *   pid.rs  --  This file is part of linux-rs.                           *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{
    pid_namespace::PidNamespace,
    pid_types::PIDTYPE_MAX,
    refcount::RefCountT,
    spinlock_types::SpinlockT,
    types::{HlistHead, RcuHead},
    wait::WaitQueueHeadT,
};

pub struct Pid {
    count: RefCountT,
    level: u32,
    lock: SpinlockT,
    /// lists of tasks that use this pid
    tasks: [HlistHead; PIDTYPE_MAX],
    inodes: HlistHead,
    /// wait queue for pidfd notifications
    wait_pidfd: WaitQueueHeadT,
    rcu: RcuHead,
    numbers: [Upid; 10],
}

///  struct upid is used to get the id of the struct pid, as it is
///  seen in particular namespace. Later the struct pid is found with
///  find_pid_ns() using the int nr and struct pid_namespace *ns.
struct Upid {
    nr: i32,
    ns: *mut PidNamespace,
}
