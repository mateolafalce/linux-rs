/**************************************************************************
 *   driver.rs  --  This file is part of linux-rs.                        *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::{
    drivers::{
        base::regmap::internal::Regmap,
        regulator::{core::RegulatorEnableGpio, internal::Regulator},
    },
    include::linux::{
        dcache::Dentry,
        device::Device,
        ktime::KtimeT,
        linear_range::LinearRange,
        module::Module,
        notifier::BlockingNotifierHead,
        regulator::{coupler::RegulatorCoupler, machine::RegulationConstraints},
        sched::TaskStruct,
        spinlock_types::SpinlockT,
        types::ListHead,
        workqueue::DelayedWork,
        ww_mutex::WwMutex,
    },
};

///  struct regulator_dev
///
///  Voltage / Current regulator class device. One for each
///  regulator.
///
///  This should *not* be used directly by anything except the regulator
///  core and notification injection (which should take the mutex and do
///  no other direct access).
pub struct RegulatorDev {
    desc: *const RegulatorDesc,
    exclusive: i32,
    use_count: u32,
    open_count: u32,
    bypass_count: u32,
    /// lists we belong to
    list: ListHead,
    /// lists we own
    consumer_list: ListHead,
    coupling_desc: CouplingDesc,
    notifier: BlockingNotifierHead,
    mutex: WwMutex,
    mutex_owner: *mut TaskStruct,
    ref_cnt: i32,
    owner: *mut Module,
    dev: Device,
    constraints: *mut RegulationConstraints,
    supply: *mut Regulator,
    supply_name: *const char,
    regmap: *mut Regmap,
    disable_work: DelayedWork,
    reg_data: *mut u32,
    debugfs: *mut Dentry,
    ena_pin: *mut RegulatorEnableGpio,
    ena_gpio_state: bool,
    is_switch: bool,
    /// time when this regulator was disabled last time
    last_off: KtimeT,
    cached_err: i32,
    use_cached_err: bool,
    err_lock: SpinlockT,
}

struct RegulatorDesc {
    name: *const char,
    supply_name: *const char,
    of_match: *const char,
    of_match_full_name: bool,
    regulators_node: *const char,
    id: i32,
    continuous_voltage_range: bool,
    n_voltages: u32,
    n_current_limits: u32,
    // TODO ops: *const RegulatorOps,
    irq: i32,
    // TODO impl enum regulator_type
    owner: *mut Module,
    min_uV: u32,
    uV_step: u32,
    linear_min_sel: u32,
    fixed_uV: i32,
    ramp_delay: u32,
    min_dropout_uV: i32,
    linear_ranges: *const LinearRange,
    linear_range_selectors_bitfield: u32,
    n_linear_ranges: i32,
    volt_table: u32,
    curr_table: u32,
    vsel_range_reg: u32,
    vsel_range_mask: u32,
    range_applied_by_vsel: bool,
    vsel_reg: u32,
    vsel_mask: u32,
    vsel_step: u32,
    csel_reg: u32,
    csel_mask: u32,
    apply_reg: u32,
    apply_bit: u32,
    enable_reg: u32,
    enable_mask: u32,
    enable_val: u32,
    disable_val: u32,
    enable_is_inverted: bool,
    bypass_reg: u32,
    bypass_mask: u32,
    bypass_val_on: u32,
    bypass_val_off: u32,
    active_discharge_on: u32,
    active_discharge_off: u32,
    active_discharge_mask: u32,
    active_discharge_reg: u32,
    soft_start_reg: u32,
    soft_start_mask: u32,
    soft_start_val_on: u32,
    pull_down_reg: u32,
    pull_down_mask: u32,
    pull_down_val_on: u32,
    ramp_reg: u32,
    ramp_mask: u32,
    ramp_delay_table: u32,
    n_ramp_values: u32,
    enable_time: u32,
    off_on_delay: u32,
    poll_enabled_time: u32,
}

/*int (*of_parse_cb)(struct device_node *,
          const struct regulator_desc *,
          struct regulator_config *);
unsigned int (*of_map_mode)(unsigned int mode);*/

///  struct coupling_desc
///
///  Describes coupling of regulators. Each regulator should have
///  at least a pointer to itself in coupled_rdevs array.
///  When a new coupled regulator is resolved, n_resolved is
///  incremented.
struct CouplingDesc {
    coupled_rdevs: *mut RegulatorDev,
    coupler: *mut RegulatorCoupler,
    n_resolved: i32,
    n_coupled: i32,
}
