/**************************************************************************
 *   maquine.rs  --  This file is part of linux-rs.                       *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

///  struct regulation_constraints - regulator operating constraints.
///
///  This struct describes regulator and board/machine specific constraints.
///
///  @name: Descriptive name for the constraints, used for display purposes.
///
///  @min_uV: Smallest voltage consumers may set.
///  @max_uV: Largest voltage consumers may set.
///  @uV_offset: Offset applied to voltages from consumer to compensate for
///              voltage drops.
///
///  @min_uA: Smallest current consumers may set.
///  @max_uA: Largest current consumers may set.
///  @ilim_uA: Maximum input current.
///  @system_load: Load that isn't captured by any consumer requests.
///
///  @over_curr_limits:        Limits for acting on over current.
///  @over_voltage_limits:    Limits for acting on over voltage.
///  @under_voltage_limits:    Limits for acting on under voltage.
///  @temp_limits:        Limits for acting on over temperature.
///
///  @max_spread: Max possible spread between coupled regulators
///  @max_uV_step: Max possible step change in voltage
///  @valid_modes_mask: Mask of modes which may be configured by consumers.
///  @valid_ops_mask: Operations which may be performed by consumers.
///
///  @always_on: Set if the regulator should never be disabled.
///  @boot_on: Set if the regulator is enabled when the system is initially
///            started.  If the regulator is not enabled by the hardware or
///            bootloader then it will be enabled when the constraints are
///            applied.
///  @apply_uV: Apply the voltage constraint when initialising.
///  @ramp_disable: Disable ramp delay when initialising or when setting voltage.
///  @soft_start: Enable soft start so that voltage ramps slowly.
///  @pull_down: Enable pull down when regulator is disabled.
///  @system_critical: Set if the regulator is critical to system stability or
///                    functionality.
///  @over_current_protection: Auto disable on over current event.
///
///  @over_current_detection: Configure over current limits.
///  @over_voltage_detection: Configure over voltage limits.
///  @under_voltage_detection: Configure under voltage limits.
///  @over_temp_detection: Configure over temperature limits.
///
///  @input_uV: Input voltage for regulator when supplied by another regulator.
///
///  @state_disk: State for regulator when system is suspended in disk mode.
///  @state_mem: State for regulator when system is suspended in mem mode.
///  @state_standby: State for regulator when system is suspended in standby
///                  mode.
///  @initial_state: Suspend state to set by default.
///  @initial_mode: Mode to set at startup.
///  @ramp_delay: Time to settle down after voltage change (unit: uV/us)
///  @settling_time: Time to settle down after voltage change when voltage
///            change is non-linear (unit: microseconds).
///  @settling_time_up: Time to settle down after voltage increase when voltage
///               change is non-linear (unit: microseconds).
///  @settling_time_down : Time to settle down after voltage decrease when
///              voltage change is non-linear (unit: microseconds).
///  @active_discharge: Enable/disable active discharge. The enum
///               regulator_active_discharge values are used for
///               initialisation.
///  @enable_time: Turn-on time of the rails (unit: microseconds)
///  @uv_less_critical_window_ms: Specifies the time window (in milliseconds)
///                               following a critical under-voltage (UV) event
///                               during which less critical actions can be
///                               safely carried out by the system (for example
///                               logging). After this time window more critical
///                               actions should be done (for example prevent
///                               HW damage).
pub struct RegulationConstraints {
    name: *const char,
    min_uV: i32,
    max_uV: i32,
    uV_offset: i32,
    min_uA: i32,
    max_uA: i32,
    ilim_uA: i32,
    system_load: i32,
    max_spread: *mut u32,
    max_uV_step: i32,
    valid_modes_mask: u32,
    valid_ops_mask: u32,
    input_uV: i32,
    state_disk: RegulatorState,
    state_mem: RegulatorState,
    state_standby: RegulatorState,
    over_curr_limits: NotificationLimit,
    over_voltage_limits: NotificationLimit,
    under_voltage_limits: NotificationLimit,
    temp_limits: NotificationLimit,
    initial_state: i32,
    initial_mode: u32,
    ramp_delay: u32,
    settling_time: u32,
    settling_time_up: u32,
    settling_time_down: u32,
    enable_time: u32,
    uv_less_critical_window_ms: u32,
    active_discharge: u32,

    always_on: u32, // check for bool
    boot_on: u32,
    apply_uV: u32,
    ramp_disable: u32,
    soft_start: u32,
    pull_down: u32,
    system_critical: u32,
    over_current_protection: u32,
    over_current_detection: u32,
    over_voltage_detection: u32,
    under_voltage_detection: u32,
    over_temp_detection: u32,
}

///  struct regulator_state - regulator state during low power system states
///
///  This describes a regulators state during a system wide low power
///  state.  One of enabled or disabled must be set for the
///  configuration to be applied.
///
///  @uV: Default operating voltage during suspend, it can be adjusted
///     among <min_uV, max_uV>.
///  @min_uV: Minimum suspend voltage may be set.
///  @max_uV: Maximum suspend voltage may be set.
///  @mode: Operating mode during suspend.
///  @enabled: operations during suspend.
///          - DO_NOTHING_IN_SUSPEND
///          - DISABLE_IN_SUSPEND
///          - ENABLE_IN_SUSPEND
///  @changeable: Is this state can be switched between enabled/disabled,
struct RegulatorState {
    uV: i32,
    min_uV: i32,
    max_uV: i32,
    mode: u32,
    enabled: i32,
    changeable: bool,
}

struct NotificationLimit {
    prot: i32,
    err: i32,
    warn: i32,
}
