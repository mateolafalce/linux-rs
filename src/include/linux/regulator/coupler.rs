/**************************************************************************
 *   coupler.rs  --  This file is part of linux-rs.                       *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::types::ListHead;

///  struct regulator_coupler - customized regulator's coupler
///
///  Regulator's coupler allows to customize coupling algorithm.
///
///  @list: couplers list entry
///  @attach_regulator: Callback invoked on creation of a coupled regulator,
///                     couples are unresolved at this point. The callee should
///                     check that it could handle the regulator and return 0 on
///                     success, -errno on failure and 1 if given regulator is
///                     not suitable for this coupler (case of having multiple
///                     regulators in a system). Callback shall be implemented.
///  @detach_regulator: Callback invoked on destruction of a coupled regulator.
///                     This callback is optional and could be NULL.
///  @balance_voltage: Callback invoked when voltage of a coupled regulator is
///                    changing. Called with all of the coupled rdev's being held
///                    under "consumer lock". The callee should perform voltage
///                    balancing, changing voltage of the coupled regulators as
///                    needed. It's up to the coupler to verify the voltage
///                    before changing it in hardware, i.e. coupler should
///                    check consumer's min/max and etc. This callback is
///                    optional and could be NULL, in which case a generic
///                    voltage balancer will be used.
pub struct RegulatorCoupler {
    list: ListHead,
}

/*TODO impl 	int (*attach_regulator)(struct regulator_coupler *coupler,
            struct regulator_dev *rdev);
int (*detach_regulator)(struct regulator_coupler *coupler,
            struct regulator_dev *rdev);
int (*balance_voltage)(struct regulator_coupler *coupler,
               struct regulator_dev *rdev,
               suspend_state_t state);*/
