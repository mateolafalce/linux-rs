/**************************************************************************
 *   task_stack.rs  --  This file is part of linux-rs.                    *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::sched::TaskStruct;

/// Return the address of the last usable long on the stack.
/// When the stack grows down, this is just above the thread
/// info struct. Going any lower will corrupt the threadinfo.
/// When the stack grows up, this is the highest address.
/// Beyond that position, we corrupt data on the next page.
pub fn end_of_stack(task: *mut TaskStruct) -> *mut u64 {
    #[cfg(feature = "CONFIG_STACK_GROWSUP")]
    return 0; //(unsigned long *)((unsigned long)task->stack + THREAD_SIZE) - 1;
    #[cfg(not(feature = "CONFIG_STACK_GROWSUP"))]
    return (*task).stack;
}
