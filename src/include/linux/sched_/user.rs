/**************************************************************************
 *   user.rs  --  This file is part of linux-rs.                          *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{
    percpu_counter::PercpuCounter,
    ratelimit_types::RatelimitState,
    refcount::RefCountT,
    types::{AtomicLongT, AtomicT, HlistNode},
    uidgid_types::KuidT,
};

/// Some day this will be a full-fledged user tracking system..
pub struct UserStruct {
    /// reference count
    __count: RefCountT,
    /// The number of file descriptors currently watched
    #[cfg(feature = "CONFIG_EPOLL")]
    epoll_watches: PercpuCounter,
    /// How many files in flight in unix sockets
    unix_inflight: u64,
    /// how many pages are allocated in pipe buffers
    pipe_bufs: AtomicLongT,
    /// Hash table maintenance information
    uidhash_node: HlistNode,
    uid: KuidT,
    #[cfg(any(
        feature = "CONFIG_PERF_EVENTS",
        feature = "CONFIG_BPF_SYSCALL",
        feature = "CONFIG_NET",
        feature = "CONFIG_IO_URING",
        feature = "CONFIG_VFIO_PCI_ZDEV_KVM",
        feature = "CONFIG_IOMMUFD",
    ))]
    locked_vm: AtomicLongT,
    /// The number of watches this user currently has
    #[cfg(feature = "CONFIG_WATCH_QUEUE")]
    nr_watches: AtomicT,
    /// Miscellaneous per-user rate limit
    ratelimit: RatelimitState,
}
