/**************************************************************************
 *   prio.rs  --  This file is part of linux-rs.                          *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

const MAX_NICE: i32 = 19;
const MIN_NICE: i32 = 20;
const NICE_WIDTH: i32 = MAX_NICE - MIN_NICE + 1;

/// Priority of a process goes from 0..MAX_PRIO-1, valid RT
/// priority is 0..MAX_RT_PRIO-1, and SCHED_NORMAL/SCHED_BATCH
/// tasks are in the range MAX_RT_PRIO..MAX_PRIO-1. Priority
/// values are inverted: lower p->prio value means higher priority.
pub const MAX_RT_PRIO: i32 = 100;
pub const MAX_PRIO: i32 = MAX_RT_PRIO + NICE_WIDTH;
