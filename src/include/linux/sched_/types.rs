/**************************************************************************
 *   types.rs  --  This file is part of linux-rs.                         *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

/// struct task_cputime - collected CPU time counts
/// @stime: time spent in kernel mode, in nanoseconds
/// @utime: time spent in user mode, in nanoseconds
/// @sum_exec_runtime: total time spent on the CPU, in nanoseconds
///
/// This structure groups together three kinds of CPU time that are tracked for
/// threads and thread groups.  Most things considering CPU time want to group
/// these counts together and treat all three of them in parallel.
pub struct TaskCputime {
    stime: u64,
    utime: u64,
    sum_exec_runtime: u64,
}
