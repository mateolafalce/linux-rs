/**************************************************************************
 *   i2c.rs  --  This file is part of linux-rs.                           *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::{
    drivers::{
        pinctrl::core::{Pinctrl, PinctrlState},
        regulator::internal::Regulator,
    },
    include::{
        linux::{
            completion::Completion, dcache::Dentry, device::Device, irqdomain::IrqDomain,
            module::Module, mutex_types::Mutex, rtmutex::RtMutex, types::ListHead,
        },
        uapi::linux::synclink::GpioDesc,
    },
};

/// i2c_adapter is the structure used to identify a physical i2c bus along
/// with the access algorithms necessary to access it.
pub struct I2cAdapter {
    owner: *mut Module,
    /// classes to allow probing for
    class: u32,
    // the algorithm to access the bus
    // TODO impl fn const struct i2c_algorithm *algo;
    algo_data: *mut u32,
    /// data fields that are valid for all devices
    // TODO const struct i2c_lock_operations *lock_ops;
    bus_lock: RtMutex,
    mux_lock: RtMutex,
    /// in jiffies
    timeout: i32,
    retries: i32,
    /// the adapter device
    dev: Device,
    /// owned by the I2C core
    locked_flags: u64,
    nr: i32,
    name: [char; 48],
    dev_released: Completion,
    userspace_clients_lock: Mutex,
    userspace_clients: ListHead,
    bus_recovery_info: *mut I2cBusRecoveryInfo,
    quirks: *const I2cAdapterQuirks,
    host_notify_domain: *mut IrqDomain,
    bus_regulator: *mut Regulator,
    debugfs: *mut Dentry,
}

/// struct i2c_bus_recovery_info - I2C bus recovery information
/// @recover_bus: Recover routine. Either pass driver's recover_bus() routine, or
///  i2c_generic_scl_recovery().
/// @get_scl: This gets current value of SCL line. Mandatory for generic SCL
///      recovery. Populated internally for generic GPIO recovery.
/// @set_scl: This sets/clears the SCL line. Mandatory for generic SCL recovery.
///      Populated internally for generic GPIO recovery.
/// @get_sda: This gets current value of SDA line. This or set_sda() is mandatory
///  for generic SCL recovery. Populated internally, if sda_gpio is a valid
///  GPIO, for generic GPIO recovery.
/// @set_sda: This sets/clears the SDA line. This or get_sda() is mandatory for
///  generic SCL recovery. Populated internally, if sda_gpio is a valid GPIO,
///  for generic GPIO recovery.
/// @get_bus_free: Returns the bus free state as seen from the IP core in case it
///  has a more complex internal logic than just reading SDA. Optional.
/// @prepare_recovery: This will be called before starting recovery. Platform may
///  configure padmux here for SDA/SCL line or something else they want.
/// @unprepare_recovery: This will be called after completing recovery. Platform
///  may configure padmux here for SDA/SCL line or something else they want.
/// @scl_gpiod: gpiod of the SCL line. Only required for GPIO recovery.
/// @sda_gpiod: gpiod of the SDA line. Only required for GPIO recovery.
/// @pinctrl: pinctrl used by GPIO recovery to change the state of the I2C pins.
///      Optional.
/// @pins_default: default pinctrl state of SCL/SDA lines, when they are assigned
///      to the I2C bus. Optional. Populated internally for GPIO recovery, if
///      state with the name PINCTRL_STATE_DEFAULT is found and pinctrl is valid.
/// @pins_gpio: recovery pinctrl state of SCL/SDA lines, when they are used as
///      GPIOs. Optional. Populated internally for GPIO recovery, if this state
///      is called "gpio" or "recovery" and pinctrl is valid.
pub struct I2cBusRecoveryInfo {
    /*TODO int (*recover_bus)(struct i2c_adapter *adap);
    int (*get_scl)(struct i2c_adapter *adap);
    void (*set_scl)(struct i2c_adapter *adap, int val);
    int (*get_sda)(struct i2c_adapter *adap);
    void (*set_sda)(struct i2c_adapter *adap, int val);
    int (*get_bus_free)(struct i2c_adapter *adap);
    void (*prepare_recovery)(struct i2c_adapter *adap);
    void (*unprepare_recovery)(struct i2c_adapter *adap);*/
    /// gpio recovery
    scl_gpiod: *mut GpioDesc,
    sda_gpiod: *mut GpioDesc,
    pinctrl: *mut Pinctrl,
    pins_default: *mut PinctrlState,
    pins_gpio: *mut PinctrlState,
}

/// struct i2c_adapter_quirks - describe flaws of an i2c adapter
/// @flags: see I2C_AQ_* for possible flags and read below
/// @max_num_msgs: maximum number of messages per transfer
/// @max_write_len: maximum length of a write message
/// @max_read_len: maximum length of a read message
/// @max_comb_1st_msg_len: maximum length of the first msg in a combined message
/// @max_comb_2nd_msg_len: maximum length of the second msg in a combined message
///
/// Note about combined messages: Some I2C controllers can only send one message
/// per transfer, plus something called combined message or write-then-read.
/// This is (usually) a small write message followed by a read message and
/// barely enough to access register based devices like EEPROMs. There is a flag
/// to support this mode. It implies max_num_msg = 2 and does the length checks
/// with max_comb_*_len because combined message mode usually has its own
/// limitations. Because of HW implementations, some controllers can actually do
/// write-then-anything or other variants. To support that, write-then-read has
/// been broken out into smaller bits like write-first and read-second which can
/// be combined as needed.
struct I2cAdapterQuirks {
    flags: u64,
    max_num_msgs: i32,
    max_write_len: u16,
    max_read_len: u16,
    max_comb_1st_msg_len: u16,
    max_comb_2nd_msg_len: u16,
}

impl I2cAdapter {
    const I2C_ALF_IS_SUSPENDED: usize = 0;
    const I2C_ALF_SUSPEND_REPORTED: usize = 1;
}
