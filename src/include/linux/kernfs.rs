/**************************************************************************
 *   kernfs.rs  --  This file is part of linux-rs.                        *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::{
    fs::kernfs::{
        file::KernfsOpenNode,
        kernfs_internal::{KernfsIattrs, KernfsRoot},
    },
    include::linux::{
        rbtree_types::{RbNode, RbRoot},
        types::AtomicT,
    },
};

/// kernfs_node - the building block of kernfs hierarchy.  Each and every
/// kernfs node is represented by single kernfs_node.  Most fields are
/// private to kernfs and shouldn't be accessed directly by kernfs users.
///
/// As long as count reference is held, the kernfs_node itself is
/// accessible.  Dereferencing elem or any other outer entity requires
/// active reference.
#[cfg(not(feature = "CONFIG_CGROUPS"))]
pub struct KernfsNode {
    count: AtomicT,
    active: AtomicT,
    #[cfg(feature = "CONFIG_DEBUG_LOCK_ALLOC")]
    dep_map: LockdepMap,
    /// Use kernfs_get_parent() and kernfs_name/path() instead of
    /// accessing the following two fields directly.  If the node is
    /// never moved to a different parent, it is safe to access the
    /// parent directly.
    parent: *mut KernfsNode,
    name: *const char,
    rb: RbNode,
    /// namespace tag
    ns: *const u32,
    /// ns + name hash
    hash: u32,
    dir: KernfsElemDir,
    symlink: KernfsElemSymlink,
    attr: KernfsElemAttr,
    priv_: *mut u32,
    /// 64bit unique ID.  On 64bit ino setups, id is the ino.  On 32bit,
    /// the low 32bits are ino and upper generation.
    id: u64,
    flags: u16,
    mode: u16,
    iattr: *mut KernfsIattrs,
}

#[cfg(feature = "CONFIG_CGROUPS")]
pub struct KernfsNode;

/// type-specific structures for kernfs_node union members
struct KernfsElemDir {
    subdirs: u64,
    /// children rbtree starts here and goes through kn->rb
    children: RbRoot,
    /// The kernfs hierarchy this directory belongs to.  This fits
    /// better directly in kernfs_node but is here to save space.
    root: *mut KernfsRoot,
    /// Monotonic revision counter, used to identify if a directory
    /// node has changed during negative dentry revalidation.
    rev: u64,
}

struct KernfsElemSymlink {
    target_kn: *mut KernfsNode,
}

struct KernfsElemAttr {
    // TODO: impl fns const struct kernfs_ops *ops;
    open: *mut KernfsOpenNode,
    size: u64,
    /// for kernfs_notify()
    notify_next: *mut KernfsNode,
}
