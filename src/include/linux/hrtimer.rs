/**************************************************************************
 *   hrtimer.rs  --  This file is part of linux-rs.                       *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{
    hrtimer_types::Hrtimer, spinlock_types_raw::RawSpinlockT, timerqueue_types::TimerqueueHead,
    types::ClockidT,
};

/// struct hrtimer_clock_base - the timer base for a specific clock
/// @cpu_base:      per cpu clock base
/// @index:     clock type index for per_cpu support when moving a
///         timer to a base on another cpu.
/// @clockid:       clock id for per_cpu support
/// @seq:       seqcount around __run_hrtimer
/// @running:       pointer to the currently running hrtimer
/// @active:        red black tree root node for the active timers
/// @get_time:      function to retrieve the current time of the clock
/// @offset:        offset of this clock to the monotonic base
pub struct HrtimerClockBase {
    cpu_base: *mut HrtimerCpuBase,
    index: u32,
    clockid: ClockidT,
    //TODO implf seqs seq: SeqcountRawSpinlockT,
    running: *mut Hrtimer,
    active: TimerqueueHead,
    // TODO: impl fn ktime_t  (*get_time)(void);
    offset: i64,
}

/// struct hrtimer_cpu_base - the per cpu clock bases
/// @lock:      lock protecting the base and associated clock bases
///         and timers
/// @cpu:       cpu number
/// @active_bases:  Bitfield to mark bases with active timers
/// @clock_was_set_seq: Sequence counter of clock was set events
/// @hres_active:   State of high resolution mode
/// @in_hrtirq:     hrtimer_interrupt() is currently executing
/// @hang_detected: The last hrtimer interrupt detected a hang
/// @softirq_activated: displays, if the softirq is raised - update of softirq
///         related settings is not required then.
/// @nr_events:     Total number of hrtimer interrupt events
/// @nr_retries:        Total number of hrtimer interrupt retries
/// @nr_hangs:      Total number of hrtimer interrupt hangs
/// @max_hang_time: Maximum time spent in hrtimer_interrupt
/// @softirq_expiry_lock: Lock which is taken while softirq based hrtimers are
///          expired
/// @timer_waiters: A hrtimer_cancel() invocation waits for the timer
///         callback to finish.
/// @expires_next:  absolute time of the next event, is required for remote
///         hrtimer enqueue; it is the total first expiry time (hard
///         and soft hrtimers are taken into account)
/// @next_timer:        Pointer to the first expiring timer
/// @softirq_expires_next: Time to check, if soft queues need also to be expired
/// @softirq_next_timer: Pointer to the first expiring softirq based timer
/// @clock_base:        array of clock bases for this cpu
///
/// Note: next_timer is just an optimization for __remove_hrtimer().
///  Do not dereference the pointer because it is not reliable on
///  cross cpu removals.
pub struct HrtimerCpuBase {
    lock: RawSpinlockT,
    cpu: u32,
    active_bases: u32,
    clock_was_set_seq: u32,
    hres_active: bool,
    in_hrtirq: bool,
    hang_detected: bool,
    softirq_activated: bool,
    #[cfg(feature = "CONFIG_HIGH_RES_TIMERS")]
    nr_events: u32,
    #[cfg(feature = "CONFIG_HIGH_RES_TIMERS")]
    nr_retries: u16,
    #[cfg(feature = "CONFIG_HIGH_RES_TIMERS")]
    nr_hangs: u16,
    #[cfg(feature = "CONFIG_HIGH_RES_TIMERS")]
    max_hang_time: u32,
    #[cfg(feature = "CONFIG_PREEMPT_RT")]
    softirq_expiry_lock: SpinlockT,
    #[cfg(feature = "CONFIG_PREEMPT_RT")]
    timer_waiters: AtomicT,
    expires_next: i64,
    next_timer: *mut Hrtimer,
    softirq_expires_next: i64,
    softirq_next_timer: Hrtimer,
    clock_base: [HrtimerClockBase; HrtimerCpuBase::HRTIMER_MAX_CLOCK_BASES],
}

impl HrtimerCpuBase {
    const HRTIMER_BASE_MONOTONIC: usize = 0;
    const HRTIMER_BASE_REALTIME: usize = 1;
    const HRTIMER_BASE_BOOTTIME: usize = 2;
    const HRTIMER_BASE_TAI: usize = 3;
    const HRTIMER_BASE_MONOTONIC_SOFT: usize = 4;
    const HRTIMER_BASE_REALTIME_SOFT: usize = 5;
    const HRTIMER_BASE_BOOTTIME_SOFT: usize = 6;
    const HRTIMER_BASE_TAI_SOFT: usize = 7;
    const HRTIMER_MAX_CLOCK_BASES: usize = 8;
}
