/**************************************************************************
 *   pid_namespace.rs  --  This file is part of linux-rs.                 *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::{
    include::linux::{
        idr::Idr, ns_common::NsCommon, sched::TaskStruct, types::RcuHead, user_namespace::Ucounts,
        user_namespace::UserNamespace,
    },
    mm::slab::KmemCache,
};

pub struct PidNamespace {
    idr: Idr,
    rcu: RcuHead,
    pid_allocated: u32,
    child_reaper: *mut TaskStruct,
    pid_cachep: *mut KmemCache,
    level: u32,
    parent: *mut PidNamespace,
    /*TODO#ifdef CONFIG_BSD_PROCESS_ACCT
        struct fs_pin *bacct;
    #endif*/
    user_ns: *mut UserNamespace,
    ucounts: *mut Ucounts,
    reboot: i32,
    ns: NsCommon,
    /*TODO#if defined(CONFIG_SYSCTL) && defined(CONFIG_MEMFD_CREATE)
        int memfd_noexec_scope;
    #endif*/
}
