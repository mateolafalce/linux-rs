/**************************************************************************
 *   mmzone.rs  --  This file is part of linux-rs.                        *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{
    spinlock_types::SpinlockT,
    types::{AtomicLongT, ListHead},
    zswap::ZswapLruvecState,
};

const WORKINGSET_ANON: usize = 0;
const WORKINGSET_FILE: usize = 1;
const ANON_AND_FILE: usize = 2;

const LRU_BASE: usize = 0;
const LRU_ACTIVE: usize = 1;
const LRU_FILE: usize = 2;

const LRU_INACTIVE_ANON: usize = LRU_BASE;
const LRU_ACTIVE_ANON: usize = LRU_BASE + LRU_ACTIVE;
const LRU_INACTIVE_FILE: usize = LRU_BASE + LRU_FILE;
const LRU_ACTIVE_FILE: usize = LRU_BASE + LRU_FILE + LRU_ACTIVE;
const LRU_UNEVICTABLE: usize = 4;
pub const NR_LRU_LISTS: usize = 5;

#[cfg(not(any(
    feature = "CONFIG_SHADOW_CALL_STACK",
    feature = "CONFIG_SWAP",
    feature = "CONFIG_NUMA_BALANCING"
)))]
const NR_LRU_BASE: usize = 0;
#[cfg(not(any(
    feature = "CONFIG_SHADOW_CALL_STACK",
    feature = "CONFIG_SWAP",
    feature = "CONFIG_NUMA_BALANCING"
)))]
const NR_INACTIVE_ANON: usize = NR_LRU_BASE;
#[cfg(not(any(
    feature = "CONFIG_SHADOW_CALL_STACK",
    feature = "CONFIG_SWAP",
    feature = "CONFIG_NUMA_BALANCING"
)))]
const NR_ACTIVE_ANON: usize = 1;
#[cfg(not(any(
    feature = "CONFIG_SHADOW_CALL_STACK",
    feature = "CONFIG_SWAP",
    feature = "CONFIG_NUMA_BALANCING"
)))]
const NR_INACTIVE_FILE: usize = 2;
#[cfg(not(any(
    feature = "CONFIG_SHADOW_CALL_STACK",
    feature = "CONFIG_SWAP",
    feature = "CONFIG_NUMA_BALANCING"
)))]
const NR_ACTIVE_FILE: usize = 3;
#[cfg(not(any(
    feature = "CONFIG_SHADOW_CALL_STACK",
    feature = "CONFIG_SWAP",
    feature = "CONFIG_NUMA_BALANCING"
)))]
const NR_UNEVICTABLE: usize = 4;
#[cfg(not(any(
    feature = "CONFIG_SHADOW_CALL_STACK",
    feature = "CONFIG_SWAP",
    feature = "CONFIG_NUMA_BALANCING"
)))]
const NR_SLAB_RECLAIMABLE_B: usize = 5;
#[cfg(not(any(
    feature = "CONFIG_SHADOW_CALL_STACK",
    feature = "CONFIG_SWAP",
    feature = "CONFIG_NUMA_BALANCING"
)))]
const NR_SLAB_UNRECLAIMABLE_B: usize = 6;
/// Temporary isolated pages from anon lru
#[cfg(not(any(
    feature = "CONFIG_SHADOW_CALL_STACK",
    feature = "CONFIG_SWAP",
    feature = "CONFIG_NUMA_BALANCING"
)))]
const NR_ISOLATED_ANON: usize = 7;
/// Temporary isolated pages from file lru
#[cfg(not(any(
    feature = "CONFIG_SHADOW_CALL_STACK",
    feature = "CONFIG_SWAP",
    feature = "CONFIG_NUMA_BALANCING"
)))]
const NR_ISOLATED_FILE: usize = 8;
#[cfg(not(any(
    feature = "CONFIG_SHADOW_CALL_STACK",
    feature = "CONFIG_SWAP",
    feature = "CONFIG_NUMA_BALANCING"
)))]
const WORKINGSET_NODES: usize = 9;
#[cfg(not(any(
    feature = "CONFIG_SHADOW_CALL_STACK",
    feature = "CONFIG_SWAP",
    feature = "CONFIG_NUMA_BALANCING"
)))]
const WORKINGSET_REFAULT_BASE: usize = 10;
#[cfg(not(any(
    feature = "CONFIG_SHADOW_CALL_STACK",
    feature = "CONFIG_SWAP",
    feature = "CONFIG_NUMA_BALANCING"
)))]
const WORKINGSET_REFAULT_ANON: usize = WORKINGSET_REFAULT_BASE;
#[cfg(not(any(
    feature = "CONFIG_SHADOW_CALL_STACK",
    feature = "CONFIG_SWAP",
    feature = "CONFIG_NUMA_BALANCING"
)))]
const WORKINGSET_REFAULT_FILE: usize = 11;
#[cfg(not(any(
    feature = "CONFIG_SHADOW_CALL_STACK",
    feature = "CONFIG_SWAP",
    feature = "CONFIG_NUMA_BALANCING"
)))]
const WORKINGSET_ACTIVATE_BASE: usize = 12;
#[cfg(not(any(
    feature = "CONFIG_SHADOW_CALL_STACK",
    feature = "CONFIG_SWAP",
    feature = "CONFIG_NUMA_BALANCING"
)))]
const WORKINGSET_ACTIVATE_ANON: usize = WORKINGSET_ACTIVATE_BASE;
#[cfg(not(any(
    feature = "CONFIG_SHADOW_CALL_STACK",
    feature = "CONFIG_SWAP",
    feature = "CONFIG_NUMA_BALANCING"
)))]
const WORKINGSET_ACTIVATE_FILE: usize = 13;
#[cfg(not(any(
    feature = "CONFIG_SHADOW_CALL_STACK",
    feature = "CONFIG_SWAP",
    feature = "CONFIG_NUMA_BALANCING"
)))]
const WORKINGSET_RESTORE_BASE: usize = 14;
#[cfg(not(any(
    feature = "CONFIG_SHADOW_CALL_STACK",
    feature = "CONFIG_SWAP",
    feature = "CONFIG_NUMA_BALANCING"
)))]
const WORKINGSET_RESTORE_ANON: usize = WORKINGSET_RESTORE_BASE;
#[cfg(not(any(
    feature = "CONFIG_SHADOW_CALL_STACK",
    feature = "CONFIG_SWAP",
    feature = "CONFIG_NUMA_BALANCING"
)))]
const WORKINGSET_RESTORE_FILE: usize = 15;
#[cfg(not(any(
    feature = "CONFIG_SHADOW_CALL_STACK",
    feature = "CONFIG_SWAP",
    feature = "CONFIG_NUMA_BALANCING"
)))]
const WORKINGSET_NODERECLAIM: usize = 16;
/// Mapped anonymous pages
#[cfg(not(any(
    feature = "CONFIG_SHADOW_CALL_STACK",
    feature = "CONFIG_SWAP",
    feature = "CONFIG_NUMA_BALANCING"
)))]
const NR_ANON_MAPPED: usize = 17;
/// pagecache pages mapped into pagetables.only modified from process context
#[cfg(not(any(
    feature = "CONFIG_SHADOW_CALL_STACK",
    feature = "CONFIG_SWAP",
    feature = "CONFIG_NUMA_BALANCING"
)))]
const NR_FILE_MAPPED: usize = 18;
#[cfg(not(any(
    feature = "CONFIG_SHADOW_CALL_STACK",
    feature = "CONFIG_SWAP",
    feature = "CONFIG_NUMA_BALANCING"
)))]
const NR_FILE_PAGES: usize = 19;
#[cfg(not(any(
    feature = "CONFIG_SHADOW_CALL_STACK",
    feature = "CONFIG_SWAP",
    feature = "CONFIG_NUMA_BALANCING"
)))]
const NR_FILE_DIRTY: usize = 20;
#[cfg(not(any(
    feature = "CONFIG_SHADOW_CALL_STACK",
    feature = "CONFIG_SWAP",
    feature = "CONFIG_NUMA_BALANCING"
)))]
const NR_WRITEBACK: usize = 21;
/// Writeback using temporary buffers
#[cfg(not(any(
    feature = "CONFIG_SHADOW_CALL_STACK",
    feature = "CONFIG_SWAP",
    feature = "CONFIG_NUMA_BALANCING"
)))]
const NR_WRITEBACK_TEMP: usize = 22;
/// shmem pages (included tmpfs/GEM pages)
#[cfg(not(any(
    feature = "CONFIG_SHADOW_CALL_STACK",
    feature = "CONFIG_SWAP",
    feature = "CONFIG_NUMA_BALANCING"
)))]
const NR_SHMEM: usize = 23;
#[cfg(not(any(
    feature = "CONFIG_SHADOW_CALL_STACK",
    feature = "CONFIG_SWAP",
    feature = "CONFIG_NUMA_BALANCING"
)))]
const NR_SHMEM_THPS: usize = 24;
#[cfg(not(any(
    feature = "CONFIG_SHADOW_CALL_STACK",
    feature = "CONFIG_SWAP",
    feature = "CONFIG_NUMA_BALANCING"
)))]
const NR_SHMEM_PMDMAPPED: usize = 25;
#[cfg(not(any(
    feature = "CONFIG_SHADOW_CALL_STACK",
    feature = "CONFIG_SWAP",
    feature = "CONFIG_NUMA_BALANCING"
)))]
const NR_FILE_THPS: usize = 26;
#[cfg(not(any(
    feature = "CONFIG_SHADOW_CALL_STACK",
    feature = "CONFIG_SWAP",
    feature = "CONFIG_NUMA_BALANCING"
)))]
const NR_FILE_PMDMAPPED: usize = 27;
#[cfg(not(any(
    feature = "CONFIG_SHADOW_CALL_STACK",
    feature = "CONFIG_SWAP",
    feature = "CONFIG_NUMA_BALANCING"
)))]
const NR_ANON_THPS: usize = 28;
#[cfg(not(any(
    feature = "CONFIG_SHADOW_CALL_STACK",
    feature = "CONFIG_SWAP",
    feature = "CONFIG_NUMA_BALANCING"
)))]
const NR_VMSCAN_WRITE: usize = 29;
/// Prioritise for reclaim when writeback ends
#[cfg(not(any(
    feature = "CONFIG_SHADOW_CALL_STACK",
    feature = "CONFIG_SWAP",
    feature = "CONFIG_NUMA_BALANCING"
)))]
const NR_VMSCAN_IMMEDIATE: usize = 29;
/// page dirtyings since bootup
#[cfg(not(any(
    feature = "CONFIG_SHADOW_CALL_STACK",
    feature = "CONFIG_SWAP",
    feature = "CONFIG_NUMA_BALANCING"
)))]
const NR_DIRTIED: usize = 30;
/// page writings since bootup
#[cfg(not(any(
    feature = "CONFIG_SHADOW_CALL_STACK",
    feature = "CONFIG_SWAP",
    feature = "CONFIG_NUMA_BALANCING"
)))]
const NR_WRITTEN: usize = 31;
/// NR_WRITTEN while reclaim throttled
#[cfg(not(any(
    feature = "CONFIG_SHADOW_CALL_STACK",
    feature = "CONFIG_SWAP",
    feature = "CONFIG_NUMA_BALANCING"
)))]
const NR_THROTTLED_WRITTEN: usize = 32;
/// reclaimable non-slab kernel pages
#[cfg(not(any(
    feature = "CONFIG_SHADOW_CALL_STACK",
    feature = "CONFIG_SWAP",
    feature = "CONFIG_NUMA_BALANCING"
)))]
const NR_KERNEL_MISC_RECLAIMABLE: usize = 33;
/// via: pin_user_page(), gup flag: FOLL_PIN
#[cfg(not(any(
    feature = "CONFIG_SHADOW_CALL_STACK",
    feature = "CONFIG_SWAP",
    feature = "CONFIG_NUMA_BALANCING"
)))]
const NR_FOLL_PIN_ACQUIRED: usize = 34;
/// pages returned via unpin_user_page()
#[cfg(not(any(
    feature = "CONFIG_SHADOW_CALL_STACK",
    feature = "CONFIG_SWAP",
    feature = "CONFIG_NUMA_BALANCING"
)))]
const NR_FOLL_PIN_RELEASED: usize = 35;
/// measured in KiB
#[cfg(not(any(
    feature = "CONFIG_SHADOW_CALL_STACK",
    feature = "CONFIG_SWAP",
    feature = "CONFIG_NUMA_BALANCING"
)))]
const NR_KERNEL_STACK_KB: usize = 36;
/// used for pagetables
#[cfg(not(any(
    feature = "CONFIG_SHADOW_CALL_STACK",
    feature = "CONFIG_SWAP",
    feature = "CONFIG_NUMA_BALANCING"
)))]
const NR_PAGETABLE: usize = 37;
/// secondary pagetables, e.g. KVM pagetables
#[cfg(not(any(
    feature = "CONFIG_SHADOW_CALL_STACK",
    feature = "CONFIG_SWAP",
    feature = "CONFIG_NUMA_BALANCING"
)))]
const NR_SECONDARY_PAGETABLE: usize = 38;
/// PGDEMOTE_*: pages demoted
#[cfg(not(any(
    feature = "CONFIG_SHADOW_CALL_STACK",
    feature = "CONFIG_SWAP",
    feature = "CONFIG_NUMA_BALANCING"
)))]
const PGDEMOTE_KSWAPD: usize = 39;
#[cfg(not(any(
    feature = "CONFIG_SHADOW_CALL_STACK",
    feature = "CONFIG_SWAP",
    feature = "CONFIG_NUMA_BALANCING"
)))]
const PGDEMOTE_DIRECT: usize = 40;
#[cfg(not(any(
    feature = "CONFIG_SHADOW_CALL_STACK",
    feature = "CONFIG_SWAP",
    feature = "CONFIG_NUMA_BALANCING"
)))]
const PGDEMOTE_KHUGEPAGED: usize = 40;
#[cfg(not(any(
    feature = "CONFIG_SHADOW_CALL_STACK",
    feature = "CONFIG_SWAP",
    feature = "CONFIG_NUMA_BALANCING"
)))]
pub const NR_VM_NODE_STAT_ITEMS: usize = 41;

/*
TODO: complete combinations
enum node_stat_item {
    NR_LRU_BASE,
    NR_INACTIVE_ANON = NR_LRU_BASE, /* must match order of LRU_[IN]ACTIVE */
    NR_ACTIVE_ANON,
    NR_INACTIVE_FILE,
    NR_ACTIVE_FILE,
    NR_UNEVICTABLE,
    NR_SLAB_RECLAIMABLE_B,
    NR_SLAB_UNRECLAIMABLE_B,
    NR_ISOLATED_ANON,
    NR_ISOLATED_FILE,
    WORKINGSET_NODES,
    WORKINGSET_REFAULT_BASE,
    WORKINGSET_REFAULT_ANON = WORKINGSET_REFAULT_BASE,
    WORKINGSET_REFAULT_FILE,
    WORKINGSET_ACTIVATE_BASE,
    WORKINGSET_ACTIVATE_ANON = WORKINGSET_ACTIVATE_BASE,
    WORKINGSET_ACTIVATE_FILE,
    WORKINGSET_RESTORE_BASE,
    WORKINGSET_RESTORE_ANON = WORKINGSET_RESTORE_BASE,
    WORKINGSET_RESTORE_FILE,
    WORKINGSET_NODERECLAIM,
    NR_ANON_MAPPED,
    NR_FILE_MAPPED,
    NR_FILE_PAGES,
    NR_FILE_DIRTY,
    NR_WRITEBACK,
    NR_WRITEBACK_TEMP,
    NR_SHMEM,
    NR_SHMEM_THPS,
    NR_SHMEM_PMDMAPPED,
    NR_FILE_THPS,
    NR_FILE_PMDMAPPED,
    NR_ANON_THPS,
    NR_VMSCAN_WRITE,
    NR_VMSCAN_IMMEDIATE,
    NR_DIRTIED,
    NR_WRITTEN,
    NR_THROTTLED_WRITTEN,
    NR_KERNEL_MISC_RECLAIMABLE,
    NR_FOLL_PIN_ACQUIRED,
    NR_FOLL_PIN_RELEASED,
    NR_KERNEL_STACK_KB,

#if IS_ENABLED(CONFIG_SHADOW_CALL_STACK)
    NR_KERNEL_SCS_KB,	/* measured in KiB */
#endif

    NR_PAGETABLE,
    NR_SECONDARY_PAGETABLE,

#ifdef CONFIG_SWAP
    NR_SWAPCACHE,
#endif

#ifdef CONFIG_NUMA_BALANCING
    PGPROMOTE_SUCCESS,	/* promote successfully */
    PGPROMOTE_CANDIDATE,	/* candidate pages to promote */
#endif

    PGDEMOTE_KSWAPD,
    PGDEMOTE_DIRECT,
    PGDEMOTE_KHUGEPAGED,
    NR_VM_NODE_STAT_ITEMS
};*/

pub struct Lruvec {
    lists: [ListHead; NR_LRU_LISTS],
    /// per lruvec lru_lock for memcg
    lru_lock: SpinlockT,
    /// These track the cost of reclaiming one LRU - file or anon -
    /// over the other. As the observed cost of reclaiming one LRU
    /// increases, the reclaim scan balance tips toward the other.
    anon_cost: u64,
    file_cost: u64,
    /// Non-resident age, driven by LRU movement
    nonresident_age: AtomicLongT,
    /// Refaults at the time of last reclaim cycle
    refaults: [u64; ANON_AND_FILE],
    /// Various lruvec state flags (enum lruvec_flags)
    flags: u64,
    /// evictable pages divided into generations
    #[cfg(feature = "CONFIG_LRU_GEN")]
    lrugen: LruGenFolio,
    /// to concurrently iterate lru_gen_mm_list
    #[cfg(all(feature = "CONFIG_LRU_GEN", feature = "CONFIG_LRU_GEN_WALKS_MMU"))]
    mm_state: LruGenMmState,
    #[cfg(feature = "CONFIG_MEMCG")]
    pgdat: *mut PglistData,
    zswap_lruvec_state: ZswapLruvecState,
}
