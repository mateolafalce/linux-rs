/**************************************************************************
 *   fs.rs  --  This file is part of linux-rs.                            *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

#[cfg(feature = "CONFIG_FS_ENCRYPTION")]
use crate::fs::crypto::fscrypt_private::FscryptInodeInfo;
#[cfg(feature = "CONFIG_FS_VERITY")]
use crate::fs::verity::fsverity_private::FsverityInfo;
#[cfg(feature = "CONFIG_FSNOTIFY")]
use crate::include::linux::fsnotify_backend::FsnotifyMarkConnector;

#[cfg(feature = "CONFIG_CGROUP_WRITEBACK")]
use crate::include::linux::backing_dev_defs::BdiWriteback;

#[cfg(feature = "__NEED_I_SIZE_ORDERED")]
use crate::include::linux::seqlock_types::SeqcountT;

use crate::include::linux::{
    backing_dev_defs::BackingDevInfo,
    blk_types::BlockDevice,
    blkdev::BdevHandle,
    cdev::Cdev,
    cred::Cred,
    dcache::Dentry,
    errseq::ErrseqT,
    filelock::FileLockContext,
    gfp_types::GfpT,
    list_bl::HlistBlHead,
    list_lru::ListLru,
    llist::LlistNode,
    lockdep_types::LockClassKey,
    module::Module,
    mtd::mtd::MtdInfo,
    mutex_types::Mutex,
    path::Path,
    percpu_rwsem::PercpuRwSemaphore,
    pid::Pid,
    pipe_fs_i::PipeInodeInfo,
    quota::QuotaInfo,
    rbtree_types::RbRootCached,
    rwlock_types::RwlockT,
    rwsem::RwSemaphore,
    shrinker::Shrinker,
    spinlock_types::SpinlockT,
    time64::Time64T,
    time64::Timespec64,
    types::{
        Atomic64T, AtomicLongT, AtomicT, BlkcntT, FmodeT, HlistHead, HlistNode, ListHead, PgoffT,
        RcuHead,
    },
    uidgid_types::{KgidT, KuidT},
    user_namespace::UserNamespace,
    uuid::UuidT,
    workqueue::{WorkStruct, WorkqueueStruct},
    xarray::Xarray,
    xattr::XattrHandler,
};

/// Possible states of 'frozen' field
/// FS is unfrozen
const SB_UNFROZEN: usize = 0;
///  Writes, dir ops, ioctls frozen       
const SB_FREEZE_WRITE: usize = 1;
/// Page faults stopped as well        
const SB_FREEZE_PAGEFAULT: usize = 2;
/// For internal FS use (e.g. to stop internal threads if needed)
const SB_FREEZE_FS: usize = 3;
/// ->freeze_fs finished successfully    
const SB_FREEZE_COMPLETE: usize = 4;

const SB_FREEZE_LEVELS: usize = SB_FREEZE_COMPLETE - 1;

/// f_{lock,count,pos_lock} members can be highly contended and share
/// the same cacheline. f_{lock,mode} are very frequently used together
/// and so share the same cacheline as well. The read-mostly
/// f_{path,inode,op} are kept on a separate cacheline.
pub struct File {
    f_llist: LlistNode,
    f_rcuhead: RcuHead,
    f_iocb_flags: u32,
    /// Protects f_ep, f_flags.
    /// Must not be taken from IRQ context.
    f_lock: SpinlockT,
    f_mode: FmodeT,
    f_count: AtomicLongT,
    f_pos_lock: Mutex,
    f_pos: i64,
    f_flags: u32,
    f_owner: FownStruct,
    f_cred: *const Cred,
    f_ra: FileRaState,
    f_path: Path,
    /// cached value
    f_inode: *mut Inode,
    f_op: *const FileOperations,
    f_version: u64,
    #[cfg(feature = "CONFIG_SECURITY")]
    f_security: *mut u32,
    /// needed for tty driver, and maybe others
    private_data: *mut u32,
    #[cfg(feature = "CONFIG_EPOLL")]
    /// Used by fs/eventpoll.c to link all the hooks to this file
    f_ep: *mut HlistHead,
    f_mapping: *mut AddressSpace,
    f_wb_err: ErrseqT,
    /// for syncfs
    f_sb_err: ErrseqT,
}

/// struct address_space - Contents of a cacheable, mappable object.
/// @host: Owner, either the inode or the block_device.
/// @i_pages: Cached pages.
/// @invalidate_lock: Guards coherency between page cache contents and
///   file offset->disk block mappings in the filesystem during invalidates.
///   It is also used to block modification of page cache contents through
///   memory mappings.
/// @gfp_mask: Memory allocation flags to use for allocating pages.
/// @i_mmap_writable: Number of VM_SHARED, VM_MAYWRITE mappings.
/// @nr_thps: Number of THPs in the pagecache (non-shmem only).
/// @i_mmap: Tree of private and shared mappings.
/// @i_mmap_rwsem: Protects @i_mmap and @i_mmap_writable.
/// @nrpages: Number of page entries, protected by the i_pages lock.
/// @writeback_index: Writeback starts here.
/// @a_ops: Methods.
/// @flags: Error bits and flags (AS_*).
/// @wb_err: The most recent error which has occurred.
/// @private_lock: For use by the owner of the address_space.
/// @private_list: For use by the owner of the address_space.
/// @private_data: For use by the owner of the address_space.
pub struct AddressSpace {
    host: *mut Inode,
    i_pages: Xarray,
    invalidate_lock: RwSemaphore,
    gfp_mask: GfpT,
    i_mmap_writable: AtomicT,
    #[cfg(feature = "CONFIG_READ_ONLY_THP_FOR_FS")]
    /// number of thp, only for non-shmem files
    nr_thps: AtomicT,
    i_mmap: RbRootCached,
    nrpages: u64,
    writeback_index: PgoffT,
    flags: u64,
    i_mmap_rwsem: RwSemaphore,
    wb_err: ErrseqT,
    private_lock: SpinlockT,
    private_list: ListHead,
    private_data: *mut u32,
}

/// Keep mostly read-only and often accessed (especially for
/// the RCU path lookup and 'stat' data) fields at the beginning
/// of the 'struct inode'
pub struct Inode {
    i_mode: u16,
    i_opflags: u16,
    i_uid: KuidT,
    i_gid: KgidT,
    i_flags: u32,
    #[cfg(feature = "CONFIG_FS_POSIX_ACL")]
    i_acl: *mut PosixAcl,
    #[cfg(feature = "CONFIG_FS_POSIX_ACL")]
    i_default_acl: *mut PosixAcl,
    i_sb: *mut SuperBlock,
    i_mapping: AddressSpace,
    #[cfg(feature = "CONFIG_SECURITY")]
    i_security: *mut u32,
    /// Stat data, not accessed from path walking
    i_ino: u64,
    /// Filesystems may only read i_nlink directly.  They shall use the
    /// following functions for modification:
    ///
    ///    (set|clear|inc|drop)_nlink
    ///    inode_(inc|dec)_link_count
    i_nlink: u32,
    __i_nlink: u32,
    i_rdev: u32,
    i_size: i64,
    __i_atime: Timespec64,
    __i_mtime: Timespec64,
    /// use inode_*_ctime accessors!
    __i_ctime: Timespec64,
    /// i_blocks, i_bytes, maybe i_size
    i_lock: SpinlockT,
    i_bytes: u16,
    i_blkbits: u8,
    i_write_hint: u8,
    i_blocks: BlkcntT,
    #[cfg(feature = "__NEED_I_SIZE_ORDERED")]
    i_size_seqcount: SeqcountT,
    /// Misc
    i_state: u64,
    i_rwsem: RwSemaphore,
    /// jiffies of first dirtying
    dirtied_when: u64,
    dirtied_time_when: u64,
    i_hash: HlistNode,
    /// backing dev IO list
    i_io_list: ListHead,
    #[cfg(feature = "CONFIG_CGROUP_WRITEBACK")]
    c_c_w: ConfigCgroupWriteback,
    /// inode LRU list
    i_lru: ListHead,
    i_sb_list: ListHead,
    /// backing dev writeback list
    i_wb_list: ListHead,
    i_dentry: HlistHead,
    i_rcu: RcuHead,
    i_version: Atomic64T,
    /// see futex
    i_sequence: Atomic64T,
    i_count: AtomicT,
    i_dio_count: AtomicT,
    i_writecount: AtomicT,
    #[cfg(any(feature = "CONFIG_IMA", feature = "CONFIG_FILE_LOCKING"))]
    /// struct files open RO
    i_readcount: AtomicT,
    /// former ->i_op->default_file_ops
    i_fop: *const FileOperations,
    //free_inode: fn(&mut Inode),
    i_flctx: *mut FileLockContext,
    i_data: AddressSpace,
    i_devices: ListHead,
    i_pipe: PipeInodeInfo,
    i_cdev: *mut Cdev,
    i_link: *mut char,
    i_dir_seq: u32,
    i_generation: u32,
    #[cfg(feature = "CONFIG_FSNOTIFY")]
    /// all events this inode cares about
    i_fsnotify_mask: u32,
    #[cfg(feature = "CONFIG_FSNOTIFY")]
    i_fsnotify_marks: *mut FsnotifyMarkConnector,
    #[cfg(feature = "CONFIG_FS_ENCRYPTION")]
    i_crypt_info: FscryptInodeInfo,
    #[cfg(feature = "CONFIG_FS_VERITY")]
    i_verity_info: FsverityInfo,
    /// i_private;  fs or device private pointer
    i_private: *mut u32,
}

#[cfg(feature = "CONFIG_CGROUP_WRITEBACK")]
struct ConfigCgroupWriteback {
    /// the associated cgroup wb
    i_wb: *mut BdiWriteback,
    /// foreign inode detection, see wbc_detach_inode()
    i_wb_frn_winner: i32,
    i_wb_frn_avg_time: u16,
    i_wb_frn_history: u16,
}

pub struct SuperBlock {
    /// Keep this first
    s_list: ListHead,
    /// search index; _not_ kdev_t
    s_dev: u32,
    s_blocksize_bits: u8,
    s_blocksize: u64,
    /// Max file size
    s_maxbytes: i64,
    s_type: *mut FileSystemType,
    s_flags: u64,
    /// internal SB_I_* flags
    s_iflags: u64,
    s_magic: u64,
    s_root: *mut Dentry,
    s_umount: RwSemaphore,
    s_count: i32,
    s_active: AtomicT,
    #[cfg(feature = "CONFIG_SECURITY")]
    s_security: *mut u32,
    s_xattr: *const XattrHandler,
    #[cfg(feature = "CONFIG_FS_ENCRYPTION")]
    s_cop: *const FscryptOperations,
    /// master crypto keys in use
    #[cfg(feature = "CONFIG_FS_ENCRYPTION")]
    s_master_keys: *mut FscryptKeyring,
    #[cfg(feature = "CONFIG_FS_VERITY")]
    s_vop: *const FsverityOperations,
    #[cfg(feature = "CONFIG_UNICODE")]
    s_encoding: *mut UnicodeMap,
    #[cfg(feature = "CONFIG_UNICODE")]
    s_encoding_flags: u16,
    /// alternate root dentries for NFS
    s_roots: HlistBlHead,
    /// list of mounts; _not_ for fs use
    s_mounts: ListHead,
    s_bdev: BlockDevice,
    s_bdev_handle: *mut BdevHandle,
    s_bdi: *mut BackingDevInfo,
    s_mtd: *mut MtdInfo,
    s_instances: HlistNode,
    /// Bitmask of supported quota types
    s_quota_types: u32,
    /// Diskquota specific options
    s_dquot: QuotaInfo,
    s_writers: SbWriters,
    /// Keep s_fs_info, s_time_gran, s_fsnotify_mask, and
    /// s_fsnotify_marks together for cache efficiency. They are frequently
    /// accessed and rarely modified.
    /// Filesystem private info
    s_fs_info: *mut u32,
    /// Granularity of c/m/atime in ns (cannot be worse than a second)
    s_time_gran: u32,
    /// Time limits for c/m/atime in seconds
    s_time_min: Time64T,
    s_time_max: Time64T,
    #[cfg(feature = "CONFIG_FSNOTIFY")]
    s_fsnotify_mask: u32,
    #[cfg(feature = "CONFIG_FSNOTIFY")]
    s_fsnotify_marks: *mut FsnotifyMarkConnector,
    /// Informational name
    s_id: [char; 32],
    /// UUID
    s_uuid: UuidT,
    s_max_links: u32,
    /// The next field is for VFS *only*. No filesystems have any business
    /// even looking at it. You had been warned.
    /// Kludge
    s_vfs_rename_mutex: Mutex,
    /// Filesystem subtype.  If non-empty the filesystem type field
    /// in /proc/mounts will be "type.subtype"
    s_subtype: *const char,
    /// per-sb shrinker handle
    s_shrink: *mut Shrinker,
    /// Number of inodes with nlink == 0 but still referenced
    s_remove_count: AtomicLongT,
    /// Number of inode/mount/sb objects that are being watched, note that
    /// inodes objects are currently double-accounted.
    s_fsnotify_connectors: AtomicLongT,
    /// Read-only state of the superblock is being changed
    s_readonly_remount: i32,
    /// per-sb errseq_t for reporting writeback errors via syncfs
    s_wb_err: ErrseqT,
    /// AIO completions deferred from interrupt context
    s_dio_done_wq: *mut WorkqueueStruct,
    s_pins: HlistHead,
    /// Owning user namespace and default context in which to
    /// interpret filesystem uids, gids, quotas, device nodes,
    /// xattrs and security labels.
    s_user_ns: *mut UserNamespace,
    /// The list_lru structure is essentially just a pointer to a table
    /// of per-node lru lists, each of which has its own spinlock.
    /// There is no need to put them into separate cachelines.
    s_dentry_lru: ListLru,
    s_inode_lru: ListLru,
    rcu: RcuHead,
    destroy_work: WorkStruct,
    /// sync serialisation lock
    s_sync_lock: Mutex,
    /// Indicates how deep in a filesystem stack this SB is
    s_stack_depth: i32,
    /// s_inode_list_lock protects s_inodes
    s_inode_list_lock: SpinlockT,
    /// all inodes
    s_inodes: ListHead,
    s_inode_wblist_lock: SpinlockT,
    /// writeback inodes
    s_inodes_wb: ListHead,
}

/*
TODO: impl fns
struct super_block {
    const struct super_operations   *s_op;
    const struct dquot_operations   *dq_op;
    const struct quotactl_ops   *s_qcop;
    const struct export_operations *s_export_op;
    const struct dentry_operations *s_d_op; /* default d_op for dentries */
*/

pub struct FileSystemType {
    name: *const char,
    fs_flags: i32,
    owner: *mut Module,
    next: *mut FileSystemType,
    fs_supers: HlistHead,
    s_lock_key: LockClassKey,
    s_umount_key: LockClassKey,
    s_vfs_rename_key: LockClassKey,
    s_writers_key: [LockClassKey; SB_FREEZE_LEVELS],
    i_lock_key: LockClassKey,
    i_mutex_key: LockClassKey,
    invalidate_lock_key: LockClassKey,
    i_mutex_dir_key: LockClassKey,
}

/*TODO: impl fns
    int (*init_fs_context)(struct fs_context *);
    const struct fs_parameter_spec *parameters;
    struct dentry *(*mount) (struct file_system_type *, int,const char *, void *);
    void (*kill_sb) (struct super_block *);
};*/

pub struct FasyncStruct {
    fa_lock: RwlockT,
    magic: i32,
    fa_fd: i32,
    /// singly linked list
    fa_next: *mut FasyncStruct,
    fa_file: *mut File,
    fa_rcu: RcuHead,
}

/// struct file_ra_state - Track a file's readahead state.
/// @start: Where the most recent readahead started.
/// @size: Number of pages read in the most recent readahead.
/// @async_size: Numer of pages that were/are not needed immediately
///      and so were/are genuinely "ahead".  Start next readahead when
///      the first of these pages is accessed.
/// @ra_pages: Maximum size of a readahead request, copied from the bdi.
/// @mmap_miss: How many mmap accesses missed in the page cache.
/// @prev_pos: The last byte in the most recent read request.
///
/// When this structure is passed to ->readahead(), the "most recent"
/// readahead means the current readahead.
struct FileRaState {
    start: PgoffT,
    size: u32,
    async_size: u32,
    ra_pages: u32,
    mmap_miss: u32,
    prev_pos: i64,
}

struct FownStruct {
    /// protects pid, uid, euid fields
    lock: RwlockT,
    /// pid or -pgrp where SIGIO should be sent
    pid: *mut Pid,
    /// uid/euid of process setting the owner
    uid: KuidT,
    euid: KuidT,
    /// posix.1b rt signal to be delivered on IO
    signum: i32,
}

struct SbWriters {
    /// Is sb frozen?
    frozen: bool,
    /// How many kernel freeze requests?
    freeze_kcount: i32,
    /// How many userspace freeze requests?
    freeze_ucount: i32,
    rw_sem: [PercpuRwSemaphore; SB_FREEZE_LEVELS],
}

struct FileOperations {
    owner: *mut Module,
}

/*
TODO impl fns
struct file_operations {
    loff_t (*llseek) (struct file *, loff_t, int);
    ssize_t (*read) (struct file *, char __user *, size_t, loff_t *);
    ssize_t (*write) (struct file *, const char __user *, size_t, loff_t *);
    ssize_t (*read_iter) (struct kiocb *, struct iov_iter *);
    ssize_t (*write_iter) (struct kiocb *, struct iov_iter *);
    int (*iopoll)(struct kiocb *kiocb, struct io_comp_batch *,unsigned int flags);
    int (*iterate_shared) (struct file *, struct dir_context *);
    __poll_t (*poll) (struct file *, struct poll_table_struct *);
    long (*unlocked_ioctl) (struct file *, unsigned int, unsigned long);
    long (*compat_ioctl) (struct file *, unsigned int, unsigned long);
    int (*mmap) (struct file *, struct vm_area_struct *);
    unsigned long mmap_supported_flags;
    int (*open) (struct inode *, struct file *);
    int (*flush) (struct file *, fl_owner_t id);
    int (*release) (struct inode *, struct file *);
    int (*fsync) (struct file *, loff_t, loff_t, int datasync);
    int (*fasync) (int, struct file *, int);
    int (*lock) (struct file *, int, struct file_lock *);
    unsigned long (*get_unmapped_area)(struct file *, unsigned long, unsigned long, unsigned long, unsigned long);
    int (*check_flags)(int);
    int (*flock) (struct file *, int, struct file_lock *);
    ssize_t (*splice_write)(struct pipe_inode_info *, struct file *, loff_t *, size_t, unsigned int);
    ssize_t (*splice_read)(struct file *, loff_t *, struct pipe_inode_info *, size_t, unsigned int);
    void (*splice_eof)(struct file *file);
    int (*setlease)(struct file *, int, struct file_lock **, void **);
    long (*fallocate)(struct file *file, int mode, loff_t offset,loff_t len);
    void (*show_fdinfo)(struct seq_file *m, struct file *f);
#ifndef CONFIG_MMU
    unsigned (*mmap_capabilities)(struct file *);
#endif
    ssize_t (*copy_file_range)(struct file *, loff_t, struct file *,loff_t, size_t, unsigned int);
    loff_t (*remap_file_range)(struct file *file_in, loff_t pos_in,struct file *file_out, loff_t pos_out,loff_t len, unsigned int remap_flags);
    int (*fadvise)(struct file *, loff_t, loff_t, int);
    int (*uring_cmd)(struct io_uring_cmd *ioucmd, unsigned int issue_flags);
    int (*uring_cmd_iopoll)(struct io_uring_cmd *, struct io_comp_batch *,
                unsigned int poll_flags);
} __randomize_layout;*/

/*
TODO: impl fn
impl AddressSpace {}
struct address_space_operations {
    int (*writepage)(struct page *page, struct writeback_control *wbc);
    int (*read_folio)(struct file *, struct folio *);

    /* Write back some dirty pages from this mapping. */
    int (*writepages)(struct address_space *, struct writeback_control *);

    /* Mark a folio dirty.  Return true if this dirtied it */
    bool (*dirty_folio)(struct address_space *, struct folio *);

    void (*readahead)(struct readahead_control *);

    int (*write_begin)(struct file *, struct address_space *mapping,
                loff_t pos, unsigned len,
                struct page **pagep, void **fsdata);
    int (*write_end)(struct file *, struct address_space *mapping,
                loff_t pos, unsigned len, unsigned copied,
                struct page *page, void *fsdata);

    /* Unfortunately this kludge is needed for FIBMAP. Don't use it */
    sector_t (*bmap)(struct address_space *, sector_t);
    void (*invalidate_folio) (struct folio *, size_t offset, size_t len);
    bool (*release_folio)(struct folio *, gfp_t);
    void (*free_folio)(struct folio *folio);
    ssize_t (*direct_IO)(struct kiocb *, struct iov_iter *iter);
    /*
     * migrate the contents of a folio to the specified target. If
     * migrate_mode is MIGRATE_ASYNC, it must not block.
     */
    int (*migrate_folio)(struct address_space *, struct folio *dst,
            struct folio *src, enum migrate_mode);
    int (*launder_folio)(struct folio *);
    bool (*is_partially_uptodate) (struct folio *, size_t from,
            size_t count);
    void (*is_dirty_writeback) (struct folio *, bool *dirty, bool *wb);
    int (*error_remove_folio)(struct address_space *, struct folio *);

    /* swapfile support */
    int (*swap_activate)(struct swap_info_struct *sis, struct file *file,
                sector_t *span);
    void (*swap_deactivate)(struct file *file);
    int (*swap_rw)(struct kiocb *iocb, struct iov_iter *iter);
};*/

/*
TODO: impl fn
impl AddressSpace {}
struct inode_operations {
    struct dentry * (*lookup) (struct inode *,struct dentry *, unsigned int);
    const char * (*get_link) (struct dentry *, struct inode *, struct delayed_call *);
    int (*permission) (struct mnt_idmap *, struct inode *, int);
    struct posix_acl * (*get_inode_acl)(struct inode *, int, bool);

    int (*readlink) (struct dentry *, char __user *,int);

    int (*create) (struct mnt_idmap *, struct inode *,struct dentry *,
               umode_t, bool);
    int (*link) (struct dentry *,struct inode *,struct dentry *);
    int (*unlink) (struct inode *,struct dentry *);
    int (*symlink) (struct mnt_idmap *, struct inode *,struct dentry *,
            const char *);
    int (*mkdir) (struct mnt_idmap *, struct inode *,struct dentry *,
              umode_t);
    int (*rmdir) (struct inode *,struct dentry *);
    int (*mknod) (struct mnt_idmap *, struct inode *,struct dentry *,
              umode_t,dev_t);
    int (*rename) (struct mnt_idmap *, struct inode *, struct dentry *,
            struct inode *, struct dentry *, unsigned int);
    int (*setattr) (struct mnt_idmap *, struct dentry *, struct iattr *);
    int (*getattr) (struct mnt_idmap *, const struct path *,
            struct kstat *, u32, unsigned int);
    ssize_t (*listxattr) (struct dentry *, char *, size_t);
    int (*fiemap)(struct inode *, struct fiemap_extent_info *, u64 start,
              u64 len);
    int (*update_time)(struct inode *, int);
    int (*atomic_open)(struct inode *, struct dentry *,
               struct file *, unsigned open_flag,
               umode_t create_mode);
    int (*tmpfile) (struct mnt_idmap *, struct inode *,
            struct file *, umode_t);
    struct posix_acl *(*get_acl)(struct mnt_idmap *, struct dentry *,
                     int);
    int (*set_acl)(struct mnt_idmap *, struct dentry *,
               struct posix_acl *, int);
    int (*fileattr_set)(struct mnt_idmap *idmap,
                struct dentry *dentry, struct fileattr *fa);
    int (*fileattr_get)(struct dentry *dentry, struct fileattr *fa);
    struct offset_ctx *(*get_offset_ctx)(struct inode *inode);
} ____cacheline_aligned;
*/

impl FownStruct {
    const PIDTYPE_PID: usize = 0;
    const PIDTYPE_TGID: usize = 1;
    const PIDTYPE_PGID: usize = 2;
    const PIDTYPE_SID: usize = 3;
    const PIDTYPE_MAX: usize = 4;
}

impl FileSystemType {
    const FS_REQUIRES_DEV: usize = 1;
    const FS_BINARY_MOUNTDATA: usize = 2;
    const FS_HAS_SUBTYPE: usize = 4;
    /// Can be mounted by userns root
    const FS_USERNS_MOUNT: usize = 8;
    ///  Disable fanotify permission events
    const FS_DISALLOW_NOTIFY_PERM: usize = 16;
    /// FS has been updated to handle vfs idmappings.
    const FS_ALLOW_IDMAP: usize = 32;
    /// FS will handle d_move() during rename() internally.
    const FS_RENAME_DOES_D_MOVE: usize = 32768;
}
