/**************************************************************************
 *   shrinker.rs  --  This file is part of linux-rs.                      *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::{
    asm_generic::bitsperlong::BITS_PER_LONG,
    linux::{
        completion::Completion,
        refcount::RefCountT,
        types::{AtomicLongT, ListHead, RcuHead},
    },
};

const SHRINKER_UNIT_BITS: usize = BITS_PER_LONG;

/// A callback you can register to apply pressure to ageable caches.
///
/// @count_objects should return the number of freeable items in the cache. If
/// there are no objects to free, it should return SHRINK_EMPTY, while 0 is
/// returned in cases of the number of freeable items cannot be determined
/// or shrinker should skip this cache for this time (e.g., their number
/// is below shrinkable limit). No deadlock checks should be done during the
/// count callback - the shrinker relies on aggregating scan counts that couldn't
/// be executed due to potential deadlocks to be run at a later call when the
/// deadlock condition is no longer pending.
///
/// @scan_objects will only be called if @count_objects returned a non-zero
/// value for the number of freeable objects. The callout should scan the cache
/// and attempt to free items from the cache. It should then return the number
/// of objects freed during the scan, or SHRINK_STOP if progress cannot be made
/// due to potential deadlocks. If SHRINK_STOP is returned, then no further
/// attempts to call the @scan_objects will be made from the current reclaim
/// context.
///
/// @flags determine the shrinker abilities, like numa awareness
pub struct Shrinker {
    // TODO: impl fn unsigned long (*count_objects)(struct shrinker *,struct shrink_control *sc);
    // TODO: impl fn unsigned long (*scan_objects)(struct shrinker *,struct shrink_control *sc);
    /// reclaim batch size, 0 = default
    batch: i64,
    /// seeks to recreate an obj
    seeks: i32,
    flags: u32,
    /// The reference count of this shrinker. Registered shrinker have an
    /// initial refcount of 1, then the lookup operations are now allowed
    /// to use it via shrinker_try_get(). Later in the unregistration step,
    /// the initial refcount will be discarded, and will free the shrinker
    /// asynchronously via RCU after its refcount reaches 0.
    refcount: RefCountT,
    /// use to wait for refcount to reach 0
    done: Completion,
    rcu: RcuHead,
    private_data: *mut u32,
    /// These are for internal use
    list: ListHead,
    /// ID in shrinker_idr
    #[cfg(feature = "CONFIG_MEMCG")]
    id: i32,
    #[cfg(feature = "CONFIG_SHRINKER_DEBUG")]
    debugfs_id: i32,
    #[cfg(feature = "CONFIG_SHRINKER_DEBUG")]
    name: *const char,
    #[cfg(feature = "CONFIG_SHRINKER_DEBUG")]
    debugfs_entry: *mut Dentry,
    /// objs pending delete, per node
    nr_deferred: *mut AtomicLongT,
}

pub struct ShrinkerInfo {
    rcu: RcuHead,
    map_nr_max: i32,
    unit: [*mut ShrinkerInfoUnit; 10],
}

/// Bitmap and deferred work of shrinker::id corresponding to memcg-aware
/// shrinkers, which have elements charged to the memcg.
struct ShrinkerInfoUnit {
    nr_deferred: [AtomicLongT; SHRINKER_UNIT_BITS],
    //LOOK DECLARE_BITMAP(map, SHRINKER_UNIT_BITS);
}
