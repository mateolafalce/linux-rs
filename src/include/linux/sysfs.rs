/**************************************************************************
 *   sysfs.rs  --  This file is part of linux-rs.                         *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::types::SizeT;

pub struct AttributeGroup {
    name: *const char,
    attrs: *mut Attribute,
    bin_attrs: *mut BinAttribute,
    //TODO: impl fns
    //u16         (*is_visible)(struct kobject *, struct attribute *, int);
    //u16         (*is_bin_visible)(struct kobject *, struct bin_attribute *, int);
}

pub struct Attribute {
    name: *const char,
    mode: u16,
    #[cfg(feature = "CONFIG_DEBUG_LOCK_ALLOC")]
    ignore_lockdep: bool,
    #[cfg(feature = "CONFIG_DEBUG_LOCK_ALLOC")]
    key: *mut LockClassKey,
    #[cfg(feature = "CONFIG_DEBUG_LOCK_ALLOC")]
    skey: LockClassKey,
}

pub struct BinAttribute {
    attr: Attribute,
    size: SizeT,
    private: *mut u32,
    /*TODO impl fns
    struct address_space *(*f_mapping)(void);
    ssize_t (*read)(struct file *, struct kobject *, struct bin_attribute *,char *, loff_t, size_t);
    ssize_t (*write)(struct file *, struct kobject *, struct bin_attribute *,char *, loff_t, size_t);
    loff_t (*llseek)(struct file *, struct kobject *, struct bin_attribute *,loff_t, int);
    int (*mmap)(struct file *, struct kobject *, struct bin_attribute *attr,struct vm_area_struct *vma);*/
}
