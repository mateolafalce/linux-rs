/**************************************************************************
 *   phylink.rs  --  This file is part of linux-rs.                       *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::{drivers::net::phy::phylink::Phylink, include::linux::device::Device};

///  struct phylink_config - PHYLINK configuration structure
///  @dev: a pointer to a struct device associated with the MAC
///  @type: operation type of PHYLINK instance
///  @poll_fixed_state: if true, starts link_poll,
///               if MAC link is at %MLO_AN_FIXED mode.
///  @mac_managed_pm: if true, indicate the MAC driver is responsible for PHY PM.
///  @mac_requires_rxc: if true, the MAC always requires a receive clock from PHY.
///                     The PHY driver should start the clock signal as soon as
///                     possible and avoid stopping it during suspend events.
///  @ovr_an_inband: if true, override PCS to MLO_AN_INBAND
///  @get_fixed_state: callback to execute to determine the fixed link state,
///              if MAC link is at %MLO_AN_FIXED mode.
///  @supported_interfaces: bitmap describing which PHY_INTERFACE_MODE_xxx
///                         are supported by the MAC/PCS.
///  @mac_capabilities: MAC pause/speed/duplex capabilities.
pub struct PhylinkConfig {
    dev: *mut Device,
    // TODO impl enum phylink_op_type
    poll_fixed_state: bool,
    mac_managed_pm: bool,
    mac_requires_rxc: bool,
    ovr_an_inband: bool,
    mac_capabilities: u64,
}
/*void (*get_fixed_state)(struct phylink_config *config,
                struct phylink_link_state *state);
DECLARE_PHY_INTERFACE_MASK(supported_interfaces);*/

///  struct phylink_link_state - link state structure
///  @advertising: ethtool bitmask containing advertised link modes
///  @lp_advertising: ethtool bitmask containing link partner advertised link
///    modes
///  @interface: link &typedef phy_interface_t mode
///  @speed: link speed, one of the SPEED_* constants.
///  @duplex: link duplex mode, one of DUPLEX_* constants.
///  @pause: link pause state, described by MLO_PAUSE_* constants.
///  @rate_matching: rate matching being performed, one of the RATE_MATCH_*
///    constants. If rate matching is taking place, then the speed/duplex of
///    the medium link mode (@speed and @duplex) and the speed/duplex of the phy
///    interface mode (@interface) are different.
///  @link: true if the link is up.
///  @an_complete: true if autonegotiation has completed.
pub struct PhylinkLinkState {
    speed: i32,
    duplex: i32,
    pause: i32,
    rate_matching: i32,
    link: bool,
    an_complete: bool,
}

///  struct phylink_pcs - PHYLINK PCS instance
///  @ops: a pointer to the &struct phylink_pcs_ops structure
///  @phylink: pointer to &struct phylink_config
///  @neg_mode: provide PCS neg mode via "mode" argument
///  @poll: poll the PCS for link changes
///
///  This structure is designed to be embedded within the PCS private data,
///  and will be passed between phylink and the PCS.
///
///  The @phylink member is private to phylink and must not be touched by
///  the PCS driver.
pub struct PhylinkPcs {
    //TODO -> ops: *const PhylinkPcsOps,
    phylink: *mut Phylink,
    neg_mode: bool,
    poll: bool,
}
