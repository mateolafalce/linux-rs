/**************************************************************************
 *   sbitmap.rs  --  This file is part of linux-rs.                       *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{spinlock_types::SpinlockT, types::AtomicT, wait::WaitQueueHeadT};

/// struct sbitmap - Scalable bitmap.
///
/// A &struct sbitmap is spread over multiple cachelines to avoid ping-pong. This
/// trades off higher memory usage for better scalability.
pub struct Sbitmap {
    /// @depth: Number of bits used in the whole bitmap.
    depth: u32,
    /// @shift: log2(number of bits used per word)
    shift: u32,
    /// @map_nr: Number of words (cachelines) being used for the bitmap.
    map_nr: u32,
    /// @round_robin: Allocate bits in strict round-robin order.
    round_robin: bool,
    /// @map: Allocated bitmap.
    map: *mut SbitmapWord,
    /// @alloc_hint: Cache of last successfully allocated or freed bit.
    ///
    /// This is per-cpu, which allows multiple users to stick to different
    /// cachelines until the map is exhausted.
    alloc_hint: *mut u32,
}

/// struct sbitmap_queue - Scalable bitmap with the added ability to wait on free
/// bits.
///
/// A &struct sbitmap_queue uses multiple wait queues and rolling wakeups to
/// avoid contention on the wait queue spinlock. This ensures that we don't hit a
/// scalability wall when we run out of free bits and have to start putting tasks
/// to sleep.
pub struct SbitmapQueue {
    /// @sb: Scalable bitmap.
    sb: Sbitmap,
    /// @wake_batch: Number of bits which must be freed
    /// before we wake up any waiters.
    wake_batch: u32,
    /// @wake_index: Next wait queue in @ws to wake up.
    wake_index: AtomicT,
    /// @ws: Wait queues.
    ws: *mut SbqWaitState,
    /// @ws_active: count of currently active ws waitqueues
    ws_active: AtomicT,
    /// @min_shallow_depth: The minimum shallow depth which
    /// may be passed to sbitmap_queue_get_shallow()
    min_shallow_depth: u32,
    /// @completion_cnt: Number of bits cleared
    /// passed to the wakeup function.
    completion_cnt: AtomicT,
    /// @wakeup_cnt: Number of thread wake ups issued.
    wakeup_cnt: AtomicT,
}

/// struct sbitmap_word - Word in a &struct sbitmap.
/// @word: word holding free bits
/// @cleared: word holding cleared bits
/// @swap_lock: serializes simultaneous updates of ->word and ->cleared
struct SbitmapWord {
    word: u64,
    cleared: u64,
    swap_lock: SpinlockT,
}

///  struct sbq_wait_state - Wait queue in a &struct sbitmap_queue.
///  @wait: Wait queue.
struct SbqWaitState {
    wait: WaitQueueHeadT,
}
