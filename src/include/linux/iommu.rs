/**************************************************************************
 *   iommu.rs  --  This file is part of linux-rs.                         *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::{
    drivers::iommu::{io_pgfault::IopfDeviceParam, iommu::IommuGroup},
    include::linux::{device::Device, fwnode::FwnodeHandle, mutex_types::Mutex, types::ListHead},
};

/// struct dev_iommu - Collection of per-device IOMMU data
///
/// @fault_param: IOMMU detected device fault reporting data
/// @iopf_param: I/O Page Fault queue and data
/// @fwspec:IOMMU fwspec data
/// @iommu_dev:IOMMU device this device is linked to
/// @priv:IOMMU Driver private data
/// @max_pasids:  number of PASIDs this device can consume
/// @attach_deferred: the dma domain attachment is deferred
/// @pci_32bit_workaround: Limit DMA allocations to 32-bit IOVAs
/// @require_direct: device requires IOMMU_RESV_DIRECT regions
/// @shadow_on_flush: IOTLB flushes are used to sync shadow tables
///
/// TODO: migrate other per device data pointers under iommu_dev_data, e.g.
/// struct iommu_group *iommu_group;
pub struct DevIommu {
    lock: Mutex,
    fault_param: *mut IommuFaultParam,
    iopf_param: *mut IopfDeviceParam,
    fwspec: *mut IommuFwspec,
    iommu_dev: *mut IommuDevice,
    priv_: *mut u32,
    max_pasids: u32,
    attach_deferred: u32,
    pci_32bit_workaround: u32,
    require_direct: u32,
    shadow_on_flush: u32,
}

/// struct iommu_fault_param - per-device IOMMU fault data
/// @handler: Callback function to handle IOMMU faults at device level
/// @data: handler private data
/// @faults: holds the pending faults which needs response
/// @lock: protect pending faults list
#[cfg(feature = "CONFIG_IOMMU_API")]
pub struct IommuFaultParam {
    handler: IommuDevFaultHandlerT,
    data: *mut u32,
    faults: ListHead,
    lock: Mutex,
}

#[cfg(not(feature = "CONFIG_IOMMU_API"))]
pub struct IommuFaultParam {}

/// struct iommu_fwspec - per-device IOMMU instance data
/// @ops: ops for this device's IOMMU
/// @iommu_fwnode: firmware handle for this device's IOMMU
/// @flags: IOMMU_FWSPEC_* flags
/// @num_ids: number of associated device IDs
/// @ids: IDs which this device may present to the IOMMU
///
/// Note that the IDs (and any other information, really) stored in this structure should be
/// considered private to the IOMMU device driver and are not to be used directly by IOMMU
/// consumers.
struct IommuFwspec {
    // TODO: impl fns const struct iommu_ops  *ops;
    iommu_fwnode: *mut FwnodeHandle,
    flags: u32,
    num_ids: u32,
    ids: [u32; 10],
}

/// struct iommu_device - IOMMU core representation of one IOMMU hardware
/// instance
/// @list: Used by the iommu-core to keep a list of registered iommus
/// @ops: iommu-ops for talking to this iommu
/// @dev: struct device for sysfs handling
/// @singleton_group: Used internally for drivers that have only one group
/// @max_pasids: number of supported PASIDs
struct IommuDevice {
    list: ListHead,
    // TODO: impl fns const struct iommu_ops *ops;
    fwnode: *mut FwnodeHandle,
    dev: *mut Device,
    singleton_group: *mut IommuGroup,
    max_pasids: u32,
}
