/**************************************************************************
 *   igmp.rs  --  This file is part of linux-rs.                          *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{
    inetdevice::InDevice, refcount::RefCountT, spinlock_types::SpinlockT, timer::TimerList,
    types::RcuHead,
};

pub struct IpMcList {
    interface: *mut InDevice,
    multiaddr: u32,
    sfmode: u32,
    sources: *mut IpSfList,
    tomb: *mut IpSfList,
    sfcount: [u64; 2],
    next: *mut IpMcList,
    next_rcu: *mut IpMcList,
    next_hash: *mut IpMcList,
    timer: TimerList,
    users: i32,
    refcnt: RefCountT,
    lock: SpinlockT,
    tm_running: i8,
    reporter: i8,
    unsolicit_count: i8,
    loaded: i8,
    /// check source marks?
    gsquery: u8,
    crcount: u8,
    rcu: RcuHead,
}

struct IpSfList {
    sf_next: *mut IpSfList,
    /// include/exclude counts
    sf_count: [u64; 2],
    sf_inaddr: u32,
    /// include in g & s response?
    sf_gsresp: u8,
    /// change state
    sf_oldin: u8,
    /// retrans. left to send
    sf_crcount: u8,
}
