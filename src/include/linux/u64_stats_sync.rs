/**************************************************************************
 *   u64_stats_sync.rs  --  This file is part of linux-rs.                *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::asm_generic::local64::Local64T;

/// Protect against 64-bit values tearing on 32-bit architectures. This is
/// typically used for statistics read/update in different subsystems.
///
/// Key points :
///
/// -  Use a seqcount on 32-bit
/// -  The whole thing is a no-op on 64-bit architectures.
///
/// Usage constraints:
///
/// 1) Write side must ensure mutual exclusion, or one seqcount update could
///    be lost, thus blocking readers forever.
///
/// 2) Write side must disable preemption, or a seqcount reader can preempt the
///    writer and also spin forever.
///
/// 3) Write side must use the _irqsave() variant if other writers, or a reader,
///    can be invoked from an IRQ context. On 64bit systems this variant does not
///    disable interrupts.
///
/// 4) If reader fetches several counters, there is no guarantee the whole values
///    are consistent w.r.t. each other (remember point #2: seqcounts are not
///    used for 64bit architectures).
///
/// 5) Readers are allowed to sleep or be preempted/interrupted: they perform
///    pure reads.
///
/// Usage :
///
/// Stats producer (writer) should use following template granted it already got
/// an exclusive access to counters (a lock is already taken, or per cpu
/// data is used [in a non preemptable context])
///
///   spin_lock_bh(...) or other synchronization to get exclusive access
///   ...
///   u64_stats_update_begin(&stats->syncp);
///   u64_stats_add(&stats->bytes64, len); // non atomic operation
///   u64_stats_inc(&stats->packets64);    // non atomic operation
///   u64_stats_update_end(&stats->syncp);
///
/// While a consumer (reader) should use following template to get consistent
/// snapshot for each variable (but no guarantee on several ones)
///
/// u64 tbytes, tpackets;
/// unsigned int start;
///
/// do {
///         start = u64_stats_fetch_begin(&stats->syncp);
///         tbytes = u64_stats_read(&stats->bytes64); // non atomic operation
///         tpackets = u64_stats_read(&stats->packets64); // non atomic operation
/// } while (u64_stats_fetch_retry(&stats->syncp, start));
///
///
/// Example of use in drivers/net/loopback.c, using per_cpu containers,
/// in BH disabled context.
pub struct U64StatsSync {
    // if BITS_PER_LONG == 32 -> seqcount_t	seq (actually just 64 support)
}

pub struct U64StatsT {
    v: Local64T,
}
