/**************************************************************************
 *   user_namespace.rs  --  This file is part of linux-rs.                *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{
    ns_common::NsCommon,
    types::{AtomicLongT, AtomicT, HlistNode},
    uidgid_types::{KgidT, KuidT},
    workqueue::WorkStruct,
};

const UID_GID_MAP_MAX_BASE_EXTENTS: usize = 5;

const UCOUNT_RLIMIT_NPROC: usize = 0;
const UCOUNT_RLIMIT_MSGQUEUE: usize = 1;
const UCOUNT_RLIMIT_SIGPENDING: usize = 2;
const UCOUNT_RLIMIT_MEMLOCK: usize = 3;
const UCOUNT_RLIMIT_COUNTS: usize = 4;

const UCOUNT_USER_NAMESPACES: usize = 0;
const UCOUNT_PID_NAMESPACES: usize = 1;
const UCOUNT_UTS_NAMESPACES: usize = 2;
const UCOUNT_IPC_NAMESPACES: usize = 3;
const UCOUNT_NET_NAMESPACES: usize = 4;
const UCOUNT_MNT_NAMESPACES: usize = 5;
const UCOUNT_CGROUP_NAMESPACES: usize = 6;
const UCOUNT_TIME_NAMESPACES: usize = 7;
const UCOUNT_COUNTS: usize = 8;

pub struct UserNamespace {
    uid_map: UidGidMap,
    gid_map: UidGidMap,
    projid_map: UidGidMap,
    parent: *mut UserNamespace,
    level: i32,
    owner: KuidT,
    group: KgidT,
    ns: NsCommon,
    flags: u64,
    /// parent_could_setfcap: true if the creator if this ns had CAP_SETFCAP
    /// in its effective capability set at the child ns creation time.
    parent_could_setfcap: bool,
    /// List of joinable keyrings in this namespace.  Modification access of
    /// these pointers is controlled by keyring_sem.  Once
    /// user_keyring_register is set, it won't be changed, so it can be
    /// accessed directly with READ_ONCE().
    #[cfg(feature = "CONFIG_KEYS")]
    keyring_name_list: ListHead,
    #[cfg(feature = "CONFIG_KEYS")]
    user_keyring_register: *mut Key,
    #[cfg(feature = "CONFIG_KEYS")]
    keyring_sem: RwSemaphore,
    /// Register of per-UID persistent keyrings for this namespace
    #[cfg(feature = "CONFIG_PERSISTENT_KEYRINGS")]
    persistent_keyring_register: *mut Key,
    work: WorkStruct,
    #[cfg(feature = "CONFIG_SYSCTL")]
    set: CtlTableSet,
    #[cfg(feature = "CONFIG_SYSCTL")]
    sysctls: *mut CtlTableHeader,
    ucounts: *mut Ucounts,
    ucount_max: [i64; UCOUNT_COUNTS],
    rlimit_max: [i64; UCOUNT_RLIMIT_COUNTS],
    #[cfg(feature = "CONFIG_BINFMT_MISC")]
    binfmt_misc: *mut BinfmtMisc,
}

struct UidGidExtent {
    first: u32,
    lower_first: u32,
    count: u32,
}

pub struct Ucounts {
    node: HlistNode,
    ns: UserNamespace,
    uid: KuidT,
    count: AtomicT,
    ucount: [AtomicLongT; UCOUNT_COUNTS],
    rlimit: [AtomicLongT; UCOUNT_RLIMIT_COUNTS],
}

pub struct UidGidMap {
    nr_extents: u32,
    extent: [UidGidExtent; UID_GID_MAP_MAX_BASE_EXTENTS],
    forward: *mut UidGidExtent,
    reverse: *mut UidGidExtent,
}
