/**************************************************************************
 *   percpu_rwsem.rs  --  This file is part of linux-rs.                  *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{
    rcu_sync::RcuSync, rcuwait::Rcuwait, types::AtomicT, wait::WaitQueueHeadT,
};

pub struct PercpuRwSemaphore {
    rss: RcuSync,
    read_count: *mut u32,
    writer: Rcuwait,
    waiters: WaitQueueHeadT,
    block: AtomicT,
    #[cfg(feature = "CONFIG_DEBUG_LOCK_ALLOC")]
    dep_map: LockdepMap,
}
