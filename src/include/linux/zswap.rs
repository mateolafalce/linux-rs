/**************************************************************************
 *   zswap.rs  --  This file is part of linux-rs.                         *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::types::AtomicLongT;

pub struct ZswapLruvecState {
    /// Number of pages in zswap that should be protected from the shrinker.
    /// This number is an estimate of the following counts:
    ///
    /// a) Recent page faults.
    /// b) Recent insertion to the zswap LRU. This includes new zswap stores,
    ///    as well as recent zswap LRU rotations.
    ///
    /// These pages are likely to be warm, and might incur IO if the are written
    /// to swap.
    nr_zswap_protected: AtomicLongT,
}
