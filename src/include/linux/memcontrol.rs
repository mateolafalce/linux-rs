/**************************************************************************
 *   memcontrol.rs  --  This file is part of linux-rs.                    *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::{
    fs::eventfd::EventfdCtx,
    include::linux::{
        cgroup_defs::CgroupFile,
        cgroup_defs::CgroupSubsysState,
        mmzone::{Lruvec, NR_LRU_LISTS, NR_VM_NODE_STAT_ITEMS},
        mutex_types::Mutex,
        page_counter::PageCounter,
        percpu_refcount::PercpuRef,
        rbtree_types::RbNode,
        refcount::RefCountT,
        sched::TaskStruct,
        shrinker::ShrinkerInfo,
        spinlock_types::SpinlockT,
        types::{AtomicLongT, AtomicT, ListHead, RcuHead},
        vmpressure::Vmpressure,
        workqueue::WorkStruct,
    },
    mm::memcontrol::{MemcgVmstats, MemcgVmstatsPercpu},
    tools::testing::memblock::linux::mmzone::MAX_NR_ZONES,
};

/// Cgroup-specific page state, on top of universal node page state
const MEMCG_SWAP: usize = NR_VM_NODE_STAT_ITEMS;
const MEMCG_SOCK: usize = NR_VM_NODE_STAT_ITEMS + 1;
const MEMCG_PERCPU_B: usize = NR_VM_NODE_STAT_ITEMS + 2;
const MEMCG_VMALLOC: usize = NR_VM_NODE_STAT_ITEMS + 3;
const MEMCG_KMEM: usize = NR_VM_NODE_STAT_ITEMS + 4;
const MEMCG_ZSWAP_B: usize = NR_VM_NODE_STAT_ITEMS + 5;
const MEMCG_ZSWAPPED: usize = NR_VM_NODE_STAT_ITEMS + 6;
pub const MEMCG_NR_STAT: usize = NR_VM_NODE_STAT_ITEMS + 7;

const MEMCG_LOW: usize = 0;
const MEMCG_HIGH: usize = 1;
const MEMCG_MAX: usize = 2;
const MEMCG_OOM: usize = 3;
const MEMCG_OOM_KILL: usize = 4;
const MEMCG_OOM_GROUP_KILL: usize = 5;
const MEMCG_SWAP_HIGH: usize = 6;
const MEMCG_SWAP_MAX: usize = 7;
const MEMCG_SWAP_FAIL: usize = 8;
const MEMCG_NR_MEMORY_EVENTS: usize = 9;

/// The memory controller data structure. The memory controller controls both
/// page cache and RSS per cgroup. We would eventually like to provide
/// statistics based on the statistics developed by Rik Van Riel for clock-pro,
/// to help the administrator determine what knobs to tune.
pub struct MemCgroup {
    css: CgroupSubsysState,
    /// Private memcg ID. Used to ID objects that outlive the cgroup
    id: MemCgroupId,
    /// Accounted resources
    /// Both v1 & v2
    memory: PageCounter,
    /// v2 only
    swap: PageCounter,
    /// v1 only
    memsw: PageCounter,
    /// Legacy consumer-oriented counters
    /// v1 only
    kmem: PageCounter,
    /// v1 only
    tcpmem: PageCounter,
    /// Range enforcement for interrupt charges
    high_work: WorkStruct,
    #[cfg(all(feature = "CONFIG_MEMCG_KMEM", feature = "CONFIG_ZSWAP"))]
    zswap_max: u64,
    /// Prevent pages from this memcg from being written back from zswap to
    /// swap, and from being swapped out on zswap store failures.
    #[cfg(all(feature = "CONFIG_MEMCG_KMEM", feature = "CONFIG_ZSWAP"))]
    zswap_writeback: bool,
    soft_limit: u64,
    /// vmpressure notifications
    vmpressure: Vmpressure,
    /// Should the OOM killer kill all belonging tasks, had it kill one?
    oom_group: bool,
    /// protected by memcg_oom_lock
    oom_lock: bool,
    under_oom: i32,
    swappiness: i32,
    /// OOM-Killer disable
    oom_kill_disable: i32,
    /// memory.events and memory.events.local
    events_file: CgroupFile,
    events_local_file: CgroupFile,
    /// handle for "memory.swap.events"
    swap_events_file: CgroupFile,
    /// protect arrays of thresholds
    thresholds_lock: Mutex,
    /// thresholds for memory usage. RCU-protected
    thresholds: MemCgroupThresholds,
    /// thresholds for mem+swap usage. RCU-protected
    memsw_thresholds: MemCgroupThresholds,
    /// For oom notifier event fd
    oom_notify: ListHead,
    /// Should we move charges of a task when a task is moved into this
    /// mem_cgroup ? And what type of charges should we move ?
    move_charge_at_immigrate: u64,
    /// taken only while moving_account > 0
    move_lock: SpinlockT,
    move_lock_flags: u64,
    /// memory.stat
    vmstats: *mut MemcgVmstats,
    /// memory.events
    memory_events: [AtomicLongT; MEMCG_NR_MEMORY_EVENTS],
    memory_events_local: [AtomicLongT; MEMCG_NR_MEMORY_EVENTS],
    /// Hint of reclaim pressure for socket memroy management. Note
    /// that this indicator should NOT be used in legacy cgroup mode
    /// where socket memory is accounted/charged separately.
    socket_pressure: u64,
    /// Legacy tcp memory accounting
    tcpmem_active: bool,
    tcpmem_pressure: i32,
    #[cfg(feature = "CONFIG_MEMCG_KMEM")]
    kmemcg_id: i32,
    /// memcg->objcg is wiped out as a part of the objcg repaprenting
    /// process. memcg->orig_objcg preserves a pointer (and a reference)
    /// to the original objcg until the end of live of memcg.
    #[cfg(feature = "CONFIG_MEMCG_KMEM")]
    objcg: *mut ObjCgroup,
    #[cfg(feature = "CONFIG_MEMCG_KMEM")]
    orig_objcg: *mut ObjCgroup,
    #[cfg(feature = "CONFIG_MEMCG_KMEM")]
    /// list of inherited objcgs, protected by objcg_lock
    objcg_list: ListHead,
    /// set > 0 if pages under this cgroup are moving to other cgroup.
    moving_account: AtomicT,
    move_lock_task: *mut TaskStruct,
    vmstats_percpu: *mut MemcgVmstatsPercpu,
    #[cfg(feature = "CONFIG_CGROUP_WRITEBACK")]
    cgwb_list: ListHead,
    #[cfg(feature = "CONFIG_CGROUP_WRITEBACK")]
    cgwb_domain: WbDomain,
    #[cfg(feature = "CONFIG_CGROUP_WRITEBACK")]
    cgwb_frn: [MemcgCgwbFrn; MEMCG_CGWB_FRN_CNT],
    /// List of events which userspace want to receive
    event_list: ListHead,
    event_list_lock: SpinlockT,
    #[cfg(feature = "CONFIG_TRANSPARENT_HUGEPAGE")]
    deferred_split_queue: DeferredSplit,
    /// per-memcg mm_struct list
    #[cfg(feature = "CONFIG_LRU_GEN_WALKS_MMU")]
    mm_list: LruGenMmList,
    nodeinfo: *mut MemCgroupPerNode,
}

/// Bucket for arbitrarily byte-sized objects charged to a memory
/// cgroup. The bucket can be reparented in one piece when the cgroup
/// is destroyed, without having to round up the individual references
/// of all live memory objects in the wild.
pub struct ObjCgroup {
    refcnt: PercpuRef,
    memcg: *mut MemCgroup,
    nr_charged_bytes: AtomicT,
    /// protected by objcg_lock
    list: ListHead,
    rcu: RcuHead,
}

struct MemCgroupId {
    id: i32,
    ref_: RefCountT,
}

struct MemCgroupThresholds {
    /// Primary thresholds array
    primary: *mut MemCgroupThresholdAry,
    /// Spare threshold array.
    /// This is needed to make mem_cgroup_unregister_event() "never fail".
    /// It must be able to store at least primary->size - 1 entries.
    spare: *mut MemCgroupThresholdAry,
}

/// per-node information in memory controller.
struct MemCgroupPerNode {
    lruvec: Lruvec,
    lruvec_stats_percpu: *mut LruvecStatsPercpu,
    lruvec_stats: LruvecStats,
    lru_zone_size: [[u64; NR_LRU_LISTS]; MAX_NR_ZONES],
    iter: MemCgroupReclaimIter,
    shrinker_info: *mut ShrinkerInfo,
    /// RB tree node
    tree_node: RbNode,
    /// Set to the value by which the soft limit is exceeded
    usage_in_excess: u64,
    on_tree: bool,
    /// Back pointer, we cannot use container_of
    memcg: *mut MemCgroup,
}

/// For threshold
struct MemCgroupThresholdAry {
    /// An array index points to threshold just below or equal to usage.
    current_threshold: i32,
    /// Size of entries[]
    size: u32,
    /// Array of thresholds
    entries: [MemCgroupThreshold; 10],
}

struct LruvecStatsPercpu {
    /// Local (CPU and cgroup) state
    state: [i64; NR_VM_NODE_STAT_ITEMS],
    /// Delta calculation for lockless upward propagation
    state_prev: [i64; NR_VM_NODE_STAT_ITEMS],
}

struct LruvecStats {
    /// Aggregated (CPU and subtree) state
    state: [i64; NR_VM_NODE_STAT_ITEMS],
    /// Non-hierarchical (CPU aggregated) state
    state_local: [i64; NR_VM_NODE_STAT_ITEMS],
    /// Pending child counts during tree propagation
    state_pending: [i64; NR_VM_NODE_STAT_ITEMS],
}

struct MemCgroupReclaimIter {
    position: *mut MemCgroup,
    /// scan generation, increased every round-trip
    generation: u32,
}

struct MemCgroupThreshold {
    eventfd: *mut EventfdCtx,
    threshold: u64,
}
