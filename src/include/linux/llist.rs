/**************************************************************************
 *   llist.rs  --  This file is part of linux-rs.                         *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

pub struct LlistHead {
    first: *mut LlistNode,
}

/// list of a node
pub struct LlistNode {
    first: *mut LlistNode,
    next: *mut LlistNode,
}

/// default -> null
impl Default for LlistNode {
    fn default() -> Self {
        Self::new()
    }
}

impl LlistNode {
    pub fn new() -> Self {
        LlistNode {
            first: core::ptr::null_mut(),
            next: core::ptr::null_mut(),
        }
    }

    pub fn set_next(&mut self, next: *mut LlistNode) {
        self.next = next;
    }

    pub fn llist_head(&self) -> *mut LlistNode {
        self.first
    }
}
