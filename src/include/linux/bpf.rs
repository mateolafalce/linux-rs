/**************************************************************************
 *   bpf.rs  --  This file is part of linux-rs.                           *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::{
    include::{
        linux::{
            bpf_cgroup::BpfCgroupStorage,
            module::Module,
            mutex_types::Mutex,
            spinlock_types::SpinlockT,
            types::{Atomic64T, RcuHead},
            workqueue::WorkStruct,
        },
        uapi::linux::bpf::BPF_OBJ_NAME_LEN,
    },
    kernel::bpf::{btf::Btf, preload::iterators::iterators_bpf::BpfProg},
};

const BPF_CGROUP_STORAGE_SHARED: usize = 0;
const BPF_CGROUP_STORAGE_PERCPU: usize = 1;
const __BPF_CGROUP_STORAGE_MAX: usize = 2;
const MAX_BPF_CGROUP_STORAGE_TYPE: usize = __BPF_CGROUP_STORAGE_MAX;

/// an array of programs to be executed under rcu_lock.
///
/// Typical usage:
/// ret = bpf_prog_run_array(rcu_dereference(&bpf_prog_array), ctx, bpf_prog_run);
///
/// the structure returned by bpf_prog_array_alloc() should be populated
/// with program pointers and the last pointer must be NULL.
/// The user has to keep refcnt on the program and make sure the program
/// is removed from the array before bpf_prog_put().
/// The 'struct bpf_prog_array *' should only be replaced with xchg()
/// since other cpus are walking the array of pointers in parallel.
struct BpfProgArrayItem {
    prog: *mut BpfProg,
    cgroup_storage: [*mut BpfCgroupStorage; MAX_BPF_CGROUP_STORAGE_TYPE],
    bpf_cookie: u64,
}

pub struct BpfProgArray {
    rcu: RcuHead,
    items: [BpfProgArrayItem; 10],
}

pub struct BpfMap {
    inner_map_meta: *mut BpfMap,
    #[cfg(feature = "CONFIG_SECURITY")]
    security: *mut *mut u32,
    key_size: u32,
    value_size: u32,
    max_entries: u32,
    /// any per-map-type extra fields
    map_extra: u64,
    map_flags: u32,
    id: u32,
    record: *mut BtfRecord,
    numa_node: i32,
    btf_key_type_id: u32,
    btf_value_type_id: u32,
    btf_vmlinux_value_type_id: u32,
    btf: *mut Btf,
    #[cfg(feature = "CONFIG_MEMCG_KMEM")]
    objcg: *mut ObjCgroup,
    name: [char; BPF_OBJ_NAME_LEN],
    /// The 3rd and 4th cacheline with misc members to avoid false sharing
    /// particularly with refcounting.
    refcnt: Atomic64T,
    usercnt: Atomic64T,
    /// rcu is used before freeing and work is only used during freeing
    work: WorkStruct,
    rcu: RcuHead,
    freeze_mutex: Mutex,
    writecnt: Atomic64T,
    /// 'Ownership' of program-containing map is claimed by the first program
    /// that is going to use this map or by the first program which FD is
    /// stored in the map to make sure that all callers and callees have the
    /// same prog type, JITed flag and xdp_has_frags flag.
    lock: SpinlockT,
    jited: bool,
    xdp_has_frags: bool,
    bypass_spec_v1: bool,
    /// write-once; write-protected by freeze_mutex
    frozen: bool,
    free_after_mult_rcu_gp: bool,
    free_after_rcu_gp: bool,
    sleepable_refcnt: Atomic64T,
    elem_count: *mut i64,
}

/*struct bpf_map {
    TODO: impl fns
    /* The first two cachelines with read-mostly members of which some
     * are also accessed in fast-path (e.g. ops, max_entries).
     */
    const struct bpf_map_ops *ops ____cacheline_aligned;
};*/

struct BtfFieldKptr {
    btf: *mut Btf,
    module: *mut Module,
    /// dtor used if btf_is_kernel(btf), otherwise the type is
    /// program-allocated, dtor is NULL,  and __bpf_obj_drop_impl is used
    // TODO impl fn dtor: BtfDtorKfuncT,
    btf_id: u32,
}

struct BtfFieldGraphRoot {
    btf: *mut Btf,
    value_btf_id: u32,
    node_offset: u32,
    value_rec: *mut BtfRecord,
}

struct BtfRecord {
    cnt: u32,
    field_mask: u32,
    spin_lock_off: i32,
    timer_off: i32,
    refcount_off: i32,
    fields: [BtfField; 10],
}

struct BtfField {
    offset: u32,
    size: u32,
    kptr: BtfFieldKptr,
    graph_root: BtfFieldGraphRoot,
}

impl BtfField {
    const BPF_SPIN_LOCK: usize = 1 << 0;
    const BPF_TIMER: usize = 1 << 1;
    const BPF_KPTR_UNREF: usize = 1 << 2;
    const BPF_KPTR_REF: usize = 1 << 3;
    const BPF_KPTR_PERCPU: usize = 1 << 4;
    const BPF_KPTR: usize =
        BtfField::BPF_KPTR_UNREF | BtfField::BPF_KPTR_REF | BtfField::BPF_KPTR_PERCPU;
    const BPF_LIST_HEAD: usize = 1 << 5;
    const BPF_LIST_NODE: usize = 1 << 6;
    const BPF_RB_ROOT: usize = 1 << 7;
    const BPF_RB_NODE: usize = 1 << 8;
    const BPF_GRAPH_NODE: usize = BtfField::BPF_RB_NODE | BtfField::BPF_LIST_NODE;
    const BPF_GRAPH_ROOT: usize = BtfField::BPF_RB_ROOT | BtfField::BPF_LIST_HEAD;
    const BPF_REFCOUNT: usize = 1 << 9;
}

impl BpfMap {
    const BPF_MAP_TYPE_UNSPEC: usize = 0;
    const BPF_MAP_TYPE_HASH: usize = 1;
    const BPF_MAP_TYPE_ARRAY: usize = 2;
    const BPF_MAP_TYPE_PROG_ARRAY: usize = 3;
    const BPF_MAP_TYPE_PERF_EVENT_ARRAY: usize = 4;
    const BPF_MAP_TYPE_PERCPU_HASH: usize = 5;
    const BPF_MAP_TYPE_PERCPU_ARRAY: usize = 6;
    const BPF_MAP_TYPE_STACK_TRACE: usize = 7;
    const BPF_MAP_TYPE_CGROUP_ARRAY: usize = 8;
    const BPF_MAP_TYPE_LRU_HASH: usize = 9;
    const BPF_MAP_TYPE_LRU_PERCPU_HASH: usize = 10;
    const BPF_MAP_TYPE_LPM_TRIE: usize = 11;
    const BPF_MAP_TYPE_ARRAY_OF_MAPS: usize = 12;
    const BPF_MAP_TYPE_HASH_OF_MAPS: usize = 13;
    const BPF_MAP_TYPE_DEVMAP: usize = 14;
    const BPF_MAP_TYPE_SOCKMAP: usize = 15;
    const BPF_MAP_TYPE_CPUMAP: usize = 16;
    const BPF_MAP_TYPE_XSKMAP: usize = 17;
    const BPF_MAP_TYPE_SOCKHASH: usize = 18;
    const BPF_MAP_TYPE_CGROUP_STORAGE_DEPRECATED: usize = 19;
    /// BPF_MAP_TYPE_CGROUP_STORAGE is available to bpf programs attaching
    /// to a cgroup. The newer BPF_MAP_TYPE_CGRP_STORAGE is available to
    /// both cgroup-attached and other progs and supports all functionality
    /// provided by BPF_MAP_TYPE_CGROUP_STORAGE. So mark
    /// BPF_MAP_TYPE_CGROUP_STORAGE deprecated.
    const BPF_MAP_TYPE_CGROUP_STORAGE: usize = BpfMap::BPF_MAP_TYPE_CGROUP_STORAGE_DEPRECATED;
    const BPF_MAP_TYPE_REUSEPORT_SOCKARRAY: usize = 20;
    const BPF_MAP_TYPE_PERCPU_CGROUP_STORAGE_DEPRECATED: usize = 21;
    /// BPF_MAP_TYPE_PERCPU_CGROUP_STORAGE is available to bpf programs
    /// attaching to a cgroup. The new mechanism (BPF_MAP_TYPE_CGRP_STORAGE +
    /// local percpu kptr) supports all BPF_MAP_TYPE_PERCPU_CGROUP_STORAGE
    /// functionality and more. So mark * BPF_MAP_TYPE_PERCPU_CGROUP_STORAGE
    /// deprecated.
    const BPF_MAP_TYPE_PERCPU_CGROUP_STORAGE: usize =
        BpfMap::BPF_MAP_TYPE_PERCPU_CGROUP_STORAGE_DEPRECATED;
    const BPF_MAP_TYPE_QUEUE: usize = 22;
    const BPF_MAP_TYPE_STACK: usize = 23;
    const BPF_MAP_TYPE_SK_STORAGE: usize = 24;
    const BPF_MAP_TYPE_DEVMAP_HASH: usize = 25;
    const BPF_MAP_TYPE_STRUCT_OPS: usize = 26;
    const BPF_MAP_TYPE_RINGBUF: usize = 27;
    const BPF_MAP_TYPE_INODE_STORAGE: usize = 28;
    const BPF_MAP_TYPE_TASK_STORAGE: usize = 29;
    const BPF_MAP_TYPE_BLOOM_FILTER: usize = 30;
    const BPF_MAP_TYPE_USER_RINGBUF: usize = 31;
    const BPF_MAP_TYPE_CGRP_STORAGE: usize = 32;
    /// Note that tracing related programs such as
    /// BPF_PROG_TYPE_{KPROBE,TRACEPOINT,PERF_EVENT,RAW_TRACEPOINT}
    /// are not subject to a stable API since kernel internal data
    /// structures can change from release to release and may
    /// therefore break existing tracing BPF programs. Tracing BPF
    /// programs correspond to /a/ specific kernel which is to be
    /// analyzed, and not /a/ specific kernel /and/ all future ones.
    const BPF_PROG_TYPE_UNSPEC: usize = 0;
    const BPF_PROG_TYPE_SOCKET_FILTER: usize = 1;
    const BPF_PROG_TYPE_KPROBE: usize = 2;
    const BPF_PROG_TYPE_SCHED_CLS: usize = 3;
    const BPF_PROG_TYPE_SCHED_ACT: usize = 4;
    const BPF_PROG_TYPE_TRACEPOINT: usize = 5;
    const BPF_PROG_TYPE_XDP: usize = 6;
    const BPF_PROG_TYPE_PERF_EVENT: usize = 7;
    const BPF_PROG_TYPE_CGROUP_SKB: usize = 8;
    const BPF_PROG_TYPE_CGROUP_SOCK: usize = 9;
    const BPF_PROG_TYPE_LWT_IN: usize = 10;
    const BPF_PROG_TYPE_LWT_OUT: usize = 11;
    const BPF_PROG_TYPE_LWT_XMIT: usize = 12;
    const BPF_PROG_TYPE_SOCK_OPS: usize = 13;
    const BPF_PROG_TYPE_SK_SKB: usize = 14;
    const BPF_PROG_TYPE_CGROUP_DEVICE: usize = 15;
    const BPF_PROG_TYPE_SK_MSG: usize = 16;
    const BPF_PROG_TYPE_RAW_TRACEPOINT: usize = 17;
    const BPF_PROG_TYPE_CGROUP_SOCK_ADDR: usize = 18;
    const BPF_PROG_TYPE_LWT_SEG6LOCAL: usize = 19;
    const BPF_PROG_TYPE_LIRC_MODE2: usize = 20;
    const BPF_PROG_TYPE_SK_REUSEPORT: usize = 21;
    const BPF_PROG_TYPE_FLOW_DISSECTOR: usize = 22;
    const BPF_PROG_TYPE_CGROUP_SYSCTL: usize = 23;
    const BPF_PROG_TYPE_RAW_TRACEPOINT_WRITABLE: usize = 24;
    const BPF_PROG_TYPE_CGROUP_SOCKOPT: usize = 25;
    const BPF_PROG_TYPE_TRACING: usize = 26;
    const BPF_PROG_TYPE_STRUCT_OPS: usize = 27;
    const BPF_PROG_TYPE_EXT: usize = 28;
    const BPF_PROG_TYPE_LSM: usize = 29;
    const BPF_PROG_TYPE_SK_LOOKUP: usize = 30;
    /// a program that can execute syscalls
    const BPF_PROG_TYPE_SYSCALL: usize = 31;
    const BPF_PROG_TYPE_NETFILTER: usize = 32;
}
