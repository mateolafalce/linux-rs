/**************************************************************************
 *   rbtree_types.rs  --  This file is part of linux-rs.                  *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

/// Rb Node for opts
pub struct RbNode {
    __rb_parent_color: u64,
    rb_right: *mut RbNode,
    rb_left: *mut RbNode,
}

pub struct RbRoot {
    rb_node: *mut RbNode,
}

/// Leftmost-cached rbtrees.
///
/// We do not cache the rightmost node based on footprint
/// size vs number of potential users that could benefit
/// from O(1) rb_last(). Just not worth it, users that want
/// this feature can always implement the logic explicitly.
/// Furthermore, users that want to cache both pointers may
/// find it a bit asymmetric, but that's ok.
pub struct RbRootCached {
    rb_root: RbRoot,
    rb_leftmost: *mut RbNode,
}
