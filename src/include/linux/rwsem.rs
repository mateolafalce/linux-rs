/**************************************************************************
 *   rwsem.rs  --  This file is part of linux-rs.                         *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{
    spinlock_types_raw::RawSpinlockT,
    types::{AtomicLongT, ListHead},
};

#[cfg(feature = "CONFIG_DEBUG_LOCK_ALLOC")]
use crate::include::linux::lockdep_types::LockdepMap;

#[cfg(feature = "CONFIG_RWSEM_SPIN_ON_OWNER")]
use crate::include::linux::osq_lock::OptimisticSpinQueue;

/// For an uncontended rwsem, count and owner are the only fields a task
/// needs to touch when acquiring the rwsem. So they are put next to each
/// other to increase the chance that they will share the same cacheline.
///
/// In a contended rwsem, the owner is likely the most frequently accessed
/// field in the structure as the optimistic waiter that holds the osq lock
/// will spin on owner. For an embedded rwsem, other hot fields in the
/// containing structure should be moved further away from the rwsem to
/// reduce the chance that they will share the same cacheline causing
/// cacheline bouncing problem.
#[cfg(not(feature = "CONFIG_PREEMPT_RT"))]
pub struct RwSemaphore {
    count: AtomicLongT,
    /// Write owner or one of the read owners as well flags regarding
    /// the current state of the rwsem. Can be used as a speculative
    /// check to see if the write owner is running on the cpu.
    owner: AtomicLongT,
    #[cfg(feature = "CONFIG_RWSEM_SPIN_ON_OWNER")]
    /// spinner MCS lock
    osq: OptimisticSpinQueue,
    wait_lock: RawSpinlockT,
    wait_list: ListHead,
    #[cfg(feature = "CONFIG_DEBUG_RWSEMS")]
    magic: *mut u32,
    #[cfg(feature = "CONFIG_DEBUG_LOCK_ALLOC")]
    dep_map: LockdepMap,
}

#[cfg(feature = "CONFIG_PREEMPT_RT")]
pub struct RwSemaphore {
    rwbase: RwbaseRt,
    #[cfg(feature = "CONFIG_DEBUG_LOCK_ALLOC")]
    dep_map: LockdepMap,
}
