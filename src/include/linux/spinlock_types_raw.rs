/**************************************************************************
 *   spinlock_types_raw.rs  --  This file is part of linux-rs.            *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

#[cfg(feature = "CONFIG_DEBUG_LOCK_ALLOC")]
use crate::include::linux::lockdep_types::LockdepMap;
use crate::include::linux::spinlock_types_up::ArchSpinlockT;

pub type RawSpinlock = RawSpinlockT;

pub struct RawSpinlockT {
    raw_lock: ArchSpinlockT,
    #[cfg(feature = "CONFIG_DEBUG_SPINLOCK")]
    config_debug_spinlock: ConfigDebugSpinlock,
    #[cfg(feature = "CONFIG_DEBUG_SPINLOCK")]
    owner: *mut u32,
    #[cfg(feature = "CONFIG_DEBUG_LOCK_ALLOC")]
    dep_map: LockdepMap,
}

#[cfg(feature = "CONFIG_DEBUG_SPINLOCK")]
struct ConfigDebugSpinlock {
    magic: u64,
    owner_cpu: u64,
    owner: *mut u32,
}
