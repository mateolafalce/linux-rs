/**************************************************************************
 *   netdevice.rs  --  This file is part of linux-rs.                     *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::{
    drivers::net::phy::sfp_bus::SfpBus,
    include::{
        linux::{
            device::Device,
            hrtimer::Hrtimer,
            inetdevice::InDevice,
            lockdep_types::LockClassKey,
            netdev_features::NetdevFeaturesT,
            phy::PhyDevice,
            rbtree_types::RbRoot,
            ref_tracker::RefTrackerDir,
            refcount::RefCountT,
            sched::TaskStruct,
            skbuff::SkBuff,
            spinlock_types::SpinlockT,
            sysfs::AttributeGroup,
            timer::TimerList,
            types::{AtomicLongT, AtomicT, HlistNode, ListHead, RcuHead},
            u64_stats_sync::{U64StatsSync, U64StatsT},
        },
        net::{
            devlink::DevlinkPort, if_inet6::Inet6Dev, net_namespace::PossibleNetT,
            net_trackers::NetdeviceTracker, netdev_rx_queue::NetdevRxQueue, sch_generic::Qdisc,
        },
        uapi::linux::{if_::IFNAMSIZ, if_link::RtnlHwStats64, netdevice::MAX_ADDR_LEN},
    },
    kernel::bpf::{devmap::XdpDevBulkQueue, preload::iterators::iterators_bpf::BpfProg},
    net::{
        core::dev::{BpfXdpLink, NetdevNameNode},
        ipv4::udp_tunnel_nic::UdpTunnelNic,
    },
};

/// size of gro hash buckets, must less than bit number of
/// napi_struct::gro_bitmask
const GRO_HASH_BUCKETS: usize = 8;
const TC_MAX_QUEUE: usize = 16;
const XDP_MODE_SKB: usize = 0;
const XDP_MODE_DRV: usize = 1;
const XDP_MODE_HW: usize = 2;
const __MAX_XDP_MODE: usize = 3;
const MAX_PHYS_ITEM_ID_LEN: usize = 32;

/// Structure for NAPI scheduling similar to tasklet but with weighting
pub struct NapiStruct {
    /// The poll_list must only be managed by the entity which
    /// changes the state of the NAPI_STATE_SCHED bit.  This means
    /// whoever atomically sets that bit can add this napi_struct
    /// to the per-CPU poll_list, and whoever clears that bit
    /// can remove from the list right before clearing the bit.
    poll_list: ListHead,
    state: u64,
    weight: i32,
    defer_hard_irqs_count: i32,
    gro_bitmask: u64,
    // int	(*poll)(struct napi_struct *, int); TODO
    /// CPU actively polling if netpoll is configured
    #[cfg(feature = "CONFIG_NETPOLL")]
    poll_owner: i32,
    /// CPU on which NAPI has been scheduled for processing
    list_owner: i32,
    dev: *mut NetDevice,
    gro_hash: [GroList; GRO_HASH_BUCKETS],
    skb: *mut SkBuff,
    /// Pending GRO_NORMAL skbs
    rx_list: ListHead,
    /// length of rx_list
    rx_count: i32,
    napi_id: u32,
    timer: Hrtimer,
    thread: *mut TaskStruct,
    /// control-path-only fields follow
    dev_list: ListHead,
    napi_hash_node: HlistNode,
}

/// struct net_device - The DEVICE structure.
///
/// Actually, this whole structure is a big mistake.  It mixes I/O
/// data with strictly "high-level" data, and it has to know about
/// almost every data structure used in the INET module.
///
/// @name:    This is the first field of the "visible" part of this structure
///     (i.e. as seen by users in the "Space.c" file).  It is the name
///     of the interface.
///
/// @name_node:    Name hashlist node
/// @ifalias:    SNMP alias
/// @mem_end:    Shared memory end
/// @mem_start:    Shared memory start
/// @base_addr:    Device I/O address
/// @irq:        Device IRQ number
///
/// @state:        Generic network queuing layer state, see netdev_state_t
/// @dev_list:    The global list of network devices
/// @napi_list:    List entry used for polling NAPI devices
/// @unreg_list:    List entry  when we are unregistering the
///        device; see the function unregister_netdev
/// @close_list:    List entry used when we are closing the device
/// @ptype_all:     Device-specific packet handlers for all protocols
/// @ptype_specific: Device-specific, protocol-specific packet handlers
///
/// @adj_list:    Directly linked devices, like slaves for bonding
/// @features:    Currently active device features
/// @hw_features:    User-changeable features
///
/// @wanted_features:    User-requested features
/// @vlan_features:        Mask of features inheritable by VLAN devices
///
/// @hw_enc_features:    Mask of features inherited by encapsulating devices
///        This field indicates what encapsulation
///        offloads the hardware is capable of doing,
///        and drivers will need to set them appropriately.
///
/// @mpls_features:    Mask of features inheritable by MPLS
/// @gso_partial_features: value(s) from NETIF_F_GSO\*
///
/// @ifindex:    interface index
/// @group:        The group the device belongs to
///
/// @stats:        Statistics struct, which was left as a legacy, use
///        rtnl_link_stats64 instead
///
/// @core_stats:    core networking counters,
///        do not use this in drivers
/// @carrier_up_count:    Number of times the carrier has been up
/// @carrier_down_count:    Number of times the carrier has been down
///
/// @wireless_handlers:    List of functions to handle Wireless Extensions,
///            instead of ioctl,
///            see <net/iw_handler.h> for details.
/// @wireless_data:    Instance data managed by the core of wireless extensions
///
/// @netdev_ops:    Includes several pointers to callbacks,
///        if one wants to override the ndo_*() functions
/// @xdp_metadata_ops:    Includes pointers to XDP metadata callbacks.
/// @xsk_tx_metadata_ops:    Includes pointers to AF_XDP TX metadata callbacks.
/// @ethtool_ops:    Management operations
/// @l3mdev_ops:    Layer 3 master device operations
/// @ndisc_ops:    Includes callbacks for different IPv6 neighbour
///        discovery handling. Necessary for e.g. 6LoWPAN.
/// @xfrmdev_ops:    Transformation offload operations
/// @tlsdev_ops:    Transport Layer Security offload operations
/// @header_ops:    Includes callbacks for creating,parsing,caching,etc
///        of Layer 2 headers.
///
/// @flags:        Interface flags (a la BSD)
/// @xdp_features:    XDP capability supported by the device
/// @priv_flags:    Like 'flags' but invisible to userspace,
///        see if.h for the definitions
/// @gflags:    Global flags ( kept as legacy )
/// @padded:    How much padding added by alloc_netdev()
/// @operstate:    RFC2863 operstate
/// @link_mode:    Mapping policy to operstate
/// @if_port:    Selectable AUI, TP, ...
/// @dma:        DMA channel
/// @mtu:        Interface MTU value
/// @min_mtu:    Interface Minimum MTU value
/// @max_mtu:    Interface Maximum MTU value
/// @type:        Interface hardware type
/// @hard_header_len: Maximum hardware header length.
/// @min_header_len:  Minimum hardware header length
///
/// @needed_headroom: Extra headroom the hardware may need, but not in all
///           cases can this be guaranteed
/// @needed_tailroom: Extra tailroom the hardware may need, but not in all
///           cases can this be guaranteed. Some cases also use
///           LL_MAX_HEADER instead to allocate the skb
///
/// interface address info:
///
///     @perm_addr:     Permanent hw address
///     @addr_assign_type:  Hw address assignment type
///     @addr_len:      Hardware address length
/// @upper_level:       Maximum depth level of upper devices.
/// @lower_level:       Maximum depth level of lower devices.
/// @neigh_priv_len:    Used in neigh_alloc()
///     @dev_id:        Used to differentiate devices that share
///                 the same link layer address
///     @dev_port:      Used to differentiate devices that share
///                 the same function
/// @addr_list_lock:    XXX: need comments on this one
/// @name_assign_type:  network interface name assignment type
/// @uc_promisc:        Counter that indicates promiscuous mode
///             has been enabled due to the need to listen to
///             additional unicast addresses in a device that
///             does not implement ndo_set_rx_mode()
/// @uc:            unicast mac addresses
/// @mc:            multicast mac addresses
/// @dev_addrs:     list of device hw addresses
/// @queues_kset:       Group of all Kobjects in the Tx and RX queues
/// @promiscuity:       Number of times the NIC is told to work in
///             promiscuous mode; if it becomes 0 the NIC will
///             exit promiscuous mode
/// @allmulti:      Counter, enables or disables allmulticast mode
///
/// @vlan_info: VLAN info
/// @dsa_ptr:   dsa specific data
/// @tipc_ptr:  TIPC specific data
/// @atalk_ptr: AppleTalk link
/// @ip_ptr:    IPv4 specific data
/// @ip6_ptr:   IPv6 specific data
/// @ax25_ptr:  AX.25 specific data
/// @ieee80211_ptr: IEEE 802.11 specific data, assign before registering
/// @ieee802154_ptr: IEEE 802.15.4 low-rate Wireless Personal Area Network
///          device struct
/// @mpls_ptr:  mpls_dev struct pointer
/// @mctp_ptr:  MCTP specific data
///
/// @dev_addr:  Hw address (before bcast,
///         because most packets are unicast)
///
/// @_rx:           Array of RX queues
/// @num_rx_queues:     Number of RX queues
///             allocated at register_netdev() time
/// @real_num_rx_queues:    Number of RX queues currently active in device
/// @xdp_prog:      XDP sockets filter program pointer
/// @gro_flush_timeout: timeout for GRO layer in NAPI
/// @napi_defer_hard_irqs:  If not zero, provides a counter that would
///             allow to avoid NIC hard IRQ, on busy queues.
///
/// @rx_handler:        handler for received packets
/// @rx_handler_data:   XXX: need comments on this one
/// @tcx_ingress:       BPF & clsact qdisc specific data for ingress processing
/// @ingress_queue:     XXX: need comments on this one
/// @nf_hooks_ingress:  netfilter hooks executed for ingress packets
/// @broadcast:     hw bcast address
///
/// @rx_cpu_rmap:   CPU reverse-mapping for RX completion interrupts,
///         indexed by RX queue number. Assigned by driver.
///         This must only be set if the ndo_rx_flow_steer
///         operation is defined
/// @index_hlist:       Device index hash chain
///
/// @_tx:           Array of TX queues
/// @num_tx_queues:     Number of TX queues allocated at alloc_netdev_mq() time
/// @real_num_tx_queues:    Number of TX queues currently active in device
/// @qdisc:         Root qdisc from userspace point of view
/// @tx_queue_len:      Max frames per queue allowed
/// @tx_global_lock:    XXX: need comments on this one
/// @xdp_bulkq:     XDP device bulk queue
/// @xps_maps:      all CPUs/RXQs maps
/// @xps_maps:  XXX: need comments on this one
/// @tcx_egress:        BPF & clsact qdisc specific data for egress processing
/// @nf_hooks_egress:   netfilter hooks executed for egress packets
/// @qdisc_hash:        qdisc hash table
/// @watchdog_timeo:    Represents the timeout that is used by
///             the watchdog (see dev_watchdog())
/// @watchdog_timer:    List of timers
///
/// @proto_down_reason: reason a netdev interface is held down
/// @pcpu_refcnt:       Number of references to this device
/// @dev_refcnt:        Number of references to this device
/// @refcnt_tracker:    Tracker directory for tracked references to this device
/// @todo_list:     Delayed register/unregister
/// @link_watch_list:   XXX: need comments on this one
///
/// @reg_state:     Register/unregister state machine
/// @dismantle:     Device is going to be freed
/// @rtnl_link_state:   This enum represents the phases of creating
///             a new link
///
/// @needs_free_netdev: Should unregister perform free_netdev?
/// @priv_destructor:   Called from unregister
/// @npinfo:        XXX: need comments on this one
///     @nd_net:        Network namespace this network device is inside
///
///     @ml_priv:   Mid-layer private
/// @ml_priv_type:  Mid-layer private type
///
/// @pcpu_stat_type:    Type of device statistics which the core should
///             allocate/free: none, lstats, tstats, dstats. none
///             means the driver is handling statistics allocation/
///             freeing internally.
/// @lstats:        Loopback statistics: packets, bytes
/// @tstats:        Tunnel statistics: RX/TX packets, RX/TX bytes
/// @dstats:        Dummy statistics: RX/TX/drop packets, RX/TX bytes
///
/// @garp_port: GARP
/// @mrp_port:  MRP
///
/// @dm_private:    Drop monitor private
///
/// @dev:       Class/net/name entry
/// @sysfs_groups:  Space for optional device, statistics and wireless
///         sysfs groups
///
/// @sysfs_rx_queue_group:  Space for optional per-rx queue attributes
/// @rtnl_link_ops: Rtnl_link_ops
///
/// @gso_max_size:  Maximum size of generic segmentation offload
/// @tso_max_size:  Device (as in HW) limit on the max TSO request size
/// @gso_max_segs:  Maximum number of segments that can be passed to the
///         NIC for GSO
/// @tso_max_segs:  Device (as in HW) limit on the max TSO segment count
///     @gso_ipv4_max_size: Maximum size of generic segmentation offload,
///                 for IPv4.
///
/// @dcbnl_ops: Data Center Bridging netlink ops
/// @num_tc:    Number of traffic classes in the net device
/// @tc_to_txq: XXX: need comments on this one
/// @prio_tc_map:   XXX: need comments on this one
///
/// @fcoe_ddp_xid:  Max exchange id for FCoE LRO by ddp
///
/// @priomap:   XXX: need comments on this one
/// @phydev:    Physical device may attach itself
///         for hardware timestamping
/// @sfp_bus:   attached &struct sfp_bus structure.
///
/// @qdisc_tx_busylock: lockdep class annotating Qdisc->busylock spinlock
///
/// @proto_down:    protocol port state information can be sent to the
///         switch driver and used to set the phys state of the
///         switch port.
///
/// @wol_enabled:   Wake-on-LAN is enabled
///
/// @threaded:  napi threaded mode is enabled
///
/// @net_notifier_list: List of per-net netdev notifier block
///             that follow this device when it is moved
///             to another network namespace.
///
/// @macsec_ops:    MACsec offloading ops
///
/// @udp_tunnel_nic_info:   static structure describing the UDP tunnel
///             offload capabilities of the device
/// @udp_tunnel_nic:    UDP tunnel offload state
/// @xdp_state:     stores info on attached XDP BPF programs
///
/// @nested_level:  Used as a parameter of spin_lock_nested() of
///         dev->addr_list_lock.
/// @unlink_list:   As netif_addr_lock() can be called recursively,
///         keep a list of interfaces
/// @gro_max_size:  Maximum size of aggregated packet in generic
///         receive offload (GRO)
///     @gro_ipv4_max_size: Maximum size of aggregated packet in generic
///                 receive offload (GRO), for IPv4.
/// @xdp_zc_max_segs:   Maximum number of segments supported by AF_XDP
///             zero copy driver
///
/// @dev_addr_shadow:   Copy of @dev_addr to catch direct writes.
/// @linkwatch_dev_tracker: refcount tracker used by linkwatch.
/// @watchdog_dev_tracker:  refcount tracker used by watchdog.
/// @dev_registered_tracker:    tracker for reference held while
///                 registered
/// @offload_xstats_l3: L3 HW stats for this netdevice.
///
/// @devlink_port:  Pointer to related devlink port structure.
///         Assigned by a driver before netdev registration using
///         SET_NETDEV_DEVLINK_PORT macro. This pointer is static
///         during the time netdevice is registered.
///
/// @dpll_pin: Pointer to the SyncE source pin of a DPLL subsystem,
///        where the clock is recovered.
pub struct NetDevice {
    /// Cacheline organization can be found documented in
    /// Documentation/networking/net_cachelines/net_device.rst.
    /// Please update the document when adding new fields.
    /// TX read-mostly hotpath
    priv_flags: usize,
    gso_partial_features: NetdevFeaturesT,
    real_num_tx_queues: u32,
    gso_max_size: u32,
    gso_ipv4_max_size: u32,
    gso_max_segs: u16,
    num_tc: i16,
    /// Note : dev->mtu is often read without holding a lock.
    /// Writers usually hold RTNL.
    /// It is recommended to use READ_ONCE() to annotate the reads,
    /// and to use WRITE_ONCE() to annotate the writes.
    mtu: u32,
    needed_headroom: u16,
    tc_to_txq: [NetdevTcTxq; TC_MAX_QUEUE],
    #[cfg(feature = "CONFIG_XPS")]
    xps_maps: [*mut XpsDevMaps; XPS_MAPS_MAX],
    #[cfg(feature = "CONFIG_NETFILTER_EGRESS")]
    nf_hooks_egress: NfHookEntries,
    #[cfg(feature = "CONFIG_NET_XGRESS")]
    tcx_egress: *mut BpfMprogEntry,
    /// TXRX read-mostly hotpath
    flags: u32,
    hard_header_len: u16,
    features: NetdevFeaturesT,
    ip6_ptr: *mut Inet6Dev,
    /// RX read-mostly hotpath
    xdp_prog: *mut BpfProg,
    ptype_specific: ListHead,
    ifindex: i32,
    real_num_rx_queues: u32,
    _rx: *mut NetdevRxQueue,
    gro_flush_timeout: u64,
    napi_defer_hard_irqs: i32,
    gro_max_size: u32,
    gro_ipv4_max_size: u32,
    rx_handler_data: *mut u32,
    nd_net: PossibleNetT,
    #[cfg(feature = "CONFIG_NETPOLL")]
    npinfo: *mut NetpollInfo,
    #[cfg(feature = "CONFIG_NET_XGRESS")]
    tcx_ingress: BpfMprogEntry,
    name: [char; IFNAMSIZ],
    name_node: *mut NetdevNameNode,
    ifalias: DevIfalias,
    ///  I/O specific fields
    ///  FIXME: Merge these and struct ifmap into one
    mem_end: u64,
    mem_start: u64,
    base_addr: u64,
    ///  Some hardware also needs these fields (state,dev_list,
    ///  napi_list,unreg_list,close_list) but they are not
    ///  part of the usual set specified in Space.rs.
    state: u64,
    dev_list: ListHead,
    napi_list: ListHead,
    unreg_list: ListHead,
    close_list: ListHead,
    ptype_all: ListHead,
    upper: ListHead,
    lower: ListHead,
    /// Read-mostly cache-line for fast-path access
    xdp_features: u32,
    gflags: u16,
    needed_tailroom: u16,
    hw_features: NetdevFeaturesT,
    wanted_features: NetdevFeaturesT,
    vlan_features: NetdevFeaturesT,
    hw_enc_features: NetdevFeaturesT,
    mpls_features: NetdevFeaturesT,
    min_mtu: u32,
    max_mtu: u32,
    type_: u16,
    min_header_len: u8,
    name_assign_type: u8,
    group: i32,
    // not used by modern drivers
    // LOOK -> stats: NetDeviceStats,
    core_stats: *mut NetDeviceCoreStats,
    /// Stats to monitor link on/off, flapping
    carrier_up_count: AtomicT,
    carrier_down_count: AtomicT,
    #[cfg(feature = "CONFIG_WIRELESS_EXT")]
    // TODO: impl fns wireless_handlers: *const IwHandler_def
    #[cfg(feature = "CONFIG_WIRELESS_EXT")]
    wireless_data: *mut IwPublicData,
    operstate: u8,
    link_mode: u8,
    if_port: u8,
    dma: u8,
    /// Interface address info.
    perm_addr: [u8; MAX_ADDR_LEN],
    addr_assign_type: u8,
    addr_len: u8,
    upper_level: u8,
    lower_level: u8,
    neigh_priv_len: u16,
    dev_id: u16,
    dev_port: u16,
    padded: u16,
    addr_list_lock: SpinlockT,
    irq: i32,
    uc: NetdevHwAddrList,
    mc: NetdevHwAddrList,
    dev_addrs: NetdevHwAddrList,
    #[cfg(feature = "CONFIG_SYSFS")]
    queues_kset: *mut Kset,
    #[cfg(feature = "CONFIG_LOCKDEP")]
    unlink_list: ListHead,
    promiscuity: u32,
    allmulti: u32,
    uc_promisc: bool,
    #[cfg(feature = "CONFIG_LOCKDEP")]
    nested_level: u8,
    /// Protocol-specific pointers
    ip_ptr: *mut InDevice,
    #[cfg(feature = "CONFIG_VLAN_8021Q")]
    vlan_info: VlanInfo,
    #[cfg(feature = "CONFIG_NET_DSA")]
    dsa_ptr: *mut DsaPort,
    #[cfg(feature = "CONFIG_TIPC")]
    tipc_ptr: *mut TipcBearer,
    #[cfg(feature = "CONFIG_ATALK")]
    atalk_ptr: *mut u32,
    #[cfg(feature = "CONFIG_AX25")]
    ax25_ptr: *mut *mut u32,
    #[cfg(feature = "CONFIG_CFG80211")]
    ieee80211_ptr: *mut WirelessDev,
    #[cfg(any(feature = "CONFIG_IEEE802154", feature = "CONFIG_6LOWPAN"))]
    ieee802154_ptr: *mut WpanDev,
    #[cfg(feature = "CONFIG_MPLS_ROUTING")]
    mpls_ptr: *mut MplsDev,
    #[cfg(feature = "CONFIG_MCTP")]
    mctp_ptr: *mut MctpDev,
    /// Cache lines mostly used on receive path (including eth_type_trans())
    /// Interface address info used in eth_type_trans()
    dev_addr: *const u8,
    num_rx_queues: u32,
    xdp_zc_max_segs: u32,
    #[cfg(feature = "CONFIG_NETFILTER_INGRESS")]
    nf_hooks_ingress: NfHookEntries,
    broadcast: [u8; MAX_ADDR_LEN],
    #[cfg(feature = "CONFIG_RFS_ACCEL")]
    rx_cpu_rmap: *mut CpuRmap,
    index_hlist: HlistNode,
    /// Cache lines mostly used on transmit path
    num_tx_queues: u32,
    qdisc: Qdisc,
    tx_queue_len: u32,
    tx_global_lock: SpinlockT,
    xdp_bulkq: XdpDevBulkQueue,
    #[cfg(feature = "CONFIG_NET_SCHED")]
    qdisc_hash: HlistHead,
    /// These may be needed for future network-power-down code.
    watchdog_timer: TimerList,
    watchdog_timeo: i32,
    proto_down_reason: u32,
    todo_list: ListHead,
    #[cfg(feature = "CONFIG_PCPU_DEV_REFCNT")]
    pcpu_refcnt: i32,
    #[cfg(not(feature = "CONFIG_PCPU_DEV_REFCNT"))]
    dev_refcnt: RefCountT,
    refcnt_tracker: RefTrackerDir,
    link_watch_list: ListHead,
    dismantle: bool,
    needs_free_netdev: bool,
    /// mid-layer private
    ml_priv: *mut u32,
    lstats: *mut PcpuLstats,
    tstats: *mut PcpuSwNetstats,
    dstats: *mut PcpuDstats,
    #[cfg(feature = "CONFIG_GARP")]
    garp_port: GarpPort,
    #[cfg(feature = "CONFIG_MRP")]
    mrp_port: *mut MrpPort,
    #[cfg(feature = "CONFIG_NET_DROP_MONITOR")]
    dm_private: DmHwStatDelta,
    dev: Device,
    sysfs_groups: [*const AttributeGroup; 4],
    sysfs_rx_queue_group: *const AttributeGroup,
    tso_max_size: u32,
    tso_max_segs: u16,
    #[cfg(feature = "CONFIG_DCB")]
    //TODO: impl fns const struct dcbnl_rtnl_ops *dcbnl_ops;
    prio_tc_map: [u8; TC_BITMASK + 1],
    #[cfg(feature = "CONFIG_FCOE")]
    fcoe_ddp_xid: u32,
    #[cfg(feature = "CONFIG_CGROUP_NET_PRIO")]
    priomap: *mut NetprioMap,
    phydev: PhyDevice,
    sfp_bus: *mut SfpBus,
    qdisc_tx_busylock: *mut LockClassKey,
    proto_down: bool,
    wol_enabled: bool,
    threaded: bool,
    net_notifier_list: ListHead,
    /// MACsec management functions
    #[cfg(feature = "CONFIG_MACSEC")]
    // TODO impl fns const struct macsec_ops *macsec_ops;
    udp_tunnel_nic_info: *const UdpTunnelNicInfo,
    udp_tunnel_nic: *mut UdpTunnelNic,
    /// protected by rtnl_lock
    xdp_state: [BpfXdpEntity; __MAX_XDP_MODE],
    dev_addr_shadow: [u8; MAX_ADDR_LEN],
    linkwatch_dev_tracker: NetdeviceTracker,
    watchdog_dev_tracker: NetdeviceTracker,
    dev_registered_tracker: NetdeviceTracker,
    offload_xstats_l3: *mut RtnlHwStats64,
    devlink_port: *mut DevlinkPort,
    #[cfg(feature = "CONFIG_DPLL")]
    dpll_pin: *mut DpllPin,
    /// @page_pools: page pools created for this netdevice
    #[cfg(feature = "CONFIG_DPLL")]
    page_pools: HlistHead,
}
/*

struct net_device {
    TODO impl fns
    const struct net_device_ops *netdev_ops;
    const struct header_ops *header_ops;
    rx_handler_func_t __rcu *rx_handler;
    const struct xdp_metadata_ops *xdp_metadata_ops;
    const struct xsk_tx_metadata_ops *xsk_tx_metadata_ops;
    const struct ethtool_ops *ethtool_ops;

#ifdef CONFIG_NET_L3_MASTER_DEV
    const struct l3mdev_ops *l3mdev_ops;
#endif
#if IS_ENABLED(CONFIG_IPV6)
    const struct ndisc_ops *ndisc_ops;
#endif
#ifdef CONFIG_XFRM_OFFLOAD
    const struct xfrmdev_ops *xfrmdev_ops;
#endif
#if IS_ENABLED(CONFIG_TLS_DEVICE)
    const struct tlsdev_ops *tlsdev_ops;
#endif
DECLARE_HASHTABLE(qdisc_hash, 4);

    void (*priv_destructor)(struct net_device *dev);
    const struct rtnl_link_ops *rtnl_link_ops;
*/

struct GroList {
    list: ListHead,
    count: i32,
}

/// HW offloaded queuing disciplines txq count and offset maps
struct NetdevTcTxq {
    count: u16,
    offset: u16,
}

struct DevIfalias {
    rcuhead: RcuHead,
    ifalias: [char; 10],
}

/// per-cpu stats, allocated on demand.
/// Try to fit them in a single cache line, for dev_get_stats() sake.
struct NetDeviceCoreStats {
    rx_dropped: u64,
    tx_dropped: u64,
    rx_nohandler: u64,
    rx_otherhost_dropped: u64,
}

struct NetdevHwAddrList {
    list: ListHead,
    count: i32,
    /// Auxiliary tree for faster lookup on addition and deletion
    tree: RbRoot,
}

struct PcpuLstats {
    packets: U64StatsT,
    bytes: U64StatsT,
    syncp: U64StatsSync,
}

/// often modified stats are per-CPU, other are shared (netdev->stats)
struct PcpuSwNetstats {
    rx_packets: U64StatsT,
    rx_bytes: U64StatsT,
    tx_packets: U64StatsT,
    tx_bytes: U64StatsT,
    syncp: U64StatsSync,
}

struct BpfXdpEntity {
    prog: *mut BpfProg,
    link: *mut BpfXdpLink,
}

struct PcpuDstats {
    rx_packets: u64,
    rx_bytes: u64,
    rx_drops: u64,
    tx_packets: u64,
    tx_bytes: u64,
    tx_drops: u64,
    syncp: U64StatsSync,
}

/// This structure holds a unique identifier to identify some
/// physical item (port for example) used by a netdevice.
pub struct NetdevPhysItemId {
    id: [u8; MAX_PHYS_ITEM_ID_LEN],
    id_len: u8,
}

impl NetDevice {
    const GRO_LEGACY_MAX_SIZE: u32 = 65536;
    /// TCP minimal MSS is 8 (TCP_MIN_GSO_SIZE),
    /// and shinfo->gso_segs is a 16bit field.
    const GRO_MAX_SIZE: u32 = 8 * 65535;
    const NETREG_UNINITIALIZED: u8 = 0;
    /// completed register_netdevice
    const NETREG_REGISTERED: u8 = 1;
    /// called unregister_netdevice
    const NETREG_UNREGISTERING: u8 = 2;
    /// completed unregister todo
    const NETREG_UNREGISTERED: u8 = 3;
    /// called free_netdev
    const NETREG_RELEASED: u8 = 4;
    /// dummy device for NAPI poll
    const NETREG_DUMMY: u8 = 5;
    const RTNL_LINK_INITIALIZED: u8 = 0;
    const RTNL_LINK_INITIALIZING: u8 = 1;
    /// Specifies the type of the struct net_device::ml_priv pointer
    const ML_PRIV_NONE: u8 = 0;
    const ML_PRIV_CAN: u8 = 1;
    const NETDEV_PCPU_STAT_NONE: u8 = 0;
    /// struct pcpu_lstats
    const NETDEV_PCPU_STAT_LSTATS: u8 = 1;
    /// struct pcpu_sw_netstats
    const NETDEV_PCPU_STAT_TSTATS: u8 = 2;
    /// struct pcpu_dstats
    const NETDEV_PCPU_STAT_DSTATS: u8 = 3;
    /// for setting kernel sock attribute on TCP connection setup
    const GSO_MAX_SEGS: u32 = 65535;
    const GSO_LEGACY_MAX_SIZE: u32 = 65536;
    /// TCP minimal MSS is 8 (TCP_MIN_GSO_SIZE),
    /// and shinfo->gso_segs is a 16bit field.
    const GSO_MAX_SIZE: u32 = 8 * NetDevice::GSO_MAX_SEGS;
    const TSO_LEGACY_MAX_SIZE: u32 = 65536;
    const TSO_MAX_SIZE: i16 = i16::MAX; //UINT_MAX
    const TSO_MAX_SEGS: u16 = u16::MAX; //U16_MAX

    const __QUEUE_STATE_DRV_XOFF: usize = 0;
    const __QUEUE_STATE_STACK_XOFF: usize = 1;
    const __QUEUE_STATE_FROZEN: usize = 2;
}

///  __QUEUE_STATE_DRV_XOFF is used by drivers to stop the transmit queue.  The
///  netif_tx_* functions below are used to manipulate this flag.  The
///  __QUEUE_STATE_STACK_XOFF flag is used by the stack to stop the transmit
///  queue independently.  The netif_xmit_*stopped functions below are called
///  to check if the queue has been stopped by the driver or stack (either
///  of the XOFF bits are set in the state).  Drivers should not need to call
///  netif_xmit*stopped functions, they should only be using netif_tx_*.
pub struct NetdevQueue {
    dev: *mut NetDevice,
    dev_tracker: NetdeviceTracker,
    qdisc: *mut Qdisc,
    qdisc_sleeping: *mut Qdisc,
    tx_maxrate: u64,
    trans_timeout: AtomicLongT,
    sb_dev: *mut NetDevice,
    _xmit_lock: SpinlockT,
    xmit_lock_owner: i32,
    trans_start: u64,
    state: u64,
    napi: *mut NapiStruct,
}
