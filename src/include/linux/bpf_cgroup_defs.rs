/**************************************************************************
 *   bpf_cgroup_defs.rs  --  This file is part of linux-rs.               *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{
    bpf::BpfProgArray,
    percpu_refcount::PercpuRef,
    types::{HlistHead, ListHead},
    workqueue::WorkStruct,
};

/// Maximum number of concurrently attachable per-cgroup LSM hooks.
#[cfg(feature = "CONFIG_BPF_LSM")]
const CGROUP_LSM_NUM: usize = 10;

#[cfg(not(feature = "CONFIG_BPF_LSM"))]
const CGROUP_LSM_NUM: usize = 0;

const CGROUP_BPF_ATTACH_TYPE_INVALID: isize = -1;
const CGROUP_INET_INGRESS: usize = 0;
const CGROUP_INET_EGRESS: usize = 1;
const CGROUP_INET_SOCK_CREATE: usize = 2;
const CGROUP_SOCK_OPS: usize = 3;
const CGROUP_DEVICE: usize = 4;
const CGROUP_INET4_BIND: usize = 5;
const CGROUP_INET6_BIND: usize = 6;
const CGROUP_INET4_CONNECT: usize = 7;
const CGROUP_INET6_CONNECT: usize = 8;
const CGROUP_UNIX_CONNECT: usize = 9;
const CGROUP_INET4_POST_BIND: usize = 10;
const CGROUP_INET6_POST_BIND: usize = 11;
const CGROUP_UDP4_SENDMSG: usize = 12;
const CGROUP_UDP6_SENDMSG: usize = 13;
const CGROUP_UNIX_SENDMSG: usize = 14;
const CGROUP_SYSCTL: usize = 15;
const CGROUP_UDP4_RECVMSG: usize = 16;
const CGROUP_UDP6_RECVMSG: usize = 17;
const CGROUP_UNIX_RECVMSG: usize = 18;
const CGROUP_GETSOCKOPT: usize = 19;
const CGROUP_SETSOCKOPT: usize = 20;
const CGROUP_INET4_GETPEERNAME: usize = 21;
const CGROUP_INET6_GETPEERNAME: usize = 22;
const CGROUP_UNIX_GETPEERNAME: usize = 23;
const CGROUP_INET4_GETSOCKNAME: usize = 24;
const CGROUP_INET6_GETSOCKNAME: usize = 25;
const CGROUP_UNIX_GETSOCKNAME: usize = 26;
const CGROUP_INET_SOCK_RELEASE: usize = 27;
const CGROUP_LSM_START: usize = 28;
const CGROUP_LSM_END: usize = CGROUP_LSM_START + CGROUP_LSM_NUM - 1;
const MAX_CGROUP_BPF_ATTACH_TYPE: usize = 29;

pub struct CgroupBpf {
    /// array of effective progs in this cgroup
    #[cfg(feature = "CONFIG_CGROUP_BPF")]
    effective: [*mut BpfProgArray; MAX_CGROUP_BPF_ATTACH_TYPE],
    /// attached progs to this cgroup and attach flags
    /// when flags == 0 or BPF_F_ALLOW_OVERRIDE the progs list will
    /// have either zero or one element
    /// when BPF_F_ALLOW_MULTI the list can have up to BPF_CGROUP_MAX_PROGS
    #[cfg(feature = "CONFIG_CGROUP_BPF")]
    progs: [HlistHead; MAX_CGROUP_BPF_ATTACH_TYPE],
    #[cfg(feature = "CONFIG_CGROUP_BPF")]
    flags: [u8; MAX_CGROUP_BPF_ATTACH_TYPE],
    /// list of cgroup shared storages
    #[cfg(feature = "CONFIG_CGROUP_BPF")]
    storages: ListHead,
    /// temp storage for effective prog array used by prog_attach/detach
    #[cfg(feature = "CONFIG_CGROUP_BPF")]
    inactive: *mut BpfProgArray,
    /// reference counter used to detach bpf programs after cgroup removal
    #[cfg(feature = "CONFIG_CGROUP_BPF")]
    refcnt: PercpuRef,
    /// cgroup_bpf is released using a work queue
    #[cfg(feature = "CONFIG_CGROUP_BPF")]
    release_work: WorkStruct,
}
