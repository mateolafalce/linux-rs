/**************************************************************************
 *   sched.rs  --  This file is part of linux-rs.                         *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

#[cfg(feature = "CONFIG_THREAD_INFO_IN_TASK")]
use crate::arch::x86::include::asm::thread_info::ThreadInfo;
#[cfg(feature = "CONFIG_VIRT_CPU_ACCOUNTING_NATIVE")]
use crate::include::linux::spinlock_types_raw::RawSpinlockT;
#[cfg(feature = "CONFIG_SMP")]
use crate::include::linux::{plist::PlistHead, smp_types::__CallSingleNode};
#[cfg(feature = "CONFIG_FAIR_GROUP_SCHED")]
use crate::kernel::sched::sched::CfsRq;
#[cfg(feature = "CONFIG_RT_GROUP_SCHED")]
use crate::{include::linux::cgroup_defs::CgroupSubsysState, kernel::sched::sched::RtRq};

use crate::include::linux::{
    cpumask::CpumaskT, fs::AddressSpace, hrtimer_types::Hrtimer, mm_types::MmStruct,
    rbtree_types::RbNode, refcount::RefCountT, sched_::prio::MAX_RT_PRIO, types::ListHead,
};

const UTIL_EST_WEIGHT_SHIFT: u8 = 2;
const UTIL_AVG_UNCHANGED: u32 = 0x80000000;
/// I am a kernel thread
pub const PF_KTHREAD: u32 = 0x00200000;

/// The load/runnable/util_avg accumulates an infinite geometric series
/// (see __update_load_avg_cfs_rq() in kernel/sched/pelt.c).
///
/// [load_avg definition]
///
///   load_avg = runnable% * scale_load_down(load)
///
/// [runnable_avg definition]
///
///  runnable_avg = runnable% * SCHED_CAPACITY_SCALE
///
/// [util_avg definition]
///
///   util_avg = running% * SCHED_CAPACITY_SCALE
///
/// where runnable% is the time ratio that a sched_entity is runnable and
/// running% the time ratio that a sched_entity is running.
///
/// For cfs_rq, they are the aggregated values of all runnable and blocked
/// sched_entities.
///
/// The load/runnable/util_avg doesn't directly factor frequency scaling and CPU
/// capacity scaling. The scaling is done through the rq_clock_pelt that is used
/// for computing those signals (see update_rq_clock_pelt())
///
/// N.B., the above ratios (runnable% and running%) themselves are in the
/// range of [0, 1]. To do fixed point arithmetics, we therefore scale them
/// to as large a range as necessary. This is for example reflected by
/// util_avg's SCHED_CAPACITY_SCALE.
///
/// [Overflow issue]
///
/// The 64-bit load_sum can have 4353082796 (=2^64/47742/88761) entities
/// with the highest load (=88761), always runnable on a single cfs_rq,
/// and should not overflow as the number already hits PID_MAX_LIMIT.
///
/// For all other cases (including 32-bit kernels), struct load_weight's
/// weight will overflow first before we do, because:
///
/// Max(load_avg) <= Max(load.weight)
///
/// Then it is the load_weight's responsibility to consider overflow
/// issues.
pub struct SchedAvg {
    last_update_time: u64,
    load_sum: u64,
    runnable_sum: u64,
    util_sum: u32,
    period_contrib: u32,
    load_avg: u64,
    runnable_avg: u64,
    util_avg: u64,
    util_est: UtilEst,
}

/// struct util_est - Estimation utilization of FAIR tasks
/// @enqueued: instantaneous estimated utilization of a task/cpu
/// @ewma:     the Exponential Weighted Moving Average (EWMA)
///            utilization of a task
///
/// Support data structure to track an Exponential Weighted Moving Average
/// (EWMA) of a FAIR task's utilization. New samples are added to the moving
/// average each time a task completes an activation. Sample's weight is chosen
/// so that the EWMA will be relatively insensitive to transient changes to the
/// task's workload.
///
/// The enqueued attribute has a slightly different meaning for tasks and cpus:
/// - task:   the task's util_avg at last task dequeue time
/// - cfs_rq: the sum of util_est.enqueued for each RUNNABLE task on that CPU
/// Thus, the util_est.enqueued of a task represents the contribution on the
/// estimated utilization of the CPU where that task is currently enqueued.
///
/// Only for tasks we track a moving average of the past instantaneous
/// estimated utilization. This allows to absorb sporadic drops in utilization
/// of an otherwise almost periodic task.
///
/// The UTIL_AVG_UNCHANGED flag is used to synchronize util_est with util_avg
/// updates. When a task is dequeued, its util_est should not be updated if its
/// util_avg has not been updated in the meantime.
/// This information is mapped into the MSB bit of util_est.enqueued at dequeue
/// time. Since max value of util_est.enqueued for a task is 1024 (PELT util_avg
/// for a task) it is safe to use MSB.
struct UtilEst {
    enqueued: u32,
    ewma: u32,
}

pub struct SchedEntity {
    /// For load-balancing:
    load: LoadWeight,
    run_node: RbNode,
    deadline: u64,
    min_deadline: u64,

    group_node: ListHead,
    on_rq: u32,

    exec_start: u64,
    sum_exec_runtime: u64,
    prev_sum_exec_runtime: u64,
    vruntime: u64,
    vlag: i64,
    slice: u64,

    nr_migrations: u64,

    #[cfg(feature = "CONFIG_FAIR_GROUP_SCHED")]
    config_fair_group_sched: ConfigFairGroupSched,
    #[cfg(feature = "CONFIG_SMP")]
    config_smp: ConfigSmp,
}

#[cfg(feature = "CONFIG_FAIR_GROUP_SCHED")]
struct ConfigFairGroupSched {
    depth: i32,
    parent: *mut SchedEntity,
    /// rq on which this entity is (to be) queued:
    cfs_rq: *mut CfsRq,
    /// rq "owned" by this entity/group:
    my_q: *mut CfsRq,
    /// cached value of my_q->h_nr_running
    runnable_weight: u64,
}

#[cfg(feature = "CONFIG_SMP")]
struct ConfigSmp {
    /// Per entity load average tracking.
    /// Put into separate cache line so it does not
    /// collide with read-mostly values above.
    avg: SchedAvg,
}

/// Load weight for balancing
pub struct LoadWeight {
    weight: u64,
    inv_weight: u32,
}

/// Struct of a kernel task
pub struct TaskStruct {
    /// For reasons of header soup (see current_thread_info()), this
    /// must be the first element of task_struct.
    #[cfg(feature = "CONFIG_THREAD_INFO_IN_TASK")]
    pub thread_info: ThreadInfo,
    pub __state: u32,
    /// saved state for "spinlock sleepers"
    pub saved_state: u32,
    /// stack pointer
    pub stack: *mut u64,
    /// usage -> Ref Count T
    pub usage: RefCountT,
    /// Per task flags (PF_*), defined further below:
    pub flags: u32,
    pub ptrace: u32,
    #[cfg(feature = "CONFIG_SMP")]
    pub on_cpu: i32,
    #[cfg(feature = "CONFIG_SMP")]
    pub wake_entry: __CallSingleNode,
    #[cfg(feature = "CONFIG_SMP")]
    pub wakee_flips: u32,
    #[cfg(feature = "CONFIG_SMP")]
    pub wakee_flip_decay_ts: u64,
    #[cfg(feature = "CONFIG_SMP")]
    pub last_wakee: *mut TaskStruct,
    /// recent_used_cpu is initially set as the last CPU used by a task
    /// that wakes affine another task. Waker/wakee relationships can
    /// push tasks around a CPU where each wakeup moves to the next one.
    /// Tracking a recently used CPU allows a quick search for a recently
    /// used CPU that may be idle.
    #[cfg(feature = "CONFIG_SMP")]
    pub recent_used_cpu: i32,
    #[cfg(feature = "CONFIG_SMP")]
    pub wake_cpu: i32,
    pub on_rq: i32,
    pub prio: i32,
    pub static_prio: i32,
    pub normal_prio: i32,
    pub rt_priority: u32,
    pub se: SchedEntity,
    pub rt: SchedRtEntity,
    pub dl: SchedDlEntity,
    pub dl_server: *mut SchedDlEntity,
    //TODO: impl fns pub sched_class: *const SchedClass,
    #[cfg(feature = "CONFIG_SCHED_CORE")]
    pub core_node: RbNode,
    #[cfg(feature = "CONFIG_SCHED_CORE")]
    pub core_cookie: u64,
    #[cfg(feature = "CONFIG_SCHED_CORE")]
    pub core_occupation: u32,
    #[cfg(feature = "CONFIG_CGROUP_SCHED")]
    pub sched_task_group: *mut TaskGroup,
    /// Clamp values requested for a scheduling entity.
    /// Must be updated with task_rq_lock() held.
    #[cfg(feature = "CONFIG_UCLAMP_TASK")]
    pub uclamp_req: [UclampSe; UCLAMP_CNT],
    /// Effective clamp values used for a scheduling entity.
    /// Must be updated with task_rq_lock() held.
    #[cfg(feature = "CONFIG_UCLAMP_TASK")]
    pub uclamp: [UclampSe; UCLAMP_CNT],
    pub stats: SchedStatistics,
    /// List of struct preempt_notifier:
    #[cfg(feature = "CONFIG_PREEMPT_NOTIFIERS")]
    pub preempt_notifiers: HlistHead,
    #[cfg(feature = "CONFIG_BLK_DEV_IO_TRACE")]
    pub btrace_seq: u32,
    pub policy: u32,
    pub nr_cpus_allowed: i32,
    pub cpus_ptr: *const CpumaskT,
    pub user_cpus_ptr: *mut CpumaskT,
    pub cpus_mask: *mut CpumaskT,
    pub migration_pending: *mut u32,
    #[cfg(feature = "CONFIG_SMP")]
    pub migration_disabled: u16,
    pub migration_flags: u16,
    #[cfg(feature = "CONFIG_PREEMPT_RCU")]
    pub rcu_read_lock_nesting: i32,
    #[cfg(feature = "CONFIG_PREEMPT_RCU")]
    pub rcu_read_unlock_special: RcuSpecial,
    #[cfg(feature = "CONFIG_PREEMPT_RCU")]
    pub rcu_node_entry: ListHead,
    #[cfg(feature = "CONFIG_PREEMPT_RCU")]
    pub rcu_blocked_node: *mut RcuNode,
    #[cfg(feature = "CONFIG_TASKS_RCU")]
    pub rcu_tasks_nvcsw: u64,
    #[cfg(feature = "CONFIG_TASKS_RCU")]
    pub rcu_tasks_holdout: u8,
    #[cfg(feature = "CONFIG_TASKS_RCU")]
    pub rcu_tasks_idx: u8,
    #[cfg(feature = "CONFIG_TASKS_RCU")]
    pub rcu_tasks_idle_cpu: i32,
    #[cfg(feature = "CONFIG_TASKS_RCU")]
    pub rcu_tasks_holdout_list: ListHead,
    #[cfg(feature = "CONFIG_TASKS_TRACE_RCU")]
    pub trc_reader_nesting: i32,
    #[cfg(feature = "CONFIG_TASKS_TRACE_RCU")]
    pub trc_ipi_to_cpu: i32,
    #[cfg(feature = "CONFIG_TASKS_TRACE_RCU")]
    pub trc_reader_special: RcuSpecial,
    #[cfg(feature = "CONFIG_TASKS_TRACE_RCU")]
    pub trc_holdout_list: ListHead,
    #[cfg(feature = "CONFIG_TASKS_TRACE_RCU")]
    pub trc_blkd_node: ListHead,
    #[cfg(feature = "CONFIG_TASKS_TRACE_RCU")]
    pub trc_blkd_cpu: i32,
    pub sched_info: SchedInfo,
    pub tasks: ListHead,
    #[cfg(feature = "CONFIG_SMP")]
    pushable_tasks: PlistNode,
    #[cfg(feature = "CONFIG_SMP")]
    pushable_dl_tasks: RbNode,
    mm: *mut MmStruct,
    active_mm: *mut MmStruct,
    faults_disabled_mapping: *mut AddressSpace,
}

/*
struct task_struct {
    int             exit_state;
    int             exit_code;
    int             exit_signal;
    /* The signal sent when the parent dies: */
    int             pdeath_signal;
    /* JOBCTL_*, siglock protected: */
    unsigned long           jobctl;

    /* Used for emulating ABI behavior of previous Linux versions: */
    unsigned int            personality;

    /* Scheduler bits, serialized by scheduler locks: */
    unsigned            sched_reset_on_fork:1;
    unsigned            sched_contributes_to_load:1;
    unsigned            sched_migrated:1;

    /* Force alignment to the next boundary: */
    unsigned            :0;

    /* Unserialized, strictly 'current' */

    /*
     * This field must not be in the scheduler word above due to wakelist
     * queueing no longer being serialized by p->on_cpu. However:
     *
     * p->XXX = X;          ttwu()
     * schedule()             if (p->on_rq && ..) // false
     *   smp_mb__after_spinlock();    if (smp_load_acquire(&p->on_cpu) && //true
     *   deactivate_task()            ttwu_queue_wakelist())
     *     p->on_rq = 0;            p->sched_remote_wakeup = Y;
     *
     * guarantees all stores of 'current' are visible before
     * ->sched_remote_wakeup gets used, so it can be in this word.
     */
    unsigned            sched_remote_wakeup:1;
#ifdef CONFIG_RT_MUTEXES
    unsigned            sched_rt_mutex:1;
#endif

    /* Bit to tell LSMs we're in execve(): */
    unsigned            in_execve:1;
    unsigned            in_iowait:1;
#ifndef TIF_RESTORE_SIGMASK
    unsigned            restore_sigmask:1;
#endif
#ifdef CONFIG_MEMCG
    unsigned            in_user_fault:1;
#endif
#ifdef CONFIG_LRU_GEN
    /* whether the LRU algorithm may apply to this access */
    unsigned            in_lru_fault:1;
#endif
#ifdef CONFIG_COMPAT_BRK
    unsigned            brk_randomized:1;
#endif
#ifdef CONFIG_CGROUPS
    /* disallow userland-initiated cgroup migration */
    unsigned            no_cgroup_migration:1;
    /* task is frozen/stopped (used by the cgroup freezer) */
    unsigned            frozen:1;
#endif
#ifdef CONFIG_BLK_CGROUP
    unsigned            use_memdelay:1;
#endif
#ifdef CONFIG_PSI
    /* Stalled due to lack of memory */
    unsigned            in_memstall:1;
#endif
#ifdef CONFIG_PAGE_OWNER
    /* Used by page_owner=on to detect recursion in page tracking. */
    unsigned            in_page_owner:1;
#endif
#ifdef CONFIG_EVENTFD
    /* Recursion prevention for eventfd_signal() */
    unsigned            in_eventfd:1;
#endif
#ifdef CONFIG_IOMMU_SVA
    unsigned            pasid_activated:1;
#endif
#ifdef  CONFIG_CPU_SUP_INTEL
    unsigned            reported_split_lock:1;
#endif
#ifdef CONFIG_TASK_DELAY_ACCT
    /* delay due to memory thrashing */
    unsigned                        in_thrashing:1;
#endif

    unsigned long           atomic_flags; /* Flags requiring atomic access. */

    struct restart_block        restart_block;

    pid_t               pid;
    pid_t               tgid;

#ifdef CONFIG_STACKPROTECTOR
    /* Canary value for the -fstack-protector GCC feature: */
    unsigned long           stack_canary;
#endif
    /*
     * Pointers to the (original) parent process, youngest child, younger sibling,
     * older sibling, respectively.  (p->father can be replaced with
     * p->real_parent->pid)
     */

    /* Real parent process: */
    struct task_struct __rcu    *real_parent;

    /* Recipient of SIGCHLD, wait4() reports: */
    struct task_struct __rcu    *parent;

    /*
     * Children/sibling form the list of natural children:
     */
    struct list_head        children;
    struct list_head        sibling;
    struct task_struct      *group_leader;

    /*
     * 'ptraced' is the list of tasks this task is using ptrace() on.
     *
     * This includes both natural children and PTRACE_ATTACH targets.
     * 'ptrace_entry' is this task's link on the p->parent->ptraced list.
     */
    struct list_head        ptraced;
    struct list_head        ptrace_entry;

    /* PID/PID hash table linkage. */
    struct pid          *thread_pid;
    struct hlist_node       pid_links[PIDTYPE_MAX];
    struct list_head        thread_node;

    struct completion       *vfork_done;

    /* CLONE_CHILD_SETTID: */
    int __user          *set_child_tid;

    /* CLONE_CHILD_CLEARTID: */
    int __user          *clear_child_tid;

    /* PF_KTHREAD | PF_IO_WORKER */
    void                *worker_private;

    u64             utime;
    u64             stime;
#ifdef CONFIG_ARCH_HAS_SCALED_CPUTIME
    u64             utimescaled;
    u64             stimescaled;
#endif
    u64             gtime;
    struct prev_cputime     prev_cputime;
#ifdef CONFIG_VIRT_CPU_ACCOUNTING_GEN
    struct vtime            vtime;
#endif

#ifdef CONFIG_NO_HZ_FULL
    atomic_t            tick_dep_mask;
#endif
    /* Context switch counts: */
    unsigned long           nvcsw;
    unsigned long           nivcsw;

    /* Monotonic time in nsecs: */
    u64             start_time;

    /* Boot based time in nsecs: */
    u64             start_boottime;

    /* MM fault and swap info: this can arguably be seen as either mm-specific or thread-specific: */
    unsigned long           min_flt;
    unsigned long           maj_flt;

    /* Empty if CONFIG_POSIX_CPUTIMERS=n */
    struct posix_cputimers      posix_cputimers;

#ifdef CONFIG_POSIX_CPU_TIMERS_TASK_WORK
    struct posix_cputimers_work posix_cputimers_work;
#endif

    /* Process credentials: */

    /* Tracer's credentials at attach: */
    const struct cred __rcu     *ptracer_cred;

    /* Objective and real subjective task credentials (COW): */
    const struct cred __rcu     *real_cred;

    /* Effective (overridable) subjective task credentials (COW): */
    const struct cred __rcu     *cred;

#ifdef CONFIG_KEYS
    /* Cached requested key. */
    struct key          *cached_requested_key;
#endif

    /*
     * executable name, excluding path.
     *
     * - normally initialized setup_new_exec()
     * - access it with [gs]et_task_comm()
     * - lock it with task_lock()
     */
    char                comm[TASK_COMM_LEN];

    struct nameidata        *nameidata;

#ifdef CONFIG_SYSVIPC
    struct sysv_sem         sysvsem;
    struct sysv_shm         sysvshm;
#endif
#ifdef CONFIG_DETECT_HUNG_TASK
    unsigned long           last_switch_count;
    unsigned long           last_switch_time;
#endif
    /* Filesystem information: */
    struct fs_struct        *fs;

    /* Open file information: */
    struct files_struct     *files;

#ifdef CONFIG_IO_URING
    struct io_uring_task        *io_uring;
#endif

    /* Namespaces: */
    struct nsproxy          *nsproxy;

    /* Signal handlers: */
    struct signal_struct        *signal;
    struct sighand_struct __rcu     *sighand;
    sigset_t            blocked;
    sigset_t            real_blocked;
    /* Restored if set_restore_sigmask() was used: */
    sigset_t            saved_sigmask;
    struct sigpending       pending;
    unsigned long           sas_ss_sp;
    size_t              sas_ss_size;
    unsigned int            sas_ss_flags;

    struct callback_head        *task_works;

#ifdef CONFIG_AUDIT
#ifdef CONFIG_AUDITSYSCALL
    struct audit_context        *audit_context;
#endif
    kuid_t              loginuid;
    unsigned int            sessionid;
#endif
    struct seccomp          seccomp;
    struct syscall_user_dispatch    syscall_dispatch;

    /* Thread group tracking: */
    u64             parent_exec_id;
    u64             self_exec_id;

    /* Protection against (de-)allocation: mm, files, fs, tty, keyrings, mems_allowed, mempolicy: */
    spinlock_t          alloc_lock;

    /* Protection of the PI data structures: */
    raw_spinlock_t          pi_lock;

    struct wake_q_node      wake_q;

#ifdef CONFIG_RT_MUTEXES
    /* PI waiters blocked on a rt_mutex held by this task: */
    struct rb_root_cached       pi_waiters;
    /* Updated under owner's pi_lock and rq lock */
    struct task_struct      *pi_top_task;
    /* Deadlock detection and priority inheritance handling: */
    struct rt_mutex_waiter      *pi_blocked_on;
#endif

#ifdef CONFIG_DEBUG_MUTEXES
    /* Mutex deadlock detection: */
    struct mutex_waiter     *blocked_on;
#endif

#ifdef CONFIG_DEBUG_ATOMIC_SLEEP
    int             non_block_count;
#endif

#ifdef CONFIG_TRACE_IRQFLAGS
    struct irqtrace_events      irqtrace;
    unsigned int            hardirq_threaded;
    u64             hardirq_chain_key;
    int             softirqs_enabled;
    int             softirq_context;
    int             irq_config;
#endif
#ifdef CONFIG_PREEMPT_RT
    int             softirq_disable_cnt;
#endif

#ifdef CONFIG_LOCKDEP
# define MAX_LOCK_DEPTH         48UL
    u64             curr_chain_key;
    int             lockdep_depth;
    unsigned int            lockdep_recursion;
    struct held_lock        held_locks[MAX_LOCK_DEPTH];
#endif

#if defined(CONFIG_UBSAN) && !defined(CONFIG_UBSAN_TRAP)
    unsigned int            in_ubsan;
#endif

    /* Journalling filesystem info: */
    void                *journal_info;

    /* Stacked block device info: */
    struct bio_list         *bio_list;

    /* Stack plugging: */
    struct blk_plug         *plug;

    /* VM state: */
    struct reclaim_state        *reclaim_state;

    struct io_context       *io_context;

#ifdef CONFIG_COMPACTION
    struct capture_control      *capture_control;
#endif
    /* Ptrace state: */
    unsigned long           ptrace_message;
    kernel_siginfo_t        *last_siginfo;

    struct task_io_accounting   ioac;
#ifdef CONFIG_PSI
    /* Pressure stall state */
    unsigned int            psi_flags;
#endif
#ifdef CONFIG_TASK_XACCT
    /* Accumulated RSS usage: */
    u64             acct_rss_mem1;
    /* Accumulated virtual memory usage: */
    u64             acct_vm_mem1;
    /* stime + utime since last update: */
    u64             acct_timexpd;
#endif
#ifdef CONFIG_CPUSETS
    /* Protected by ->alloc_lock: */
    nodemask_t          mems_allowed;
    /* Sequence number to catch updates: */
    seqcount_spinlock_t     mems_allowed_seq;
    int             cpuset_mem_spread_rotor;
    int             cpuset_slab_spread_rotor;
#endif
#ifdef CONFIG_CGROUPS
    /* Control Group info protected by css_set_lock: */
    struct css_set __rcu        *cgroups;
    /* cg_list protected by css_set_lock and tsk->alloc_lock: */
    struct list_head        cg_list;
#endif
#ifdef CONFIG_X86_CPU_RESCTRL
    u32             closid;
    u32             rmid;
#endif
#ifdef CONFIG_FUTEX
    struct robust_list_head __user  *robust_list;
#ifdef CONFIG_COMPAT
    struct compat_robust_list_head __user *compat_robust_list;
#endif
    struct list_head        pi_state_list;
    struct futex_pi_state       *pi_state_cache;
    struct mutex            futex_exit_mutex;
    unsigned int            futex_state;
#endif
#ifdef CONFIG_PERF_EVENTS
    struct perf_event_context   *perf_event_ctxp;
    struct mutex            perf_event_mutex;
    struct list_head        perf_event_list;
#endif
#ifdef CONFIG_DEBUG_PREEMPT
    unsigned long           preempt_disable_ip;
#endif
#ifdef CONFIG_NUMA
    /* Protected by alloc_lock: */
    struct mempolicy        *mempolicy;
    short               il_prev;
    short               pref_node_fork;
#endif
#ifdef CONFIG_NUMA_BALANCING
    int             numa_scan_seq;
    unsigned int            numa_scan_period;
    unsigned int            numa_scan_period_max;
    int             numa_preferred_nid;
    unsigned long           numa_migrate_retry;
    /* Migration stamp: */
    u64             node_stamp;
    u64             last_task_numa_placement;
    u64             last_sum_exec_runtime;
    struct callback_head        numa_work;

    /*
     * This pointer is only modified for current in syscall and
     * pagefault context (and for tasks being destroyed), so it can be read
     * from any of the following contexts:
     *  - RCU read-side critical section
     *  - current->numa_group from everywhere
     *  - task's runqueue locked, task not running
     */
    struct numa_group __rcu     *numa_group;

    /*
     * numa_faults is an array split into four regions:
     * faults_memory, faults_cpu, faults_memory_buffer, faults_cpu_buffer
     * in this precise order.
     *
     * faults_memory: Exponential decaying average of faults on a per-node
     * basis. Scheduling placement decisions are made based on these
     * counts. The values remain static for the duration of a PTE scan.
     * faults_cpu: Track the nodes the process was running on when a NUMA
     * hinting fault was incurred.
     * faults_memory_buffer and faults_cpu_buffer: Record faults per node
     * during the current scan window. When the scan completes, the counts
     * in faults_memory and faults_cpu decay and these values are copied.
     */
    unsigned long           *numa_faults;
    unsigned long           total_numa_faults;

    /*
     * numa_faults_locality tracks if faults recorded during the last
     * scan window were remote/local or failed to migrate. The task scan
     * period is adapted based on the locality of the faults with different
     * weights depending on whether they were shared or private faults
     */
    unsigned long           numa_faults_locality[3];

    unsigned long           numa_pages_migrated;
#endif /* CONFIG_NUMA_BALANCING */

#ifdef CONFIG_RSEQ
    struct rseq __user *rseq;
    u32 rseq_len;
    u32 rseq_sig;
    /*
     * RmW on rseq_event_mask must be performed atomically
     * with respect to preemption.
     */
    unsigned long rseq_event_mask;
#endif

#ifdef CONFIG_SCHED_MM_CID
    int             mm_cid;     /* Current cid in mm */
    int             last_mm_cid;    /* Most recent cid in mm */
    int             migrate_from_cpu;
    int             mm_cid_active;  /* Whether cid bitmap is active */
    struct callback_head        cid_work;
#endif

    struct tlbflush_unmap_batch tlb_ubc;

    /* Cache last used pipe for splice(): */
    struct pipe_inode_info      *splice_pipe;

    struct page_frag        task_frag;

#ifdef CONFIG_TASK_DELAY_ACCT
    struct task_delay_info      *delays;
#endif

#ifdef CONFIG_FAULT_INJECTION
    int             make_it_fail;
    unsigned int            fail_nth;
#endif
    /*
     * When (nr_dirtied >= nr_dirtied_pause), it's time to call
     * balance_dirty_pages() for a dirty throttling pause:
     */
    int             nr_dirtied;
    int             nr_dirtied_pause;
    /* Start of a write-and-pause period: */
    unsigned long           dirty_paused_when;

#ifdef CONFIG_LATENCYTOP
    int             latency_record_count;
    struct latency_record       latency_record[LT_SAVECOUNT];
#endif
    /*
     * Time slack values; these are used to round up poll() and
     * select() etc timeout values. These are in nanoseconds.
     */
    u64             timer_slack_ns;
    u64             default_timer_slack_ns;

#if defined(CONFIG_KASAN_GENERIC) || defined(CONFIG_KASAN_SW_TAGS)
    unsigned int            kasan_depth;
#endif

#ifdef CONFIG_KCSAN
    struct kcsan_ctx        kcsan_ctx;
#ifdef CONFIG_TRACE_IRQFLAGS
    struct irqtrace_events      kcsan_save_irqtrace;
#endif
#ifdef CONFIG_KCSAN_WEAK_MEMORY
    int             kcsan_stack_depth;
#endif
#endif

#ifdef CONFIG_KMSAN
    struct kmsan_ctx        kmsan_ctx;
#endif

#if IS_ENABLED(CONFIG_KUNIT)
    struct kunit            *kunit_test;
#endif

#ifdef CONFIG_FUNCTION_GRAPH_TRACER
    /* Index of current stored address in ret_stack: */
    int             curr_ret_stack;
    int             curr_ret_depth;

    /* Stack of return addresses for return function tracing: */
    struct ftrace_ret_stack     *ret_stack;

    /* Timestamp for last schedule: */
    unsigned long long      ftrace_timestamp;

    /*
     * Number of functions that haven't been traced
     * because of depth overrun:
     */
    atomic_t            trace_overrun;

    /* Pause tracing: */
    atomic_t            tracing_graph_pause;
#endif

#ifdef CONFIG_TRACING
    /* Bitmask and counter of trace recursion: */
    unsigned long           trace_recursion;
#endif /* CONFIG_TRACING */

#ifdef CONFIG_KCOV
    /* See kernel/kcov.c for more details. */

    /* Coverage collection mode enabled for this task (0 if disabled): */
    unsigned int            kcov_mode;

    /* Size of the kcov_area: */
    unsigned int            kcov_size;

    /* Buffer for coverage collection: */
    void                *kcov_area;

    /* KCOV descriptor wired with this task or NULL: */
    struct kcov         *kcov;

    /* KCOV common handle for remote coverage collection: */
    u64             kcov_handle;

    /* KCOV sequence number: */
    int             kcov_sequence;

    /* Collect coverage from softirq context: */
    unsigned int            kcov_softirq;
#endif

#ifdef CONFIG_MEMCG
    struct mem_cgroup       *memcg_in_oom;
    gfp_t               memcg_oom_gfp_mask;
    int             memcg_oom_order;

    /* Number of pages to reclaim on returning to userland: */
    unsigned int            memcg_nr_pages_over_high;

    /* Used by memcontrol for targeted memcg charge: */
    struct mem_cgroup       *active_memcg;
#endif

#ifdef CONFIG_MEMCG_KMEM
    struct obj_cgroup       *objcg;
#endif

#ifdef CONFIG_BLK_CGROUP
    struct gendisk          *throttle_disk;
#endif

#ifdef CONFIG_UPROBES
    struct uprobe_task      *utask;
#endif
#if defined(CONFIG_BCACHE) || defined(CONFIG_BCACHE_MODULE)
    unsigned int            sequential_io;
    unsigned int            sequential_io_avg;
#endif
    struct kmap_ctrl        kmap_ctrl;
#ifdef CONFIG_DEBUG_ATOMIC_SLEEP
    unsigned long           task_state_change;
# ifdef CONFIG_PREEMPT_RT
    unsigned long           saved_state_change;
# endif
#endif
    struct rcu_head         rcu;
    refcount_t          rcu_users;
    int             pagefault_disabled;
#ifdef CONFIG_MMU
    struct task_struct      *oom_reaper_list;
    struct timer_list       oom_reaper_timer;
#endif
#ifdef CONFIG_VMAP_STACK
    struct vm_struct        *stack_vm_area;
#endif
#ifdef CONFIG_THREAD_INFO_IN_TASK
    /* A live task holds one reference: */
    refcount_t          stack_refcount;
#endif
#ifdef CONFIG_LIVEPATCH
    int patch_state;
#endif
#ifdef CONFIG_SECURITY
    /* Used by LSM modules for access restriction: */
    void                *security;
#endif
#ifdef CONFIG_BPF_SYSCALL
    /* Used by BPF task local storage */
    struct bpf_local_storage __rcu  *bpf_storage;
    /* Used for BPF run context */
    struct bpf_run_ctx      *bpf_ctx;
#endif

#ifdef CONFIG_GCC_PLUGIN_STACKLEAK
    unsigned long           lowest_stack;
    unsigned long           prev_lowest_stack;
#endif

#ifdef CONFIG_X86_MCE
    void __user         *mce_vaddr;
    __u64               mce_kflags;
    u64             mce_addr;
    __u64               mce_ripv : 1,
                    mce_whole_page : 1,
                    __mce_reserved : 62;
    struct callback_head        mce_kill_me;
    int             mce_count;
#endif

#ifdef CONFIG_KRETPROBES
    struct llist_head               kretprobe_instances;
#endif
#ifdef CONFIG_RETHOOK
    struct llist_head               rethooks;
#endif

#ifdef CONFIG_ARCH_HAS_PARANOID_L1D_FLUSH
    /*
     * If L1D flush is supported on mm context switch
     * then we use this callback head to queue kill work
     * to kill tasks that are not running on SMT disabled
     * cores
     */
    struct callback_head        l1d_flush_kill;
#endif

#ifdef CONFIG_RV
    /*
     * Per-task RV monitor. Nowadays fixed in RV_PER_TASK_MONITORS.
     * If we find justification for more monitors, we can think
     * about adding more or developing a dynamic method. So far,
     * none of these are justified.
     */
    union rv_task_monitor       rv[RV_PER_TASK_MONITORS];
#endif

#ifdef CONFIG_USER_EVENTS
    struct user_event_mm        *user_event_mm;
#endif

    /*
     * New fields for task_struct should be added above here, so that
     * they are included in the randomized portion of task_struct.
     */
    randomized_struct_fields_end

    /* CPU-specific state of this task: */
    struct thread_struct        thread;

    /*
     * WARNING: on x86, 'thread_struct' contains a variable-sized
     * structure.  It *MUST* be at the end of 'task_struct'.
     *
     * Do not put anything below here!
     */
};
*/

struct SchedRtEntity {
    run_list: ListHead,
    timeout: u64,
    watchdog_stamp: u64,
    time_slice: u32,
    on_rq: u16,
    on_list: u16,

    back: *mut SchedRtEntity,
    #[cfg(feature = "CONFIG_RT_GROUP_SCHED")]
    config_rt_group_sched: ConfigRtGroupSched,
}

#[cfg(feature = "CONFIG_RT_GROUP_SCHED")]
struct ConfigRtGroupSched {
    parent: *mut SchedRtEntity,
    /// rq on which this entity is (to be) queued:
    rt_rq: *mut RtRq,
    /// rq "owned" by this entity/group:
    my_q: *mut RtRq,
}

pub struct HighestPrio {
    curr: i32,
    next: i32,
}

#[cfg(feature = "CONFIG_SMP")]
pub struct RtRqConfigSmp {
    overloaded: i32,
    pushable_tasks: PlistHead,
}

#[cfg(feature = "CONFIG_RT_GROUP_SCHED")]
pub struct RtRqCRGS {
    rt_nr_boosted: u32,
    rq: *mut Rq,
    tg: TaskGroup,
}

/// Task group related information
#[cfg(feature = "CONFIG_RT_GROUP_SCHED")]
struct TaskGroup {
    css: CgroupSubsysState,
}

struct Rq;

/*
struct task_group {
    struct cgroup_subsys_state css;

#ifdef CONFIG_FAIR_GROUP_SCHED
    /* schedulable entities of this group on each CPU */
    struct sched_entity **se;
    /* runqueue "owned" by this group on each CPU */
    struct cfs_rq       **cfs_rq;
    unsigned long       shares;

    /* A positive value indicates that this is a SCHED_IDLE group. */
    int         idle;

#ifdef  CONFIG_SMP
    /*
     * load_avg can be heavily contended at clock tick time, so put
     * it in its own cacheline separated from the fields above which
     * will also be accessed at each tick.
     */
    atomic_long_t       load_avg ____cacheline_aligned;
#endif
#endif

#ifdef CONFIG_RT_GROUP_SCHED
    struct sched_rt_entity  **rt_se;
    struct rt_rq        **rt_rq;

    struct rt_bandwidth rt_bandwidth;
#endif

    struct rcu_head     rcu;
    struct list_head    list;

    struct task_group   *parent;
    struct list_head    siblings;
    struct list_head    children;

#ifdef CONFIG_SCHED_AUTOGROUP
    struct autogroup    *autogroup;
#endif

    struct cfs_bandwidth    cfs_bandwidth;

#ifdef CONFIG_UCLAMP_TASK_GROUP
    /* The two decimal precision [%] value requested from user-space */
    unsigned int        uclamp_pct[UCLAMP_CNT];
    /* Clamp values requested for a task group */
    struct uclamp_se    uclamp_req[UCLAMP_CNT];
    /* Effective clamp values used for a task group */
    struct uclamp_se    uclamp[UCLAMP_CNT];
#endif

};
*/

/// This is the priority-queue data structure of the RT scheduling class:
pub struct RtPrioArray {
    bitmap: [u64; MAX_RT_PRIO as usize + 1],
    queue: [ListHead; MAX_RT_PRIO as usize],
}

/// struct prev_cputime - snapshot of system and user cputime
/// @utime: time spent in user mode
/// @stime: time spent in system mode
/// @lock: protects the above two fields
///
/// Stores previous user/system time values such that we can guarantee
/// monotonicity.
#[cfg(feature = "CONFIG_VIRT_CPU_ACCOUNTING_NATIVE")]
pub struct PrevCputime {
    utime: u64,
    stime: u64,
    lock: RawSpinlockT,
}

#[cfg(not(feature = "CONFIG_VIRT_CPU_ACCOUNTING_NATIVE"))]
pub struct PrevCputime {}

pub struct SchedDlEntity {
    rb_node: RbNode,
    /// Original scheduling parameters. Copied here from sched_attr
    /// during sched_setattr(), they will remain the same until
    /// the next sched_setattr().
    /// Maximum runtime for each instance
    dl_runtime: u64,
    /// Relative deadline of each instance
    dl_deadline: u64,
    /// Separation of two instances (period)
    dl_period: u64,
    /// dl_runtime / dl_period
    dl_bw: u64,
    /// dl_runtime / dl_deadline
    dl_density: u64,
    /// Actual scheduling parameters. Initialized with the values above,
    /// they are continuously updated during task execution. Note that
    /// the remaining runtime could be < 0 in case we are in overrun.
    /// Remaining runtime for this instance
    runtime: i64,
    /// Absolute deadline for this instance
    deadline: u64,
    /// Specifying the scheduler behaviour
    flags: u32,
    /// Some bool flags:
    ///
    /// @dl_throttled tells if we exhausted the runtime. If so, the
    /// task has to wait for a replenishment to be performed at the
    /// next firing of dl_timer.
    ///
    /// @dl_yielded tells if task gave up the CPU before consuming
    /// all its available runtime during the last job.
    ///
    /// @dl_non_contending tells if the task is inactive while still
    /// contributing to the active utilization. In other words, it
    /// indicates if the inactive timer has been armed and its handler
    /// has not been executed yet. This flag is useful to avoid race
    /// conditions between the inactive timer handler and the wakeup
    /// code.
    ///
    /// @dl_overrun tells if the task asked to be informed about runtime
    /// overruns.
    dl_throttled: bool,
    dl_yielded: bool,
    dl_non_contending: bool,
    dl_overrun: bool,
    dl_server: bool,
    /// Bandwidth enforcement timer. Each -deadline task has its
    /// own bandwidth to be enforced, thus we need one timer per task.
    dl_timer: Hrtimer,
    /// Inactive timer, responsible for decreasing the active utilization
    /// at the "0-lag time". When a -deadline task blocks, it contributes
    /// to GRUB's active utilization until the "0-lag time", hence a
    /// timer is needed to decrease the active utilization at the correct
    /// time.
    inactive_timer: Hrtimer,
    /// Bits for DL-server functionality. Also see the comment near
    /// dl_server_update().
    ///
    /// @rq the runqueue this server is for
    ///
    /// @server_has_tasks() returns true if @server_pick return a
    /// runnable task.
    rq: *mut Rq,
    // TODO impl fn typedef bool (*dl_server_has_tasks_f)(struct sched_dl_entity *);
    // server_has_tasks: DlServerHasTasksF,
    // TODO impl fn typedef struct task_struct *(*dl_server_pick_f)(struct sched_dl_entity *);
    // server_pick: DlServerPickF,
    /// Priority Inheritance. When a DEADLINE scheduling entity is boosted
    /// pi_se points to the donor, otherwise points to the dl_se it belongs
    /// to (the original one/itself).
    #[cfg(feature = "CONFIG_RT_MUTEXES")]
    pi_se: *mut SchedDlEntity,
}

struct SchedStatistics {
    #[cfg(feature = "CONFIG_SCHEDSTATS")]
    wait_start: u64,
    #[cfg(feature = "CONFIG_SCHEDSTATS")]
    wait_max: u64,
    #[cfg(feature = "CONFIG_SCHEDSTATS")]
    wait_count: u64,
    #[cfg(feature = "CONFIG_SCHEDSTATS")]
    wait_sum: u64,
    #[cfg(feature = "CONFIG_SCHEDSTATS")]
    iowait_count: u64,
    #[cfg(feature = "CONFIG_SCHEDSTATS")]
    iowait_sum: u64,
    #[cfg(feature = "CONFIG_SCHEDSTATS")]
    sleep_start: u64,
    #[cfg(feature = "CONFIG_SCHEDSTATS")]
    sleep_max: u64,
    #[cfg(feature = "CONFIG_SCHEDSTATS")]
    sum_sleep_runtime: i64,
    #[cfg(feature = "CONFIG_SCHEDSTATS")]
    block_start: u64,
    #[cfg(feature = "CONFIG_SCHEDSTATS")]
    block_max: u64,
    #[cfg(feature = "CONFIG_SCHEDSTATS")]
    sum_block_runtime: i64,
    #[cfg(feature = "CONFIG_SCHEDSTATS")]
    exec_max: i64,
    #[cfg(feature = "CONFIG_SCHEDSTATS")]
    slice_max: u64,
    #[cfg(feature = "CONFIG_SCHEDSTATS")]
    nr_migrations_cold: u64,
    #[cfg(feature = "CONFIG_SCHEDSTATS")]
    nr_failed_migrations_affine: u64,
    #[cfg(feature = "CONFIG_SCHEDSTATS")]
    nr_failed_migrations_running: u64,
    #[cfg(feature = "CONFIG_SCHEDSTATS")]
    nr_failed_migrations_hot: u64,
    #[cfg(feature = "CONFIG_SCHEDSTATS")]
    nr_forced_migrations: u64,
    #[cfg(feature = "CONFIG_SCHEDSTATS")]
    nr_wakeups: u64,
    #[cfg(feature = "CONFIG_SCHEDSTATS")]
    nr_wakeups_sync: u64,
    #[cfg(feature = "CONFIG_SCHEDSTATS")]
    nr_wakeups_migrate: u64,
    #[cfg(feature = "CONFIG_SCHEDSTATS")]
    nr_wakeups_local: u64,
    #[cfg(feature = "CONFIG_SCHEDSTATS")]
    nr_wakeups_remote: u64,
    #[cfg(feature = "CONFIG_SCHEDSTATS")]
    nr_wakeups_affine: u64,
    #[cfg(feature = "CONFIG_SCHEDSTATS")]
    nr_wakeups_affine_attempts: u64,
    #[cfg(feature = "CONFIG_SCHEDSTATS")]
    nr_wakeups_passive: u64,
    #[cfg(feature = "CONFIG_SCHEDSTATS")]
    nr_wakeups_idle: u64,
    #[cfg(all(feature = "CONFIG_SCHEDSTATS", feature = "CONFIG_SCHEDSTATS"))]
    core_forceidle_sum: u64,
}

struct SchedInfo {
    /// Cumulative counters:
    /// # of times we have run on this CPU:
    #[cfg(feature = "CONFIG_SCHED_INFO")]
    pcount: u64,
    /// Time spent waiting on a runqueue:
    #[cfg(feature = "CONFIG_SCHED_INFO")]
    run_delay: u64,
    ///  Timestamps:
    /// When did we last run on a CPU?
    #[cfg(feature = "CONFIG_SCHED_INFO")]
    last_arrival: u64,
    /// When were we last queued to run?
    #[cfg(feature = "CONFIG_SCHED_INFO")]
    last_queued: u64,
}
