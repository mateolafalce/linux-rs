/**************************************************************************
 *   mm_types.rs  --  This file is part of linux-rs.                      *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::{
    arch::x86::include::asm::mmu::MmContextT,
    include::{
        asm_generic::page::PgdT,
        linux::{
            auxvec::AT_VECTOR_SIZE_BASE,
            binfmts::LinuxBinfmt,
            fs::{AddressSpace, File},
            maple_tree::MapleTree,
            memremap::DevPagemap,
            mm_types_task::NR_MM_COUNTERS,
            percpu_counter::PercpuCounter,
            rwsem::RwSemaphore,
            seqlock_types::SeqcountT,
            spinlock_types::SpinlockT,
            types::{Atomic64T, AtomicLongT, AtomicT, ListHead, PgoffT, RcuHead},
            uprobes::UprobesState,
            user_namespace::UserNamespace,
            workqueue::WorkStruct,
        },
        net::page_pool::types::PagePool,
    },
};

const AT_VECTOR_SIZE_ARCH: usize = 0;
const AT_VECTOR_SIZE: usize = 2 * (AT_VECTOR_SIZE_ARCH + AT_VECTOR_SIZE_BASE + 1);

pub struct Page {
    /// Atomic flags, some possibly - updated asynchronously
    flags: u64,
    /// Five words (20/40 bytes) are available in this union.
    /// WARNING: bit 0 of the first word is used for PageTail(). That
    /// means the other users of this union MUST NOT use the bit to
    /// avoid collision and false-positive PageTail().
    lru: ListHead,
    /// Always even, to negate PageTail
    __filler: *mut u32,
    /// Count page's or folio's mlocks
    mlock_count: u32,
    /// Or, free page
    buddy_list: ListHead,
    pcp_list: ListHead,
    /// See page-flags.h for PAGE_MAPPING_FLAGS
    mapping: *mut AddressSpace,
    /// Our offset within mapping.
    index: PgoffT,
    /// share count for fsdax
    share: u64,
    /// @private: Mapping-private opaque data.
    /// Usually used for buffer_heads if PagePrivate.
    /// Used for swp_entry_t if PageSwapCache.
    /// Indicates order in the buddy system if PageBuddy.
    private: u64,
    /// page_pool used by netstack
    /// @pp_magic: magic value to avoid recycling non
    /// page_pool allocated pages.
    pp_magic: u64,
    pp: *mut PagePool,
    _pp_mapping_pad: u64,
    dma_addr: u64,
    pp_frag_count: AtomicLongT,
    /// Tail pages of compound page
    /// Bit zero is set
    compound_head: u64,
    /// ZONE_DEVICE pages
    /// @pgmap: Points to the hosting device page map.
    pgmap: *mut DevPagemap,
    zone_device_data: *mut u32,
    /// ZONE_DEVICE private pages are counted as being
    /// mapped so the next 3 words hold the mapping, index,
    /// and private fields from the source anonymous or
    /// page cache page while the page is migrated to device
    /// private memory.
    /// ZONE_DEVICE MEMORY_DEVICE_FS_DAX pages also
    /// use the mapping, index, and private fields when
    /// pmem backed DAX files are mapped.

    /// @rcu_head: You can use this to free a page by RCU.
    rcu_head: RcuHead,
    /// This union is 4 bytes in size.
    /// If the page can be mapped to userspace, encodes the number
    /// of times this page is referenced by a page table.
    _mapcount: AtomicT,
    /// If the page is neither PageSlab nor mappable to userspace,
    /// the value stored here may help determine what this page
    /// is used for.  See page-flags.h for a list of page types
    /// which are currently stored here.
    page_type: u32,
    /// Usage count. *DO NOT USE DIRECTLY*. See page_ref.rs
    _refcount: AtomicT,
    #[cfg(feature = "CONFIG_MEMCG")]
    memcg_data: u64,
    /// On machines where all RAM is mapped into kernel address space,
    /// we can simply calculate the virtual address. On machines with
    /// highmem some memory is mapped into kernel virtual memory
    /// dynamically, so we need a place to store that address.
    /// Note that this field could be 16 bits on x86 ... ;)
    ///
    /// Architectures with slow multiplication can define
    /// WANT_PAGE_VIRTUAL in asm/page.rs
    /// Kernel virtual address (NULL if not kmapped, ie. highmem)
    #[cfg(feature = "WANT_PAGE_VIRTUAL")]
    virtual_: *mut *mut u32,
    #[cfg(feature = "LAST_CPUPID_NOT_IN_PAGE_FLAGS")]
    _last_cpupid: i32,
    #[cfg(feature = "CONFIG_KMSAN")]
    /// KMSAN metadata for this page:
    ///  - shadow page: every bit indicates whether the corresponding
    ///    bit of the original page is initialized (0) or not (1);
    ///  - origin page: every 4 bytes contain an id of the stack trace
    ///    where the uninitialized value was created.
    kmsan_shadow: *mut Page,
    kmsan_origin: *mut Page,
}

pub struct MmStruct {
    /// Fields which are often written to are placed in a separate
    /// cache line.
    /// @mm_count: The number of references to &struct
    /// mm_struct (@mm_users count as 1).
    ///
    /// Use mmgrab()/mmdrop() to modify. When this drops to
    /// 0, the &struct mm_struct is freed.
    mm_count: AtomicT,
    mm_mt: MapleTree,
    /// base of mmap area
    mmap_base: u64,
    /// base of mmap area in bottom-up allocations
    mmap_legacy_base: u64,
    /// Base addresses for compatible mmap()
    #[cfg(feature = "CONFIG_HAVE_ARCH_COMPAT_MMAP_BASES")]
    mmap_compat_base: u64,
    #[cfg(feature = "CONFIG_HAVE_ARCH_COMPAT_MMAP_BASES")]
    mmap_compat_legacy_base: u64,
    /// size of task vm space
    task_size: u64,
    pgd: *mut PgdT,
    #[cfg(feature = "CONFIG_MEMBARRIER")]
    /// @membarrier_state: Flags controlling membarrier behavior.
    ///
    /// This field is close to @pgd to hopefully fit in the same
    /// cache-line, which needs to be touched by switch_mm().
    membarrier_state: AtomicT,
    /// @mm_users: The number of users including userspace.
    ///
    /// Use mmget()/mmget_not_zero()/mmput() to modify. When this
    /// drops to 0 (i.e. when the task exits and there are no other
    /// temporary reference holders), we also release a reference on
    /// @mm_count (which may then free the &struct mm_struct if
    /// @mm_count also drops to 0).
    mm_users: AtomicT,
    /// @pcpu_cid: Per-cpu current cid.
    ///
    /// Keep track of the currently allocated MmCid for each cpu.
    /// The per-cpu MmCid values are serialized by their respective
    /// runqueue locks.
    #[cfg(feature = "CONFIG_SCHED_MM_CID")]
    pcpu_cid: MmCid,
    /// @mm_cid_next_scan: Next mm_cid scan (in jiffies).
    ///
    /// When the next mm_cid scan is due (in jiffies).
    #[cfg(feature = "CONFIG_SCHED_MM_CID")]
    mm_cid_next_scan: u64,
    /// size of all page tables
    #[cfg(feature = "CONFIG_MMU")]
    pgtables_bytes: AtomicLongT,
    /// number of VMAs
    map_count: i32,
    /// Protects page tables and some counters
    page_table_lock: SpinlockT,
    /// With some kernel config, the current mmap_lock's offset
    /// inside 'mm_struct' is at 0x120, which is very optimal, as
    /// its two hot fields 'count' and 'owner' sit in 2 different
    /// cachelines,  and when mmap_lock is highly contended, both
    /// of the 2 fields will be accessed frequently, current layout
    /// will help to reduce cache bouncing.
    ///
    /// So please be careful with adding new fields before
    /// mmap_lock, which can easily push the 2 fields into one
    /// cacheline.
    mmap_lock: RwSemaphore,
    /// List of maybe swapped mm's. These are globally strung together off
    /// init_mm.mmlist, and are protected by mmlist_lock
    mmlist: ListHead,
    /// This field has lock-like semantics, meaning it is sometimes
    /// accessed with ACQUIRE/RELEASE semantics.
    /// Roughly speaking, incrementing the sequence number is
    /// equivalent to releasing locks on VMAs; reading the sequence
    /// number can be part of taking a read lock on a VMA.
    ///
    /// Can be modified under write mmap_lock using RELEASE
    /// semantics.
    /// Can be read with no other protection when holding write
    /// mmap_lock.
    /// Can be read with ACQUIRE semantics if not holding write
    /// mmap_lock.
    #[cfg(feature = "CONFIG_PER_VMA_LOCK")]
    mm_lock_seq: i32,
    /// High-watermark of RSS usage
    hiwater_rss: u64,
    /// High-water virtual memory usage
    hiwater_vm: u64,
    /// Total pages mapped
    total_vm: u64,
    /// Pages that have PG_mlocked set
    locked_vm: u64,
    /// Refcount permanently increased
    pinned_vm: Atomic64T,
    /// VM_WRITE & ~VM_SHARED & ~VM_STACK
    data_vm: u64,
    /// VM_EXEC & ~VM_WRITE & ~VM_STACK
    exec_vm: u64,
    /// VM_STACK
    stack_vm: u64,
    def_flags: u64,
    /// @write_protect_seq: Locked when any thread is write
    /// protecting pages mapped by this mm to enforce a later COW,
    /// for instance during page table copying for fork().
    write_protect_seq: SeqcountT,
    /// protect the below fields
    arg_lock: SpinlockT,
    start_code: u64,
    end_code: u64,
    start_data: u64,
    end_data: u64,
    start_brk: u64,
    brk: u64,
    start_stack: u64,
    arg_start: u64,
    arg_end: u64,
    env_start: u64,
    env_end: u64,
    /// for /proc/PID/auxv
    saved_auxv: [u64; AT_VECTOR_SIZE],
    rss_stat: [PercpuCounter; NR_MM_COUNTERS],
    binfmt: *mut LinuxBinfmt,
    /// Architecture-specific MM context
    context: MmContextT,
    /// Must use atomic bitops to access
    flags: u64,
    #[cfg(feature = "CONFIG_AIO")]
    ioctx_lock: SpinlockT,
    #[cfg(feature = "CONFIG_AIO")]
    ioctx_table: *mut KioctxTable,
    /// "owner" points to a task that is regarded as the canonical
    /// user/owner of this mm. All of the following must be true in
    /// order for it to be changed:
    ///
    /// current == mm->owner
    /// current->mm != mm
    /// new_owner->mm == mm
    /// new_owner->alloc_lock is held
    #[cfg(feature = "CONFIG_MEMCG")]
    owner: TaskStruct,
    user_ns: UserNamespace,
    /// store ref to file /proc/<pid>/exe symlink points to
    exe_file: *mut File,
    #[cfg(feature = "CONFIG_MMU_NOTIFIER")]
    notifier_subscriptions: *mut MmuNotifierSubscriptions,
    /// protected by page_table_lock
    #[cfg(feature = "CONFIG_TRANSPARENT_HUGEPAGE")]
    #[cfg(not(feature = "USE_SPLIT_PMD_PTLOCKS"))]
    pmd_huge_pte: PgtableT,
    /// numa_next_scan is the next time that PTEs will be remapped
    /// PROT_NONE to trigger NUMA hinting faults; such faults gather
    /// statistics and migrate pages to new nodes if necessary.
    #[cfg(feature = "CONFIG_NUMA_BALANCING")]
    numa_next_scan: u64,
    /// Restart point for scanning and remapping PTEs.
    #[cfg(feature = "CONFIG_NUMA_BALANCING")]
    numa_scan_offset: u64,
    /// numa_scan_seq prevents two threads remapping PTEs.
    #[cfg(feature = "CONFIG_NUMA_BALANCING")]
    numa_scan_seq: i32,
    /// An operation with batched TLB flushing is going on. Anything
    /// that can move process memory needs to flush the TLB when
    /// moving a PROT_NONE mapped page.
    tlb_flush_pending: AtomicT,
    /// See flush_tlb_batched_pending()
    #[cfg(feature = "CONFIG_ARCH_WANT_BATCHED_UNMAP_TLB_FLUSH")]
    tlb_flush_batched: AtomicT,
    uprobes_state: UprobesState,
    #[cfg(feature = "CONFIG_PREEMPT_RT")]
    delayed_drop: RcuHead,
    #[cfg(feature = "CONFIG_HUGETLB_PAGE")]
    hugetlb_usage: AtomicLongT,
    async_put_work: WorkStruct,
    #[cfg(feature = "CONFIG_IOMMU_SVA")]
    pasid: u32,
    /// Represent how many pages of this process are involved in KSM
    /// merging (not including ksm_zero_pages).
    #[cfg(feature = "CONFIG_KSM")]
    ksm_merging_pages: u64,
    /// Represent how many pages are checked for ksm merging
    /// including merged and not merged.
    #[cfg(feature = "CONFIG_KSM")]
    ksm_rmap_items: u64,
    /// Represent how many empty pages are merged with kernel zero
    /// pages when enabling KSM use_zero_pages.
    #[cfg(feature = "CONFIG_KSM")]
    ksm_zero_pages: u64,
    /// this mm_struct is on lru_gen_mm_list
    #[cfg(feature = "CONFIG_LRU_GEN_WALKS_MMU")]
    list: ListHead,
    /// Set when switching to this mm_struct, as a hint of
    ///  whether it has been used since the last time per-node
    /// page table walkers cleared the corresponding bits.
    #[cfg(feature = "CONFIG_LRU_GEN_WALKS_MMU")]
    bitmap: u64,
    /// points to the memcg of "owner" above
    #[cfg(all(feature = "CONFIG_LRU_GEN_WALKS_MMU", feature = "CONFIG_MEMCG"))]
    memcg: *mut MemCgroup,
    /// The mm_cpumask needs to be at the end of mm_struct, because it
    /// is dynamically sized based on nr_cpu_ids.
    cpu_bitmap: [u64; 10],
}
