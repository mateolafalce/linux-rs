/**************************************************************************
 *   lockdep_types.rs  --  This file is part of linux-rs.                 *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::types::HlistNode;

const MAX_LOCKDEP_SUBCLASSES: usize = 8;

/// hash_entry is used to keep track of dynamically allocated keys.
pub struct LockClassKey {
    hash_entry: HlistNode,
    subkeys: [LockdepSubclassKey; MAX_LOCKDEP_SUBCLASSES],
}

/// Map the lock object (the lock instance) to the lock-class object.
/// This is embedded into specific lock instances:
#[cfg(feature = "CONFIG_LOCKDEP")]
pub struct LockdepMap {
    key: *mut LockClassKey,
    class_cache: [*mut LockClass; NR_LOCKDEP_CACHING_CLASSES],
    name: *const char,
    /// can be taken in this context
    wait_type_outer: u8,
    /// presents this context
    wait_type_inner: u8,
    lock_type: u8,
    #[cfg(not(feature = "CONFIG_LOCK_STAT"))]
    cpu: i32,
    #[cfg(not(feature = "CONFIG_LOCK_STAT"))]
    ip: u64,
}

/// A lockdep key is associated with each lock object. For static locks we use
/// the lock address itself as the key. Dynamically allocated lock objects can
/// have a statically or dynamically allocated key. Dynamically allocated lock
/// keys must be registered before being used and must be unregistered before
/// the key memory is freed.
struct LockdepSubclassKey {
    __one_byte: u8,
}

/// The lockdep_map takes no space if lockdep is disabled:
#[cfg(not(feature = "CONFIG_LOCKDEP"))]
pub struct LockdepMap {}

#[cfg(feature = "CONFIG_LOCKDEP")]
impl LockdepMap {
    pub fn new() -> Self {
        Self {
            key: &mut LockClassKey::new(),
        }
    }
}
