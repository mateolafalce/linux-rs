/**************************************************************************
 *   mtd.rs  --  This file is part of linux-rs.                           *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::{
    drivers::nvmem::internals::NvmemDevice,
    include::{
        linux::{
            dcache::Dentry, device::Device, kref::Kref, module::Module, mutex_types::Mutex,
            notifier::NotifierBlock, types::ListHead,
        },
        uapi::mtd::mtd_abi::MtdEccStats,
    },
};

pub struct MtdInfo {
    type_: u8,
    flags: u32,
    /// Total size of the MTD
    size: u64,
    /// "Major" erase size for the device. Naïve users may take this
    /// to be the only erase size available, or may use the more detailed
    /// information below if they desire
    erasesize: u32,
    /// Minimal writable flash unit size. In case of NOR flash it is 1 (even
    /// though individual bits can be cleared), in case of NAND flash it is
    /// one NAND page (or half, or one-fourths of it), in case of ECC-ed NOR
    /// it is of ECC block size, etc. It is illegal to have writesize = 0.
    /// Any driver registering a struct mtd_info must ensure a writesize of
    /// 1 or larger.
    writesize: u32,
    /// Size of the write buffer used by the MTD. MTD devices having a write
    /// buffer can write multiple writesize chunks at a time. E.g. while
    /// writing 4 * writesize bytes to a device with 2 * writesize bytes
    /// buffer the MTD driver can (but doesn't have to) do 2 writesize
    /// operations, but not 4. Currently, all NANDs have writebufsize
    /// equivalent to writesize (NAND page size). Some NOR flashes do have
    /// writebufsize greater than writesize.
    writebufsize: u32,
    /// Amount of OOB data per block (e.g. 16)
    oobsize: u32,
    /// Available OOB bytes per block
    oobavail: u32,
    /// If erasesize is a power of 2 then the shift is stored in
    /// erasesize_shift otherwise erasesize_shift is zero. Ditto writesize.
    erasesize_shift: u32,
    writesize_shift: u32,
    /// Masks based on erasesize_shift and writesize_shift
    erasesize_mask: u32,
    writesize_mask: u32,
    /// read ops return -EUCLEAN if max number of bitflips corrected on any
    /// one region comprising an ecc step equals or exceeds this value.
    /// Settable by driver, else defaults to ecc_strength.  User can override
    /// in sysfs.  N.B. The meaning of the -EUCLEAN return code has changed;
    /// see Documentation/ABI/testing/sysfs-class-mtd for more detail.
    bitflip_threshold: u32,
    /// Kernel-only stuff starts here.
    name: *const char,
    index: i32,
    // OOB layout description
    // TODO impl fns const struct mtd_ooblayout_ops *ooblayout;
    /// NAND pairing scheme, only provided for MLC/TLC NANDs
    pairing: *const MtdPairingScheme,
    /// the ecc step size.
    ecc_step_size: u32,
    /// max number of correctible bit errors per ecc step
    ecc_strength: u32,
    /// Data for variable erase regions. If numeraseregions is zero,
    /// it means that the whole device has erasesize as given above.
    numeraseregions: i32,
    eraseregions: *mut MtdEraseRegionInfo,
    /// flag indicates a panic write, low level drivers can take appropriate
    /// action if required to ensure writes go through
    oops_panic_write: bool,
    /// default mode before reboot
    reboot_notifier: NotifierBlock,
    /// ECC status information
    ecc_stats: MtdEccStats,
    /// Subpage shift (NAND)
    subpage_sft: i32,
    priv_: *mut *mut u32,
    owner: *mut Module,
    dev: Device,
    refcnt: Kref,
    dbg: MtdDebugInfo,
    nvmem: *mut NvmemDevice,
    otp_user_nvmem: *mut NvmemDevice,
    otp_factory_nvmem: *mut NvmemDevice,
    /// Parent device from the MTD partition point of view.
    ///
    /// MTD masters do not have any parent, MTD partitions do. The parent
    /// MTD device can itself be a partition.
    parent: *mut MtdInfo,
    /// List of partitions attached to this MTD device
    partitions: ListHead,
    part: MtdPart,
    master: MtdMaster,
}

/*struct mtd_info {
    TODO impl fns
    /*
     * Do not call via these pointers, use corresponding mtd_*()
     * wrappers instead.
     */
    int (*_erase) (struct mtd_info *mtd, struct erase_info *instr);
    int (*_point) (struct mtd_info *mtd, loff_t from, size_t len,
               size_t *retlen, void **virt, resource_size_t *phys);
    int (*_unpoint) (struct mtd_info *mtd, loff_t from, size_t len);
    int (*_read) (struct mtd_info *mtd, loff_t from, size_t len,
              size_t *retlen, u_char *buf);
    int (*_write) (struct mtd_info *mtd, loff_t to, size_t len,
               size_t *retlen, const u_char *buf);
    int (*_panic_write) (struct mtd_info *mtd, loff_t to, size_t len,
                 size_t *retlen, const u_char *buf);
    int (*_read_oob) (struct mtd_info *mtd, loff_t from,
              struct mtd_oob_ops *ops);
    int (*_write_oob) (struct mtd_info *mtd, loff_t to,
               struct mtd_oob_ops *ops);
    int (*_get_fact_prot_info) (struct mtd_info *mtd, size_t len,
                    size_t *retlen, struct otp_info *buf);
    int (*_read_fact_prot_reg) (struct mtd_info *mtd, loff_t from,
                    size_t len, size_t *retlen, u_char *buf);
    int (*_get_user_prot_info) (struct mtd_info *mtd, size_t len,
                    size_t *retlen, struct otp_info *buf);
    int (*_read_user_prot_reg) (struct mtd_info *mtd, loff_t from,
                    size_t len, size_t *retlen, u_char *buf);
    int (*_write_user_prot_reg) (struct mtd_info *mtd, loff_t to,
                     size_t len, size_t *retlen,
                     const u_char *buf);
    int (*_lock_user_prot_reg) (struct mtd_info *mtd, loff_t from,
                    size_t len);
    int (*_erase_user_prot_reg) (struct mtd_info *mtd, loff_t from,
                     size_t len);
    int (*_writev) (struct mtd_info *mtd, const struct kvec *vecs,
            unsigned long count, loff_t to, size_t *retlen);
    void (*_sync) (struct mtd_info *mtd);
    int (*_lock) (struct mtd_info *mtd, loff_t ofs, uint64_t len);
    int (*_unlock) (struct mtd_info *mtd, loff_t ofs, uint64_t len);
    int (*_is_locked) (struct mtd_info *mtd, loff_t ofs, uint64_t len);
    int (*_block_isreserved) (struct mtd_info *mtd, loff_t ofs);
    int (*_block_isbad) (struct mtd_info *mtd, loff_t ofs);
    int (*_block_markbad) (struct mtd_info *mtd, loff_t ofs);
    int (*_max_bad_blocks) (struct mtd_info *mtd, loff_t ofs, size_t len);
    int (*_suspend) (struct mtd_info *mtd);
    void (*_resume) (struct mtd_info *mtd);
    void (*_reboot) (struct mtd_info *mtd);
    /*
     * If the driver is something smart, like UBI, it may need to maintain
     * its own reference counting. The below functions are only for driver.
     */
    int (*_get_device) (struct mtd_info *mtd);
    void (*_put_device) (struct mtd_info *mtd);
};*/

/// struct mtd_pairing_scheme - page pairing scheme description
///
/// @ngroups: number of groups. Should be related to the number of bits
///       per cell.
/// @get_info: converts a write-unit (page number within an erase block) into
///        mtd_pairing information (pair + group). This function should
///        fill the info parameter based on the wunit index or return
///       -EINVAL if the wunit parameter is invalid.
/// @get_wunit: converts pairing information into a write-unit (page) number.
///         This function should return the wunit index pointed by the
///         pairing information described in the info argument. It should
///         return -EINVAL, if there's no wunit corresponding to the
///         passed pairing information.
///
/// See mtd_pairing_info documentation for a detailed explanation of the
/// pair and group concepts.
///
/// The mtd_pairing_scheme structure provides a generic solution to represent
/// NAND page pairing scheme. Instead of exposing two big tables to do the
/// write-unit <-> (pair + group) conversions, we ask the MTD drivers to
/// implement the ->get_info() and ->get_wunit() functions.
///
/// MTD users will then be able to query these information by using the
/// mtd_pairing_info_to_wunit() and mtd_wunit_to_pairing_info() helpers.
///
/// @ngroups is here to help MTD users iterating over all the pages in a
/// given pair. This value can be retrieved by MTD users using the
/// mtd_pairing_groups() helper.
///
/// Examples are given in the mtd_pairing_info_to_wunit() and
/// mtd_wunit_to_pairing_info() documentation.
struct MtdPairingScheme {
    ngroups: i32, //TODO: impl fns int (*get_info)(struct mtd_info *mtd, int wunit,struct mtd_pairing_info *info);
                  //TODO: impl fns int (*get_wunit)(struct mtd_info *mtd,const struct mtd_pairing_info *info);
}

struct MtdEraseRegionInfo {
    /// At which this region starts, from the beginning of the MTD
    offset: u64,
    /// For this region
    erasesize: u32,
    /// Number of blocks of erasesize in this region
    numblocks: u32,
    ///  If keeping bitmap of locks
    lockmap: *mut u32,
}

/// struct mtd_debug_info - debugging information for an MTD device.
///
/// @dfs_dir: direntry object of the MTD device debugfs directory
struct MtdDebugInfo {
    dfs_dir: *mut Dentry,
}

/// struct mtd_part - MTD partition specific fields
///
/// @node: list node used to add an MTD partition to the parent partition list
/// @offset: offset of the partition relatively to the parent offset
/// @size: partition size. Should be equal to mtd->size unless
///    MTD_SLC_ON_MLC_EMULATION is set
/// @flags: original flags (before the mtdpart logic decided to tweak them based
///     on flash constraints, like eraseblock/pagesize alignment)
///
/// This struct is embedded in mtd_info and contains partition-specific
/// properties/fields.
struct MtdPart {
    node: ListHead,
    offset: u64,
    size: u64,
    flags: u32,
}

/// struct mtd_master - MTD master specific fields
///
/// @partitions_lock: lock protecting accesses to the partition list. Protects
///           not only the master partition list, but also all
///           sub-partitions.
/// @suspended: et to 1 when the device is suspended, 0 otherwise
///
/// This struct is embedded in mtd_info and contains master-specific
/// properties/fields. The master is the root MTD device from the MTD partition
// point of view.
struct MtdMaster {
    partitions_lock: Mutex,
    chrdev_lock: Mutex,
    suspended: bool,
}
