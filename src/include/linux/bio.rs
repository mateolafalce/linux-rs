/**************************************************************************
 *   bio.rs  --  This file is part of linux-rs.                           *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::{
    include::linux::{
        blk_types::Bio,
        mempool::MempoolT,
        spinlock_types::SpinlockT,
        types::HlistNode,
        workqueue::{WorkStruct, WorkqueueStruct},
    },
    mm::slab::KmemCache,
};

pub struct BioSet {
    bio_slab: *mut KmemCache,
    front_pad: u32,
    /// per-cpu bio alloc cache
    cache: *mut BioAllocCache,
    bio_pool: MempoolT,
    bvec_pool: MempoolT,
    #[cfg(feature = "CONFIG_BLK_DEV_INTEGRITY")]
    bio_integrity_pool: MempoolT,
    #[cfg(feature = "CONFIG_BLK_DEV_INTEGRITY")]
    bvec_integrity_pool: MempoolT,
    back_pad: u32,
    /// Deadlock avoidance for stacking block drivers:
    /// see comments in bio_alloc_bioset() for details
    rescue_lock: SpinlockT,
    rescue_list: BioList,
    rescue_work: WorkStruct,
    rescue_workqueue: *mut WorkqueueStruct,
    /// Hot un-plug notifier for the per-cpu cache, if used
    cpuhp_dead: HlistNode,
}

struct BioAllocCache {
    free_list: *mut Bio,
    free_list_irq: *mut Bio,
    nr: u32,
    nr_irq: u32,
}

/// BIO list management for use by remapping drivers (e.g. DM or MD) and loop.
///
/// A bio_list anchors a singly-linked list of bios chained through the bi_next
/// member of the bio.  The bio_list also caches the last list member to allow
/// fast access to the tail.
struct BioList {
    head: *mut Bio,
    tail: *mut Bio,
}
