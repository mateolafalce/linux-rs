/**************************************************************************
 *   list_lru.rs  --  This file is part of linux-rs.                      *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{spinlock_types::SpinlockT, types::ListHead};

pub struct ListLru {
    node: *mut ListLruNode,
    #[cfg(feature = "CONFIG_MEMCG_KMEM")]
    list: ListHead,
    #[cfg(feature = "CONFIG_MEMCG_KMEM")]
    shrinker_id: i32,
    #[cfg(feature = "CONFIG_MEMCG_KMEM")]
    memcg_aware: bool,
    #[cfg(feature = "CONFIG_MEMCG_KMEM")]
    xa: Xarray,
}

struct ListLruNode {
    /// protects all lists on the node, including per cgroup
    lock: SpinlockT,
    /// global list, used for the root cgroup in cgroup aware lrus
    lru: ListLruOne,
    nr_items: i64,
}

struct ListLruOne {
    list: ListHead,
    /// may become negative during memcg reparenting
    nr_items: i64,
}
