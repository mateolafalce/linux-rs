/**************************************************************************
 *   pipe_fs_i.rs  --  This file is part of linux-rs.                     *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

#[cfg(feature = "CONFIG_WATCH_QUEUE")]
use crate::include::linux::watch_queue::WatchQueue;

use crate::include::linux::{
    fs::FasyncStruct, mm_types::Page, mutex_types::Mutex, sched_::user::UserStruct,
    wait::WaitQueueHeadT,
};

/// struct pipe_inode_info - a linux kernel pipe
/// @mutex: mutex protecting the whole thing
/// @rd_wait: reader wait point in case of empty pipe
/// @wr_wait: writer wait point in case of full pipe
/// @head: The point of buffer production
/// @tail: The point of buffer consumption
/// @note_loss: The next read() should insert a data-lost message
/// @max_usage: The maximum number of slots that may be used in the ring
/// @ring_size: total number of buffers (should be a power of 2)
/// @nr_accounted: The amount this pipe accounts for in user->pipe_bufs
/// @tmp_page: cached released page
/// @readers: number of current readers of this pipe
/// @writers: number of current writers of this pipe
/// @files: number of struct file referring this pipe (protected by ->i_lock)
/// @r_counter: reader counter
/// @w_counter: writer counter
/// @poll_usage: is this pipe used for epoll, which has crazy wakeups?
/// @fasync_readers: reader side fasync
/// @fasync_writers: writer side fasync
/// @bufs: the circular array of pipe buffers
/// @user: the user who created this pipe
/// @watch_queue: If this pipe is a watch_queue, this is the stuff for that
pub struct PipeInodeInfo {
    mutex: Mutex,
    rd_wait: WaitQueueHeadT,
    wr_wait: WaitQueueHeadT,
    head: u32,
    tail: u32,
    max_usage: u32,
    ring_size: u32,
    nr_accounted: u32,
    readers: u32,
    writers: u32,
    files: u32,
    r_counter: u32,
    w_counter: u32,
    poll_usage: bool,
    #[cfg(feature = "CONFIG_WATCH_QUEUE")]
    note_loss: bool,
    tmp_page: *mut Page,
    fasync_readers: *mut FasyncStruct,
    fasync_writers: *mut FasyncStruct,
    bufs: *mut PipeBuffer,
    user: *mut UserStruct,
    #[cfg(feature = "CONFIG_WATCH_QUEUE")]
    watch_queue: *mut WatchQueue,
}

///  struct PipeBuffer - a linux kernel pipe buffer
///  @page: the page containing the data for the pipe buffer
///  @offset: offset of data inside the @page
///  @len: length of data inside the @page
///  @ops: operations associated with this buffer. See @pipe_buf_operations.
///  @flags: pipe buffer flags. See above.
///  @private: private data owned by the ops.
struct PipeBuffer {
    page: *mut Page,
    offset: u32,
    len: u32,
    //ops: Opst, // TODO use impl
    flags: u32,
    private: u64,
}
