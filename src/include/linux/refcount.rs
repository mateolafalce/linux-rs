/**************************************************************************
 *   refcount.rs  --  This file is part of linux-rs.                      *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::types::AtomicT;

/// typedef RefCountT - variant of AtomicT specialized for reference counts
/// @refs: AtomicT counter field
///
/// The counter saturates at REFCOUNT_SATURATED and will not move once
/// there. This avoids wrapping the counter and causing 'spurious'
/// use-after-free bugs.
pub struct RefCountT {
    refs: AtomicT,
}

impl RefCountT {
    pub fn new(n: i32) -> Self {
        RefCountT {
            refs: AtomicT::new(n),
        }
    }
}
