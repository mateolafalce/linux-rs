/**************************************************************************
 *   blk_mq.rs  --  This file is part of linux-rs.                        *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::{
    block::{
        blk::BlkFlushQueue,
        blk_mq::{BlkMqCtx, HCTX_MAX_TYPES},
    },
    include::linux::{
        blk_types::{Bio, BlkOpfT, BlockDevice},
        blkdev::RequestQueue,
        bvec::BioVec,
        iocontext::IoCq,
        kobject::Kobject,
        llist::LlistNode,
        mutex_types::Mutex,
        rbtree_types::RbNode,
        sbitmap::{Sbitmap, SbitmapQueue},
        spinlock_types::SpinlockT,
        srcutree::SrcuStruct,
        types::{AtomicT, HlistNode, ListHead, SectorT},
        wait::WaitQueueEntryT,
        workqueue::DelayedWork,
    },
};

/// request flags
type ReqFlagsT = u32;

/// struct blk_mq_hw_ctx - State for a hardware queue
/// facing the hardware block device
pub struct BlkMqHwCtx {
    /// @lock: Protects the dispatch list.
    lock: SpinlockT,
    /// @dispatch: Used for requests that are ready to be
    /// dispatched to the hardware but for some reason (e.g. lack of
    /// resources) could not be sent to the hardware. As soon as the
    /// driver can send new requests, requests at this list will
    /// be sent first for a fairer dispatch.
    dispatch: ListHead,
    /// @state: BLK_MQ_S_* flags. Defines the state of the hw
    /// queue (active, scheduled to restart, stopped).
    state: u64,
    /// @run_work: Used for scheduling a hardware queue run at a later time.
    run_work: DelayedWork,
    // @cpumask: Map of available CPUs where this hctx can run.
    // LOOK struct cpumask: CpumaskVarT,
    /// @next_cpu: Used by blk_mq_hctx_next_cpu() for
    /// round-robin CPU selection from @cpumask.
    next_cpu: i32,
    /// @next_cpu_batch: Counter of how many works left in
    /// the batch before changing to the next CPU
    next_cpu_batch: i32,
    /// @flags: BLK_MQ_F_* flags. Defines the behaviour of the queue.
    flags: u64,
    /// @sched_data: Pointer owned by the IO scheduler attached to a request
    /// queue. It's up to the IO scheduler how to use this pointer.
    sched_data: *mut u32,
    /// @queue: Pointer to the request queue that owns this hardware context.
    queue: *mut RequestQueue,
    /// @fq: Queue of requests that need to perform a flush operation.
    fq: *mut BlkFlushQueue,
    /// @driver_data: Pointer to data owned by the block
    /// driver that created this hctx
    driver_data: *mut u32,
    /// @ctx_map: Bitmap for each software queue. If bit is on,
    /// there is a pending request in that software queue.
    ctx_map: Sbitmap,
    /// @dispatch_from: Software queue to be used
    /// when no scheduler was selected.
    dispatch_from: BlkMqCtx,
    /// @dispatch_busy: Number used by blk_mq_update_dispatch_busy() to
    /// decide if the hw_queue is busy using Exponential Weighted Moving
    /// Average algorithm.
    dispatch_busy: u32,
    ///  @type: HCTX_TYPE_* flags. Type of hardware queue.
    type_: u16,
    /// @nr_ctx: Number of software queues.
    nr_ctx: u16,
    /// @ctxs: Array of software queues.
    ctxs: *mut BlkMqCtx,
    /// @dispatch_wait_lock: Lock for dispatch_wait queue.
    dispatch_wait_lock: SpinlockT,
    /// @dispatch_wait: Waitqueue to put requests when there is no tag
    /// available at the moment, to wait for another try in the future.
    dispatch_wait: WaitQueueEntryT,
    /// @wait_index: Index of next available
    /// dispatch_wait queue to insert requests.
    wait_index: AtomicT,
    /// @tags: Tags owned by the block driver. A tag at this set is only
    /// assigned when a request is dispatched from a hardware queue.
    tags: *mut BlkMqTags,
    /// @sched_tags: Tags owned by I/O scheduler. If there is an I/O
    /// scheduler associated with a request queue, a tag is assigned when
    /// that request is allocated. Else, this member is not used.
    sched_tags: *mut BlkMqTags,
    /// @numa_node: NUMA node the storage adapter has been connected to.
    numa_node: u32,
    /// @queue_num: Index of this hardware queue.
    queue_num: u32,
    /// @nr_active: Number of active requests. Only used when
    /// a tag set is shared across request queues.
    nr_active: AtomicT,
    /// @cpuhp_online: List to store request if CPU is going to die
    cpuhp_online: HlistNode,
    /// @cpuhp_dead: List to store request if some CPU die.
    cpuhp_dead: HlistNode,
    /// @kobj: Kernel object for sysfs.
    kobj: Kobject,
    /// @debugfs_dir: debugfs directory for this hardware queue.
    /// Named as cpu<cpu_number>.
    #[cfg(feature = "CONFIG_BLK_DEBUG_FS")]
    debugfs_dir: *mut Dentry,
    /// @sched_debugfs_dir:	debugfs directory for the scheduler.
    #[cfg(feature = "CONFIG_BLK_DEBUG_FS")]
    sched_debugfs_dir: *mut Dentry,
    /// @hctx_list: if this hctx is not in use,
    /// this is an entry in q->unused_hctx_list.
    hctx_list: ListHead,
}

/// Try to put the fields that are referenced together in the same cacheline.
///
/// If you modify this structure, make sure to update blk_rq_init() and
/// especially blk_mq_rq_ctx_init() to take care of the added fields.
pub struct Request {
    q: *mut RequestQueue,
    mq_ctx: *mut BlkMqCtx,
    mq_hctx: *mut BlkMqHwCtx,
    /// op and common flags
    cmd_flags: BlkOpfT,
    rq_flags: ReqFlagsT,
    tag: i32,
    internal_tag: i32,
    timeout: u32,
    /// the following two fields are internal, NEVER access directly
    /// total data len
    __data_len: u32,
    /// sector cursor
    __sector: SectorT,
    bio: *mut Bio,
    biotail: *mut Bio,
    queuelist: ListHead,
    rq_next: *mut Request,
    part: *mut BlockDevice,
    /// Time that the first bio started allocating this request.
    #[cfg(feature = "CONFIG_BLK_RQ_ALLOC_TIME")]
    alloc_time_ns: u64,
    /// Time that this request was allocated for this IO.
    start_time_ns: u64,
    /// Time that I/O was submitted to the device.
    io_start_time_ns: u64,
    #[cfg(feature = "CONFIG_BLK_WBT")]
    wbt_flags: u16,
    /// rq sectors used for blk stats. It has the same value
    /// with blk_rq_sectors(rq), except that it never be zeroed
    /// by completion.
    stats_sectors: u16,
    /// Number of scatter-gather DMA addr+len pairs after
    /// physical address coalescing is performed.
    nr_phys_segments: u16,
    #[cfg(feature = "CONFIG_BLK_DEV_INTEGRITY")]
    nr_integrity_segments: u16,
    #[cfg(feature = "CONFIG_BLK_INLINE_ENCRYPTION")]
    crypt_ctx: *mut BioCryptCtx,
    #[cfg(feature = "CONFIG_BLK_INLINE_ENCRYPTION")]
    crypt_keyslot: *mut BlkCryptoKeyslot,
    ioprio: u16,
    ref_: AtomicT,
    deadline: u64,
    /// The hash is used inside the scheduler, and killed once the
    /// request reaches the dispatch list. The ipi_list is only used
    /// to queue the request for softirq completion, which is long
    /// after the request has been unhashed (and even removed from
    /// the dispatch list).
    /// merge hash
    hash: HlistNode,
    ipi_list: LlistNode,
    /// The rb_node is only used inside the io scheduler, requests
    /// are pruned when moved to the dispatch queue. special_vec must
    /// only be used if RQF_SPECIAL_PAYLOAD is set, and those cannot be
    /// insert into an IO scheduler.
    /// sort/lookup
    rb_node: RbNode,
    special_vec: BioVec,
    /// Three pointers are available for the IO schedulers, if they need
    /// more they have to dynamically allocate it.
    icq: *mut IoCq,
    priv_: [*mut u32; 2],
    seq: u32,
    //TODO impl fn saved_end_io: *mut RqEndIoFn,
    fifo_time: u64,
    /// completion callback.
    //TODO impl fn end_io: *mut RqEndIoFn,
    end_io_data: *mut u32,
}

/// Tag address space map.
pub struct BlkMqTags {
    nr_tags: u32,
    nr_reserved_tags: u32,
    active_queues: u32,
    bitmap_tags: SbitmapQueue,
    breserved_tags: SbitmapQueue,
    rqs: *mut Request,
    static_rqs: *mut Request,
    page_list: ListHead,
    /// used to clear request reference in rqs[]
    /// before freeing one request pool
    lock: SpinlockT,
}

/// struct blk_mq_tag_set - tag set that can be shared between request queues
/// @ops: Pointers to functions that implement block driver behavior.
/// @map: One or more ctx -> hctx mappings. One map exists for each
/// hardware queue type (enum hctx_type) that the driver wishes
/// to support. There are no restrictions on maps being of the
/// same size, and it's perfectly legal to share maps between
/// types.
/// @nr_maps: Number of elements in the @map array. A number in the range
/// [1, HCTX_MAX_TYPES].
/// @nr_hw_queues: Number of hardware queues supported by the block driver that
/// owns this data structure.
/// @queue_depth: Number of tags per hardware queue, reserved tags included.
/// @reserved_tags: Number of tags to set aside for BLK_MQ_REQ_RESERVED tag
/// allocations.
/// @cmd_size: Number of additional bytes to allocate per request. The block
/// driver owns these additional bytes.
/// @numa_node: NUMA node the storage adapter has been connected to.
/// @timeout: Request processing timeout in jiffies.
/// @flags: Zero or more BLK_MQ_F_* flags.
/// @driver_data: Pointer to data owned by the block driver that created this
/// tag set.
/// @tags: Tag sets. One tag set per hardware queue. Has @nr_hw_queues
/// elements.
/// @shared_tags:
/// Shared set of tags. Has @nr_hw_queues elements. If set,
/// shared by all @tags.
/// @tag_list_lock: Serializes tag_list accesses.
/// @tag_list: List of the request queues that use this tag set. See also
/// request_queue.tag_set_list.
/// @srcu: Use as lock when type of the request queue is blocking
/// (BLK_MQ_F_BLOCKING).
pub struct BlkMqTagSet {
    //TODO impl fns const struct blk_mq_ops *ops;
    map: [BlkMqQueueMap; HCTX_MAX_TYPES],
    nr_maps: u32,
    nr_hw_queues: u32,
    queue_depth: u32,
    reserved_tags: u32,
    cmd_size: u32,
    numa_node: i32,
    timeout: u32,
    flags: u32,
    driver_data: *mut u32,
    tags: *mut BlkMqTags,
    shared_tags: *mut BlkMqTags,
    tag_list_lock: Mutex,
    tag_list: ListHead,
    srcu: *mut SrcuStruct,
}

/// struct blk_mq_queue_map - Map software queues to hardware queues
/// @mq_map: CPU ID to hardware queue index map. This is an array
/// with nr_cpu_ids elements. Each element has a value in the range
/// [@queue_offset, @queue_offset + @nr_queues).
/// @nr_queues: Number of hardware queues to map CPU IDs onto.
/// @queue_offset: First hardware queue to map onto. Used by the PCIe NVMe
/// driver to map each hardware queue type (enum hctx_type) onto a distinct
/// set of hardware queues.
struct BlkMqQueueMap {
    mq_map: *mut u32,
    nr_queues: u32,
    queue_offset: u32,
}

impl Request {
    const MQ_RQ_IDLE: usize = 0;
    const MQ_RQ_IN_FLIGHT: usize = 1;
    const MQ_RQ_COMPLETE: usize = 2;
}
