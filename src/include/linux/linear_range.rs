/**************************************************************************
 *   linear_range.rs  --  This file is part of linux-rs.                  *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

///  struct linear_range - table of selector - value pairs
///
///  Define a lookup-table for range of values. Intended to help when looking
///  for a register value matching certaing physical measure (like voltage).
///  Usable when increment of one in register always results a constant increment
///  of the physical measure (like voltage).
///
///  @min:  Lowest value in range
///  @min_sel: Lowest selector for range
///  @max_sel: Highest selector for range
///  @step: Value step size
pub struct LinearRange {
    min: u32,
    min_sel: u32,
    max_sel: u32,
    step: u32,
}
