/**************************************************************************
 *   device.rs  --  This file is part of linux-rs.                        *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::{
    arch::x86::include::asm::device::DevArchdata,
    drivers::{base::base::DevicePrivate, iommu::iommu::IommuGroup},
    include::linux::{
        device_::{bus::BusType, class::Class, driver::DeviceDriver},
        dma_direct::BusDmaRegion,
        fwnode::FwnodeHandle,
        iommu::DevIommu,
        kobject::Kobject,
        kref::Kref,
        mutex_types::Mutex,
        of::DeviceNode,
        pm::{DevPmDomain, DevPmInfo},
        refcount::RefCountT,
        spinlock_types::SpinlockT,
        sysfs::{Attribute, AttributeGroup},
        types::ListHead,
        workqueue::WorkStruct,
    },
};

/// struct device - The basic device structure
/// @parent:The device's "parent" device, the device to which it is attached.
/// In most cases, a parent device is some sort of bus or host
/// controller. If parent is NULL, the device, is a top-level device,
/// which is not usually what you want.
/// @p: Holds the private data of the driver core portions of the device.
/// See the comment of the struct device_private for detail.
/// @kobj:A top-level, abstract class from which other classes are derived.
/// @init_name: Initial name of the device.
/// @type:The type of device.
/// This identifies the device type and carries type-specific
/// information.
/// @mutex:Mutex to synchronize calls to its driver.
/// @bus:Type of bus device is on.
/// @driver:Which driver has allocated this
/// @platform_data: Platform data specific to the device.
/// Example: For devices on custom boards, as typical of embedded
/// and SOC based hardware, Linux often uses platform_data to point
/// to board-specific structures describing devices and how they
/// are wired.  That can include what ports are available, chip
/// variants, which GPIO pins act in what additional roles, and so
/// on.  This shrinks the "Board Support Packages" (BSPs) and
/// minimizes board-specific #ifdefs in drivers.
/// @driver_data: Private pointer for driver specific info.
/// @links: Links to suppliers and consumers of this device.
/// @power: For device power management.
/// See Documentation/driver-api/pm/devices.rst for details.
/// @pm_domain: Provide callbacks that are executed during system suspend,
/// hibernation, system resume and during runtime PM transitions
/// along with subsystem-level and driver-level callbacks.
/// @em_pd: device's energy model performance domain
/// @pins:  For device pin management.
/// See Documentation/driver-api/pin-control.rst for details.
/// @msi:   MSI related data
/// @numa_node: NUMA node this device is close to.
/// @dma_ops: DMA mapping operations for this device.
/// @dma_mask:Dma mask (if dma'ble device).
/// @coherent_dma_mask: Like dma_mask, but for alloc_coherent mapping as not all
/// hardware supports 64-bit addresses for consistent allocations
/// such descriptors.
/// @bus_dma_limit: Limit of an upstream bridge or bus which imposes a smaller
/// DMA limit than the device itself supports.
/// @dma_range_map: map for DMA memory ranges relative to that of RAM
/// @dma_parms: A low level driver may set these to teach IOMMU code about
/// segment limitations.
/// @dma_pools: Dma pools (if dma'ble device).
/// @dma_mem:Internal for coherent mem override.
/// @cma_area:Contiguous memory area for dma allocations
/// @dma_io_tlb_mem: Software IO TLB allocator.  Not for driver use.
/// @dma_io_tlb_pools:List of transient swiotlb memory pools.
/// @dma_io_tlb_lock:Protects changes to the list of active pools.
/// @dma_uses_io_tlb: %true if device has used the software IO TLB.
/// @archdata:For arch-specific additions.
/// @of_node:Associated device tree node.
/// @fwnode:Associated device node supplied by platform firmware.
/// @devt:For creating the sysfs "dev".
/// @id:device instance
/// @devres_lock: Spinlock to protect the resource of the device.
/// @devres_head: The resources list of the device.
/// @knode_class: The node used to add the device to the class list.
/// @class: The class of the device.
/// @groups:Optional attribute groups.
/// @release:Callback to free the device after all references have
/// gone away. This should be set by the allocator of the
/// device (i.e. the bus driver that discovered the device).
/// @iommu_group: IOMMU group the device belongs to.
/// @iommu: Per device generic IOMMU runtime data
/// @physical_location: Describes physical location of the device connection
/// point in the system housing.
/// @removable:  Whether the device can be removed from the system. This
/// should be set by the subsystem / bus driver that discovered
/// the device.
///
/// @offline_disabled: If set, the device is permanently online.
/// @offline:Set after successful invocation of bus type's .offline().
/// @of_node_reused: Set if the device-tree node is shared with an ancestor
///  device.
/// @state_synced: The hardware state of this device has been synced to match
/// the software state of this device by calling the driver/bus
/// sync_state() callback.
/// @can_match: The device has matched with a driver at least once or it is in
/// a bus (like AMBA) which can't check for matching drivers until
/// other devices probe successfully.
/// @dma_coherent: this particular device is dma coherent, even if the
/// architecture supports non-coherent devices.
/// @dma_ops_bypass: If set to %true then the dma_ops are bypassed for the
/// streaming DMA operations (->map_* / ->unmap_* / ->sync_*
/// operations).
/// @and optionall (if the coherent mask is large enough) also
/// for dma allocations.  This flag is managed by the dma ops
/// instance from ->dma_supported.
///
/// At the lowest level, every device in a Linux system is represented by an
/// instance of struct device. The device structure contains the information
/// that the device model core needs to model the system. Most subsystems,
/// however, track additional information about the devices they host. As a
/// result, it is rare for devices to be represented by bare device structures;
/// instead, that structure, like kobject structures, is usually embedded within
/// a higher-level representation of the device.
pub struct Device {
    kobj: Kobject,
    parent: *mut Device,
    p: *mut DevicePrivate,
    /// initial name of the device
    init_name: *const char,
    type_: *const DeviceType,
    /// type of bus device is on
    bus: *const BusType,
    /// which driver has allocated this device
    driver: *mut DeviceDriver,
    /// Platform specific data, device core doesn't touch it
    platform_data: *mut u32,
    /// Driver data, set and get with dev_set_drvdata/dev_get_drvdata
    driver_data: *mut u32,
    /// mutex to synchronize calls to its driver.
    mutex: Mutex,
    links: DevLinksInfo,
    power: DevPmInfo,
    pm_domain: *mut DevPmDomain,
    #[cfg(feature = "CONFIG_ENERGY_MODEL")]
    em_pd: *mut EmPerfDomain,
    #[cfg(feature = "CONFIG_PINCTRL")]
    pins: *mut DevPinInfo,
    msi: DevMsiInfo,
    #[cfg(feature = "CONFIG_DMA_OPS")]
    dma_ops: *const DmaMapOps,
    /// dma mask (if dma'able device)
    dma_mask: *mut u64,
    /// Like dma_mask, but for alloc_coherent mappings as
    /// not all hardware supports 64 bit addresses for consistent
    /// allocations such descriptors.
    coherent_dma_mask: u64,
    /// upstream dma constraint
    bus_dma_limit: u64,
    dma_range_map: *const BusDmaRegion,
    dma_parms: *mut DeviceDmaParameters,
    /// dma pools (if dma'ble)
    dma_pools: ListHead,
    /// internal for coherent mem override
    #[cfg(feature = "CONFIG_DMA_DECLARE_COHERENT")]
    dma_mem: *mut DmaCoherentMem,
    /// contiguous memory area for dma allocations
    #[cfg(feature = "CONFIG_DMA_CMA")]
    cma_area: *mut Cma,
    #[cfg(feature = "CONFIG_SWIOTLB")]
    dma_io_tlb_mem: *mut IoTlbMem,
    #[cfg(feature = "CONFIG_SWIOTLB_DYNAMIC")]
    dma_io_tlb_pools: ListHead,
    #[cfg(feature = "CONFIG_SWIOTLB_DYNAMIC")]
    dma_io_tlb_lock: SpinlockT,
    #[cfg(feature = "CONFIG_SWIOTLB_DYNAMIC")]
    dma_uses_io_tlb: bool,
    /// arch specific additions
    archdata: DevArchdata,
    /// associated device tree node
    of_node: *mut DeviceNode,
    /// firmware device node
    fwnode: *mut FwnodeHandle,
    /// NUMA node this device is close to
    #[cfg(feature = "CONFIG_NUMA")]
    numa_node: i32,
    /// dev_t, creates the sysfs "dev"
    devt: u32,
    /// device instance
    id: u32,
    devres_lock: SpinlockT,
    devres_head: ListHead,
    class: *const Class,
    /// optional groups
    groups: *const AttributeGroup,
    iommu_group: *mut IommuGroup,
    iommu: *mut DevIommu,
    physical_location: *mut DevicePhysicalLocation,
    offline_disabled: bool,
    offline: bool,
    of_node_reused: bool,
    state_synced: bool,
    can_match: bool,
    #[cfg(any(
        feature = "CONFIG_ARCH_HAS_SYNC_DMA_FOR_DEVICE",
        feature = "CONFIG_ARCH_HAS_SYNC_DMA_FOR_CPU",
        feature = "CONFIG_ARCH_HAS_SYNC_DMA_FOR_CPU_ALL"
    ))]
    dma_coherent: bool,
    #[cfg(feature = "CONFIG_DMA_OPS_BYPASS")]
    dma_ops_bypass: bool,
}

/*TODO impl fn
    void	(*release)(struct device *dev);
};
*/

/// The type of device, "struct device" is embedded in. A class
/// or bus can contain devices of different types
/// like "partitions" and "disks", "mouse" and "event".
/// This identifies the device type and carries type-specific
/// information, equivalent to the kobj_type of a kobject.
/// If "name" is specified, the uevent will contain it in
/// the DEVTYPE variable.
pub struct DeviceType {
    name: *const char,
    groups: *const AttributeGroup,
}

/*TODO: impl fns
    int (*uevent)(const struct device *dev, struct kobj_uevent_env *env);
    char *(*devnode)(const struct device *dev, umode_t *mode,
             kuid_t *uid, kgid_t *gid);
    void (*release)(struct device *dev);
    const struct dev_pm_ops *pm;
};*/

/// struct dev_msi_info - Device data related to MSI
/// @domain: The MSI interrupt domain associated to the device
/// @data: Pointer to MSI device data
struct DevMsiInfo {
    #[cfg(feature = "CONFIG_GENERIC_MSI_IRQ")]
    domain: *mut IrqDomain,
    #[cfg(feature = "CONFIG_GENERIC_MSI_IRQ")]
    data: *mut MsiDeviceData,
}

struct DeviceDmaParameters {
    /// a low level driver may set these to teach
    /// IOMMU code about sg limitations.
    max_segment_size: u32,
    min_align_mask: u32,
    segment_boundary_mask: u64,
}

/// struct dev_links_info - Device data related to device links.
/// @suppliers: List of links to supplier devices.
/// @consumers: List of links to consumer devices.
/// @defer_sync: Hook to global list of devices that have deferred sync_state.
/// @status: Driver status information.
struct DevLinksInfo {
    suppliers: ListHead,
    consumers: ListHead,
    defer_sync: ListHead,
}

/// struct device_physical_location - Device data related to physical location
/// of the device connection point.
/// @dock: Set if the device connection point resides in a docking station or
///        port replicator.
/// @lid: Set if this device connection point resides on the lid of laptop
///       system.
struct DevicePhysicalLocation {
    dock: bool,
    lid: bool,
}

///  struct device_attribute - Interface for exporting device attributes.
///  @attr: sysfs attribute definition.
///  @show: Show handler.
///  @store: Store handler.
pub struct DeviceAttribute {
    attr: Attribute,
    // TODO impl fn ssize_t (*show)(struct device *dev, struct device_attribute *attr,char *buf);
    // TODOssize_t (*store)(struct device *dev, struct device_attribute *attr, const char *buf, size_t count);
}

///  struct device_link - Device link representation.
///  @supplier: The device on the supplier end of the link.
///  @s_node: Hook to the supplier device's list of links to consumers.
///  @consumer: The device on the consumer end of the link.
///  @c_node: Hook to the consumer device's list of links to suppliers.
///  @link_dev: device used to expose link details in sysfs
///  @status: The state of the link (with respect to the presence of drivers).
///  @flags: Link flags.
///  @rpm_active: Whether or not the consumer device is runtime-PM-active.
///  @kref: Count repeated addition of the same link.
///  @rm_work: Work structure used for removing the link.
///  @supplier_preactivated: Supplier has been made active before consumer probe.
pub struct DeviceLink {
    supplier: *mut Device,
    s_node: ListHead,
    consumer: *mut Device,
    c_node: ListHead,
    link_dev: Device,
    // TODO impl enum device_link_state
    flags: u32,
    rpm_active: RefCountT,
    kref: Kref,
    rm_work: WorkStruct,
    supplier_preactivated: bool,
}

impl DevicePhysicalLocation {
    /// Describes which panel surface of the
    /// system's housing the device connection point resides on.
    /// @DEVICE_PANEL_TOP: Device connection point is on the top panel.
    /// @DEVICE_PANEL_BOTTOM: Device connection point is on the bottom panel.
    /// @DEVICE_PANEL_LEFT: Device connection point is on the left panel.
    /// @DEVICE_PANEL_RIGHT: Device connection point is on the right panel.
    /// @DEVICE_PANEL_FRONT: Device connection point is on the front panel.
    /// @DEVICE_PANEL_BACK: Device connection point is on the back panel.
    /// @DEVICE_PANEL_UNKNOWN: The panel with device connection point is unknown.
    const DEVICE_PANEL_TOP: usize = 0;
    const DEVICE_PANEL_BOTTOM: usize = 1;
    const DEVICE_PANEL_LEFT: usize = 2;
    const DEVICE_PANEL_RIGHT: usize = 3;
    const DEVICE_PANEL_FRONT: usize = 4;
    const DEVICE_PANEL_BACK: usize = 5;
    const DEVICE_PANEL_UNKNOWN: usize = 6;
    /// Describes vertical
    /// position of the device connection point on the panel surface.
    /// @DEVICE_VERT_POS_UPPER: Device connection point is at upper part of panel.
    /// @DEVICE_VERT_POS_CENTER: Device connection point is at center part of panel.
    /// @DEVICE_VERT_POS_LOWER: Device connection point is at lower part of panel.
    const DEVICE_VERT_POS_UPPER: usize = 0;
    const DEVICE_VERT_POS_CENTER: usize = 1;
    const DEVICE_VERT_POS_LOWER: usize = 2;
    /// Describes horizontal
    /// position of the device connection point on the panel surface.
    /// @DEVICE_HORI_POS_LEFT: Device connection point is at left part of panel.
    /// @DEVICE_HORI_POS_CENTER: Device connection point is at center part of panel.
    /// @DEVICE_HORI_POS_RIGHT: Device connection point is at right part of panel.
    const DEVICE_HORI_POS_LEFT: usize = 0;
    const DEVICE_HORI_POS_CENTER: usize = 1;
    const DEVICE_HORI_POS_RIGHT: usize = 2;
}

impl DevLinksInfo {
    /// enum dl_dev_state - Device driver presence tracking information.
    /// @DL_DEV_NO_DRIVER: There is no driver attached to the device.
    /// @DL_DEV_PROBING: A driver is probing.
    /// @DL_DEV_DRIVER_BOUND: The driver has been bound to the device.
    /// @DL_DEV_UNBINDING: The driver is unbinding from the device.
    const DL_DEV_NO_DRIVER: usize = 0;
    const DL_DEV_PROBING: usize = 1;
    const DL_DEV_DRIVER_BOUND: usize = 2;
    const DL_DEV_UNBINDING: usize = 3;
}

impl Device {
    /// enum device_removable - Whether the device is removable. The criteria for a
    /// device to be classified as removable is determined by its subsystem or bus.
    /// @DEVICE_REMOVABLE_NOT_SUPPORTED: This attribute is not supported for this
    ///                  device (default).
    /// @DEVICE_REMOVABLE_UNKNOWN:  Device location is Unknown.
    /// @DEVICE_FIXED: Device is not removable by the user.
    /// @DEVICE_REMOVABLE: Device is removable by the user.
    const DEVICE_REMOVABLE_NOT_SUPPORTED: usize = 0;
    const DEVICE_REMOVABLE_UNKNOWN: usize = 1;
    const DEVICE_FIXED: usize = 2;
    const DEVICE_REMOVABLE: usize = 3;
}
