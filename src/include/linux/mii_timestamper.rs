/**************************************************************************
 *   mii_timestamper.rs  --  This file is part of linux-rs.               *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::device::Device;

///  struct mii_timestamper - Callback interface to MII time stamping devices.
///
///  @rxtstamp:    Requests a Rx timestamp for 'skb'.  If the skb is accepted,
///         the MII time stamping device promises to deliver it using
///         netif_rx() as soon as a timestamp becomes available. One of
///         the PTP_CLASS_ values is passed in 'type'.  The function
///         must return true if the skb is accepted for delivery.
///
///  @txtstamp:    Requests a Tx timestamp for 'skb'.  The MII time stamping
///         device promises to deliver it using skb_complete_tx_timestamp()
///         as soon as a timestamp becomes available. One of the PTP_CLASS_
///         values is passed in 'type'.
///
///  @hwtstamp:    Handles SIOCSHWTSTAMP ioctl for hardware time stamping.
///
///  @link_state: Allows the device to respond to changes in the link
///         state.  The caller invokes this function while holding
///         the phy_device mutex.
///
///  @ts_info:    Handles ethtool queries for hardware time stamping.
///  @device:    Remembers the device to which the instance belongs.
///
///  Drivers for PHY time stamping devices should embed their
///  mii_timestamper within a private structure, obtaining a reference
///  to it using container_of().
///
///  Drivers for non-PHY time stamping devices should return a pointer
///  to a mii_timestamper from the probe_channel() callback of their
///  mii_timestamping_ctrl interface.
pub struct MiiTimestamper {
    device: *mut Device,
}
/*
TODO:
    bool (*rxtstamp)(struct mii_timestamper *mii_ts,
             struct sk_buff *skb, int type);

    void (*txtstamp)(struct mii_timestamper *mii_ts,
             struct sk_buff *skb, int type);

    int  (*hwtstamp)(struct mii_timestamper *mii_ts,
             struct kernel_hwtstamp_config *kernel_config,
             struct netlink_ext_ack *extack);

    void (*link_state)(struct mii_timestamper *mii_ts,
               struct phy_device *phydev);

    int  (*ts_info)(struct mii_timestamper *mii_ts,
            struct ethtool_ts_info *ts_info);
*/
