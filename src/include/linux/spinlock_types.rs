/**************************************************************************
 *   spinlock_types.rs  --  This file is part of linux-rs.                *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

#[cfg(feature = "CONFIG_RT_GROUP_SCHED")]
#[cfg(not(feature = "CONFIG_PREEMPT_RT"))]
use crate::include::linux::lockdep_types::LockdepMap;
use crate::include::linux::spinlock_types_raw::RawSpinlock;
#[cfg(all(feature = "CONFIG_RT_GROUP_SCHED", feature = "CONFIG_PREEMPT_RT"))]
use crate::include::linux::{lockdep_types::LockdepMap, rtmutex::RtMutexBase};

#[cfg(not(feature = "CONFIG_PREEMPT_RT"))]
/// Non PREEMPT_RT kernels map spinlock to raw_spinlock
pub struct SpinlockT {
    rlock: RawSpinlock,
    #[cfg(feature = "CONFIG_DEBUG_LOCK_ALLOC")]
    config: ConfigDebugLockAlloc,
}

#[cfg(feature = "CONFIG_RT_GROUP_SCHED")]
#[cfg(not(feature = "CONFIG_PREEMPT_RT"))]
struct ConfigDebugLockAlloc {
    __padding: [u8; LOCK_PADSIZE],
    dep_map: LockdepMap,
}

#[cfg(feature = "CONFIG_PREEMPT_RT")]
pub struct SpinlockT {
    lock: RtMutexBase,
    #[cfg(feature = "CONFIG_DEBUG_LOCK_ALLOC")]
    dep_map: LockdepMap,
}
