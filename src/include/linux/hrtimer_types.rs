/**************************************************************************
 *   hrtimer_types.rs  --  This file is part of linux-rs.                 *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{hrtimer::HrtimerClockBase, timerqueue_types::TimerqueueNode};

/// struct hrtimer - the basic hrtimer structure
/// @node: timerqueue node, which also manages node.expires,
/// the absolute expiry time in the hrtimers internal
/// representation. The time is related to the clock on
/// which the timer is based. Is setup by adding
/// slack to the _softexpires value. For non range timers
/// identical to _softexpires.
/// @_softexpires: the absolute earliest expiry time of the hrtimer.
/// The time which was given as expiry time when the timer
/// was armed.
/// @function:timer expiry callback function
/// @base:pointer to the timer base (per cpu and per clock)
/// @state: state information (See bit values above)
/// @is_rel:Set if the timer was armed relative
/// @is_soft:Set if hrtimer will be expired in soft interrupt context.
/// @is_hard:Set if hrtimer will be expired in hard interrupt context
/// even on RT.
/// The hrtimer structure must be initialized by hrtimer_init()
pub struct Hrtimer {
    node: TimerqueueNode,
    _softexpires: i64,
    base: *mut HrtimerClockBase,
    state: u8,
    is_rel: u8,
    is_soft: u8,
    is_hard: u8,
}

/// Return values for the callback function
impl Hrtimer {
    /// Timer is not restarted
    const HRTIMER_NORESTART: u8 = 0;
    /// Timer must be restarted
    const HRTIMER_RESTART: u8 = 1;
}
