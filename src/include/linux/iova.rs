/**************************************************************************
 *   iova.rs  --  This file is part of linux-rs.                          *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::{
    drivers::iommu::iova::IovaRcache,
    include::linux::{
        rbtree_types::{RbNode, RbRoot},
        spinlock_types::SpinlockT,
        types::HlistNode,
    },
};

/// iova structure
struct Iova {
    node: RbNode,
    /// Highest allocated pfn
    pfn_hi: u64,
    /// Lowest allocated pfn
    pfn_lo: u64,
}

///  holds all the iova translations for a domain
pub struct IovaDomain {
    /// Lock to protect update of rbtree
    iova_rbtree_lock: SpinlockT,
    /// iova domain rbtree root
    rbroot: RbRoot,
    /// Save last alloced node
    cached_node: *mut RbNode,
    /// Save last 32-bit alloced node
    cached32_node: *mut RbNode,
    /// pfn granularity for this domain
    granule: u64,
    /// Lower limit for this domain
    start_pfn: u64,
    dma_32bit_pfn: u64,
    /// Size of last failed allocation
    max32_alloc_size: u64,
    /// rbtree lookup anchor
    anchor: Iova,
    rcaches: IovaRcache,
    cpuhp_dead: HlistNode,
}
