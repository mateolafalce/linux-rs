/**************************************************************************
 *   quota.rs  --  This file is part of linux-rs.                         *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::{
    linux::{fs::Inode, module::Module, rwsem::RwSemaphore, types::ListHead},
    uapi::linux::quota::MAXQUOTAS,
};

type QsizeT = u64;

pub struct QuotaInfo {
    /// Flags for diskquotas on this device
    flags: u32,
    /// Lock quota file while I/O in progress
    dqio_sem: RwSemaphore,
    /// inodes of quotafiles
    files: [*mut Inode; MAXQUOTAS],
    /// Information for each quota type
    info: [MemDqinfo; MAXQUOTAS],
    // TODO impl fns const struct quota_format_ops *ops[MAXQUOTAS];
}

struct QuotaFormatType {
    qf_fmt_id: i32,
    // TODO: const struct quota_format_ops *qf_ops;
    qf_owner: *mut Module,
    qf_next: *mut QuotaFormatType,
}

struct MemDqinfo {
    dqi_format: *mut QuotaFormatType,
    dqi_fmt_id: i32,
    dqi_dirty_list: ListHead,
    dqi_flags: u64,
    dqi_bgrace: u32,
    dqi_igrace: u32,
    dqi_max_spc_limit: QsizeT,
    dqi_max_ino_limit: QsizeT,
    dqi_priv: *mut u32,
}
