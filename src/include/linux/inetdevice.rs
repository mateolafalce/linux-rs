/**************************************************************************
 *   inetdevice.rs  --  This file is part of linux-rs.                    *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::{
    linux::{
        igmp::IpMcList,
        netdevice::NetDevice,
        refcount::RefCountT,
        spinlock_types::SpinlockT,
        timer::TimerList,
        types::{HlistNode, RcuHead},
    },
    net::{neighbour::NeighParms, net_trackers::NetdeviceTracker},
    uapi::linux::{if_::IFNAMSIZ, ip::IPV4_DEVCONF_MAX},
};

pub struct InDevice {
    dev: *mut NetDevice,
    dev_tracker: NetdeviceTracker,
    refcnt: RefCountT,
    dead: i32,
    /// IP ifaddr chain
    ifa_list: *mut InIfaddr,
    /// IP multicast filter chain
    mc_list: *mut IpMcList,
    mc_hash: *mut IpMcList,
    /// Number of installed mcasts
    mc_count: i32,
    mc_tomb_lock: SpinlockT,
    mc_tomb: *mut IpMcList,
    mr_v1_seen: u64,
    mr_v2_seen: u64,
    mr_maxdelay: u64,
    /// Query Interval
    mr_qi: u64,
    /// Query Response Interval
    mr_qri: u64,
    /// Query Robustness Variable
    mr_qrv: u8,
    mr_gq_running: u8,
    mr_ifc_count: u32,
    /// general query timer
    mr_gq_timer: TimerList,
    /// interface change timer
    mr_ifc_timer: TimerList,
    arp_parms: *mut NeighParms,
    cnf: Ipv4Devconf,
    rcu_head: RcuHead,
}

struct InIfaddr {
    hash: HlistNode,
    ifa_next: *mut InIfaddr,
    ifa_dev: *mut InDevice,
    rcu_head: RcuHead,
    ifa_local: u32,
    ifa_address: u32,
    ifa_mask: u32,
    ifa_rt_priority: u32,
    ifa_broadcast: u32,
    ifa_scope: u8,
    ifa_prefixlen: u8,
    ifa_proto: u8,
    ifa_flags: u32,
    ifa_label: [i8; IFNAMSIZ],
    /// In seconds, relative to tstamp. Expiry is at tstamp + HZ * lft.
    ifa_valid_lft: u32,
    ifa_preferred_lft: u32,
    /// created timestamp
    ifa_cstamp: u64,
    /// updated timestamp
    ifa_tstamp: u64,
}

struct Ipv4Devconf {
    sysctl: *mut u32,
    data: [i32; IPV4_DEVCONF_MAX],
    //LOOK DECLARE_BITMAP(state, IPV4_DEVCONF_MAX);
}
