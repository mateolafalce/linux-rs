/**************************************************************************
 *   workqueue.rs  --  This file is part of linux-rs.                     *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

#[cfg(feature = "CONFIG_LOCKDEP")]
use crate::include::linux::lockdep_types::LockdepMap;

use crate::{
    include::linux::{
        timer::TimerList,
        types::{AtomicLongT, ListHead, RcuHead},
    },
    kernel::workqueue::WorkqueueStruct,
};

pub struct WorkStruct {
    data: AtomicLongT,
    entry: ListHead,
    // TODO impl fn func: WorkFuncT,
    #[cfg(feature = "CONFIG_LOCKDEP")]
    lockdep_map: LockdepMap,
}

pub struct DelayedWork {
    work: WorkStruct,
    timer: TimerList,
    /// target workqueue and CPU -> timer uses to queue -> work
    wq: *mut WorkqueueStruct,
    cpu: i32,
}

pub struct RcuWork {
    work: WorkStruct,
    rcu: RcuHead,
    /// target workqueue ->rcu uses to queue ->work
    wq: *mut WorkqueueStruct,
}
