/**************************************************************************
 *   pm.rs  --  This file is part of linux-rs.                            *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::spinlock_types::SpinlockT;

struct PmSubsysData {
    lock: SpinlockT,
    refcount: u32,
    #[cfg(feature = "CONFIG_PM_CLK")]
    clock_op_might_sleep: u32,
    #[cfg(feature = "CONFIG_PM_CLK")]
    clock_mutex: Mutex,
    #[cfg(feature = "CONFIG_PM_CLK")]
    clock_list: ListHead,
    #[cfg(feature = "CONFIG_PM_GENERIC_DOMAINS")]
    domain_data: *mut PmDomainData,
}

pub struct DevPmInfo {
    power_state: PmMessageT,
    can_wakeup: bool,
    async_suspend: bool,
    /// Owned by the PM core
    in_dpm_list: bool,
    /// Owned by the PM core
    is_prepared: bool,
    /// Ditto
    is_suspended: bool,
    is_noirq_suspended: bool,
    is_late_suspended: bool,
    no_pm: bool,
    /// Owned by the PM core
    early_init: bool,
    /// Owned by the PM core
    direct_complete: bool,
    driver_flags: u32,
    lock: SpinlockT,
    #[cfg(feature = "CONFIG_PM_SLEEP")]
    entry: ListHead,
    #[cfg(feature = "CONFIG_PM_SLEEP")]
    completion: Completion,
    #[cfg(feature = "CONFIG_PM_SLEEP")]
    wakeup: *mut WakeupSource,
    #[cfg(feature = "CONFIG_PM_SLEEP")]
    wakeup_path: bool,
    #[cfg(feature = "CONFIG_PM_SLEEP")]
    syscore: bool,
    /// Owned by the PM core
    #[cfg(feature = "CONFIG_PM_SLEEP")]
    no_pm_callbacks: bool,
    /// Owned by the PM core
    #[cfg(feature = "CONFIG_PM_SLEEP")]
    must_resume: bool,
    /// Set by subsystems
    #[cfg(feature = "CONFIG_PM_SLEEP")]
    may_skip_resume: bool,
    #[cfg(not(feature = "CONFIG_PM_SLEEP"))]
    should_wakeup: bool,
    #[cfg(feature = "CONFIG_PM")]
    suspend_timer: Hrtimer,
    #[cfg(feature = "CONFIG_PM")]
    timer_expires: u64,
    #[cfg(feature = "CONFIG_PM")]
    work: WorkStruct,
    #[cfg(feature = "CONFIG_PM")]
    wait_queue: WaitQueueHeadT,
    #[cfg(feature = "CONFIG_PM")]
    wakeirq: WakeIrq,
    #[cfg(feature = "CONFIG_PM")]
    usage_count: AtomicT,
    #[cfg(feature = "CONFIG_PM")]
    child_count: AtomicT,
    #[cfg(feature = "CONFIG_PM")]
    disable_depth: u32,
    #[cfg(feature = "CONFIG_PM")]
    idle_notification: bool,
    #[cfg(feature = "CONFIG_PM")]
    request_pending: bool,
    #[cfg(feature = "CONFIG_PM")]
    deferred_resume: bool,
    #[cfg(feature = "CONFIG_PM")]
    needs_force_resume: bool,
    #[cfg(feature = "CONFIG_PM")]
    runtime_auto: bool,
    #[cfg(feature = "CONFIG_PM")]
    ignore_children: bool,
    #[cfg(feature = "CONFIG_PM")]
    no_callbacks: bool,
    #[cfg(feature = "CONFIG_PM")]
    irq_safe: bool,
    #[cfg(feature = "CONFIG_PM")]
    use_autosuspend: bool,
    #[cfg(feature = "CONFIG_PM")]
    timer_autosuspends: bool,
    #[cfg(feature = "CONFIG_PM")]
    memalloc_noio: bool,
    #[cfg(feature = "CONFIG_PM")]
    links_count: u32,
    #[cfg(feature = "CONFIG_PM")]
    request: RpmRequest,
    #[cfg(feature = "CONFIG_PM")]
    runtime_status: RuntimeStatus,
    #[cfg(feature = "CONFIG_PM")]
    last_status: RpmStatus,
    #[cfg(feature = "CONFIG_PM")]
    runtime_error: i32,
    #[cfg(feature = "CONFIG_PM")]
    autosuspend_delay: i32,
    #[cfg(feature = "CONFIG_PM")]
    last_busy: u64,
    #[cfg(feature = "CONFIG_PM")]
    active_time: u64,
    #[cfg(feature = "CONFIG_PM")]
    suspended_time: u64,
    #[cfg(feature = "CONFIG_PM")]
    accounting_timestamp: u64,
    /// Owned by the subsystem.
    subsys_data: PmSubsysData,
}

/*
TODO: impl fns
    void (*set_latency_tolerance)(struct device *, s32);
    struct dev_pm_qos	*qos;
*/

struct PmMessageT {
    event: i32,
}

/// struct dev_pm_domain - power management domain representation.
///
/// @ops: Power management operations associated with this domain.
/// @start: Called when a user needs to start the device via the domain.
/// @detach: Called when removing a device from the domain.
/// @activate: Called before executing probe routines for bus types and drivers.
/// @sync: Called after successful driver probe.
/// @dismiss: Called after unsuccessful driver probe and after driver removal.
/// @set_performance_state: Called to request a new performance state.
///
/// Power domains provide callbacks that are executed during system suspend,
/// hibernation, system resume and during runtime PM transitions instead of
/// subsystem-level and driver-level callbacks.
pub struct DevPmDomain {}

/*
TODO: implf fns
    struct dev_pm_ops   ops;
    int (*start)(struct device *dev);
    void (*detach)(struct device *dev, bool power_off);
    int (*activate)(struct device *dev);
    void (*sync)(struct device *dev);
    void (*dismiss)(struct device *dev);
    int (*set_performance_state)(struct device *dev, unsigned int state);
};
*/
