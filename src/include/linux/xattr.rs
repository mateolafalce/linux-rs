/**************************************************************************
 *   xattr.rs  --  This file is part of linux-rs.                         *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{rbtree_types::RbRoot, rwlock_types::RwlockT};

/// struct xattr_handler: When @name is set, match attributes with exactly that
/// name.  When @prefix is set instead, match attributes with that prefix and
/// with a non-empty suffix.
pub struct XattrHandler {
    name: *const char,
    prefix: *const char,
}
/*
TODO: impl fns
    int flags;      /* fs private flags */
    bool (*list)(struct dentry *dentry);
    int (*get)(const struct xattr_handler *, struct dentry *dentry,struct inode *inode, const char *name, void *buffer,size_t size);
    int (*set)(const struct xattr_handler *,struct mnt_idmap *idmap, struct dentry *dentry,struct inode *inode, const char *name, const void *buffer,size_t size, int flags);
};
*/

pub struct SimpleXattrs {
    rb_root: RbRoot,
    lock: RwlockT,
}
