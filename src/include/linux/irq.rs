/**************************************************************************
 *   irq.rs  --  This file is part of linux-rs.                           *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{
    irqdomain::IrqDomain, msi::MsiDesc, spinlock_types_raw::RawSpinlockT, types::ListHead,
};

///  struct irq_domain_chip_generic - Generic irq chip data structure for irq domains
///  @irqs_per_chip:  Number of interrupts per chip
///  @num_chips:      Number of chips
///  @irq_flags_to_set:   IRQ* flags to set on irq setup
///  @irq_flags_to_clear: IRQ* flags to clear on irq setup
///  @gc_flags:       Generic chip specific setup flags
///  @gc:         Array of pointers to generic interrupt chips
pub struct IrqDomainChipGeneric {
    irqs_per_chip: u32,
    num_chips: u32,
    irq_flags_to_clear: u32,
    irq_flags_to_set: u32,
    gc: [*mut IrqChipGeneric; 10],
}

///  enum irq_gc_flags - Initialization flags for generic irq chips
///  @IRQ_GC_INIT_MASK_CACHE: Initialize the mask_cache by reading mask reg
///  @IRQ_GC_INIT_NESTED_LOCK:    Set the lock class of the irqs to nested for
///               irq chips which need to call irq_set_wake() on
///               the parent irq. Usually GPIO implementations
///  @IRQ_GC_MASK_CACHE_PER_TYPE: Mask cache is chip type private
///  @IRQ_GC_NO_MASK:     Do not calculate irq_data->mask
///  @IRQ_GC_BE_IO:       Use big-endian register accesses (default: LE)
impl IrqDomainChipGeneric {
    const IRQ_GC_INIT_MASK_CACHE: usize = 1 << 0;
    const IRQ_GC_INIT_NESTED_LOCK: usize = 1 << 1;
    const IRQ_GC_MASK_CACHE_PER_TYPE: usize = 1 << 2;
    const IRQ_GC_NO_MASK: usize = 1 << 3;
    const IRQ_GC_BE_IO: usize = 1 << 4;
}

///  struct irq_chip_generic - Generic irq chip data structure
///  @lock:       Lock to protect register and cache data access
///  @reg_base:       Register base address (virtual)
///  @reg_readl:      Alternate I/O accessor (defaults to readl if NULL)
///  @reg_writel:     Alternate I/O accessor (defaults to writel if NULL)
///  @suspend:        Function called from core code on suspend once per
///           chip can be useful instead of irq_chip::suspend to
///           handle chip details even when no interrupts are in use
///  @resume:     Function called from core code on resume once per chip
///           can be useful instead of irq_chip::suspend to handle
///           chip details even when no interrupts are in use
///  @irq_base:       Interrupt base nr for this chip
///  @irq_cnt:        Number of interrupts handled by this chip
///  @mask_cache:     Cached mask register shared between all chip types
///  @type_cache:     Cached type register
///  @polarity_cache: Cached polarity register
///  @wake_enabled:   Interrupt can wakeup from suspend
///  @wake_active:    Interrupt is marked as an wakeup from suspend source
///  @num_ct:     Number of available irq_chip_type instances (usually 1)
///  @private:        Private data for non generic chip callbacks
///  @installed:      bitfield to denote installed interrupts
///  @unused:     bitfield to denote unused interrupts
///  @domain:     irq domain pointer
///  @list:       List head for keeping track of instances
///  @chip_types:     Array of interrupt irq_chip_types
///
///  Note, that irq_chip_generic can have multiple irq_chip_type
///  implementations which can be associated to a particular irq line of
///  an irq_chip_generic instance. That allows to share and protect
///  state in an irq_chip_generic instance when we need to implement
///  different flow mechanisms (level/edge) for it.
pub struct IrqChipGeneric {
    /*TODO impl fn u32 (*reg_readl)(void __iomem *addr);
    void (*reg_writel)(u32 val, void __iomem *addr);
    void (*suspend)(struct irq_chip_generic *gc);
    void (*resume)(struct irq_chip_generic *gc);*/
    lock: RawSpinlockT,
    reg_base: *mut u32,
    irq_base: u32,
    irq_cnt: u32,
    mask_cache: u32,
    type_cache: u32,
    polarity_cache: u32,
    wake_enabled: u32,
    wake_active: u32,
    num_ct: u32,
    private: *mut u32,
    installed: u64,
    unused: u64,
    domain: IrqDomain,
    list: ListHead,
    hip_types: [IrqChipType; 10],
}

///  struct irq_chip_type - Generic interrupt chip instance for a flow type
///  @chip:       The real interrupt chip which provides the callbacks
///  @regs:       Register offsets for this chip
///  @handler:        Flow handler associated with this chip
///  @type:       Chip can handle these flow types
///  @mask_cache_priv:    Cached mask register private to the chip type
///  @mask_cache:     Pointer to cached mask register
///
///  A irq_generic_chip can have several instances of irq_chip_type when
///  it requires different functions and register offsets for different
///  flow types.
pub struct IrqChipType {
    chip: IrqChip,
    regs: IrqChipRegs,
    // TODO impl fn handler: IrqFlowHandlerT,
    type_: u32,
    mask_cache_priv: u32,
    mask_cache: *mut u32,
}

///  struct irq_chip - hardware interrupt chip descriptor
///
///  @name:       name for /proc/interrupts
///  @irq_startup:    start up the interrupt (defaults to ->enable if NULL)
///  @irq_shutdown:   shut down the interrupt (defaults to ->disable if NULL)
///  @irq_enable:     enable the interrupt (defaults to chip->unmask if NULL)
///  @irq_disable:    disable the interrupt
///  @irq_ack:        start of a new interrupt
///  @irq_mask:       mask an interrupt source
///  @irq_mask_ack:   ack and mask an interrupt source
///  @irq_unmask:     unmask an interrupt source
///  @irq_eoi:        end of interrupt
///  @irq_set_affinity:   Set the CPU affinity on SMP machines. If the force
///           argument is true, it tells the driver to
///           unconditionally apply the affinity setting. Sanity
///           checks against the supplied affinity mask are not
///           required. This is used for CPU hotplug where the
///           target CPU is not yet set in the cpu_online_mask.
///  @irq_retrigger:  resend an IRQ to the CPU
///  @irq_set_type:   set the flow type (IRQ_TYPE_LEVEL/etc.) of an IRQ
///  @irq_set_wake:   enable/disable power-management wake-on of an IRQ
///  @irq_bus_lock:   function to lock access to slow bus (i2c) chips
///  @irq_bus_sync_unlock:function to sync and unlock slow bus (i2c) chips
///  @irq_cpu_online: configure an interrupt source for a secondary CPU
///  @irq_cpu_offline:    un-configure an interrupt source for a secondary CPU
///  @irq_suspend:    function called from core code on suspend once per
///           chip, when one or more interrupts are installed
///  @irq_resume:     function called from core code on resume once per chip,
///           when one ore more interrupts are installed
///  @irq_pm_shutdown:    function called from core code on shutdown once per chip
///  @irq_calc_mask:  Optional function to set irq_data.mask for special cases
///  @irq_print_chip: optional to print special chip info in show_interrupts
///  @irq_request_resources:  optional to request resources before calling
///               any other callback related to this irq
///  @irq_release_resources:  optional to release resources acquired with
///               irq_request_resources
///  @irq_compose_msi_msg:    optional to compose message content for MSI
///  @irq_write_msi_msg:  optional to write message content for MSI
///  @irq_get_irqchip_state:  return the internal state of an interrupt
///  @irq_set_irqchip_state:  set the internal state of a interrupt
///  @irq_set_vcpu_affinity:  optional to target a vCPU in a virtual machine
///  @ipi_send_single:    send a single IPI to destination cpus
///  @ipi_send_mask:  send an IPI to destination cpus in cpumask
///  @irq_nmi_setup:  function called from core code before enabling an NMI
///  @irq_nmi_teardown:   function called from core code after disabling an NMI
///  @flags:      chip specific flags
struct IrqChip {
    name: *const char,
    /*TODO impl fn unsigned int    (*irq_startup)(struct irq_data *data);
        void        (*irq_shutdown)(struct irq_data *data);
        void        (*irq_enable)(struct irq_data *data);
        void        (*irq_disable)(struct irq_data *data);
        void        (*irq_ack)(struct irq_data *data);
        void        (*irq_mask)(struct irq_data *data);
        void        (*irq_mask_ack)(struct irq_data *data);
        void        (*irq_unmask)(struct irq_data *data);
        void        (*irq_eoi)(struct irq_data *data);
        int     (*irq_set_affinity)(struct irq_data *data, const struct cpumask *dest, bool force);
        int     (*irq_retrigger)(struct irq_data *data);
        int     (*irq_set_type)(struct irq_data *data, unsigned int flow_type);
        int     (*irq_set_wake)(struct irq_data *data, unsigned int on);
        void        (*irq_bus_lock)(struct irq_data *data);
        void        (*irq_bus_sync_unlock)(struct irq_data *data);
    #ifdef CONFIG_DEPRECATED_IRQ_CPU_ONOFFLINE
        void        (*irq_cpu_online)(struct irq_data *data);
        void        (*irq_cpu_offline)(struct irq_data *data);
    #endif
        void        (*irq_suspend)(struct irq_data *data);
        void        (*irq_resume)(struct irq_data *data);
        void        (*irq_pm_shutdown)(struct irq_data *data);
        void        (*irq_calc_mask)(struct irq_data *data);
        void        (*irq_print_chip)(struct irq_data *data, struct seq_file *p);
        int     (*irq_request_resources)(struct irq_data *data);
        void        (*irq_release_resources)(struct irq_data *data);
        void        (*irq_compose_msi_msg)(struct irq_data *data, struct msi_msg *msg);
        void        (*irq_write_msi_msg)(struct irq_data *data, struct msi_msg *msg);
        int     (*irq_get_irqchip_state)(struct irq_data *data, enum irqchip_irq_state which, bool *state);
        int     (*irq_set_irqchip_state)(struct irq_data *data, enum irqchip_irq_state which, bool state);
        int     (*irq_set_vcpu_affinity)(struct irq_data *data, void *vcpu_info);
        void        (*ipi_send_single)(struct irq_data *data, unsigned int cpu);
        void        (*ipi_send_mask)(struct irq_data *data, const struct cpumask *dest);
        int     (*irq_nmi_setup)(struct irq_data *data);
        void        (*irq_nmi_teardown)(struct irq_data *data);*/
    flags: u64,
}

///  struct irq_chip_regs - register offsets for struct irq_gci
///  @enable: Enable register offset to reg_base
///  @disable:    Disable register offset to reg_base
///  @mask:   Mask register offset to reg_base
///  @ack:    Ack register offset to reg_base
///  @eoi:    Eoi register offset to reg_base
///  @type:   Type configuration register offset to reg_base
///  @polarity:   Polarity configuration register offset to reg_base
pub struct IrqChipRegs {
    enable: u64,
    disable: u64,
    mask: u64,
    ack: u64,
    eoi: u64,
    type_: u64,
    polarity: u64,
}

///  struct irq_data - per irq chip data passed down to chip functions
///  @mask:       precomputed bitmask for accessing the chip registers
///  @irq:        interrupt number
///  @hwirq:      hardware interrupt number, local to the interrupt domain
///  @common:     point to data shared by all irqchips
///  @chip:       low level interrupt hardware access
///  @domain:     Interrupt translation domain responsible for mapping
///           between hwirq number and linux irq number.
///  @parent_data:    pointer to parent struct irq_data to support hierarchy
///           irq_domain
///  @chip_data:      platform-specific per-chip private data for the chip
///           methods, to allow shared chip implementations
pub struct IrqData {
    mask: u32,
    irq: u32,
    hwirq: u64,
    common: *mut IrqCommonData,
    chip: *mut IrqChip,
    domain: *mut IrqDomain,
    chip_data: *mut *mut u32,
    #[cfg(feature = "CONFIG_IRQ_DOMAIN_HIERARCHY")]
    parent_data: *mut IrqData,
}

///  struct irq_common_data - per irq data shared by all irqchips
///  @state_use_accessors: status information for irq chip functions.
///           Use accessor functions to deal with it
///  @node:       node index useful for balancing
///  @handler_data:   per-IRQ data for the irq_chip methods
///  @affinity:       IRQ affinity on SMP. If this is an IPI
///           related irq, then this is the mask of the
///           CPUs to which an IPI can be sent.
///  @effective_affinity: The effective IRQ affinity on SMP as some irq
///           chips do not allow multi CPU destinations.
///           A subset of @affinity.
///  @msi_desc:       MSI descriptor
///  @ipi_offset:     Offset of first IPI target cpu in @affinity. Optional.
struct IrqCommonData {
    state_use_accessors: u32,
    handler_data: *mut u32,
    msi_desc: *mut MsiDesc,
    #[cfg(feature = "CONFIG_NUMA")]
    node: u32,
    #[cfg(feature = "CONFIG_SMP")]
    affinity: CpumaskVarT,
    #[cfg(feature = "CONFIG_GENERIC_IRQ_EFFECTIVE_AFF_MASK")]
    effective_affinity: CpumaskVarT,
    #[cfg(feature = "CONFIG_GENERIC_IRQ_IPI")]
    ipi_offset: u32,
}
