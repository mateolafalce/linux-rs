/**************************************************************************
 *   sfp.rs  --  This file is part of linux-rs.                           *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

/// struct sfp_eeprom_id - raw SFP module identification information
/// @base: base SFP module identification structure
/// @ext: extended SFP module identification structure
///
/// See the SFF-8472 specification and related documents for the definition
/// of these structure members. This can be obtained from
/// https://www.snia.org/technology-communities/sff/specifications
pub struct SfpEepromId {
    base: SfpEepromBase,
    ext: SfpEepromExt,
}

struct SfpEepromExt {
    options: u16,
    br_max: u8,
    br_min: u8,
    vendor_sn: [char; 16],
    datecode: [char; 8],
    diagmon: u8,
    enhopts: u8,
    sff8472_compliance: u8,
    cc_ext: u8,
}

struct SfpEepromBase {
    phys_id: u8,
    phys_ext_id: u8,
    connector: u8,
    reserved62: u8,
    cc_base: u8,
}
/*TODO in SfpEepromBase
#if defined __BIG_ENDIAN_BITFIELD
    u8 e10g_base_er:1;
    u8 e10g_base_lrm:1;
    u8 e10g_base_lr:1;
    u8 e10g_base_sr:1;
    u8 if_1x_sx:1;
    u8 if_1x_lx:1;
    u8 if_1x_copper_active:1;
    u8 if_1x_copper_passive:1;

    u8 escon_mmf_1310_led:1;
    u8 escon_smf_1310_laser:1;
    u8 sonet_oc192_short_reach:1;
    u8 sonet_reach_bit1:1;
    u8 sonet_reach_bit2:1;
    u8 sonet_oc48_long_reach:1;
    u8 sonet_oc48_intermediate_reach:1;
    u8 sonet_oc48_short_reach:1;

    u8 unallocated_5_7:1;
    u8 sonet_oc12_smf_long_reach:1;
    u8 sonet_oc12_smf_intermediate_reach:1;
    u8 sonet_oc12_short_reach:1;
    u8 unallocated_5_3:1;
    u8 sonet_oc3_smf_long_reach:1;
    u8 sonet_oc3_smf_intermediate_reach:1;
    u8 sonet_oc3_short_reach:1;

    u8 e_base_px:1;
    u8 e_base_bx10:1;
    u8 e100_base_fx:1;
    u8 e100_base_lx:1;
    u8 e1000_base_t:1;
    u8 e1000_base_cx:1;
    u8 e1000_base_lx:1;
    u8 e1000_base_sx:1;

    u8 fc_ll_v:1;
    u8 fc_ll_s:1;
    u8 fc_ll_i:1;
    u8 fc_ll_l:1;
    u8 fc_ll_m:1;
    u8 fc_tech_sa:1;
    u8 fc_tech_lc:1;
    u8 fc_tech_electrical_inter_enclosure:1;

    u8 fc_tech_electrical_intra_enclosure:1;
    u8 fc_tech_sn:1;
    u8 fc_tech_sl:1;
    u8 fc_tech_ll:1;
    u8 sfp_ct_active:1;
    u8 sfp_ct_passive:1;
    u8 unallocated_8_1:1;
    u8 unallocated_8_0:1;

    u8 fc_media_tw:1;
    u8 fc_media_tp:1;
    u8 fc_media_mi:1;
    u8 fc_media_tv:1;
    u8 fc_media_m6:1;
    u8 fc_media_m5:1;
    u8 unallocated_9_1:1;
    u8 fc_media_sm:1;

    u8 fc_speed_1200:1;
    u8 fc_speed_800:1;
    u8 fc_speed_1600:1;
    u8 fc_speed_400:1;
    u8 fc_speed_3200:1;
    u8 fc_speed_200:1;
    u8 unallocated_10_1:1;
    u8 fc_speed_100:1;
#elif defined __LITTLE_ENDIAN_BITFIELD
    u8 if_1x_copper_passive:1;
    u8 if_1x_copper_active:1;
    u8 if_1x_lx:1;
    u8 if_1x_sx:1;
    u8 e10g_base_sr:1;
    u8 e10g_base_lr:1;
    u8 e10g_base_lrm:1;
    u8 e10g_base_er:1;

    u8 sonet_oc3_short_reach:1;
    u8 sonet_oc3_smf_intermediate_reach:1;
    u8 sonet_oc3_smf_long_reach:1;
    u8 unallocated_5_3:1;
    u8 sonet_oc12_short_reach:1;
    u8 sonet_oc12_smf_intermediate_reach:1;
    u8 sonet_oc12_smf_long_reach:1;
    u8 unallocated_5_7:1;

    u8 sonet_oc48_short_reach:1;
    u8 sonet_oc48_intermediate_reach:1;
    u8 sonet_oc48_long_reach:1;
    u8 sonet_reach_bit2:1;
    u8 sonet_reach_bit1:1;
    u8 sonet_oc192_short_reach:1;
    u8 escon_smf_1310_laser:1;
    u8 escon_mmf_1310_led:1;

    u8 e1000_base_sx:1;
    u8 e1000_base_lx:1;
    u8 e1000_base_cx:1;
    u8 e1000_base_t:1;
    u8 e100_base_lx:1;
    u8 e100_base_fx:1;
    u8 e_base_bx10:1;
    u8 e_base_px:1;

    u8 fc_tech_electrical_inter_enclosure:1;
    u8 fc_tech_lc:1;
    u8 fc_tech_sa:1;
    u8 fc_ll_m:1;
    u8 fc_ll_l:1;
    u8 fc_ll_i:1;
    u8 fc_ll_s:1;
    u8 fc_ll_v:1;

    u8 unallocated_8_0:1;
    u8 unallocated_8_1:1;
    u8 sfp_ct_passive:1;
    u8 sfp_ct_active:1;
    u8 fc_tech_ll:1;
    u8 fc_tech_sl:1;
    u8 fc_tech_sn:1;
    u8 fc_tech_electrical_intra_enclosure:1;

    u8 fc_media_sm:1;
    u8 unallocated_9_1:1;
    u8 fc_media_m5:1;
    u8 fc_media_m6:1;
    u8 fc_media_tv:1;
    u8 fc_media_mi:1;
    u8 fc_media_tp:1;
    u8 fc_media_tw:1;

    u8 fc_speed_100:1;
    u8 unallocated_10_1:1;
    u8 fc_speed_200:1;
    u8 fc_speed_3200:1;
    u8 fc_speed_400:1;
    u8 fc_speed_1600:1;
    u8 fc_speed_800:1;
    u8 fc_speed_1200:1;
#else
#error Unknown Endian
#endif
    u8 encoding;
    u8 br_nominal;
    u8 rate_id;
    u8 link_len[6];
    char vendor_name[16];
    u8 extended_cc;
    char vendor_oui[3];
    char vendor_pn[16];
    char vendor_rev[4];
    union {
        __be16 optical_wavelength;
        __be16 cable_compliance;
        struct {
#if defined __BIG_ENDIAN_BITFIELD
            u8 reserved60_2:6;
            u8 fc_pi_4_app_h:1;
            u8 sff8431_app_e:1;
            u8 reserved61:8;
#elif defined __LITTLE_ENDIAN_BITFIELD
            u8 sff8431_app_e:1;
            u8 fc_pi_4_app_h:1;
            u8 reserved60_2:6;
            u8 reserved61:8;
#else
#error Unknown Endian
#endif
        } __packed passive;
        struct {
#if defined __BIG_ENDIAN_BITFIELD
            u8 reserved60_4:4;
            u8 fc_pi_4_lim:1;
            u8 sff8431_lim:1;
            u8 fc_pi_4_app_h:1;
            u8 sff8431_app_e:1;
            u8 reserved61:8;
#elif defined __LITTLE_ENDIAN_BITFIELD
            u8 sff8431_app_e:1;
            u8 fc_pi_4_app_h:1;
            u8 sff8431_lim:1;
            u8 fc_pi_4_lim:1;
            u8 reserved60_4:4;
            u8 reserved61:8;
#else
#error Unknown Endian
#endif
        } __packed active;
    } __packed;
*/
