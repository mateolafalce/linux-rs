/**************************************************************************
 *   types.rs  --  This file is part of linux-rs.                         *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use core::ffi::c_void;

use crate::include::uapi::asm_generic::posix_types::{
    __KernelClockidT, __KernelGid32T, __KernelUid32T,
};

pub type SizeT = usize;
pub type UidT = __KernelUid32T;
pub type GidT = __KernelGid32T;
pub type ClockidT = __KernelClockidT;
pub type RcuHead = CallbackHead;
pub type FmodeT = u32;
pub type SlabFlagsT = u32;
/// The type of an index into the pagecache.
pub type PgoffT = u64;
pub type BlkcntT = u64;
/// The type used for indexing onto a disc or disc partition.
/// Linux-rs always considers sectors to be 512 bytes long independently
/// of the devices real block size.
/// blkcnt_t is the type of the inode's block count.
pub type SectorT = u64;
pub type NlinkT = u32;
pub type LoffT = u64;
pub type UmodeT = u16;

pub struct RcurefT {
    refcnt: AtomicT,
}

pub struct AtomicT {
    counter: i32,
}
pub struct Atomic64T {
    counter: i64,
}
pub struct AtomicLongT {
    counter: i64,
}
pub struct ListHead {
    next: *mut ListHead,
    prev: *mut ListHead,
}

pub struct HlistHead {
    first: *mut HlistNode,
}

pub struct HlistNode {
    pprev: *mut HlistNode,
    next: *mut HlistNode,
}
/// struct CallbackHead - callback structure for use with RCU and task_work
/// @next: next update requests in a list
/// @func: actual update function to call after the grace period.
///
/// The struct is aligned to size of pointer. On most architectures it happens
/// naturally due ABI requirements, but some architectures (like CRIS) have
/// weird ABI and we need to ask it explicitly.
///
/// The alignment is required to guarantee that bit 0 of @next will be
/// clear under normal conditions -- as long as we use call_rcu() or
/// call_srcu() to queue the callback.
///
/// This guarantee is important for few reasons:
///  - future call_rcu_lazy() will make use of lower bits in the pointer;
///  - the structure shares storage space in struct page with @compound_head,
///    which encode PageTail() in bit 0. The guarantee is needed to avoid
///    false-positive PageTail().
struct CallbackHead {
    //TODO impl fn next: *mut CallbackHead,
    //TODO impl fn func: fn(&mut CallbackHead),
}

impl CallbackHead {
    fn new() -> Self {
        CallbackHead {}
    }
}

impl HlistNode {
    pub fn new() -> Self {
        Self {
            pprev: core::ptr::null_mut(),
            next: core::ptr::null_mut(),
        }
    }
}

impl AtomicT {
    pub fn new(n: i32) -> Self {
        AtomicT { counter: n }
    }
}
