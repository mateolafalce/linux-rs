/**************************************************************************
 *   bvec.rs  --  This file is part of linux-rs.                          *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{mm_types::Page, types::SectorT};

pub struct BvecIter {
    /// device address in 512 byte sectors
    bi_sector: SectorT,
    /// residual I/O count
    bi_size: u32,
    /// current index into bvl_vec
    bi_idx: u32,
    /// number of bytes completed in current bvec
    bi_bvec_done: u32,
}

/// struct bio_vec - a contiguous range of physical memory addresses
/// @bv_page:   First page associated with the address range.
/// @bv_len:    Number of bytes in the address range.
/// @bv_offset: Start of the address range relative to the start of @bv_page.
///
/// The following holds for a bvec if n * PAGE_SIZE < bv_offset + bv_len:
///
///   nth_page(@bv_page, n) == @bv_page + n
///
/// This holds because page_is_mergeable() checks the above property.
pub struct BioVec {
    bv_page: *mut Page,
    bv_len: u32,
    bv_offset: u32,
}
