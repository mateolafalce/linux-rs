/**************************************************************************
 *   percpu_counter.rs  --  This file is part of linux-rs.                *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

#[cfg(feature = "CONFIG_SMP")]
use crate::include::linux::spinlock_types_raw::RawSpinlockT;

#[cfg(feature = "CONFIG_HOTPLUG_CPU")]
use crate::include::linux::types::ListHead;

#[cfg(feature = "CONFIG_SMP")]
pub struct PercpuCounter {
    lock: RawSpinlockT,
    count: i64,
    /// All percpu_counters are on a list
    #[cfg(feature = "CONFIG_HOTPLUG_CPU")]
    list: ListHead,
    counters: *mut i32,
}

#[cfg(not(feature = "CONFIG_SMP"))]
pub struct PercpuCounter {
    count: i64,
}
