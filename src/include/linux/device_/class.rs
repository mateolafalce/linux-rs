/**************************************************************************
 *   class.rs  --  This file is part of linux-rs.                         *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::sysfs::AttributeGroup;

/// struct class - device classes
/// @name:    Name of the class.
/// @class_groups: Default attributes of this class.
/// @dev_groups:    Default attributes of the devices that belong to the class.
/// @dev_uevent:    Called when a device is added, removed from this class, or a
///        few other things that generate uevents to add the environment
///        variables.
/// @devnode:    Callback to provide the devtmpfs.
/// @class_release: Called to release this class.
/// @dev_release: Called to release the device.
/// @shutdown_pre: Called at shut-down time before driver shutdown.
/// @ns_type:    Callbacks so sysfs can detemine namespaces.
/// @namespace: Namespace of the device belongs to this class.
/// @get_ownership: Allows class to specify uid/gid of the sysfs directories
///        for the devices belonging to the class. Usually tied to
///        device's namespace.
/// @pm:    The default device power management operations of this class.
/// @p:    The private data of the driver core, no one other than the
///        driver core can touch this.
///
/// A class is a higher-level view of a device that abstracts out low-level
/// implementation details. Drivers may see a SCSI disk or an ATA disk, but,
/// at the class level, they are all simply disks. Classes allow user space
/// to work with devices based on what they do, rather than how they are
/// connected or how they work.
pub struct Class {
    name: *const char,
    class_groups: *const AttributeGroup,
    dev_groups: *const AttributeGroup,
}

/*
TODO: impl fn
    int (*dev_uevent)(const struct device *dev, struct kobj_uevent_env *env);
    char *(*devnode)(const struct device *dev, umode_t *mode);
    void (*class_release)(const struct class *class);
    void (*dev_release)(struct device *dev);
    int (*shutdown_pre)(struct device *dev);
    const struct kobj_ns_type_operations *ns_type;
    const void *(*namespace)(const struct device *dev);
    void (*get_ownership)(const struct device *dev, kuid_t *uid, kgid_t *gid);
    const struct dev_pm_ops *pm;
};
*/
