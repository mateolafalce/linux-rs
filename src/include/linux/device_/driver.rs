/**************************************************************************
 *   driver.rs  --  This file is part of linux-rs.                        *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::{
    drivers::base::base::DriverPrivate,
    include::linux::{
        device_::bus::BusType,
        mod_devicetable::{AcpiDeviceId, OfDeviceId},
        module::Module,
        sysfs::AttributeGroup,
    },
};

/// struct device_driver - The basic device driver structure
/// @name: Name of the device driver.
/// @bus: The bus which the device of this driver belongs to.
/// @owner: The module owner.
/// @mod_name: Used for built-in modules.
/// @suppress_bind_attrs: Disables bind/unbind via sysfs.
/// @probe_type:Type of the probe (synchronous or asynchronous) to use.
/// @of_match_table: The open firmware table.
/// @acpi_match_table: The ACPI match table.
/// @probe: Called to query the existence of a specific device,
/// whether this driver can work with it, and bind the driver
/// to a specific device.
/// @sync_state:Called to sync device state to software state after all the
/// state tracking consumers linked to this device (present at
/// the time of late_initcall) have successfully bound to a
/// driver. If the device has no consumers, this function will
/// be called at late_initcall_sync level. If the device has
/// consumers that are never bound to a driver, this function
/// will never get called until they do.
/// @remove:Called when the device is removed from the system to
/// unbind a device from this driver.
/// @shutdown:Called at shut-down time to quiesce the device.
/// @suspend:Called to put the device to sleep mode. Usually to a
/// low power state.
/// @resume:Called to bring a device from sleep mode.
/// @groups:Default attributes that get created by the driver core
/// automatically.
/// @dev_groups:Additional attributes attached to device instance once
/// it is bound to the driver.
/// @pm:Power management operations of the device which matched
/// this driver.
/// @coredump:Called when sysfs entry is written to. The device driver
/// is expected to call the dev_coredump API resulting in a
/// uevent.
/// @p:Driver core's private data, no one other than the driver
/// core can touch this.
///
/// The device driver-model tracks all of the drivers known to the system.
/// The main reason for this tracking is to enable the driver core to match
/// up drivers with new devices. Once drivers are known objects within the
/// system, however, a number of other things become possible. Device drivers
/// can export information and configuration variables that are independent
/// of any specific device.
pub struct DeviceDriver {
    name: *const char,
    bus: *const BusType,
    owner: *mut Module,
    /// used for built-in modules
    mod_name: *const char,
    /// disables bind/unbind via sysfs
    suppress_bind_attrs: bool,
    of_match_table: *const OfDeviceId,
    acpi_match_table: *const AcpiDeviceId,
    groups: *const AttributeGroup,
    dev_groups: *const AttributeGroup,
    p: *mut DriverPrivate,
}
/*
TODO: impl fns
    int (*probe) (struct device *dev);
    void (*sync_state)(struct device *dev);
    int (*remove) (struct device *dev);
    void (*shutdown) (struct device *dev);
    int (*suspend) (struct device *dev, pm_message_t state);
    int (*resume) (struct device *dev);
    const struct dev_pm_ops *pm;
    void (*coredump) (struct device *dev);
};
*/

impl DeviceDriver {
    const PROBE_TYPE_U: usize = 0;
    const PROBE_TYPE_S: usize = 1;
    const PROBE_TYPE_X: usize = 2;
    const PROBE_TYPE_STRING: usize = 3;
    const PROBE_TYPE_BITFIELD: usize = 4;
    const PROBE_TYPE_END: usize = 5;
}
