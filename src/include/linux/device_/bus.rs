/**************************************************************************
 *   bus.rs  --  This file is part of linux-rs.                           *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::sysfs::AttributeGroup;

/// struct bus_type - The bus type of the device
///
/// @name:      The name of the bus.
/// @dev_name:  Used for subsystems to enumerate devices like ("foo%u", dev->id).
/// @bus_groups:    Default attributes of the bus.
/// @dev_groups:    Default attributes of the devices on the bus.
/// @drv_groups:    Default attributes of the device drivers on the bus.
/// @match:     Called, perhaps multiple times, whenever a new device or driver
///         is added for this bus. It should return a positive value if the
///         given device can be handled by the given driver and zero
///         otherwise. It may also return error code if determining that
///         the driver supports the device is not possible. In case of
///         -EPROBE_DEFER it will queue the device for deferred probing.
/// @uevent:    Called when a device is added, removed, or a few other things
///         that generate uevents to add the environment variables.
/// @probe:     Called when a new device or driver add to this bus, and callback
///         the specific driver's probe to initialize the matched device.
/// @sync_state:    Called to sync device state to software state after all the
///         state tracking consumers linked to this device (present at
///         the time of late_initcall) have successfully bound to a
///         driver. If the device has no consumers, this function will
///         be called at late_initcall_sync level. If the device has
///         consumers that are never bound to a driver, this function
///         will never get called until they do.
/// @remove:    Called when a device removed from this bus.
/// @shutdown:  Called at shut-down time to quiesce the device.
///
/// @online:    Called to put the device back online (after offlining it).
/// @offline:   Called to put the device offline for hot-removal. May fail.
///
/// @suspend:   Called when a device on this bus wants to go to sleep mode.
/// @resume:    Called to bring a device on this bus out of sleep mode.
/// @num_vf:    Called to find out how many virtual functions a device on this
///         bus supports.
/// @dma_configure: Called to set up DMA configuration on a device on
///         this bus.
/// @dma_cleanup:   Called to clean up DMA configuration on a device on
///         this bus.
/// @pm:        Power management operations of this bus, callback the specific
///         device driver's pm-ops.
/// @iommu_ops:  IOMMU specific operations for this bus, used to attach IOMMU
///              driver implementations to a bus and allow the driver to do
///              bus-specific setup
/// @need_parent_lock: When probing or removing a device on this bus, the
///                   device core should lock the device's parent.
///
/// A bus is a channel between the processor and one or more devices. For the
/// purposes of the device model, all devices are connected via a bus, even if
/// it is an internal, virtual, "platform" bus. Buses can plug into each other.
/// A USB controller is usually a PCI device, for example. The device model
/// represents the actual connections between buses and the devices they control.
/// A bus is represented by the bus_type structure. It contains the name, the
/// default attributes, the bus' methods, PM operations, and the driver core's
/// private data.
pub struct BusType {
    name: *const char,
    dev_name: *const char,
    bus_groups: *const AttributeGroup,
    dev_groups: *const AttributeGroup,
    drv_groups: *const AttributeGroup,
    need_parent_lock: bool,
}
/*
TODO: impl fn
    int (*match)(struct device *dev, struct device_driver *drv);
    int (*uevent)(const struct device *dev, struct kobj_uevent_env *env);
    int (*probe)(struct device *dev);
    void (*sync_state)(struct device *dev);
    void (*remove)(struct device *dev);
    void (*shutdown)(struct device *dev);
    int (*online)(struct device *dev);
    int (*offline)(struct device *dev);
    int (*suspend)(struct device *dev, pm_message_t state);
    int (*resume)(struct device *dev);
    int (*num_vf)(struct device *dev);
    int (*dma_configure)(struct device *dev);
    void (*dma_cleanup)(struct device *dev);
    const struct dev_pm_ops *pm;
    const struct iommu_ops *iommu_ops;
};
*/
