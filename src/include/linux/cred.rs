/**************************************************************************
 *   cred.rs  --  This file is part of linux-rs.                          *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{
    capability::KernelCapT,
    refcount::RefCountT,
    sched_::user::UserStruct,
    types::{AtomicLongT, RcuHead},
    uidgid_types::{KgidT, KuidT},
    user_namespace::{Ucounts, UserNamespace},
};

/// The security context of a task
///
/// The parts of the context break down into two categories:
///
///   (1) The objective context of a task.  These parts are used when some other
///       task is attempting to affect this one.
///
///   (2) The subjective context.  These details are used when the task is acting
///       upon another object, be that a file, a task, a key, or whatever.
///
/// Note that some members of this structure belong to both categories - the
/// LSM security pointer, for instance.
///
/// A task has two security pointers.  task->real_cred points to the objective
/// context that defines that task's actual details.  The objective part of this
/// context is used whenever that task is acted upon.
///
/// task->cred points to the subjective context that defines the details of how
/// that task is going to act upon another object.  This may be overridden
/// temporarily to point to another security context but normally points to the
/// same context as task->real_cred.
pub struct Cred {
    usage: AtomicLongT,
    /// real UID of the task
    uid: KuidT,
    /// real GID of the task
    gid: KgidT,
    /// saved UID of the task
    suid: KuidT,
    /// saved GID of the task
    sgid: KgidT,
    /// effective UID of the task
    euid: KuidT,
    /// effective GID of the task
    egid: KgidT,
    /// UID for VFS ops
    fsuid: KuidT,
    /// GID for VFS ops
    fsgid: KgidT,
    /// SUID-less security management
    securebits: usize,
    /// caps our children can inherit
    cap_inheritable: KernelCapT,
    /// caps we're permitted
    cap_permitted: KernelCapT,
    /// caps we can actually use
    cap_effective: KernelCapT,
    /// capability bounding set
    cap_bset: KernelCapT,
    /// Ambient capability set
    cap_ambient: KernelCapT,
    /// default keyring to attach requested keys to
    #[cfg(feature = "CONFIG_KEYS")]
    jit_keyring: u8,
    /// keyring inherited over fork
    #[cfg(feature = "CONFIG_KEYS")]
    session_keyring: *mut Key,
    /// keyring private to this process
    #[cfg(feature = "CONFIG_KEYS")]
    process_keyring: *mut Key,
    /// keyring private to this thread
    #[cfg(feature = "CONFIG_KEYS")]
    thread_keyring: *mut Key,
    /// assumed request_key authority
    #[cfg(feature = "CONFIG_KEYS")]
    request_key_auth: Key,
    /// LSM security
    #[cfg(feature = "CONFIG_SECURITY")]
    security: *mut *mut u32,
    /// real user ID subscription
    user: *mut UserStruct,
    /// user_ns the caps and keyrings are relative to.
    user_ns: *mut UserNamespace,
    ucounts: *mut Ucounts,
    /// supplementary groups for euid/fsgid
    group_info: *mut GroupInfo,
    /// RCU deletion
    /// Can we skip RCU deletion?
    non_rcu: i32,
    /// RCU deletion hook
    rcu: RcuHead,
}

/// COW Supplementary groups list
struct GroupInfo {
    usage: RefCountT,
    ngroups: i32,
    gid: [KgidT; 10],
}
