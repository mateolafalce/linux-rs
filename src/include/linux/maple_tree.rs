/**************************************************************************
 *   maple_tree.rs  --  This file is part of linux-rs.                    *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::spinlock_types::SpinlockT;

/// If the tree contains a single entry at index 0, it is usually stored in
/// tree->ma_root.  To optimise for the page cache, an entry which ends in '00',
/// '01' or '11' is stored in the root, but an entry which ends in '10' will be
/// stored in a node.  Bits 3-6 are used to store enum maple_type.
///
/// The flags are used both to store some immutable information about this tree
/// (set at tree creation time) and dynamic information set under the spinlock.
///
/// Another use of flags are to indicate global states of the tree.  This is the
/// case with the MAPLE_USE_RCU flag, which indicates the tree is currently in
/// RCU mode.  This mode was added to allow the tree to reuse nodes instead of
/// re-allocating and RCU freeing nodes when there is a single user.
pub struct MapleTree {
    ma_lock: SpinlockT,
    ma_external_lock: LockdepMapP,
    ma_flags: i32,
    ma_root: *mut u32,
}

#[cfg(feature = "CONFIG_LOCKDEP")]
type LockdepMapP = *mut LockdepMap;
#[cfg(not(feature = "CONFIG_LOCKDEP"))]
struct LockdepMapP {}
