/**************************************************************************
 *   net.rs  --  This file is part of linux-rs.                           *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::{
    linux::{
        fs::{FasyncStruct, File},
        types::RcuHead,
        wait::WaitQueueHeadT,
    },
    net::sock::Sock,
};

///  struct socket - general BSD socket
///  @state: socket state (%SS_CONNECTED, etc)
///  @type: socket type (%SOCK_STREAM, etc)
///  @flags: socket flags (%SOCK_NOSPACE, etc)
///  @ops: protocol specific socket operations
///  @file: File back pointer for gc
///  @sk: internal networking protocol agnostic socket representation
///  @wq: wait queue for several uses
pub struct Socket {
    type_: i16,
    flags: u32,
    file: *mut File,
    sk: Sock,
    /// Might change with IPV6_ADDRFORM or MPTCP.
    // ops: *const proto_ops TODO: impl fns
    wq: SocketWq,
}

pub struct SocketWq {
    /// Note: wait MUST be first field of socket_wq
    wait: WaitQueueHeadT,
    fasync_list: *mut FasyncStruct,
    /// %SOCKWQ_ASYNC_NOSPACE, etc
    flags: u64,
    rcu: RcuHead,
}

impl Socket {
    /// not allocated
    const SS_FREE: usize = 0;
    /// unconnected to any socket
    const SS_UNCONNECTED: usize = 1;
    /// in process of connecting
    const SS_CONNECTING: usize = 2;
    /// connected to socket
    const SS_CONNECTED: usize = 3;
    /// in process of disconnecting
    const SS_DISCONNECTING: usize = 4;
}
