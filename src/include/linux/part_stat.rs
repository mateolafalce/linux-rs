/**************************************************************************
 *   part_stat.rs  --  This file is part of linux-rs.                     *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::{arch::x86::include::asm::local::LocalT, include::linux::blk_types::NR_STAT_GROUPS};

pub struct DiskStats {
    nsecs: [u64; NR_STAT_GROUPS],
    sectors: [u64; NR_STAT_GROUPS],
    ios: [u64; NR_STAT_GROUPS],
    merges: [u64; NR_STAT_GROUPS],
    io_ticks: u64,
    in_flight: [LocalT; 2],
}
