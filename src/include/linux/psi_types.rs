/**************************************************************************
 *   psi_types.rs  --  This file is part of linux-rs.                     *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

#[cfg(feature = "CONFIG_PSI")]
use crate::include::linux::{
    mutex_types::Mutex,
    sched::TaskStruct,
    seqlock_types::SeqcountT,
    timer::TimerList,
    types::{AtomicT, ListHead},
    wait::WaitQueueHeadT,
    workqueue::DelayedWork,
};

use crate::include::linux::seqlock_types::SeqcountT;

#[cfg(feature = "CONFIG_PSI")]
pub struct PsiGroup {
    parent: *mut PsiGroup,
    enabled: bool,
    /// Protects data used by the aggregator
    avgs_lock: Mutex,
    /// Per-cpu task state & time tracking
    pcpu: *mut PsiGroupCpu,
    /// Running pressure averages
    avg_total: [u64; PsiGroupCpu::NR_PSI_STATES - 1],
    avg_last_update: u64,
    avg_next_update: u64,
    /// Aggregator work control
    avgs_work: DelayedWork,
    /// Unprivileged triggers against N*PSI_FREQ windows
    avg_triggers: ListHead,
    avg_nr_triggers: [u32; PsiGroupCpu::NR_PSI_STATES - 1],
    /// Total stall times and sampled pressure averages
    total: [[u64; PsiGroupCpu::NR_PSI_STATES - 1]; PsiGroupCpu::NR_PSI_AGGREGATORS],
    avg: [[u64; 3]; PsiGroupCpu::NR_PSI_STATES - 1],
    /// Monitor RT polling work control
    rtpoll_task: *mut TaskStruct,
    rtpoll_timer: TimerList,
    rtpoll_wait: WaitQueueHeadT,
    rtpoll_wakeup: AtomicT,
    rtpoll_scheduled: AtomicT,
    /// Protects data used by the monitor
    rtpoll_trigger_lock: Mutex,
    /// Configured RT polling triggers
    rtpoll_triggers: ListHead,
    rtpoll_nr_triggers: [u32; PsiGroupCpu::NR_PSI_STATES - 1],
    rtpoll_states: u32,
    rtpoll_min_period: u64,
    /// Total stall times at the start of RT polling monitor activation
    rtpoll_total: [u64; PsiGroupCpu::NR_PSI_STATES - 1],
    rtpoll_next_update: u64,
    rtpoll_until: u64,
}

#[cfg(not(feature = "CONFIG_PSI"))]
pub struct PsiGroup {}

#[cfg(feature = "CONFIG_PSI")]
pub struct PsiGroupCpu {
    /// 1st cacheline updated by the scheduler
    /// Aggregator needs to know of concurrent changes
    seq: SeqcountT,
    /// States of the tasks belonging to this group
    tasks: [u32; NR_PSI_TASK_COUNTS],
    /// Aggregate pressure state derived from the tasks
    state_mask: u32,
    /// Period time sampling buckets for each state of interest (ns)
    times: [u32; PsiGroupCpu::NR_PSI_STATES],
    /// Time of last task change in this group (rq_clock)
    state_start: u64,
    /// 2nd cacheline updated by the aggregator
    /// Delta detection against the sampling buckets
    times_prev: [[u32; PsiGroupCpu::NR_PSI_STATES]; PsiGroupCpu::NR_PSI_AGGREGATORS],
}

#[cfg(not(feature = "CONFIG_PSI"))]
pub struct PsiGroupCpu {
    /// 1st cacheline updated by the scheduler
    /// Aggregator needs to know of concurrent changes
    seq: SeqcountT,
    /// States of the tasks belonging to this group
    tasks: [u32; PsiGroupCpu::NR_PSI_TASK_COUNTS],
    /// Aggregate pressure state derived from the tasks
    state_mask: u32,
    /// Period time sampling buckets for each state of interest (ns)
    times: [u32; PsiGroupCpu::NR_PSI_STATES],
    /// Time of last task change in this group (rq_clock)
    state_start: u64,
    /// 2nd cacheline updated by the aggregator
    /// Delta detection against the sampling buckets
    times_prev: [[u32; PsiGroupCpu::NR_PSI_STATES]; PsiGroupCpu::NR_PSI_AGGREGATORS],
}

impl PsiGroupCpu {
    ///  Tracked task states
    const NR_IOWAIT: usize = 0;
    const NR_MEMSTALL: usize = 1;
    const NR_RUNNING: usize = 2;
    /// For IO and CPU stalls the presence of running/oncpu tasks
    /// in the domain means a partial rather than a full stall.
    /// For memory it's not so simple because of page reclaimers:
    /// they are running/oncpu while representing a stall. To tell
    /// whether a domain has productivity left or not, we need to
    /// distinguish between regular running (i.e. productive)
    /// threads and memstall ones.
    const NR_MEMSTALL_RUNNING: usize = 3;
    const NR_PSI_TASK_COUNTS: usize = 4;

    /// Pressure states for each resource:
    ///
    /// SOME: Stalled tasks & working tasks
    /// FULL: Stalled tasks & no working tasks
    const PSI_IO_SOME: usize = 0;
    const PSI_IO_FULL: usize = 1;
    const PSI_MEM_SOME: usize = 2;
    const PSI_MEM_FULL: usize = 3;
    const PSI_CPU_SOME: usize = 4;
    const PSI_CPU_FULL: usize = 5;

    #[cfg(feature = "CONFIG_IRQ_TIME_ACCOUNTING")]
    const PSI_IRQ_FULL: usize = 6;
    #[cfg(feature = "CONFIG_IRQ_TIME_ACCOUNTING")]
    const PSI_NONIDLE: usize = 7;
    #[cfg(feature = "CONFIG_IRQ_TIME_ACCOUNTING")]
    const NR_PSI_STATES: usize = 8;

    #[cfg(not(feature = "CONFIG_IRQ_TIME_ACCOUNTING"))]
    const PSI_NONIDLE: usize = 6;
    #[cfg(not(feature = "CONFIG_IRQ_TIME_ACCOUNTING"))]
    const NR_PSI_STATES: usize = 7;

    pub const NR_PSI_RESOURCES: usize = 0;
    const PSI_AVGS: usize = 0;
    const PSI_POLL: usize = 1;
    const NR_PSI_AGGREGATORS: usize = 2;
}
