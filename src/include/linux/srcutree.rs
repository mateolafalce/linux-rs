/**************************************************************************
 *   srcutree.rs  --  This file is part of linux-rs.                      *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{
    completion::Completion,
    lockdep_types::LockdepMap,
    mutex_types::Mutex,
    rcu_node_tree::RCU_NUM_LVLS,
    rcu_segcblist::RcuSegcblist,
    spinlock_types::SpinlockT,
    timer::TimerList,
    types::{AtomicLongT, AtomicT, RcuHead},
    workqueue::DelayedWork,
    workqueue::WorkStruct,
};

/// Per-SRCU-domain structure, similar in function to rcu_state.
pub struct SrcuStruct {
    /// Current rdr array element.
    srcu_idx: u32,
    /// Per-CPU srcu_data array.
    sda: *mut SrcuData,
    dep_map: LockdepMap,
    /// Update-side data.
    srcu_sup: *mut SrcuUsage,
}

///  Per-CPU structure feeding into leaf srcu_node, similar in function
///  to rcu_node.
struct SrcuData {
    srcu_lock_count: AtomicLongT,
    srcu_unlock_count: AtomicLongT,
    srcu_nmi_safety: i32,
    __private: SpinlockT,
    srcu_cblist: RcuSegcblist,
    srcu_gp_seq_needed: u64,
    srcu_gp_seq_needed_exp: u64,
    srcu_cblist_invoking: bool,
    delay_work: TimerList,
    work: WorkStruct,
    srcu_barrier_head: RcuHead,
    mynode: *mut SrcuNode,
    grpmask: u64,
    cpu: i32,
    ssp: *mut SrcuStruct,
}

/// Per-SRCU-domain structure, update-side data linked from srcu_struct.
struct SrcuUsage {
    node: *mut SrcuNode,
    level: *mut [SrcuNode; RCU_NUM_LVLS + 1],
    srcu_size_state: i32,
    srcu_cb_mutex: Mutex,
    lock: SpinlockT,
    srcu_gp_mutex: Mutex,
    srcu_gp_seq: u64,
    srcu_gp_seq_needed: u64,
    srcu_gp_seq_needed_exp: u64,
    srcu_gp_start: u64,
    srcu_last_gp_end: u64,
    srcu_size_jiffies: u64,
    srcu_n_lock_retries: u64,
    srcu_n_exp_nodelay: u64,
    sda_is_static: bool,
    srcu_barrier_seq: u64,
    srcu_barrier_mutex: Mutex,
    srcu_barrier_completion: Completion,
    srcu_barrier_cpu_cnt: AtomicT,
    reschedule_jiffies: u64,
    reschedule_count: u64,
    work: DelayedWork,
    srcu_ssp: *mut SrcuStruct,
}

///  Node in SRCU combining tree, similar in function to rcu_data.
struct SrcuNode {
    lock: SpinlockT,
    // GP seq for children having CBs, but only. if greater than ->srcu_gp_seq.
    srcu_have_cbs: u64,
    // Which srcu_data structs have CBs for given GP?
    srcu_data_have_cbs: u64,
    // Furthest future exp GP.
    srcu_gp_seq_needed_exp: u64,
    // Next up in tree.
    srcu_parent: *mut SrcuNode,
    // Least CPU for node.
    grplo: i32,
    // Biggest CPU for node.
    grphi: i32,
}
