/**************************************************************************
 *   seqlock_types.rs  --  This file is part of linux-rs.                 *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

#[cfg(feature = "CONFIG_DEBUG_LOCK_ALLOC")]
use crate::include::linux::lockdep_types::LockdepMap;

/// Sequence counters (seqcount_t)
///
/// This is the raw counting mechanism, without any writer protection.
///
/// Write side critical sections must be serialized and non-preemptible.
///
/// If readers can be invoked from hardirq or softirq contexts,
/// interrupts or bottom halves must also be respectively disabled before
/// entering the write section.
///
/// This mechanism can't be used if the protected data contains pointers,
/// as the writer can invalidate a pointer that a reader is following.
///
/// If the write serialization mechanism is one of the common kernel
/// locking primitives, use a sequence counter with associated lock
/// (seqcount_LOCKNAME_t) instead.
///
/// If it's desired to automatically handle the sequence counter writer
/// serialization and non-preemptibility requirements, use a sequential
/// lock (seqlock_t) instead.
pub struct SeqcountT {
    sequence: u32,
    #[cfg(feature = "CONFIG_DEBUG_LOCK_ALLOC")]
    dep_map: LockdepMap,
}
