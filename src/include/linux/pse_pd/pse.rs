/**************************************************************************
 *   pse.rs  --  This file is part of linux-rs.                           *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{
    device::Device, module::Module, mutex_types::Mutex, of::DeviceNode,
    regulator::driver::RegulatorDev, types::ListHead,
};

///  struct pse_controller_dev - PSE controller entity that might
///                              provide multiple PSE controls
///  @ops: a pointer to device specific struct pse_controller_ops
///  @owner: kernel module of the PSE controller driver
///  @list: internal list of PSE controller devices
///  @pse_control_head: head of internal list of requested PSE controls
///  @dev: corresponding driver model device struct
///  @of_pse_n_cells: number of cells in PSE line specifiers
///  @nr_lines: number of PSE controls in this controller device
///  @lock: Mutex for serialization access to the PSE controller
///  @types: types of the PSE controller
///  @pi: table of PSE PIs described in this controller device
///  @no_of_pse_pi: flag set if the pse_pis devicetree node is not used
pub struct PseControllerDev {
    owner: *mut Module,
    list: ListHead,
    pse_control_head: ListHead,
    dev: *mut Device,
    of_pse_n_cells: i32,
    nr_lines: u32,
    lock: Mutex,
    pi: *mut PsePi,
    no_of_pse_pi: bool,
}
/*TODO: const struct pse_controller_ops *ops;
enum ethtool_pse_types types;*/

/**
 * struct pse_pi - PSE PI (Power Interface) entity as described in
 *         IEEE 802.3-2022 145.2.4
 *
 * @pairset: table of the PSE PI pinout alternative for the two pairset
 * @np: device node pointer of the PSE PI node
 * @rdev: regulator represented by the PSE PI
 * @admin_state_enabled: PI enabled state
 */
struct PsePi {
    pairset: [PsePiPairset; 2],
    np: *mut DeviceNode,
    rdev: *mut RegulatorDev,
    admin_state_enabled: bool,
}

/**
 * struct pse_pi_pairset - PSE PI pairset entity describing the pinout
 *             alternative ant its phandle
 *
 * @pinout: description of the pinout alternative
 * @np: device node pointer describing the pairset phandle
 */
struct PsePiPairset {
    np: *mut DeviceNode,
}

/* PSE PI pairset pinout can either be Alternative A or Alternative B */
impl PsePiPairset {
    const ALTERNATIVE_A: usize = 0;
    const ALTERNATIVE_B: usize = 1;
}
