/**************************************************************************
 *   rcuwait.rs  --  This file is part of linux-rs.                       *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::sched::TaskStruct;

/// rcuwait provides a way of blocking and waking up a single
/// task in an rcu-safe manner.
///
/// The only time @task is non-nil is when a user is blocked (or
/// checking if it needs to) on a condition, and reset as soon as we
/// know that the condition has succeeded and are awoken.
pub struct Rcuwait {
    task: *mut TaskStruct,
}
