/**************************************************************************
 *   ipv6.rs  --  This file is part of linux-rs.                          *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::{linux::sysctl::CtlTableHeader, uapi::linux::in6::In6Addr};

/// This structure contains configuration options per IPv6 link.
pub struct Ipv6Devconf {
    disable_ipv6: i32,
    hop_limit: i32,
    mtu6: i32,
    forwarding: i32,
    disable_policy: i32,
    proxy_ndp: i32,
    accept_ra: i32,
    accept_redirects: i32,
    autoconf: i32,
    dad_transmits: i32,
    rtr_solicits: i32,
    rtr_solicit_interval: i32,
    rtr_solicit_max_interval: i32,
    rtr_solicit_delay: i32,
    force_mld_version: i32,
    mldv1_unsolicited_report_interval: i32,
    mldv2_unsolicited_report_interval: i32,
    use_tempaddr: i32,
    temp_valid_lft: i32,
    temp_prefered_lft: i32,
    regen_min_advance: i32,
    regen_max_retry: i32,
    max_desync_factor: i32,
    max_addresses: i32,
    accept_ra_defrtr: i32,
    ra_defrtr_metric: u32,
    accept_ra_min_hop_limit: i32,
    accept_ra_min_lft: i32,
    accept_ra_pinfo: i32,
    ignore_routes_with_linkdown: i32,
    drop_unicast_in_l2_multicast: i32,
    accept_dad: i32,
    force_tllao: i32,
    ndisc_notify: i32,
    suppress_frag_ndisc: i32,
    accept_ra_mtu: i32,
    drop_unsolicited_na: i32,
    accept_untracked_na: i32,
    secret: In6Addr,
    use_oif_addrs_only: i32,
    keep_addr_on_down: i32,
    seg6_enabled: i32,
    enhanced_dad: u32,
    addr_gen_mode: u32,
    ndisc_tclass: i32,
    rpl_seg_enabled: i32,
    ioam6_id: u32,
    ioam6_id_wide: u32,
    ioam6_enabled: u8,
    ndisc_evict_nocarrier: u8,
    ra_honor_pio_life: u8,
    ra_honor_pio_pflag: u8,
    sysctl_header: *mut CtlTableHeader,
}

/*TODO
__cacheline_group_begin(ipv6_devconf_read_txrx);
#ifdef CONFIG_IPV6_ROUTER_PREF
 i32 accept_ra_rtr_pref;
 i32 rtr_probe_interval;
#ifdef CONFIG_IPV6_ROUTE_INFO
 i32 accept_ra_rt_info_min_plen;
 i32 accept_ra_rt_info_max_plen;
#endif
#endif
 i32 accept_source_route;
 i32 accept_ra_from_local;
#ifdef CONFIG_IPV6_OPTIMISTIC_DAD
 i32 optimistic_dad;
 i32 use_optimistic;
#endif
#ifdef CONFIG_IPV6_MROUTE
 atomic_t mc_forwarding;
#endif
#ifdef CONFIG_IPV6_SEG6_HMAC
 i32 seg6_require_hmac;
#endif
*/
