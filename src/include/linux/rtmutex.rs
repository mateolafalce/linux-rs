/**************************************************************************
 *   rtmutex.rs  --  This file is part of linux-rs.                       *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{
    rbtree_types::RbRootCached, sched::TaskStruct, spinlock_types_raw::RawSpinlockT,
};

pub struct RtMutexBase {
    wait_lock: RawSpinlockT,
    waiters: RbRootCached,
    owner: *mut TaskStruct,
}

/// The rt_mutex structure
///
/// @wait_lock:  spinlock to protect the structure
/// @waiters:    rbtree root to enqueue waiters in priority order;
///              caches top-waiter (leftmost node).
/// @owner:  the mutex owner
pub struct RtMutex {
    rtmutex: RtMutexBase,
    #[cfg(feature = "CONFIG_DEBUG_LOCK_ALLOC")]
    dep_map: LockdepMap,
}
