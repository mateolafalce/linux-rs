/**************************************************************************
 *   bpf_local_storage.rs  --  This file is part of linux-rs.             *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{
    bpf::BpfMap,
    bpf_mem_alloc::BpfMemAlloc,
    spinlock_types_raw::RawSpinlockT,
    types::{HlistHead, RcuHead},
};
const BPF_LOCAL_STORAGE_CACHE_SIZE: usize = 16;

pub struct BpfLocalStorage {
    cache: [BpfLocalStorageData; BPF_LOCAL_STORAGE_CACHE_SIZE],
    smap: *mut BpfLocalStorageMap,
    /// List of bpf_local_storage_elem
    list: HlistHead,
    /// The object that owns the above "list" of bpf_local_storage_elem.
    owner: *mut u32,
    rcu: RcuHead,
    /// Protect adding/removing from the "list"
    lock: RawSpinlockT,
}

/// smap is used as the searching key when looking up
/// from the object's bpf_local_storage.
///
/// Put it in the same cacheline as the data to minimize
/// the number of cachelines accessed during the cache hit case.
struct BpfLocalStorageData {
    smap: *mut BpfLocalStorageMap,
    data: [u8; 8],
}

/// Thp map is not the primary owner of a bpf_local_storage_elem.
/// Instead, the container object (eg. sk->sk_bpf_storage) is.
///
/// The map (bpf_local_storage_map) is for two purposes
/// 1. Define the size of the "local storage".  It is
///    the map's value_size.
///
/// 2. Maintain a list to keep track of all elems such
///    that they can be cleaned up during the map destruction.
///
/// When a bpf local storage is being looked up for a
/// particular object,  the "bpf_map" pointer is actually used
/// as the "key" to search in the list of elem in
/// the respective bpf_local_storage owned by the object.
///
/// e.g. sk->sk_bpf_storage is the mini-map with the "bpf_map" pointer
/// as the searching key.
struct BpfLocalStorageMap {
    map: BpfMap,
    /// Lookup elem does not require accessing the map.
    ///
    /// Updating/Deleting requires a bucket lock to
    /// link/unlink the elem from the map.  Having
    /// multiple buckets to improve contention.
    buckets: *mut BpfLocalStorageMapBucket,
    bucket_log: u32,
    elem_size: u16,
    cache_idx: u16,
    selem_ma: BpfMemAlloc,
    storage_ma: BpfMemAlloc,
    bpf_ma: bool,
}

struct BpfLocalStorageMapBucket {
    list: HlistHead,
    lock: RawSpinlockT,
}
