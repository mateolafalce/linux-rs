/**************************************************************************
 *   seq_file.rs  --  This file is part of linux-rs.                      *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

#[cfg(feature = "CONFIG_RT_GROUP_SCHED")]
use crate::include::linux::{
    fs::File,
    mutex::Mutex,
    types::{LoffT, SizeT},
};

#[cfg(feature = "CONFIG_RT_GROUP_SCHED")]
pub struct SeqFile {
    buf: *mut char,
    size: SizeT,
    from: SizeT,
    count: SizeT,
    pad_until: SizeT,
    index: LoffT,
    read_pos: LoffT,
    lock: Mutex,
    op: SeqOperations, // TODO: impls fn
    poll_event: i32,
    file: *const File,
    private: *mut u32,
}
