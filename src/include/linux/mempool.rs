/**************************************************************************
 *   mempool.rs  --  This file is part of linux-rs.                       *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{spinlock_types::SpinlockT, wait::WaitQueueHeadT};

pub struct MempoolT {
    lock: SpinlockT,
    /// nr of elements at *elements
    min_nr: i32,
    /// Current nr of elements at *elements
    curr_nr: i32,
    elements: *mut u32,
    pool_data: *mut u32,
    // TODO impl fn alloc: *mut MempoolAllocT,
    // TODO impl fn free: *mut MempoolFreeT,
    wait: WaitQueueHeadT,
}
