/**************************************************************************
 *   watch_queue.rs  --  This file is part of linux-rs.                   *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

#[cfg(all(feature = "CONFIG_RT_GROUP_SCHED", feature = "CONFIG_WATCH_QUEUE"))]
use crate::include::{
    linux::{
        kref::Kref,
        mm_types::Page,
        pipe_fs_i::PipeInodeInfo,
        spinlock_types::SpinlockT,
        types::{HlistHead, RcuHead},
    },
    uapi::linux::watch_queue::WatchNotificationType,
};

#[cfg(all(feature = "CONFIG_RT_GROUP_SCHED", feature = "CONFIG_WATCH_QUEUE"))]
struct WatchTypeFilter {
    type_: WatchNotificationType,
    /// Bitmask of subtypes to filter on
    subtype_filter: u32,
    /// Filter on watch_notification::info
    info_filter: u32,
    /// Mask of relevant bits in info_filter
    info_mask: u32,
}

#[cfg(all(feature = "CONFIG_RT_GROUP_SCHED", feature = "CONFIG_WATCH_QUEUE"))]
struct WatchFilter {
    rcu: RcuHead,
    /// Number of filters
    nr_filters: u32,
    filters: [WatchTypeFilter; 10],
}

#[cfg(all(feature = "CONFIG_RT_GROUP_SCHED", feature = "CONFIG_WATCH_QUEUE"))]
pub struct WatchQueue {
    rcu: RcuHead,
    filter: *mut WatchFilter,
    /// Pipe we use as a buffer, NULL if queue closed
    pipe: *mut PipeInodeInfo,
    /// Contributory watches
    watches: HlistHead,
    /// Preallocated notifications
    notes: *mut Page,
    /// Allocation bitmap for notes
    notes_bitmap: *mut u64,
    /// Object usage count
    usage: Kref,
    lock: SpinlockT,
    /// Number of notes
    nr_notes: u32,
    /// Number of pages in notes[]
    nr_pages: u32,
}
