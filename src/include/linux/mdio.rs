/**************************************************************************
 *   mdio.rs  --  This file is part of linux-rs.                          *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::{
    linux::{
        device::Device, device_::driver::DeviceDriver, mod_devicetable::MDIO_NAME_SIZE, phy::MiiBus,
    },
    uapi::linux::synclink::GpioDesc,
};

struct ResetControl;

pub struct MdioDevice {
    dev: Device,
    bus: *mut MiiBus,
    modalias: [char; MDIO_NAME_SIZE],
    /// Bus address of the MDIO device (0-31)
    addr: i32,
    flags: i32,
    reset_state: i32,
    reset_gpio: *mut GpioDesc,
    reset_ctrl: *mut ResetControl,
    reset_assert_delay: u32,
    reset_deassert_delay: u32,
}

/*int (*bus_match)(struct device *dev, struct device_driver *drv);
void (*device_free)(struct mdio_device *mdiodev);
void (*device_remove)(struct mdio_device *mdiodev);*/

/// struct mdio_driver_common: Common to all MDIO drivers
pub struct MdioDriverCommon {
    driver: DeviceDriver,
    flags: i32,
}
