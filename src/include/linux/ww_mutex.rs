/**************************************************************************
 *   ww_mutex.rs  --  This file is part of linux-rs.                      *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{lockdep_types::LockClassKey, sched::TaskStruct, types::AtomicLongT};

pub struct WwMutex {
    // TODO base: WWMUTEXBASE,
    ctx: *mut WwAcquireCtx,
    ww_class: *mut WwClass,
}
/*TODO #ifdef DEBUG_WW_MUTEXES
    struct ww_class *ww_class;
#endif*/

struct WwAcquireCtx {
    task: *mut TaskStruct,
    stamp: u64,
    acquired: u32,
    wounded: u16,
    is_wait_die: u16,
}
/*#ifdef DEBUG_WW_MUTEXES
    unsigned int done_acquire;
    struct ww_class *ww_class;
    void *contending_lock;
#endif
#ifdef CONFIG_DEBUG_LOCK_ALLOC
    struct lockdep_map dep_map;
#endif
#ifdef CONFIG_DEBUG_WW_MUTEX_SLOWPATH
    unsigned int deadlock_inject_interval;
    unsigned int deadlock_inject_countdown;
#endif*/

struct WwClass {
    stamp: AtomicLongT,
    acquire_key: LockClassKey,
    mutex_key: LockClassKey,
    acquire_name: *const char,
    mutex_name: *const char,
    is_wait_die: u32,
}
