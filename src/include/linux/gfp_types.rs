/**************************************************************************
 *   gfp_types.rs  --  This file is part of linux-rs.                     *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

/// typedef gfp_t - Memory allocation flags.
///
/// GFP flags are commonly used throughout Linux to indicate how memory
/// should be allocated.  The GFP acronym stands for get_free_pages(),
/// the underlying memory allocation function.  Not every GFP flag is
/// supported by every function which may allocate memory.  Most users
/// will want to use a plain ``GFP_KERNEL``.
pub type GfpT = u32;
