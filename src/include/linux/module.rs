/**************************************************************************
 *   module.rs  --  This file is part of linux-rs.                        *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::{
    include::{
        asm_generic::{bug::BugEntry, extable::ExceptionTableEntry, module::ModArchSpecific},
        linux::{
            completion::Completion,
            kobject::Kobject,
            moduleparam::{KernelParam, MAX_PARAM_PREFIX_LEN},
            sysfs::Attribute,
            types::ListHead,
        },
    },
    kernel::{module::internal::KernelSymbol, params::ModuleParamAttrs},
};

#[cfg(feature = "CONFIG_PRINTK_INDEX")]
use crate::include::linux::types::AtomicT;

#[cfg(feature = "CONFIG_SYSFS")]
use crate::include::linux::mutex::Mutex;

pub struct ModuleKobject {
    kobj: Kobject,
    mod_: *mut Module,
    drivers_dir: *mut Kobject,
    mp: *mut ModuleParamAttrs,
    kobj_completion: *mut Completion,
}

pub struct Module {
    /// Member of list of modules
    list: ListHead,
    /// Unique handle for this module
    name: [char; Module::MODULE_NAME_LEN],
    /// Module build ID
    #[cfg(feature = "CONFIG_STACKTRACE_BUILD_ID")]
    build_id: [u8; BUILD_ID_SIZE_MAX],
    /// Sysfs stuff.
    mkobj: ModuleKobject,
    modinfo_attrs: *mut ModuleAttribute,
    version: *const char,
    srcversion: *const char,
    holders_dir: *mut Kobject,
    /// Exported symbols
    syms: *const KernelSymbol,
    crcs: *const i32,
    num_syms: u32,
    #[cfg(feature = "CONFIG_ARCH_USES_CFI_TRAPS")]
    kcfi_traps: *mut i32,
    #[cfg(feature = "CONFIG_ARCH_USES_CFI_TRAPS")]
    kcfi_traps_end: *mut i32,
    /// Kernel parameters.
    #[cfg(feature = "CONFIG_SYSFS")]
    param_lock: Mutex,
    kp: *mut KernelParam,
    num_kp: u32,
    /// GPL-only exported symbols
    num_gpl_syms: u32,
    gpl_syms: *const KernelSymbol,
    gpl_crcs: *const i32,
    using_gplonly_symbols: bool,
    /// Signature was verified.
    #[cfg(feature = "CONFIG_MODULE_SIG")]
    sig_ok: bool,
    async_probe_requested: bool,
    /// Exception table
    num_exentries: u32,
    extable: *mut ExceptionTableEntry,
    mem: [ModuleMemory; Module::MOD_MEM_NUM_TYPES],
    /// Arch-specific module values
    arch: ModArchSpecific,
    /// same bits as kernel:taint_flags
    taints: u64,
    /// Support for BUG
    #[cfg(feature = "CONFIG_GENERIC_BUG")]
    num_bugs: usize,
    bug_list: ListHead,
    bug_table: *mut BugEntry,
    /// Protected by RCU and/or module_mutex: use rcu_dereference()
    #[cfg(feature = "CONFIG_KALLSYMS")]
    kallsyms: ModKallsyms,
    #[cfg(feature = "CONFIG_KALLSYMS")]
    core_kallsyms: ModKallsyms,
    /// Section attributes
    #[cfg(feature = "CONFIG_KALLSYMS")]
    sect_attrs: *mut ModuleSectAttrs,
    #[cfg(feature = "CONFIG_KALLSYMS")]
    notes_attrs: *mut ModuleSectAttrs,
    /// The command line arguments (may be mangled).  People like
    /// keeping pointers to this stuff
    args: *mut char,
    /// Per-cpu data.
    #[cfg(feature = "CONFIG_SMP")]
    percpu: *mut u32,
    #[cfg(feature = "CONFIG_SMP")]
    percpu_size: u32,
    noinstr_text_start: *mut u32,
    noinstr_text_size: u32,
    #[cfg(feature = "CONFIG_TRACEPOINTS")]
    num_tracepoints: u32,
    #[cfg(feature = "CONFIG_TRACEPOINTS")]
    tracepoints_ptrs: *mut TracepointPtrT,
    #[cfg(feature = "CONFIG_TREE_SRCU")]
    num_srcu_structs: u32,
    #[cfg(feature = "CONFIG_TREE_SRCU")]
    srcu_struct_ptrs: *mut SrcuStruct,
    #[cfg(feature = "CONFIG_BPF_EVENTS")]
    num_bpf_raw_events: u32,
    #[cfg(feature = "CONFIG_BPF_EVENTS")]
    bpf_raw_events: *mut BpfRawEventMap,
    #[cfg(feature = "CONFIG_DEBUG_INFO_BTF_MODULES")]
    btf_data_size: u32,
    #[cfg(feature = "CONFIG_DEBUG_INFO_BTF_MODULES")]
    btf_data: *mut *mut u32,
    #[cfg(feature = "CONFIG_JUMP_LABEL")]
    jump_entries: *mut JumpEntry,
    #[cfg(feature = "CONFIG_JUMP_LABEL")]
    num_jump_entries: u32,
    #[cfg(feature = "CONFIG_TRACING")]
    num_trace_bprintk_fmt: u32,
    #[cfg(feature = "CONFIG_TRACING")]
    trace_bprintk_fmt_start: *const char,
    #[cfg(feature = "CONFIG_EVENT_TRACING")]
    trace_events: *mut TraceEventCall,
    #[cfg(feature = "CONFIG_EVENT_TRACING")]
    num_trace_events: u32,
    #[cfg(feature = "CONFIG_EVENT_TRACING")]
    trace_evals: TraceEvalMap,
    #[cfg(feature = "CONFIG_EVENT_TRACING")]
    num_trace_evals: u32,
    #[cfg(feature = "CONFIG_FTRACE_MCOUNT_RECORD")]
    num_ftrace_callsites: u32,
    #[cfg(feature = "CONFIG_FTRACE_MCOUNT_RECORD")]
    ftrace_callsites: *mut u64,
    #[cfg(feature = "CONFIG_KPROBES")]
    kprobes_text_start: *mut u32,
    #[cfg(feature = "CONFIG_KPROBES")]
    kprobes_text_size: u32,
    #[cfg(feature = "CONFIG_KPROBES")]
    kprobe_blacklist: *mut u64,
    #[cfg(feature = "CONFIG_KPROBES")]
    num_kprobe_blacklist: u32,
    #[cfg(feature = "CONFIG_HAVE_STATIC_CALL_INLINE")]
    num_static_call_sites: i32,
    #[cfg(feature = "CONFIG_HAVE_STATIC_CALL_INLINE")]
    static_call_sites: *mut StaticCallSite,
    #[cfg(feature = "CONFIG_KUNIT")]
    num_kunit_suites: i32,
    #[cfg(feature = "CONFIG_KUNIT")]
    kunit_suites: *mut KunitSuite,
    /// Is this a livepatch module?
    #[cfg(feature = "CONFIG_LIVEPATCH")]
    klp: bool,
    #[cfg(feature = "CONFIG_LIVEPATCH")]
    klp_alive: bool,
    /// ELF information
    #[cfg(feature = "CONFIG_LIVEPATCH")]
    klp_info: *mut KlpModinfo,
    #[cfg(feature = "CONFIG_PRINTK_INDEX")]
    printk_index_size: u32,
    #[cfg(feature = "CONFIG_PRINTK_INDEX")]
    printk_index_start: *mut PiEntry,
    /// What modules depend on me?
    #[cfg(feature = "CONFIG_PRINTK_INDEX")]
    source_list: ListHead,
    /// What modules do I depend on?
    #[cfg(feature = "CONFIG_PRINTK_INDEX")]
    target_list: ListHead,
    #[cfg(feature = "CONFIG_PRINTK_INDEX")]
    refcnt: AtomicT,
    #[cfg(feature = "CONFIG_DYNAMIC_DEBUG_CORE")]
    dyndbg_info: _DdebugInfo,
}

/* TODO: impl fns
struct module {
    /* Startup function. */
    int (*init)(void);

#ifdef CONFIG_MODULE_UNLOAD
    /* Destruction function. */
    void (*exit)(void);
#endif

#ifdef CONFIG_CONSTRUCTORS
    /* Constructor functions. */
    ctor_fn_t *ctors;
    unsigned int num_ctors;
#endif

#ifdef CONFIG_FUNCTION_ERROR_INJECTION
    struct error_injection_entry *ei_funcs;
    unsigned int num_ei_funcs;
#endif

} ____cacheline_aligned __randomize_layout;
*/

struct ModuleAttribute {
    attr: Attribute,
}
/*struct module_attribute {
    struct attribute attr;
    ssize_t (*show)(struct module_attribute *, struct module_kobject *,char *);
    ssize_t (*store)(struct module_attribute *, struct module_kobject *,const char *, size_t count);
    void (*setup)(struct module *, const char *);
    int (*test)(struct module *);
    void (*free)(struct module *);
};*/

struct ModuleMemory {
    base: *mut u32,
    size: u32,
    #[cfg(feature = "CONFIG_MODULES_TREE_LOOKUP")]
    mtn: ModTreeNode,
}

impl Module {
    /// Normal state.
    const MODULE_STATE_LIVE: usize = 0;
    /// Full formed, running module_init.
    const MODULE_STATE_COMING: usize = 1;
    /// Going away.
    const MODULE_STATE_GOING: usize = 2;
    /// Still setting it up.
    const MODULE_STATE_UNFORMED: usize = 3;
    const MODULE_NAME_LEN: usize = MAX_PARAM_PREFIX_LEN;

    const MOD_TEXT: usize = 0;
    const MOD_DATA: usize = 1;
    const MOD_RODATA: usize = 2;
    const MOD_RO_AFTER_INIT: usize = 3;
    const MOD_INIT_TEXT: usize = 4;
    const MOD_INIT_DATA: usize = 5;
    const MOD_INIT_RODATA: usize = 6;
    const MOD_MEM_NUM_TYPES: usize = 7;
    const MOD_INVALID: isize = -1;
}
