/**************************************************************************
 *   dcache.rs  --  This file is part of linux-rs.                        *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{
    fs::{Inode, SuperBlock},
    list_bl::HlistBlNode,
    lockref::Lockref,
    types::{HlistHead, HlistNode, ListHead, RcuHead},
    wait::WaitQueueHeadT,
};

/// Try to keep struct dentry aligned on 64 byte cachelines (this will
/// give reasonable cacheline footprint with larger lines without the
/// large memory footprint increase).
/// 192 bytes
const DNAME_INLINE_LEN: usize = 40;

pub struct Dentry {
    /// RCU lookup touched fields
    /// protected by d_lock
    d_flags: u32,
    /// per dentry seqlock
    //TODO: look struct d_seq: SeqcountSpinlockT,
    /// lookup hash list
    d_hash: HlistBlNode,
    /// parent directory
    d_parent: *mut Dentry,
    d_name: Qstr,
    /// Where the name belongs to - NULL is negative
    d_inode: Inode,
    /// small names
    d_iname: [u8; DNAME_INLINE_LEN],
    /// Ref lookup also touches following
    /// per-dentry lock and refcount
    d_lockref: Lockref,
    /// The root of the dentry tree
    d_sb: *mut SuperBlock,
    /// used by d_revalidate
    d_time: u64,
    /// fs-specific data
    d_fsdata: *mut u32,
    /// LRU list
    d_lru: ListHead,
    /// in-lookup ones only
    d_wait: *mut WaitQueueHeadT,
    /// child of parent list
    d_sib: HlistNode,
    /// our children
    d_children: HlistHead,
    /// d_alias and d_rcu can share memory
    /// inode alias list
    d_alias: HlistNode,
    /// only for in-lookup ones
    d_in_lookup_hash: HlistBlNode,
    d_rcu: RcuHead,
}

/*
TODO impl fn
    int (*d_revalidate)(struct dentry *, unsigned int);
    int (*d_weak_revalidate)(struct dentry *, unsigned int);
    int (*d_hash)(const struct dentry *, struct qstr *);
    int (*d_compare)(const struct dentry *,
            unsigned int, const char *, const struct qstr *);
    int (*d_delete)(const struct dentry *);
    int (*d_init)(struct dentry *);
    void (*d_release)(struct dentry *);
    void (*d_prune)(struct dentry *);
    void (*d_iput)(struct dentry *, struct inode *);
    char *(*d_dname)(struct dentry *, char *, int);
    struct vfsmount *(*d_automount)(struct path *);
    int (*d_manage)(const struct path *, bool);
    struct dentry *(*d_real)(struct dentry *, const struct inode *);
*/

/// "quick string" -- eases parameter passing, but more importantly
/// saves "metadata" about the string (ie length and the hash).
///
/// hash comes first so it snuggles against d_parent in the
/// dentry.
struct Qstr {
    hash_len: u64,
    ///  The hash is always the low bits of hash_len
    hash: u32,
    len: u32,
    name: *const u8,
}
