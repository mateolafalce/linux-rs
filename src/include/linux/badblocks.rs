/**************************************************************************
 *   badblocks.rs  --  This file is part of linux-rs.                     *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{device::Device, seqlock_types::SeqcountT, types::SectorT};

pub struct Badblocks {
    /// set by devm_init_badblocks
    dev: *mut Device,
    /// count of bad blocks
    count: i32,
    /// there probably are unacknowledged
    /// bad blocks.  This is only cleared
    /// when a read discovers none
    unacked_exist: i32,
    /// shift from sectors to block size
    /// a -ve shift means badblocks are
    /// disabled.
    shift: i32,
    /// badblock list
    page: *mut u64,
    changed: i32,
    lock: SeqcountT,
    sector: SectorT,
    /// in sectors
    size: SectorT,
}
