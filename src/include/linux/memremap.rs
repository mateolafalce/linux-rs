/**************************************************************************
 *   memremap.rs  --  This file is part of linux-rs.                      *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{completion::Completion, percpu_refcount::PercpuRef, range::Range};

/// struct dev_pagemap - metadata for ZONE_DEVICE mappings
/// @altmap: pre-allocated/reserved memory for vmemmap allocations
/// @ref: reference count that pins the devm_memremap_pages() mapping
/// @done: completion for @ref
/// @type: memory type: see MEMORY_* in memory_hotplug.h
/// @flags: PGMAP_* flags to specify defailed behavior
/// @vmemmap_shift: structural definition of how the vmemmap page metadata
///      is populated, specifically the metadata page order.
/// A zero value (default) uses base pages as the vmemmap metadata
/// representation. A bigger value will set up compound struct pages
/// of the requested order value.
/// @ops: method table
/// @owner: an opaque pointer identifying the entity that manages this
/// instance. Used by various helpers to make sure that no
/// foreign ZONE_DEVICE memory is accessed.
/// @nr_range: number of ranges to be mapped
/// @range: range to be mapped when nr_range == 1
/// @ranges: array of ranges to be mapped when nr_range > 1
pub struct DevPagemap {
    altmap: VmemAltmap,
    ref_: PercpuRef,
    done: Completion,
    flags: u32,
    vmemmap_shift: u64,
    //ops: Op2, // TODO: use impl
    owner: *mut u32,
    nr_range: i32,
    range: Range,
}

/// struct vmem_altmap - pre-allocated storage for vmemmap_populate
/// @base_pfn: base of the entire dev_pagemap mapping
/// @reserve: pages mapped, but reserved for driver use (relative to @base)
/// @free: free pages set aside in the mapping for memmap storage
/// @align: pages reserved to meet allocation alignments
/// @alloc: track pages consumed, private to vmemmap_populate()
struct VmemAltmap {
    base_pfn: u64,
    end_pfn: *const u64,
    reserve: *const u64,
    free: u64,
    align: u64,
    alloc: u64,
}

/// Specialize ZONE_DEVICE memory into multiple types each has a different
/// usage.
///
/// MEMORY_DEVICE_PRIVATE:
/// Device memory that is not directly addressable by the CPU: CPU can neither
/// read nor write private memory. In this case, we do still have struct pages
/// backing the device memory. Doing so simplifies the implementation, but it is
/// important to remember that there are certain points at which the struct page
/// must be treated as an opaque object, rather than a "normal" struct page.
///
/// A more complete discussion of unaddressable memory may be found in
/// include/linux/hmm.h and Documentation/mm/hmm.rst.
///
/// MEMORY_DEVICE_COHERENT:
/// Device memory that is cache coherent from device and CPU point of view. This
/// is used on platforms that have an advanced system bus (like CAPI or CXL). A
/// driver can hotplug the device memory using ZONE_DEVICE and with that memory
/// type. Any page of a process can be migrated to such memory. However no one
/// should be allowed to pin such memory so that it can always be evicted.
///
/// MEMORY_DEVICE_FS_DAX:
/// Host memory that has similar access semantics as System RAM i.e. DMA
/// coherent and supports page pinning. In support of coordinating page
/// pinning vs other operations MEMORY_DEVICE_FS_DAX arranges for a
/// wakeup event whenever a page is unpinned and becomes idle. This
/// wakeup is used to coordinate physical address space management (ex:
/// fs truncate/hole punch) vs pinned pages (ex: device dma).
///
/// MEMORY_DEVICE_GENERIC:
/// Host memory that has similar access semantics as System RAM i.e. DMA
/// coherent and supports page pinning. This is for example used by DAX devices
/// that expose memory using a character device.
///
/// MEMORY_DEVICE_PCI_P2PDMA:
/// Device memory residing in a PCI BAR intended for use with Peer-to-Peer
/// transactions.
impl DevPagemap {
    /// 0 is reserved to catch uninitialized type fields
    const MEMORY_DEVICE_PRIVATE: usize = 1;
    const MEMORY_DEVICE_COHERENT: usize = 2;
    const MEMORY_DEVICE_FS_DAX: usize = 3;
    const MEMORY_DEVICE_GENERIC: usize = 4;
    const MEMORY_DEVICE_PCI_P2PDMA: usize = 5;
}
