/**************************************************************************
 *   msi.rs  --  This file is part of linux-rs.                           *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{
    device::Device, interrupt::IrqAffinityDesc, msi_api::MsiInstanceCookie,
};

///  struct msi_desc - Descriptor structure for MSI based interrupts
///  @irq:    The base interrupt number
///  @nvec_used:  The number of vectors used
///  @dev:    Pointer to the device which uses this descriptor
///  @msg:    The last set MSI message cached for reuse
///  @affinity:   Optional pointer to a cpu affinity mask for this descriptor
///  @sysfs_attr: Pointer to sysfs device attribute
///
///  @write_msi_msg:  Callback that may be called when the MSI message
///           address or data changes
///  @write_msi_msg_data: Data parameter for the callback.
///
///  @msi_index:  Index of the msi descriptor
///  @pci:    PCI specific msi descriptor data
///  @data:   Generic MSI descriptor data
pub struct MsiDesc {
    /// Shared device/bus type independent data
    irq: u32,
    nvec_used: u32,
    dev: *mut Device,
    msg: MsiMsg,
    affinity: *mut IrqAffinityDesc,
    msi_index: u16,
    pci: PciMsiDesc,
    data: MsiDescData,
    #[cfg(feature = "CONFIG_IRQ_MSI_IOMMU")]
    iommu_cookie: *const u32,
    #[cfg(feature = "CONFIG_SYSFS")]
    sysfs_attrs: *mut DeviceAttribute,
    /* TODO impl fnvoid (*write_msi_msg)(struct msi_desc *entry, void *data);*/
    write_msi_msg_data: *mut u32,
}

///  msi_msg - Representation of a MSI message
///  @address_lo:        Low 32 bits of msi message address
///  @arch_addrlo:    Architecture specific shadow of @address_lo
///  @address_hi:        High 32 bits of msi message address
///             (only used when device supports it)
///  @arch_addrhi:    Architecture specific shadow of @address_hi
///  @data:        MSI message data (usually 16 bits)
///  @arch_data:        Architecture specific shadow of @data
pub struct MsiMsg {
    address_lo: u32,
    arch_addr_lo: ArchMsiMsgAddrLoT,
    address_hi: u32,
    arch_addr_hi: ArchMsiMsgAddrHiT,
    data: u32,
    arch_data: ArchMsiMsgDataT,
}

///  pci_msi_desc - PCI/MSI specific MSI descriptor data
///
///  @msi_mask:     PCI MSI    MSI cached mask bits
///  @msix_ctrl:     PCI MSI-X  MSI-X cached per vector control bits
///  @is_msix:     PCI MSI/X  True if MSI-X
///  @multiple:     PCI MSI/X  log2 num of messages allocated
///  @multi_cap:     PCI MSI/X  log2 num of messages supported
///  @can_mask:     PCI MSI/X  Masking supported?
///  @is_64:     PCI MSI/X  Address size: 0=32bit 1=64bit
///  @default_irq: PCI MSI/X  The default pre-assigned non-MSI irq
///  @mask_pos:     PCI MSI    Mask register position
///  @mask_base:     PCI MSI-X  Mask register base address
pub struct PciMsiDesc {
    msi_mask: u32,
    msix_ctrl: u32,
    is_msix: bool,
    multiple: u8,
    multi_cap: u8,
    can_mask: bool,
    is_64: bool,
    is_virtual: bool,
    default_irq: u32,
    mask_pos: u8,
    mask_base: *mut u32,
}

///  struct msi_desc_data - Generic MSI descriptor data
///  @dcookie:    Cookie for MSI domain specific data which is required
///         for irq_chip callbacks
///  @icookie:    Cookie for the MSI interrupt instance provided by
///         the usage site to the allocation function
///
///  The content of this data is implementation defined, e.g. PCI/IMS
///  implementations define the meaning of the data. The MSI core ignores
///  this data completely.
struct MsiDescData {
    mdc: MsiDomainCookie,
    mic: MsiInstanceCookie,
}

///  union msi_domain_cookie - Opaque MSI domain specific data
///  @value:    u64 value store
///  @ptr:    Pointer to domain specific data
///  @iobase:    Domain specific IOmem pointer
///
///  The content of this data is implementation defined and used by the MSI
///  domain to store domain specific information which is requried for
///  interrupt chip callbacks.
struct MsiDomainCookie {
    value: u64,
    ptr: *mut u32,
    iobase: *mut u32,
}

/// Dummy shadow structures if an architecture does not define them
struct ArchMsiMsgAddrLoT {
    address_lo: u32,
}

struct ArchMsiMsgAddrHiT {
    address_hi: u32,
}

struct ArchMsiMsgDataT {
    data: u32,
}
