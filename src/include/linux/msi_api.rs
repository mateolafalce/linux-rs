/**************************************************************************
 *   msi_api.rs  --  This file is part of linux-rs.                       *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

///  union msi_instance_cookie - MSI instance cookie
///  @value:    u64 value store
///  @ptr:    Pointer to usage site specific data
///
///  This cookie is handed to the IMS allocation function and stored in the
///  MSI descriptor for the interrupt chip callbacks.
///
///  The content of this cookie is MSI domain implementation defined.  For
///  PCI/IMS implementations this could be a PASID or a pointer to queue
///  memory.
pub struct MsiInstanceCookie {
    value: u64,
    ptr: *mut u32,
}
