/**************************************************************************
 *   phy.rs  --  This file is part of linux-rs.                           *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::{
    drivers::{
        net::phy::{phylink::Phylink, sfp_bus::SfpBus},
        pse_pd::pse_core::PseControl,
    },
    include::{
        linux::{
            device::{Device, DeviceLink},
            mdio::{MdioDevice, MdioDriverCommon},
            mii_timestamper::MiiTimestamper,
            module::Module,
            mutex_types::Mutex,
            netdevice::NetDevice,
            of::DeviceNode,
            refcount::RefCountT,
            skbuff::SkBuff,
            types::{ListHead, SizeT},
            u64_stats_sync::U64StatsSync,
            u64_stats_sync::U64StatsT,
            workqueue::DelayedWork,
        },
        uapi::linux::synclink::GpioDesc,
    },
    tools::include::uapi::linux::netlink::Nlattr,
};

const PHY_MAX_ADDR: usize = 32;
const MDIO_MMD_NUM: usize = 32;
const MII_BUS_ID_SIZE: usize = 61;

///  enum phy_interface_t - Interface Mode definitions
///
///  @PHY_INTERFACE_MODE_NA: Not Applicable - don't touch
///  @PHY_INTERFACE_MODE_INTERNAL: No interface, MAC and PHY combined
///  @PHY_INTERFACE_MODE_MII: Media-independent interface
///  @PHY_INTERFACE_MODE_GMII: Gigabit media-independent interface
///  @PHY_INTERFACE_MODE_SGMII: Serial gigabit media-independent interface
///  @PHY_INTERFACE_MODE_TBI: Ten Bit Interface
///  @PHY_INTERFACE_MODE_REVMII: Reverse Media Independent Interface
///  @PHY_INTERFACE_MODE_RMII: Reduced Media Independent Interface
///  @PHY_INTERFACE_MODE_REVRMII: Reduced Media Independent Interface in PHY role
///  @PHY_INTERFACE_MODE_RGMII: Reduced gigabit media-independent interface
///  @PHY_INTERFACE_MODE_RGMII_ID: RGMII with Internal RX+TX delay
///  @PHY_INTERFACE_MODE_RGMII_RXID: RGMII with Internal RX delay
///  @PHY_INTERFACE_MODE_RGMII_TXID: RGMII with Internal RX delay
///  @PHY_INTERFACE_MODE_RTBI: Reduced TBI
///  @PHY_INTERFACE_MODE_SMII: Serial MII
///  @PHY_INTERFACE_MODE_XGMII: 10 gigabit media-independent interface
///  @PHY_INTERFACE_MODE_XLGMII:40 gigabit media-independent interface
///  @PHY_INTERFACE_MODE_MOCA: Multimedia over Coax
///  @PHY_INTERFACE_MODE_PSGMII: Penta SGMII
///  @PHY_INTERFACE_MODE_QSGMII: Quad SGMII
///  @PHY_INTERFACE_MODE_TRGMII: Turbo RGMII
///  @PHY_INTERFACE_MODE_100BASEX: 100 BaseX
///  @PHY_INTERFACE_MODE_1000BASEX: 1000 BaseX
///  @PHY_INTERFACE_MODE_2500BASEX: 2500 BaseX
///  @PHY_INTERFACE_MODE_5GBASER: 5G BaseR
///  @PHY_INTERFACE_MODE_RXAUI: Reduced XAUI
///  @PHY_INTERFACE_MODE_XAUI: 10 Gigabit Attachment Unit Interface
///  @PHY_INTERFACE_MODE_10GBASER: 10G BaseR
///  @PHY_INTERFACE_MODE_25GBASER: 25G BaseR
///  @PHY_INTERFACE_MODE_USXGMII:  Universal Serial 10GE MII
///  @PHY_INTERFACE_MODE_10GKR: 10GBASE-KR - with Clause 73 AN
///  @PHY_INTERFACE_MODE_QUSGMII: Quad Universal SGMII
///  @PHY_INTERFACE_MODE_1000BASEKX: 1000Base-KX - with Clause 73 AN
///  @PHY_INTERFACE_MODE_MAX: Book keeping
///
///  Describes the interface between the MAC and PHY
const PHY_INTERFACE_MODE_NA: usize = 0;
const PHY_INTERFACE_MODE_INTERNAL: usize = 1;
const PHY_INTERFACE_MODE_MII: usize = 2;
const PHY_INTERFACE_MODE_GMII: usize = 3;
const PHY_INTERFACE_MODE_SGMII: usize = 4;
const PHY_INTERFACE_MODE_TBI: usize = 5;
const PHY_INTERFACE_MODE_REVMII: usize = 6;
const PHY_INTERFACE_MODE_RMII: usize = 7;
const PHY_INTERFACE_MODE_REVRMII: usize = 8;
const PHY_INTERFACE_MODE_RGMII: usize = 9;
const PHY_INTERFACE_MODE_RGMII_ID: usize = 10;
const PHY_INTERFACE_MODE_RGMII_RXID: usize = 11;
const PHY_INTERFACE_MODE_RGMII_TXID: usize = 12;
const PHY_INTERFACE_MODE_RTBI: usize = 13;
const PHY_INTERFACE_MODE_SMII: usize = 14;
const PHY_INTERFACE_MODE_XGMII: usize = 15;
const PHY_INTERFACE_MODE_XLGMII: usize = 16;
const PHY_INTERFACE_MODE_MOCA: usize = 17;
const PHY_INTERFACE_MODE_PSGMII: usize = 18;
const PHY_INTERFACE_MODE_QSGMII: usize = 19;
const PHY_INTERFACE_MODE_TRGMII: usize = 20;
const PHY_INTERFACE_MODE_100BASEX: usize = 21;
const PHY_INTERFACE_MODE_1000BASEX: usize = 22;
const PHY_INTERFACE_MODE_2500BASEX: usize = 23;
const PHY_INTERFACE_MODE_5GBASER: usize = 24;
const PHY_INTERFACE_MODE_RXAUI: usize = 25;
const PHY_INTERFACE_MODE_XAUI: usize = 26;
const PHY_INTERFACE_MODE_10GBASER: usize = 27;
const PHY_INTERFACE_MODE_25GBASER: usize = 28;
const PHY_INTERFACE_MODE_USXGMII: usize = 29;
const PHY_INTERFACE_MODE_10GKR: usize = 30;
const PHY_INTERFACE_MODE_QUSGMII: usize = 31;
const PHY_INTERFACE_MODE_1000BASEKX: usize = 32;
const PHY_INTERFACE_MODE_MAX: usize = 33;

///  struct phy_driver - Driver structure for a particular PHY type
///
///  @mdiodrv: Data common to all MDIO devices
///  @phy_id: The result of reading the UID registers of this PHY
///    type, and ANDing them with the phy_id_mask.  This driver
///    only works for PHYs with IDs which match this field
///  @name: The friendly name of this PHY type
///  @phy_id_mask: Defines the important bits of the phy_id
///  @features: A mandatory list of features (speed, duplex, etc)
///    supported by this PHY
///  @flags: A bitfield defining certain other features this PHY
///    supports (like interrupts)
///  @driver_data: Static driver data
///
///  All functions are optional. If config_aneg or read_status
///  are not implemented, the phy core uses the genphy versions.
///  Note that none of these functions should be called from
///  interrupt time. The goal is for the bus read/write functions
///  to be able to block when the bus transaction is happening,
///  and be freed up by an interrupt (The MPC85xx has this ability,
///  though it is not currently supported in the driver).
struct PhyDriver {
    mdiodrv: MdioDriverCommon,
    phy_id: u32,
    name: *mut char,
    phy_id_mask: u32,
    flags: u32,
    driver_data: *const u32,
    long: u32,
    /*
      @soft_reset: Called to issue a PHY software reset

    int (*soft_reset)(struct phy_device *phydev);

    /**
     * @config_init: Called to initialize the PHY,
     * including after a reset
     */
    int (*config_init)(struct phy_device *phydev);

    /**
     * @probe: Called during discovery.  Used to set
     * up device-specific structures, if any
     */
    int (*probe)(struct phy_device *phydev);

    /**
     * @get_features: Probe the hardware to determine what
     * abilities it has.  Should only set phydev->supported.
     */
    int (*get_features)(struct phy_device *phydev);

    /**
     * @get_rate_matching: Get the supported type of rate matching for a
     * particular phy interface. This is used by phy consumers to determine
     * whether to advertise lower-speed modes for that interface. It is
     * assumed that if a rate matching mode is supported on an interface,
     * then that interface's rate can be adapted to all slower link speeds
     * supported by the phy. If the interface is not supported, this should
     * return %RATE_MATCH_NONE.
     */
    int (*get_rate_matching)(struct phy_device *phydev,
                   phy_interface_t iface);

    /* PHY Power Management */
    /** @suspend: Suspend the hardware, saving state if needed */
    int (*suspend)(struct phy_device *phydev);
    /** @resume: Resume the hardware, restoring state if needed */
    int (*resume)(struct phy_device *phydev);

    /**
     * @config_aneg: Configures the advertisement and resets
     * autonegotiation if phydev->autoneg is on,
     * forces the speed to the current settings in phydev
     * if phydev->autoneg is off
     */
    int (*config_aneg)(struct phy_device *phydev);

    /** @aneg_done: Determines the auto negotiation result */
    int (*aneg_done)(struct phy_device *phydev);

    /** @read_status: Determines the negotiated speed and duplex */
    int (*read_status)(struct phy_device *phydev);

    /**
     * @config_intr: Enables or disables interrupts.
     * It should also clear any pending interrupts prior to enabling the
     * IRQs and after disabling them.
     */
    int (*config_intr)(struct phy_device *phydev);

    /** @handle_interrupt: Override default interrupt handling */
    irqreturn_t (*handle_interrupt)(struct phy_device *phydev);

    /** @remove: Clears up any memory if needed */
    void (*remove)(struct phy_device *phydev);

    /**
     * @match_phy_device: Returns true if this is a suitable
     * driver for the given phydev.  If NULL, matching is based on
     * phy_id and phy_id_mask.
     */
    int (*match_phy_device)(struct phy_device *phydev);

    /**
     * @set_wol: Some devices (e.g. qnap TS-119P II) require PHY
     * register changes to enable Wake on LAN, so set_wol is
     * provided to be called in the ethernet driver's set_wol
     * function.
     */
    int (*set_wol)(struct phy_device *dev, struct ethtool_wolinfo *wol);

    /**
     * @get_wol: See set_wol, but for checking whether Wake on LAN
     * is enabled.
     */
    void (*get_wol)(struct phy_device *dev, struct ethtool_wolinfo *wol);

    /**
     * @link_change_notify: Called to inform a PHY device driver
     * when the core is about to change the link state. This
     * callback is supposed to be used as fixup hook for drivers
     * that need to take action when the link state
     * changes. Drivers are by no means allowed to mess with the
     * PHY device structure in their implementations.
     */
    void (*link_change_notify)(struct phy_device *dev);

    /**
     * @read_mmd: PHY specific driver override for reading a MMD
     * register.  This function is optional for PHY specific
     * drivers.  When not provided, the default MMD read function
     * will be used by phy_read_mmd(), which will use either a
     * direct read for Clause 45 PHYs or an indirect read for
     * Clause 22 PHYs.  devnum is the MMD device number within the
     * PHY device, regnum is the register within the selected MMD
     * device.
     */
    int (*read_mmd)(struct phy_device *dev, int devnum, u16 regnum);

    /**
     * @write_mmd: PHY specific driver override for writing a MMD
     * register.  This function is optional for PHY specific
     * drivers.  When not provided, the default MMD write function
     * will be used by phy_write_mmd(), which will use either a
     * direct write for Clause 45 PHYs, or an indirect write for
     * Clause 22 PHYs.  devnum is the MMD device number within the
     * PHY device, regnum is the register within the selected MMD
     * device.  val is the value to be written.
     */
    int (*write_mmd)(struct phy_device *dev, int devnum, u16 regnum,
             u16 val);

    /** @read_page: Return the current PHY register page number */
    int (*read_page)(struct phy_device *dev);
    /** @write_page: Set the current PHY register page number */
    int (*write_page)(struct phy_device *dev, int page);

    /**
     * @module_info: Get the size and type of the eeprom contained
     * within a plug-in module
     */
    int (*module_info)(struct phy_device *dev,
               struct ethtool_modinfo *modinfo);

    /**
     * @module_eeprom: Get the eeprom information from the plug-in
     * module
     */
    int (*module_eeprom)(struct phy_device *dev,
                 struct ethtool_eeprom *ee, u8 *data);

    /** @cable_test_start: Start a cable test */
    int (*cable_test_start)(struct phy_device *dev);

    /**  @cable_test_tdr_start: Start a raw TDR cable test */
    int (*cable_test_tdr_start)(struct phy_device *dev,
                    const struct phy_tdr_config *config);

    /**
     * @cable_test_get_status: Once per second, or on interrupt,
     * request the status of the test.
     */
    int (*cable_test_get_status)(struct phy_device *dev, bool *finished);

    /* Get statistics from the PHY using ethtool */
    /** @get_sset_count: Number of statistic counters */
    int (*get_sset_count)(struct phy_device *dev);
    /** @get_strings: Names of the statistic counters */
    void (*get_strings)(struct phy_device *dev, u8 *data);
    /** @get_stats: Return the statistic counter values */
    void (*get_stats)(struct phy_device *dev,
              struct ethtool_stats *stats, u64 *data);

    /* Get and Set PHY tunables */
    /** @get_tunable: Return the value of a tunable */
    int (*get_tunable)(struct phy_device *dev,
               struct ethtool_tunable *tuna, void *data);
    /** @set_tunable: Set the value of a tunable */
    int (*set_tunable)(struct phy_device *dev,
                struct ethtool_tunable *tuna,
                const void *data);
    /** @set_loopback: Set the loopback mood of the PHY */
    int (*set_loopback)(struct phy_device *dev, bool enable);
    /** @get_sqi: Get the signal quality indication */
    int (*get_sqi)(struct phy_device *dev);
    /** @get_sqi_max: Get the maximum signal quality indication */
    int (*get_sqi_max)(struct phy_device *dev);

    /* PLCA RS interface */
    /** @get_plca_cfg: Return the current PLCA configuration */
    int (*get_plca_cfg)(struct phy_device *dev,
                struct phy_plca_cfg *plca_cfg);
    /** @set_plca_cfg: Set the PLCA configuration */
    int (*set_plca_cfg)(struct phy_device *dev,
                const struct phy_plca_cfg *plca_cfg);
    /** @get_plca_status: Return the current PLCA status info */
    int (*get_plca_status)(struct phy_device *dev,
                   struct phy_plca_status *plca_st);

    /**
     * @led_brightness_set: Set a PHY LED brightness. Index
     * indicates which of the PHYs led should be set. Value
     * follows the standard LED class meaning, e.g. LED_OFF,
     * LED_HALF, LED_FULL.
     */
    int (*led_brightness_set)(struct phy_device *dev,
                  u8 index, enum led_brightness value);

    /**
     * @led_blink_set: Set a PHY LED brightness.  Index indicates
     * which of the PHYs led should be configured to blink. Delays
     * are in milliseconds and if both are zero then a sensible
     * default should be chosen.  The call should adjust the
     * timings in that case and if it can't match the values
     * specified exactly.
     */
    int (*led_blink_set)(struct phy_device *dev, u8 index,
                 unsigned long *delay_on,
                 unsigned long *delay_off);
    /**
     * @led_hw_is_supported: Can the HW support the given rules.
     * @dev: PHY device which has the LED
     * @index: Which LED of the PHY device
     * @rules The core is interested in these rules
     *
     * Return 0 if yes,  -EOPNOTSUPP if not, or an error code.
     */
    int (*led_hw_is_supported)(struct phy_device *dev, u8 index,
                   unsigned long rules);
    /**
     * @led_hw_control_set: Set the HW to control the LED
     * @dev: PHY device which has the LED
     * @index: Which LED of the PHY device
     * @rules The rules used to control the LED
     *
     * Returns 0, or a an error code.
     */
    int (*led_hw_control_set)(struct phy_device *dev, u8 index,
                  unsigned long rules);
    /**
     * @led_hw_control_get: Get how the HW is controlling the LED
     * @dev: PHY device which has the LED
     * @index: Which LED of the PHY device
     * @rules Pointer to the rules used to control the LED
     *
     * Set *@rules to how the HW is currently blinking. Returns 0
     * on success, or a error code if the current blinking cannot
     * be represented in rules, or some other error happens.
     */
    int (*led_hw_control_get)(struct phy_device *dev, u8 index,
                  unsigned long *rules);

    /**
     * @led_polarity_set: Set the LED polarity modes
     * @dev: PHY device which has the LED
     * @index: Which LED of the PHY device
     * @modes: bitmap of LED polarity modes
     *
     * Configure LED with all the required polarity modes in @modes
     * to make it correctly turn ON or OFF.
     *
     * Returns 0, or an error code.
     */
    int (*led_polarity_set)(struct phy_device *dev, int index,
                unsigned long modes);*/
}

///  struct mdio_bus_stats - Statistics counters for MDIO busses
///  @transfers: Total number of transfers, i.e. @writes + @reads
///  @errors: Number of MDIO transfers that returned an error
///  @writes: Number of write transfers
///  @reads: Number of read transfers
///  @syncp: Synchronisation for incrementing statistics
struct MdioBusStats {
    transfers: U64StatsT,
    errors: U64StatsT,
    writes: U64StatsT,
    reads: U64StatsT,
    syncp: U64StatsSync,
}

/// struct phy_device - An instance of a PHY
///
/// @mdio: MDIO bus this PHY is on
/// @drv: Pointer to the driver for this PHY instance
/// @devlink: Create a link between phy dev and mac dev, if the external phy
/// used by current mac interface is managed by another mac interface.
/// @phy_id: UID for this device found during discovery
/// @c45_ids: 802.3-c45 Device Identifiers if is_c45.
/// @is_c45:  Set to true if this PHY uses clause 45 addressing.
/// @is_internal: Set to true if this PHY is internal to a MAC.
/// @is_pseudo_fixed_link: Set to true if this PHY is an Ethernet switch, etc.
/// @is_gigabit_capable: Set to true if PHY supports 1000Mbps
/// @has_fixups: Set to true if this PHY has fixups/quirks.
/// @suspended: Set to true if this PHY has been suspended successfully.
/// @suspended_by_mdio_bus: Set to true if this PHY was suspended by MDIO bus.
/// @sysfs_links: Internal boolean tracking sysfs symbolic links setup/removal.
/// @loopback_enabled: Set true if this PHY has been loopbacked successfully.
/// @downshifted_rate: Set true if link speed has been downshifted.
/// @is_on_sfp_module: Set true if PHY is located on an SFP module.
/// @mac_managed_pm: Set true if MAC driver takes of suspending/resuming PHY
/// @wol_enabled: Set to true if the PHY or the attached MAC have Wake-on-LAN
/// enabled.
/// @state: State of the PHY for management purposes
/// @dev_flags: Device-specific flags used by the PHY driver.
///
/// - Bits [15:0] are free to use by the PHY driver to communicate
///   driver specific behavior.
/// - Bits [23:16] are currently reserved for future use.
/// - Bits [31:24] are reserved for defining generic
/// PHY driver behavior.
/// @irq: IRQ number of the PHY's interrupt (-1 if none)
/// @phylink: Pointer to phylink instance for this PHY
/// @sfp_bus_attached: Flag indicating whether the SFP bus has been attached
/// @sfp_bus: SFP bus attached to this PHY's fiber port
/// @attached_dev: The attached enet driver's device instance ptr
/// @adjust_link: Callback for the enet controller to respond to changes: in the
/// link state.
/// @phy_link_change: Callback for phylink for notification of link change
/// @macsec_ops: MACsec offloading ops.
///
/// @speed: Current link speed
/// @duplex: Current duplex
/// @port: Current port
/// @pause: Current pause
/// @asym_pause: Current asymmetric pause
/// @supported: Combined MAC/PHY supported linkmodes
/// @advertising: Currently advertised linkmodes
/// @adv_old: Saved advertised while power saving for WoL
/// @supported_eee: supported PHY EEE linkmodes
/// @advertising_eee: Currently advertised EEE linkmodes
/// @eee_enabled: Flag indicating whether the EEE feature is enabled
/// @lp_advertising: Current link partner advertised linkmodes
/// @host_interfaces: PHY interface modes supported by host
/// @eee_broken_modes: Energy efficient ethernet modes which should be prohibited
/// @autoneg: Flag autoneg being used
/// @rate_matching: Current rate matching mode
/// @link: Current link state
/// @autoneg_complete: Flag auto negotiation of the link has completed
/// @mdix: Current crossover
/// @mdix_ctrl: User setting of crossover
/// @pma_extable: Cached value of PMA/PMD Extended Abilities Register
/// @interrupts: Flag interrupts have been enabled
/// @irq_suspended: Flag indicating PHY is suspended and therefore interrupt
/// handling shall be postponed until PHY has resumed
/// @irq_rerun: Flag indicating interrupts occurred while PHY was suspended,
/// requiring a rerun of the interrupt handler after resume
/// @interface: enum phy_interface_t value
/// @possible_interfaces: bitmap if interface modes that the attached PHY
///	will switch between depending on media speed.
/// @skb: Netlink message for cable diagnostics
/// @nest: Netlink nest used for cable diagnostics
/// @ehdr: nNtlink header for cable diagnostics
/// @phy_led_triggers: Array of LED triggers
/// @phy_num_led_triggers: Number of triggers in @phy_led_triggers
/// @led_link_trigger: LED trigger for link up/down
/// @last_triggered: last LED trigger for link speed
/// @leds: list of PHY LED structures
/// @master_slave_set: User requested master/slave configuration
/// @master_slave_get: Current master/slave advertisement
/// @master_slave_state: Current master/slave configuration
/// @mii_ts: Pointer to time stamper callbacks
/// @psec: Pointer to Power Sourcing Equipment control struct
/// @lock:  Mutex for serialization access to PHY
/// @state_queue: Work queue for state machine
/// @link_down_events: Number of times link was lost
/// @shared: Pointer to private data shared by phys in one package
/// @priv: Pointer to driver private data
///
/// interrupts currently only supports enabled or disabled,
/// but could be changed in the future to support enabling
/// and disabling specific interrupts
///
/// Contains some infrastructure for polling and interrupt
/// handling, as well as handling shifts in PHY hardware state
pub struct PhyDevice {
    mdio: MdioDevice,
    /// Information about the PHY type
    /// And management functions
    drv: *mut PhyDriver,
    devlink: *mut DeviceLink,
    phy_id: u32,
    c45_ids: PhyC45DeviceIds,
    is_c45: bool,
    is_internal: bool,
    is_pseudo_fixed_link: bool,
    is_gigabit_capable: bool,
    has_fixups: bool,
    suspended: bool,
    suspended_by_mdio_bus: bool,
    sysfs_links: bool,
    loopback_enabled: bool,
    downshifted_rate: bool,
    is_on_sfp_module: bool,
    mac_managed_pm: bool,
    wol_enabled: bool,
    autoneg: bool,
    /// The most recently read link state
    link: bool,
    autoneg_complete: bool,
    /// Interrupts are enabled
    interrupts: bool,
    irq_suspended: bool,
    irq_rerun: bool,
    rate_matching: i32,
    dev_flags: u32,
    //TODO:interface: PhyInterfaceT,
    /// forced speed & duplex (no autoneg)
    /// partner speed & duplex & pause (autoneg)
    speed: i32,
    duplex: i32,
    port: i32,
    pause: i32,
    asym_pause: i32,
    master_slave_get: u8,
    master_slave_set: u8,
    master_slave_state: u8,
    eee_enabled: bool,
    /// Energy efficient ethernet modes which should be prohibited
    eee_broken_modes: u32,
    #[cfg(feature = "CONFIG_LED_TRIGGER_PHY")]
    phy_led_triggers: PhyLedTrigger,
    #[cfg(feature = "CONFIG_LED_TRIGGER_PHY")]
    phy_num_led_triggers: u32,
    #[cfg(feature = "CONFIG_LED_TRIGGER_PHY")]
    last_triggered: *mut PhyLedTrigger,
    #[cfg(feature = "CONFIG_LED_TRIGGER_PHY")]
    led_link_trigger: *mut PhyLedTrigger,
    leds: ListHead,
    /// Interrupt number for this PHY
    /// -1 means no interrupt
    irq: i32,
    /// private data pointer
    /// For use by PHYs to maintain extra state
    priv_: *mut u32,
    /// shared data pointer
    /// For use by PHYs inside the same package that need a shared state.
    shared: *mut PhyPackageShared,
    /// Reporting cable test results
    skb: *mut SkBuff,
    ehdr: *mut u32,
    nest: *mut Nlattr,
    /// Interrupt and Polling infrastructure
    state_queue: DelayedWork,
    lock: Mutex,
    /// This may be modified under the rtnl lock
    sfp_bus_attached: bool,
    sfp_bus: SfpBus,
    phylink: *mut Phylink,
    attached_dev: *mut NetDevice,
    mii_ts: *mut MiiTimestamper,
    psec: *mut PseControl,
    mdix: u8,
    mdix_ctrl: u8,
    pma_extable: i32,
    link_down_events: u32,
    //TODO impl fns void (*phy_link_change)(struct phy_device *phydev, bool up);
    //void (*adjust_link)(struct net_device *dev);
    /// MACsec management functions
    #[cfg(feature = "CONFIG_MACSEC")]
    macsec_ops: MacsecOps, // TODO impl
}

///  struct phy_package_shared - Shared information in PHY packages
///  @base_addr: Base PHY address of PHY package used to combine PHYs
///    in one package and for offset calculation of phy_package_read/write
///  @np: Pointer to the Device Node if PHY package defined in DT
///  @refcnt: Number of PHYs connected to this shared data
///  @flags: Initialization of PHY package
///  @priv_size: Size of the shared private data @priv
///  @priv: Driver private data shared across a PHY package
///
///  Represents a shared structure between different phydev's in the same
///  package, for example a quad PHY. See phy_package_join() and
///  phy_package_leave().
struct PhyPackageShared {
    base_addr: u8,
    np: *mut DeviceNode,
    refcnt: RefCountT,
    flags: u64,
    priv_size: SizeT,
    priv_: *mut u32,
}

///  struct phy_c45_device_ids - 802.3-c45 Device Identifiers
///  @devices_in_package: IEEE 802.3 devices in package register value.
///  @mmds_present: bit vector of MMDs present.
///  @device_ids: The device identifer for each present device.
struct PhyC45DeviceIds {
    devices_in_package: u32,
    mmds_present: u32,
    device_ids: [u32; MDIO_MMD_NUM],
}

/// struct mii_bus - Represents an MDIO bus
///
/// @owner: Who owns this device
/// @name: User friendly name for this MDIO device, or driver name
/// @id: Unique identifier for this bus, typical from bus hierarchy
/// @priv: Driver private data
///
/// The Bus class for PHYs.  Devices which provide access to
/// PHYs should register using this structure
pub struct MiiBus {
    owner: *mut Module,
    name: *const char,
    id: [char; MII_BUS_ID_SIZE],
    priv_: *mut u32,
    /* TODO impl fns @read: Perform a read transfer on the bus
    int (*read)(struct mii_bus *bus, int addr, int regnum);
    /** @write: Perform a write transfer on the bus */
    int (*write)(struct mii_bus *bus, int addr, int regnum, u16 val);
    /** @read_c45: Perform a C45 read transfer on the bus */
    int (*read_c45)(struct mii_bus *bus, int addr, int devnum, int regnum);
    /** @write_c45: Perform a C45 write transfer on the bus */
    int (*write_c45)(struct mii_bus *bus, int addr, int devnum, int regnum, u16 val);
    /** @reset: Perform a reset of the bus */
    int (*reset)(struct mii_bus *bus);*/
    /// @stats: Statistic counters per device on the bus
    stats: [MdioBusStats; PHY_MAX_ADDR],
    /// @mdio_lock: A lock to ensure that only one thing
    /// can read/write the MDIO bus at a time
    mdio_lock: Mutex,
    /// @parent: Parent device of this bus
    parent: *mut Device,
    /// @dev: Kernel device representation
    dev: Device,
    /// @mdio_map: list of all MDIO devices on bus
    mdio_map: [*mut MdioDevice; PHY_MAX_ADDR],
    /// @phy_mask: PHY addresses to be ignored when probin
    phy_mask: u32,
    /// @phy_ignore_ta_mask: PHY addresses to ignore the TA/read failure
    phy_ignore_ta_mask: u32,
    /// @irq: An array of interrupts, each PHY's
    /// interrupt at the index matching its address
    irq: [i32; PHY_MAX_ADDR],
    /// @reset_delay_us: GPIO reset pulse width in microseconds
    reset_delay_us: i32,
    /// @reset_post_delay_us: GPIO reset deassert delay in microseconds
    reset_post_delay_us: i32,
    /// @reset_gpiod: Reset GPIO descriptor pointer
    reset_gpiod: *mut GpioDesc,
    /// @shared_lock: protect access to the shared element
    shared_lock: Mutex,
    /// @shared: shared state across different PHYs
    shared: [*mut PhyPackageShared; PHY_MAX_ADDR],
}

impl MiiBus {
    const MDIOBUS_ALLOCATED: usize = 1;
    const MDIOBUS_REGISTERED: usize = 2;
    const MDIOBUS_UNREGISTERED: usize = 3;
    const MDIOBUS_RELEASED: usize = 4;
}

impl PhyDevice {
    /// @PHY_DOWN: PHY device and driver are not ready for anything.  probe
    /// should be called if and only if the PHY is in this state,
    /// given that the PHY device exists.
    /// - PHY driver probe function will set the state to @PHY_READY
    const PHY_DOWN: usize = 0;
    /// @PHY_READY: PHY is ready to send and receive packets, but the
    /// controller is not.  By default, PHYs which do not implement
    /// probe will be set to this state by phy_probe().
    /// - start will set the state to UP
    const PHY_READY: usize = 1;
    /// @PHY_HALTED: PHY is up, but no polling or interrupts are done.
    /// - phy_start moves to @PHY_UP
    const PHY_HALTED: usize = 2;
    /// @PHY_ERROR: PHY is up, but is in an error state.
    /// - phy_stop moves to @PHY_HALTED
    const PHY_ERROR: usize = 3;
    /// @PHY_UP: The PHY and attached device are ready to do work.
    /// Interrupts should be started here.
    /// - timer moves to @PHY_NOLINK or @PHY_RUNNING
    const PHY_UP: usize = 4;
    /// @PHY_RUNNING: PHY is currently up, running, and possibly sending
    /// and/or receiving packets
    /// - irq or timer will set @PHY_NOLINK if link goes down
    /// - phy_stop moves to @PHY_HALTED
    const PHY_RUNNING: usize = 5;
    /// @PHY_NOLINK: PHY is up, but not currently plugged in.
    /// - irq or timer will set @PHY_RUNNING if link comes back
    /// - phy_stop moves to @PHY_HALTED
    const PHY_NOLINK: usize = 6;
    /// @PHY_CABLETEST: PHY is performing a cable test. Packet reception/sending
    /// is not expected to work, carrier will be indicated as down. PHY will be
    /// poll once per second, or on interrupt for it current state.
    /// Once complete, move to UP to restart the PHY.
    /// - phy_stop aborts the running test and moves to @PHY_HALTED
    const PHY_CABLETEST: usize = 7;
}
