/**************************************************************************
 *   page_counter.rs  --  This file is part of linux-rs.                  *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::types::AtomicLongT;

pub struct PageCounter {
    /// Make sure 'usage' does not share cacheline with any other field. The
    /// memcg->memory.usage is a hot member of struct mem_cgroup.
    usage: AtomicLongT,
    /// effective memory.min and memory.min usage tracking
    emin: u64,
    min_usage: AtomicLongT,
    children_min_usage: AtomicLongT,
    /// effective memory.low and memory.low usage tracking
    elow: u64,
    low_usage: AtomicLongT,
    children_low_usage: AtomicLongT,
    watermark: u64,
    failcnt: u64,
    min: u64,
    low: u64,
    high: u64,
    max: u64,
    parent: *mut PageCounter,
}
