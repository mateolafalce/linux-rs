/**************************************************************************
 *   binfmts.rs  --  This file is part of linux-rs.                       *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{module::Module, types::ListHead};

/// This structure defines the functions that are used to
/// load the binary formats that linux accepts.
pub struct LinuxBinfmt {
    lh: ListHead,
    module: *mut Module,
    /// minimal dump size
    #[cfg(feature = "CONFIG_COREDUMP")]
    min_coredump: u64,
}
/*
struct linux_binfmt {
    TODO impl fns

    int (*load_binary)(struct linux_binprm *);
    int (*load_shlib)(struct file *);

#ifdef CONFIG_COREDUMP
    int (*core_dump)(struct coredump_params *cprm);
#endif

}
*/
