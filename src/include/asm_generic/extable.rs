/**************************************************************************
 *   extable.rs  --  This file is part of linux-rs.                       *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

/// The exception table consists of pairs of addresses: the first is the
/// address of an instruction that is allowed to fault, and the second is
/// the address at which the program should continue.  No registers are
/// modified, so it is entirely up to the continuation code to figure out
/// what to do.
///
/// All the routines below use bits of fixup code that are out of line
/// with the main instruction path.  This means when everything is well,
/// we don't even have to jump over them.  Further, they do not intrude
/// on our cache or tlb entries.
pub struct ExceptionTableEntry {
    insn: u64,
    fixup: u64,
}
