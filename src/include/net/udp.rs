/**************************************************************************
 *   udp.rs  --  This file is part of linux-rs.                           *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{spinlock_types::SpinlockT, types::HlistHead};

/// struct udp_table - UDP table
///
/// @hash:hash table, sockets are hashed on (local port)
/// @hash2:hash table, sockets are hashed on (local port, local address)
/// @mask:number of slots in hash tables, minus 1
/// @log:log2(number of slots in hash table)
pub struct UdpTable {
    hash: *mut UdpHslot,
    hash2: *mut UdpHslot,
    mask: u32,
    log: u32,
}

/// struct udp_hslot - UDP hash slot
///
/// @head: head of list of sockets
/// @count: umber of sockets in 'head' list
/// @lock: spinlock protecting changes to head/count
pub struct UdpHslot {
    head: HlistHead,
    count: i32,
    lock: SpinlockT,
}
