/**************************************************************************
 *   netdev_rx_queue.rs  --  This file is part of linux-rs.               *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::{
    linux::{
        kobject::Kobject,
        netdevice::{NapiStruct, NetDevice},
    },
    net::{net_trackers::NetdeviceTracker, xdp::XdpRxqInfo},
};

/// This structure contains an instance of an RX queue.
pub struct NetdevRxQueue {
    xdp_rxq: XdpRxqInfo,
    #[cfg(feature = "CONFIG_RPS")]
    rps_map: *mut RpsMap,
    #[cfg(feature = "CONFIG_RPS")]
    rps_flow_table: *mut RpsDevFlowTable,
    kobj: Kobject,
    dev: *mut NetDevice,
    dev_tracker: NetdeviceTracker,
    #[cfg(feature = "CONFIG_XDP_SOCKETS")]
    pool: *mut XskBuffPool,
    /// NAPI instance for the queue
    /// Readers and writers must hold RTNL
    napi: *mut NapiStruct,
}
