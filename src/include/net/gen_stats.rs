/**************************************************************************
 *   gen_stats.rs  --  This file is part of linux-rs.                     *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::u64_stats_sync::U64StatsSync;

///  Must be initialized beforehand with gnet_stats_basic_sync_init().
///
///  If no reads can ever occur parallel to writes (e.g. stack-allocated
///  bstats), then the internal stat values can be written to and read
///  from directly. Otherwise, use _bstats_set/update() for writes and
///  gnet_stats_add_basic() for reads.
pub struct GnetStatsBasicSync {
    bytes: u64,
    packets: u64,
    syncp: U64StatsSync,
}
