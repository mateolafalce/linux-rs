/**************************************************************************
 *   neighbour.rs  --  This file is part of linux-rs.                     *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

const NEIGH_NUM_HASH_RND: usize = 4;

use crate::include::{
    linux::{
        netdevice::NetDevice,
        refcount::RefCountT,
        rwlock_types::RwlockT,
        skbuff::SkBuffHead,
        timer::TimerList,
        types::{AtomicT, HlistHead, ListHead, RcuHead},
        workqueue::DelayedWork,
    },
    net::{net_namespace::PossibleNetT, net_trackers::NetdeviceTracker},
};

enum NeighVar {
    McastProbes,
    UcastProbes,
    AppProbes,
    McastReprobes,
    RetransTime,
    BaseReachableTime,
    DelayProbeTime,
    IntervalProbeTimeMs,
    GcStaleTime,
    QueueLenBytes,
    ProxyQlen,
    AnycastDelay,
    ProxyDelay,
    LockTime,
    /// NEIGH_VAR_DATA_MAX = LockTime + 1 is represented by a constant
    DataMax,
    QueueLen,
    RetransTimeMs,
    BaseReachableTimeMs,
    // Default-specific variables
    GcInterval,
    GcThresh1,
    GcThresh2,
    GcThresh3,
    Max,
}

const NEIGH_VAR_DATA_MAX: usize = NeighVar::LockTime as usize + 1;

pub struct NeighParms {
    net: PossibleNetT,
    dev: *mut NetDevice,
    dev_tracker: NetdeviceTracker,
    list: ListHead,
    // TODO impl fn int	(*neigh_setup)(struct neighbour *);
    tbl: *mut NeighTable,
    sysctl_table: *mut u32,
    dead: i32,
    refcnt: RefCountT,
    rcu_head: RcuHead,
    reachable_time: i32,
    qlen: u32,
    data: [i32; NEIGH_VAR_DATA_MAX],
    //LOOK DECLARE_BITMAP(data_state, NEIGH_VAR_DATA_MAX);
}

struct NeighTable {
    family: i32,
    entry_size: u32,
    key_len: u32,
    protocol: i16,
    id: char,
    parms: NeighParms,
    parms_list: ListHead,
    gc_interval: i32,
    gc_thresh1: i32,
    gc_thresh2: i32,
    gc_thresh3: i32,
    last_flush: u64,
    gc_work: DelayedWork,
    managed_work: DelayedWork,
    proxy_timer: TimerList,
    proxy_queue: SkBuffHead,
    entries: AtomicT,
    gc_entries: AtomicT,
    gc_list: ListHead,
    managed_list: ListHead,
    lock: RwlockT,
    last_rand: u64,
    stats: NeighStatistics,
    nht: NeighHashTable,
    phash_buckets: PneighEntry,
}

/*
TODO
    u32 (*hash)(const void *pkey,const struct net_device *dev,u32 *hash_rnd);
    bool (*key_eq)(const struct neighbour *, const void *pkey);
    int (*constructor)(struct neighbour *);
    int (*pconstructor)(struct pneigh_entry *);
    void (*pdestructor)(struct pneigh_entry *);
    void (*proxy_redo)(struct sk_buff *skb);
    int (*is_multicast)(const void *pkey);
    bool (*allow_add)(const struct net_device *dev,struct netlink_ext_ack *extack);
*/

struct NeighStatistics {
    /// Number of allocated neighbors.
    allocs: u64,
    /// Number of destroyed neighbors.
    destroys: u64,
    /// Number of hash table resizes.
    hash_grows: u64,
    /// Number of failed resolutions.
    res_failed: u64,
    /// Number of lookups performed.
    lookups: u64,
    /// Number of successful lookups (hits among total lookups).
    hits: u64,
    /// Number of received multicast IPv6 probes.
    rcv_probes_mcast: u64,
    /// Number of received unicast IPv6 probes.
    rcv_probes_ucast: u64,
    /// Number of periodic garbage collection runs.
    periodic_gc_runs: u64,
    /// Number of forced garbage collection runs.
    forced_gc_runs: u64,
    /// Number of unresolved drops.
    unres_discards: u64,
    /// Number of times the table was full, even after garbage collection.
    table_fulls: u64,
}

struct NeighHashTable {
    hash_heads: Option<HlistHead>,
    hash_shift: u32,
    hash_rnd: [u32; NEIGH_NUM_HASH_RND],
    rcu: RcuHead,
}

struct PneighEntry {
    next: *mut PneighEntry,
    net: PossibleNetT,
    dev: *mut NetDevice,
    dev_tracker: NetdeviceTracker,
    flags: u32,
    protocol: u8,
    key: u32,
}
