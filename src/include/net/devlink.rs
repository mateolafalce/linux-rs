/**************************************************************************
 *   devlink.rs  --  This file is part of linux-rs.                       *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::{
    include::{
        linux::{
            netdevice::{NetDevice, NetdevPhysItemId},
            refcount::RefCountT,
            spinlock_types::SpinlockT,
            types::ListHead,
            workqueue::DelayedWork,
        },
        uapi::linux::if_::IFNAMSIZ,
    },
    net::devlink::{devl_internal::Devlink, linecard::DevlinkLinecard},
};

pub struct DevlinkPort {
    list: ListHead,
    region_list: ListHead,
    devlink: *mut Devlink,
    index: u32,
    type_lock: SpinlockT,
    netdev: *mut NetDevice,
    ifindex: i32,
    //ibdev: *mut IbDevice,
    attrs: DevlinkPortAttrs,
    type_warn_dw: DelayedWork,
    reporter_list: ListHead,
    devlink_rate: *mut DevlinkRate,
    linecard: *mut DevlinkLinecard,
    rel_index: u32,
    attrs_set: bool,
    switch_port: bool,
    registered: bool,
    initialized: bool,
    ifname: [char; IFNAMSIZ],
}

///  struct devlink_port_attrs - devlink port object
///  @flavour: flavour of the port
///  @split: indicates if this is split port
///  @splittable: indicates if the port can be split.
///  @lanes: maximum number of lanes the port supports. 0 value is not passed to netlink.
///  @switch_id: if the port is part of switch, this is buffer with ID, otherwise this is NULL
///  @phys: physical port attributes
///  @pci_pf: PCI PF port attributes
///  @pci_vf: PCI VF port attributes
///  @pci_sf: PCI SF port attributes
struct DevlinkPortAttrs {
    split: bool,
    splittable: bool,
    lanes: u32,
    switch_id: NetdevPhysItemId,
    phys: DevlinkPortPhysAttrs,
    pci_pf: DevlinkPortPciPfAttrs,
    pci_vf: DevlinkPortPciVfAttrs,
    pci_sf: DevlinkPortPciSfAttrs,
}

struct DevlinkRate {
    list: ListHead,
    // enum devlink_rate_type type;
    devlink: *mut Devlink,
    priv_: *mut u32,
    tx_share: u64,
    tx_max: u64,
    parent: *mut DevlinkRate,
    devlink_port: *mut DevlinkPort,
    name: &'static str,
    refcnt: RefCountT,
    tx_priority: u32,
    tx_weight: u32,
}

struct DevlinkPortPhysAttrs {
    /// Same value as "split group".
    /// A physical port which is visible to the user
    /// for a given port flavour.
    port_number: u32,
    /// If the port is split, this is the number of subport.
    split_subport_number: u32,
}

///  struct devlink_port_pci_pf_attrs - devlink port's PCI PF attributes
///  @controller: Associated controller number
///  @pf: Associated PCI PF number for this port.
///  @external: when set, indicates if a port is for an external controller
struct DevlinkPortPciPfAttrs {
    controller: u32,
    pf: u16,
    external: bool,
}

///  struct devlink_port_pci_vf_attrs - devlink port's PCI VF attributes
///  @controller: Associated controller number
///  @pf: Associated PCI PF number for this port.
///  @vf: Associated PCI VF for of the PCI PF for this port.
///  @external: when set, indicates if a port is for an external controller
struct DevlinkPortPciVfAttrs {
    controller: u32,
    pf: u16,
    vf: u16,
    external: bool,
}

///  struct devlink_port_pci_sf_attrs - devlink port's PCI SF attributes
///  @controller: Associated controller number
///  @sf: Associated PCI SF for of the PCI PF for this port.
///  @pf: Associated PCI PF number for this port.
///  @external: when set, indicates if a port is for an external controller
struct DevlinkPortPciSfAttrs {
    controller: u32,
    sf: u32,
    pf: u16,
    external: bool,
}
