/**************************************************************************
 *   xfrm.rs  --  This file is part of linux-rs.                          *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

#[cfg(all(
    feature = "CONFIG_RT_GROUP_SCHED",
    feature = "CONFIG_WATCH_QUEUE",
    feature = "CONFIG_XFRM"
))]
use crate::include::{
    linux::{
        netdevice::NetDevice,
        refcount::RefCountT,
        rwlock_types::RwlockT,
        timer::TimerList,
        types::{AtomicT, HlistNode, RcuHead},
    },
    net::net_namespace::PossibleNetT,
};

#[cfg(all(
    feature = "CONFIG_RT_GROUP_SCHED",
    feature = "CONFIG_WATCH_QUEUE",
    feature = "CONFIG_XFRM"
))]
pub struct XfrmPolicy {
    xp_net: PossibleNetT,
    bydst: HlistNode,
    byidx: HlistNode,
    /// This lock only affects elements except for entry.
    lock: RwlockT,
    refcnt: RefCountT,
    pos: u32,
    timer: TimerList,
    genid: AtomicT,
    priority: u32,
    index: u32,
    if_id: u32,
    mark: XfrmMark,
    selector: XfrmSelector,
    lft: XfrmLifetimeCfg,
    curlft: XfrmLifetimeCur,
    walk: XfrmPolicyWalkEntry,
    polq: XfrmPolicyQueue,
    bydst_reinsert: bool,
    type_: u8,
    action: u8,
    flags: u8,
    xfrm_nr: u8,
    family: u16,
    security: *mut XfrmSecCtx,
    xfrm_vec: [XfrmTmpl; XFRM_MAX_DEPTH],
    bydst_inexact_list: HlistNode,
    rcu: RcuHead,
    xdo: XfrmDevOffload,
}

#[cfg(all(
    feature = "CONFIG_RT_GROUP_SCHED",
    feature = "CONFIG_WATCH_QUEUE",
    feature = "CONFIG_XFRM"
))]
pub struct XfrmDevOffload {
    dev: *mut NetDevice,
    dev_tracker: NetdeviceTracker,
    real_dev: *mut NetDevice,
    offload_handle: u64,
    dir: u8,
    type_: u8,
    flags: u8,
}
