/**************************************************************************
 *   sch_generic.rs  --  This file is part of linux-rs.                   *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::{
    include::{
        linux::{
            netdevice::NetdevQueue,
            refcount::RefCountT,
            skbuff::{SkBuff, SkBuffHead},
            spinlock_types::SpinlockT,
            types::{HlistNode, ListHead, RcuHead},
        },
        net::{gen_stats::GnetStatsBasicSync, net_trackers::NetdeviceTracker},
        uapi::linux::{gen_stats::GnetStatsQueue, pkt_sched::TcSizespec},
    },
    net::core::gen_estimator::NetRateEstimator,
};

pub struct Qdisc {
    // TODO impl fn int (*enqueue)(struct sk_buff *skb,struct Qdisc *sch,struct sk_buff **to_free);
    // struct sk_buff *	(*dequeue)(struct Qdisc *sch);
    flags: u32,
    limit: u32,
    //TODO impl fn const struct Qdisc_ops	*ops;
    stab: *mut QdiscSizeTable,
    hash: HlistNode,
    handle: u32,
    parent: u32,
    dev_queue: *mut NetdevQueue,
    rate_est: *mut NetRateEstimator,
    cpu_bstats: *mut GnetStatsBasicSync,
    cpu_qstats: *mut GnetStatsQueue,
    pad: i32,
    refcnt: RefCountT,
    /// For performance sake on SMP, we put highly modified fields at the end
    gso_skb: SkBuffHead,
    q: QdiscSkbHead,
    bstats: GnetStatsBasicSync,
    qstats: GnetStatsQueue,
    state: u64,
    /// must be written under qdisc spinlock
    state2: u64,
    next_sched: *mut Qdisc,
    skb_bad_txq: SkBuffHead,
    busylock: SpinlockT,
    seqlock: SpinlockT,
    rcu: RcuHead,
    dev_tracker: NetdeviceTracker,
    /// private data
    privdata: [i64; 10],
}

impl Qdisc {
    const TCQ_F_BUILTIN: usize = 1;
    const TCQ_F_INGRESS: usize = 2;
    const TCQ_F_CAN_BYPASS: usize = 4;
    const TCQ_F_MQROOT: usize = 8;
    /// dequeue_skb() can assume all skbs are for
    /// q->dev_queue : It can test
    /// netif_xmit_frozen_or_stopped() before
    /// dequeueing next packet.
    /// Its true for MQ/MQPRIO slaves, or non
    /// multiqueue device.
    const TCQ_F_ONETXQUEUE: usize = 0x10;
    const TCQ_F_WARN_NONWC: usize = 1 << 16;
    /// run using percpu statistics
    const TCQ_F_CPUSTATS: usize = 0x20;
    /// root of its hierarchy :
    /// qdisc_tree_decrease_qlen() should stop.
    const TCQ_F_NOPARENT: usize = 0x40;
    /// invisible by default in dump
    const TCQ_F_INVISIBLE: usize = 0x80;
    /// qdisc does not require locking
    const TCQ_F_NOLOCK: usize = 0x100;
    /// qdisc is offloaded to HW
    const TCQ_F_OFFLOADED: usize = 0x200;
}

struct QdiscSizeTable {
    rcu: RcuHead,
    list: ListHead,
    szopts: TcSizespec,
    refcnt: i32,
    // TODO data
}

/// similar to sk_buff_head, but skb->prev pointer is undefined.
struct QdiscSkbHead {
    head: *mut SkBuff,
    tail: *mut SkBuff,
    qlen: u32,
    lock: SpinlockT,
}
