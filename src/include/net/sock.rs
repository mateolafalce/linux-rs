/**************************************************************************
 *   sock.rs  --  This file is part of linux-rs.                          *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::filter::SkFilter;

#[cfg(all(
    feature = "CONFIG_RT_GROUP_SCHED",
    feature = "CONFIG_WATCH_QUEUE",
    feature = "CONFIG_XFRM"
))]
use crate::include::net::xfrm::XfrmPolicy;

#[cfg(feature = "CONFIG_BPF_SYSCALL")]
use include::linux::bpf_local_storage::BpfLocalStorage;

#[cfg(feature = "CONFIG_IPV6")]
use include::linux::uapi::linux::in6::In6Addr;

use crate::{
    include::{
        linux::{
            cgroup_defs::SockCgroupData,
            cred::Cred,
            gfp_types::GfpT,
            ktime::KtimeT,
            list_nulls::HlistNullsNode,
            memcontrol::MemCgroup,
            mm_types_task::PageFrag,
            net::{Socket, SocketWq},
            netdev_features::NetdevFeaturesT,
            pid::Pid,
            rbtree_types::RbRoot,
            refcount::RefCountT,
            rwlock_types::RwlockT,
            skbuff::{SkBuff, SkBuffHead},
            spinlock_types::SpinlockT,
            timer::TimerList,
            types::{Atomic64T, AtomicT, HlistNode, RcuHead},
            uidgid_types::KuidT,
        },
        net::{
            dst::DstEntry, net_namespace::PossibleNetT, net_trackers::NetnsTracker,
            netns::ipv4::InetTimewaitDeathRow, sock_reuseport::SockReuseport,
        },
    },
    mm::slab::KmemCache,
};

type __Portpair = u32;
type __Addrpair = u64;

/// struct sock - network layer representation of sockets
/// @__sk_common: shared layout with inet_timewait_sock
/// @sk_shutdown: mask of %SEND_SHUTDOWN and/or %RCV_SHUTDOWN
/// @sk_userlocks: %SO_SNDBUF and %SO_RCVBUF settings
/// @sk_lock:   synchronizer
/// @sk_kern_sock: True if sock is using kernel lock classes
/// @sk_rcvbuf: size of receive buffer in bytes
/// @sk_wq: sock wait queue and async head
/// @sk_rx_dst: receive input route used by early demux
/// @sk_rx_dst_ifindex: ifindex for @sk_rx_dst
/// @sk_rx_dst_cookie: cookie for @sk_rx_dst
/// @sk_dst_cache: destination cache
/// @sk_dst_pending_confirm: need to confirm neighbour
/// @sk_policy: flow policy
/// @sk_receive_queue: incoming packets
/// @sk_wmem_alloc: transmit queue bytes committed
/// @sk_tsq_flags: TCP Small Queues flags
/// @sk_write_queue: Packet sending queue
/// @sk_omem_alloc: \"o\" is \"option\" or \"other\"
/// @sk_wmem_queued: persistent queue size
/// @sk_forward_alloc: space allocated forward
/// @sk_reserved_mem: space reserved and non-reclaimable for the socket
/// @sk_napi_id: id of the last napi context to receive data for sk
/// @sk_ll_usec: usecs to busypoll when there is no data
/// @sk_allocation: allocation mode
/// @sk_pacing_rate: Pacing rate (if supported by transport/packet scheduler)
/// @sk_pacing_status: Pacing status (requested, handled by sch_fq)
/// @sk_max_pacing_rate: Maximum pacing rate (%SO_MAX_PACING_RATE)
/// @sk_sndbuf: size of send buffer in bytes
/// @__sk_flags_offset: empty field used to determine location of bitfield
/// @sk_padding: unused element for alignment
/// @sk_no_check_tx: %SO_NO_CHECK setting, set checksum in TX packets
/// @sk_no_check_rx: allow zero checksum in RX packets
/// @sk_route_caps: route capabilities (e.g. %NETIF_F_TSO)
/// @sk_gso_disabled: if set, NETIF_F_GSO_MASK is forbidden.
/// @sk_gso_type: GSO type (e.g. %SKB_GSO_TCPV4)
/// @sk_gso_max_size: Maximum GSO segment size to build
/// @sk_gso_max_segs: Maximum number of GSO segments
/// @sk_pacing_shift: scaling factor for TCP Small Queues
/// @sk_lingertime: %SO_LINGER l_linger setting
/// @sk_backlog: always used with the per-socket spinlock held
/// @sk_callback_lock: used with the callbacks in the end of this struct
/// @sk_error_queue: rarely used
/// @sk_prot_creator: sk_prot of original sock creator (see ipv6_setsockopt,
///           IPV6_ADDRFORM for instance)
/// @sk_err: last error
/// @sk_err_soft: errors that don't cause failure but are the cause of a
///           persistent failure not just 'timed out'
/// @sk_drops: raw/udp drops counter
/// @sk_ack_backlog: current listen backlog
/// @sk_max_ack_backlog: listen backlog set in listen()
/// @sk_uid: user id of owner
/// @sk_prefer_busy_poll: prefer busypolling over softirq processing
/// @sk_busy_poll_budget: napi processing budget when busypolling
/// @sk_priority: %SO_PRIORITY setting
/// @sk_type: socket type (%SOCK_STREAM, etc)
/// @sk_protocol: which protocol this socket belongs in this network family
/// @sk_peer_lock: lock protecting @sk_peer_pid and @sk_peer_cred
/// @sk_peer_pid: &struct pid for this socket's peer
/// @sk_peer_cred: %SO_PEERCRED setting
/// @sk_rcvlowat: %SO_RCVLOWAT setting
/// @sk_rcvtimeo: %SO_RCVTIMEO setting
/// @sk_sndtimeo: %SO_SNDTIMEO setting
/// @sk_txhash: computed flow hash for use on transmit
/// @sk_txrehash: enable TX hash rethink
/// @sk_filter: socket filtering instructions
/// @sk_timer: sock cleanup timer
/// @sk_stamp: time stamp of last packet received
/// @sk_stamp_seq: lock for accessing sk_stamp on 32 bit architectures only
/// @sk_tsflags: SO_TIMESTAMPING flags
/// @sk_use_task_frag: allow sk_page_frag() to use current->task_frag.
///            Sockets that can be used under memory reclaim should
///            set this to false.
/// @sk_bind_phc: SO_TIMESTAMPING bind PHC index of PTP virtual clock
///               for timestamping
/// @sk_tskey: counter to disambiguate concurrent tstamp requests
/// @sk_zckey: counter to order MSG_ZEROCOPY notifications
/// @sk_socket: Identd and reporting IO signals
/// @sk_user_data: RPC layer private data. Write-protected by @sk_callback_lock.
/// @sk_frag: cached page frag
/// @sk_peek_off: current peek_offset value
/// @sk_send_head: front of stuff to transmit
/// @tcp_rtx_queue: TCP re-transmit queue [union with @sk_send_head]
/// @sk_security: used by security modules
/// @sk_mark: generic packet mark
/// @sk_cgrp_data: cgroup data for this cgroup
/// @sk_memcg: this socket's memory cgroup association
/// @sk_write_pending: a write to stream socket waits to start
/// @sk_disconnects: number of disconnect operations performed on this sock
/// @sk_state_change: callback to indicate change in the state of the sock
/// @sk_data_ready: callback to indicate there is data to be processed
/// @sk_write_space: callback to indicate there is bf sending space available
/// @sk_error_report: callback to indicate errors (e.g. %MSG_ERRQUEUE)
/// @sk_backlog_rcv: callback to process the backlog
/// @sk_validate_xmit_skb: ptr to an optional validate function
/// @sk_destruct: called at sock freeing time, i.e. when all refcnt == 0
/// @sk_reuseport_cb: reuseport group container
/// @sk_bpf_storage: ptr to cache and control for bpf_sk_storage
/// @sk_rcu: used during RCU grace period
/// @sk_clockid: clockid used by time-based scheduling (SO_TXTIME)
/// @sk_txtime_deadline_mode: set deadline mode for SO_TXTIME
/// @sk_txtime_report_errors: set report errors mode for SO_TXTIME
/// @sk_txtime_unused: unused txtime flags
/// @ns_tracker: tracker for netns reference
/// @sk_bind2_node: bind node in the bhash2 table
pub struct Sock {
    /// Now struct inet_timewait_sock also uses sock_common, so please just
    /// don't add nothing before this first member (__sk_common) --acme
    __sk_common: SockCommon,
    /// early demux fields
    sk_rx_dst: *mut DstEntry,
    sk_rx_dst_ifindex: i32,
    sk_rx_dst_cookie: u32,
    sk_lock: SocketLockT,
    sk_drops: AtomicT,
    sk_rcvlowat: i32,
    sk_error_queue: SkBuffHead,
    sk_receive_queue: SkBuffHead,
    /// The backlog queue is special, it is always used with
    /// the per-socket spinlock held and requires low latency
    /// access. Therefore we special case it's implementation.
    /// Note : rmem_alloc is in this structure to fill a hole
    /// on 64bit arches, not because its logically part of
    /// backlog.
    rmem_alloc: AtomicT,
    len: i32,
    head: *mut SkBuff,
    tail: *mut SkBuff,
    sk_forward_alloc: i32,
    sk_reserved_mem: u32,
    #[cfg(feature = "CONFIG_NET_RX_BUSY_POLL")]
    sk_ll_usec: u32,
    /// mostly read cache line
    #[cfg(feature = "CONFIG_NET_RX_BUSY_POLL")]
    sk_napi_id: u32,
    sk_rcvbuf: i32,
    sk_disconnects: i32,
    sk_filter: *mut SkFilter,
    sk_wq: *mut SocketWq,
    /// private:
    sk_wq_raw: SocketWq,
    #[cfg(feature = "CONFIG_XFRM")]
    sk_policy: [XfrmPolicy; 2],
    sk_dst_cache: DstEntry,
    sk_omem_alloc: AtomicT,
    sk_sndbuf: i32,
    /// cache line for TX
    sk_wmem_queued: i32,
    sk_wmem_alloc: RefCountT,
    sk_tsq_flags: u64,
    sk_send_head: *mut SkBuff,
    tcp_rtx_queue: RbRoot,
    sk_write_queue: SkBuffHead,
    sk_peek_off: i32,
    sk_write_pending: i32,
    sk_dst_pending_confirm: u32,
    /// see enum sk_pacing
    sk_pacing_status: u32,
    sk_sndtimeo: i64,
    sk_timer: TimerList,
    sk_priority: u32,
    sk_mark: u32,
    /// bytes per second
    sk_pacing_rate: u64,
    sk_max_pacing_rate: u64,
    sk_frag: PageFrag,
    sk_route_caps: NetdevFeaturesT,
    sk_gso_type: i32,
    sk_gso_max_size: u32,
    sk_allocation: GfpT,
    sk_txhash: u32,
    /// Because of non atomicity rules, all
    /// changes are protected by socket lock.
    sk_gso_disabled: u8,
    sk_kern_sock: u8,
    sk_no_check_tx: u8,
    sk_no_check_rx: u8,
    sk_userlocks: u8,
    sk_pacing_shift: u8,
    sk_type: u16,
    sk_protocol: u16,
    sk_gso_max_segs: u16,
    sk_lingertime: u64,
    sk_prot_creator: *mut Proto,
    sk_callback_lock: RwlockT,
    sk_err: i32,
    sk_err_soft: i32,
    sk_ack_backlog: u32,
    sk_max_ack_backlog: u32,
    sk_uid: KuidT,
    sk_txrehash: u8,
    #[cfg(feature = "CONFIG_NET_RX_BUSY_POLL")]
    sk_prefer_busy_poll: u8,
    #[cfg(feature = "CONFIG_NET_RX_BUSY_POLL")]
    sk_busy_poll_budget: u16,
    sk_peer_lock: SpinlockT,
    sk_bind_phc: i32,
    sk_peer_pid: *mut Pid,
    sk_peer_cred: *const Cred,
    sk_rcvtimeo: i64,
    sk_stamp: KtimeT,
    sk_tskey: AtomicT,
    sk_zckey: AtomicT,
    sk_tsflags: u32,
    sk_shutdown: u8,
    sk_clockid: u8,
    sk_txtime_deadline_mode: u8,
    sk_txtime_report_errors: u8,
    sk_txtime_unused: u8,
    sk_use_task_frag: bool,
    sk_socket: *mut Socket,
    sk_user_data: *mut u32,
    #[cfg(feature = "CONFIG_SECURITY")]
    sk_security: *mut u32,
    sk_cgrp_data: SockCgroupData,
    sk_memcg: *mut MemCgroup,
    sk_reuseport_cb: SockReuseport,
    #[cfg(feature = "CONFIG_BPF_SYSCALL")]
    sk_bpf_storage: *mut BpfLocalStorage,
    sk_rcu: RcuHead,
    ns_tracker: NetnsTracker,
    sk_bind2_node: HlistNode,
}

/* TODO impl in Sock those fn
struct sock {
    void            (*sk_state_change)(struct sock *sk);
    void            (*sk_data_ready)(struct sock *sk);
    void            (*sk_write_space)(struct sock *sk);
    void            (*sk_error_report)(struct sock *sk);
    int         (*sk_backlog_rcv)(struct sock *sk, struct sk_buff *skb);

#ifdef CONFIG_SOCK_VALIDATE_XMIT
    struct sk_buff*     (*sk_validate_xmit_skb)(struct sock *sk, struct net_device *dev, struct sk_buff *skb);
#endif

    void                    (*sk_destruct)(struct sock *sk);
};
*/

impl Sock {
    fn sk_node(&self) -> HlistNode {
        self.__sk_common.skc_node
    }
    fn sk_nulls_node(&self) -> HlistNullsNode {
        self.__sk_common.skc_nulls_node
    }
    fn sk_refcnt(&self) -> RefCountT {
        self.__sk_common.skc_refcnt
    }
    fn sk_tx_queue_mapping(&self) -> u16 {
        self.__sk_common.skc_tx_queue_mapping
    }
    #[cfg(feature = "CONFIG_SOCK_RX_QUEUE_MAPPING")]
    fn sk_rx_queue_mapping(&self) -> u16 {
        self.__sk_common.skc_rx_queue_mapping
    }
    fn sk_dontcopy_begin(&self) -> i32 {
        self.__sk_common.skc_dontcopy_begin
    }
    fn sk_dontcopy_end(&self) -> i32 {
        self.__sk_common.skc_dontcopy_end
    }
    fn sk_hash(&self) -> u32 {
        self.__sk_common.skc_hash
    }
    fn sk_portpair(&self) -> __Portpair {
        self.__sk_common.skc_portpair
    }
    fn sk_num(&self) -> u16 {
        self.__sk_common.skc_num
    }
    fn sk_dport(&self) -> u16 {
        self.__sk_common.skc_dport
    }
    fn sk_addrpair(&self) -> __Addrpair {
        self.__sk_common.skc_addrpair
    }
    fn sk_daddr(&self) -> u32 {
        self.__sk_common.skc_daddr
    }
    fn sk_rcv_saddr(&self) -> u32 {
        self.__sk_common.skc_rcv_saddr
    }
    fn sk_family(&self) -> u16 {
        self.__sk_common.skc_family
    }
    fn sk_state(&self) -> u8 {
        self.__sk_common.skc_state
    }
    fn sk_reuse(&self) -> u8 {
        self.__sk_common.skc_reuse
    }
    fn sk_reuseport(&self) -> u8 {
        self.__sk_common.skc_reuseport
    }
    fn sk_ipv6only(&self) -> u8 {
        self.__sk_common.skc_ipv6only
    }
    fn sk_net_refcnt(&self) -> u8 {
        self.__sk_common.skc_net_refcnt
    }
    fn sk_bound_dev_if(&self) -> i32 {
        self.__sk_common.skc_bound_dev_if
    }
    fn sk_bind_node(&self) -> HlistNode {
        self.__sk_common.skc_bind_node
    }
    fn sk_prot(&self) -> *mut Proto {
        self.__sk_common.skc_prot
    }
    fn sk_net(&self) -> PossibleNetT {
        self.__sk_common.skc_net
    }
    #[cfg(feature = "CONFIG_IPV6")]
    fn sk_v6_daddr(&self) -> In6Addr {
        self.__sk_common.skc_v6_daddr
    }
    #[cfg(feature = "CONFIG_IPV6")]
    fn sk_v6_rcv_saddr(&self) -> In6Addr {
        self.__sk_common.skc_v6_rcv_saddr
    }
    fn sk_cookie(&self) -> Atomic64T {
        self.__sk_common.skc_cookie
    }
    fn sk_incoming_cpu(&self) -> i32 {
        self.__sk_common.skc_incoming_cpu
    }
    fn sk_flags(&self) -> u64 {
        self.__sk_common.skc_flags
    }
    fn sk_rxhash(&self) -> u32 {
        self.__sk_common.skc_rxhash
    }
}

/// struct sock_common - minimal network layer representation of sockets
/// @skc_daddr: Foreign IPv4 addr
/// @skc_rcv_saddr: Bound local IPv4 addr
/// @skc_addrpair: 8-byte-aligned __u64 union of @skc_daddr & @skc_rcv_saddr
/// @skc_hash: hash value used with various protocol lookup tables
/// @skc_u16hashes: two u16 hash values used by UDP lookup tables
/// @skc_dport: placeholder for inet_dport/tw_dport
/// @skc_num: placeholder for inet_num/tw_num
/// @skc_portpair: __u32 union of @skc_dport & @skc_num
/// @skc_family: network address family
/// @skc_state: Connection state
/// @skc_reuse: %SO_REUSEADDR setting
/// @skc_reuseport: %SO_REUSEPORT setting
/// @skc_ipv6only: socket is IPV6 only
/// @skc_net_refcnt: socket is using net ref counting
/// @skc_bound_dev_if: bound device index if != 0
/// @skc_bind_node: bind hash linkage for various protocol lookup tables
/// @skc_portaddr_node: second hash linkage for UDP/UDP-Lite protocol
/// @skc_prot: protocol handlers inside a network family
/// @skc_net: reference to the network namespace of this socket
/// @skc_v6_daddr: IPV6 destination address
/// @skc_v6_rcv_saddr: IPV6 source address
/// @skc_cookie: socket's cookie value
/// @skc_node: main hash linkage for various protocol lookup tables
/// @skc_nulls_node: main hash linkage for TCP/UDP/UDP-Lite protocol
/// @skc_tx_queue_mapping: tx queue number for this connection
/// @skc_rx_queue_mapping: rx queue number for this connection
/// @skc_flags: place holder for sk_flags
///     %SO_LINGER (l_onoff), %SO_BROADCAST, %SO_KEEPALIVE,
///     %SO_OOBINLINE settings, %SO_TIMESTAMPING settings
/// @skc_listener: connection request listener socket (aka rsk_listener)
///     [union with @skc_flags]
/// @skc_tw_dr: (aka tw_dr) ptr to &struct inet_timewait_death_row
///     [union with @skc_flags]
/// @skc_incoming_cpu: record/match cpu processing incoming packets
/// @skc_rcv_wnd: (aka rsk_rcv_wnd) TCP receive window size (possibly scaled)
///     [union with @skc_incoming_cpu]
/// @skc_tw_rcv_nxt: (aka tw_rcv_nxt) TCP window next expected seq number
///     [union with @skc_incoming_cpu]
/// @skc_refcnt: reference count
///
/// This is the minimal network layer representation of sockets, the header
/// for struct sock and struct inet_timewait_sock.
struct SockCommon {
    skc_addrpair: __Addrpair,
    skc_daddr: u32,
    skc_rcv_saddr: u32,
    skc_hash: u32,
    skc_u16hashes: [u16; 2],
    /// skc_dport && skc_num must be grouped as well
    skc_portpair: __Portpair,
    skc_dport: u16,
    skc_num: u16,
    skc_family: u16,
    skc_state: u8,
    skc_reuse: u8,
    skc_reuseport: u8,
    skc_ipv6only: u8,
    skc_net_refcnt: u8,
    skc_bound_dev_if: i32,
    skc_bind_node: HlistNode,
    skc_portaddr_node: HlistNode,
    skc_prot: *mut Proto,
    skc_net: PossibleNetT,
    #[cfg(feature = "CONFIG_IPV6")]
    skc_v6_daddr: In6Addr,
    #[cfg(feature = "CONFIG_IPV6")]
    skc_v6_rcv_saddr: In6Addr,
    skc_cookie: Atomic64T,
    /// following fields are padding to force
    /// offset(struct sock, sk_refcnt) == 128 on 64bit arches
    /// assuming IPV6 is enabled. We use this padding differently
    /// for different kind of 'sockets'
    skc_flags: u64,
    /// request_sock
    skc_listener: *mut Sock,
    /// inet_timewait_sock
    skc_tw_dr: *mut InetTimewaitDeathRow,
    /// fields between dontcopy_begin/dontcopy_end
    /// are not copied in sock_copy()
    /// private:
    skc_dontcopy_begin: i32,
    /// public:
    skc_node: HlistNode,
    skc_nulls_node: HlistNullsNode,
    skc_tx_queue_mapping: u16,
    #[cfg(feature = "CONFIG_SOCK_RX_QUEUE_MAPPING")]
    skc_rx_queue_mapping: u16,
    skc_incoming_cpu: i32,
    skc_rcv_wnd: u32,
    /// struct tcp_timewait_sock
    skc_tw_rcv_nxt: u32,
    skc_refcnt: RefCountT,
    /// private:
    skc_dontcopy_end: i32,
    skc_rxhash: u32,
    skc_window_clamp: u32,
    /// struct tcp_timewait_sock
    skc_tw_snd_nxt: u32,
}

/// Networking protocol blocks we attach to sockets.
/// socket layer -> transport layer interface
#[cfg(all(feature = "CONFIG_RT_GROUP_SCHED", feature = "CONFIG_WATCH_QUEUE"))]
struct Proto {
    /// Current allocated memory.
    memory_allocated: *mut AtomicLongT,
    per_cpu_fw_alloc: i32,
    /// Current number of sockets.
    sockets_allocated: *mut PercpuCounter,
    /// Pressure flag: try to collapse.
    /// Technical note: it is used by multiple contexts non atomically.
    /// Make sure to use READ_ONCE()/WRITE_ONCE() for all reads/writes.
    /// All the __sk_mem_schedule() is of this nature: accounting
    /// is strict, actions are advisory and have some latency.
    memory_pressure: *mut u64,
    sysctl_mem: *mut i64,
    sysctl_wmem: *mut i32,
    sysctl_rmem: *mut i32,
    sysctl_wmem_offset: u32,
    sysctl_rmem_offset: u32,
    max_header: i32,
    no_autobind: bool,
    slab: KmemCache,
    obj_size: u32,
    ipv6_pinfo_offset: u32,
    slab_flags: u32,
    /// Usercopy region offset
    useroffset: u32,
    /// Usercopy region size
    usersize: u32,
    orphan_count: *mut u32,
    //rsk_prot: *mut RequestSockOps, TODO impl fns
    //twsk_prot: *mut TimewaitSockOps,
    hashinfo: InetHashinfo,
    udp_table: *mut UdpTable,
    raw_hash: *mut RawHashinfo,
    smc_hash: *mut SmcHashinfo,
    owner: *mut Module,
    name: [char; 32],
    node: ListHead,
}

/* TODO: impl fns
struct proto {
    void            (*close)(struct sock *sk,
                    long timeout);
    int         (*pre_connect)(struct sock *sk,
                    struct sockaddr *uaddr,
                    int addr_len);
    int         (*connect)(struct sock *sk,
                    struct sockaddr *uaddr,
                    int addr_len);
    int         (*disconnect)(struct sock *sk, int flags);

    struct sock *       (*accept)(struct sock *sk, int flags, int *err,
                      bool kern);

    int         (*ioctl)(struct sock *sk, int cmd,
                     int *karg);
    int         (*init)(struct sock *sk);
    void            (*destroy)(struct sock *sk);
    void            (*shutdown)(struct sock *sk, int how);
    int         (*setsockopt)(struct sock *sk, int level,
                    int optname, sockptr_t optval,
                    unsigned int optlen);
    int         (*getsockopt)(struct sock *sk, int level,
                    int optname, char __user *optval,
                    int __user *option);
    void            (*keepalive)(struct sock *sk, int valbool);
#ifdef CONFIG_COMPAT
    int         (*compat_ioctl)(struct sock *sk,
                    unsigned int cmd, unsigned long arg);
#endif
    int         (*sendmsg)(struct sock *sk, struct msghdr *msg,
                       size_t len);
    int         (*recvmsg)(struct sock *sk, struct msghdr *msg,
                       size_t len, int flags, int *addr_len);
    void            (*splice_eof)(struct socket *sock);
    int         (*bind)(struct sock *sk,
                    struct sockaddr *addr, int addr_len);
    int         (*bind_add)(struct sock *sk,
                    struct sockaddr *addr, int addr_len);

    int         (*backlog_rcv) (struct sock *sk,
                        struct sk_buff *skb);
    bool            (*bpf_bypass_getsockopt)(int level,
                             int optname);

    void        (*release_cb)(struct sock *sk);

    /* Keeping track of sk's, looking them up, and port selection methods. */
    int         (*hash)(struct sock *sk);
    void            (*unhash)(struct sock *sk);
    void            (*rehash)(struct sock *sk);
    int         (*get_port)(struct sock *sk, unsigned short snum);
    void            (*put_port)(struct sock *sk);

#ifdef CONFIG_BPF_SYSCALL
    int         (*psock_update_sk_prot)(struct sock *sk,struct sk_psock *psock, bool restore);
#endif

    /* Keeping track of sockets in use */
#ifdef CONFIG_PROC_FS
    unsigned int        inuse_idx;
#endif

#if IS_ENABLED(CONFIG_MPTCP)
    int         (*forward_alloc_get)(const struct sock *sk);
#endif

    bool            (*stream_memory_free)(const struct sock *sk, int wake);
    bool            (*sock_is_readable)(struct sock *sk);

    /* Memory pressure */

    void            (*enter_memory_pressure)(struct sock *sk);
    void            (*leave_memory_pressure)(struct sock *sk);
    int         (*diag_destroy)(struct sock *sk, int err);
*/

/// This is the per-socket lock.  The spinlock provides a synchronization
/// between user contexts and software interrupt processing, whereas the
/// mini-semaphore synchronizes multiple users amongst themselves.
struct SocketLockT {
    slock: SpinlockT,
    owned: i32,
    wq: WaitQueueHeadT,
}
/*#ifdef CONFIG_DEBUG_LOCK_ALLOC
    struct lockdep_map dep_map;
#endif*/
