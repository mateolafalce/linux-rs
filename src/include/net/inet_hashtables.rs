/**************************************************************************
 *   inet_hashtables.rs  --  This file is part of linux-rs.               *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::{
    include::{
        linux::{
            list_nulls::HlistNullsHead,
            spinlock_types::SpinlockT,
            types::{HlistHead, HlistNode},
            uidgid_types::KuidT,
        },
        net::net_namespace::PossibleNetT,
    },
    mm::slab::KmemCache,
};

pub struct InetHashinfo {
    /// This is for sockets with full identity only.  Sockets here will
    /// always be without wildcards and will have the following invariant:
    ///
    /// TCP_ESTABLISHED <= sk->sk_state < TCP_CLOSE
    ehash: *mut InetEhashBucket,
    ehash_locks: *mut SpinlockT,
    ehash_mask: u32,
    ehash_locks_mask: u32,
    /// Ok, let's try this, I give up, we do need a local binding
    /// TCP hash as well as the others for fast bind/connect.
    bind_bucket_cachep: *mut KmemCache,
    /// This bind table is hashed by local port
    bhash: *mut InetBindHashbucket,
    bind2_bucket_cachep: *mut KmemCache,
    /// This bind table is hashed by local port and sk->sk_rcv_saddr (ipv4)
    /// or sk->sk_v6_rcv_saddr (ipv6). This 2nd bind table is used
    /// primarily for expediting bind conflict resolution.
    bhash2: *mut InetBindHashbucket,
    bhash_size: u32,
    /// The 2nd listener table hashed by local port and address
    lhash2_mask: u32,
    lhash2: InetListenHashbucket,
    pernet: bool,
}

///  This is for all connections with a full identity, no wildcards.
///  The 'e' prefix stands for Establish, but we really put all sockets
///  but LISTEN ones.
struct InetEhashBucket {
    chain: HlistNullsHead,
}

struct InetBindBucket {
    ib_net: PossibleNetT,
    l3mdev: i32,
    port: u16,
    char: i32,
    fastuid: KuidT,
    fast_rcv_saddr: i32,
    fast_sk_family: u16,
    fast_ipv6_only: bool,
    node: HlistNode,
    bhash2: HlistHead,
    #[cfg(feature = "CONFIG_IPV6")]
    fast_v6_rcv_saddr: In6Addr,
}

struct InetBindHashbucket {
    lock: SpinlockT,
    chain: HlistHead,
}

///  Sockets can be hashed in established or listening table.
///  We must use different 'nulls' end-of-chain value for all hash buckets :
///  A socket might transition from ESTABLISH to LISTEN state without
///  RCU grace period. A lookup in ehash table needs to handle this case.
struct InetListenHashbucket {
    lock: SpinlockT,
    nulls_head: HlistNullsHead,
}
