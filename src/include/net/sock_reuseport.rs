/**************************************************************************
 *   sock_reuseport.rs  --  This file is part of linux-rs.                *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::{
    include::{linux::types::RcuHead, net::sock::Sock},
    kernel::bpf::preload::iterators::iterators_bpf::BpfProg,
};

pub struct SockReuseport {
    rcu: RcuHead,
    /// length of socks
    max_socks: u16,
    /// elements in socks
    num_socks: u16,
    /// closed elements in socks
    num_closed_socks: u16,
    incoming_cpu: u16,
    /// The last synq overflow event timestamp of this
    /// reuse->socks[] group.
    synq_overflow_ts: u32,
    /// ID stays the same even after the size of socks[] grows.
    reuseport_id: u32,
    bind_inany: u32,
    has_conns: u32,
    /// optional BPF sock selector
    prog: *mut BpfProg,
    /// array of sock pointers
    socks: [*mut Sock; 10],
}
