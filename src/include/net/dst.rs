/**************************************************************************
 *   dst.rs  --  This file is part of linux-rs.                           *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::{
    linux::{
        netdevice::NetDevice,
        types::{ListHead, RcuHead, RcurefT},
    },
    net::{ipv4::route::UncachedList, lwtunnel::LwtunnelState, net_trackers::NetdeviceTracker},
};

pub struct DstEntry {
    dev: *mut NetDevice,
    //TODO ops: *mut DstOps,
    _metrics: u64,
    expires: u64,
    #[cfg(feature = "CONFIG_XFRM")]
    xfrm: *mut XfrmState,
    #[cfg(not(feature = "CONFIG_XFRM"))]
    __pad1: *mut u32,
    flags: u16,
    /// A non-zero value of dst->obsolete forces by-hand validation
    /// of the route entry. Positive values are set by the generic
    /// dst layer to indicate that the entry has been forcefully
    /// destroyed.
    ///
    /// Negative values are used by the implementation layer code to
    /// force invocation of the dst_ops->check() method.
    obsolete: i16,
    /// more space at head required
    header_len: u16,
    /// space to reserve at tail
    trailer_len: u16,
    /// __rcuref wants to be on a different cache line from
    /// input/output/ops or performance tanks badly
    /// 64-bit offset 64
    __rcuref: RcurefT,
    __use: i32,
    lastuse: u64,
    rcu_head: RcuHead,
    error: i16,
    __pad: i16,
    tclassid: u32,
    dev_tracker: NetdeviceTracker,
    /// Used by rtable and rt6_info. Moves lwtstate into the next cache
    /// line on 64bit so that lwtstate does not cause false sharing with
    /// __rcuref under contention of __rcuref. This also puts the
    /// frequently accessed members of rtable and rt6_info out of the
    /// __rcuref cache line.
    rt_uncached: ListHead,
    rt_uncached_list: *mut UncachedList,
    lwtstate: *mut LwtunnelState,
}

#[cfg(all(feature = "CONFIG_RT_GROUP_SCHED", feature = "CONFIG_WATCH_QUEUE"))]
impl DstEntry {
    const DST_NOXFRM: usize = 0x0002;
    const DST_NOPOLICY: usize = 0x0004;
    const DST_NOCOUNT: usize = 0x0008;
    const DST_FAKE_RTABLE: usize = 0x0010;
    const DST_XFRM_TUNNEL: usize = 0x0020;
    const DST_XFRM_QUEUE: usize = 0x0040;
    const DST_METADATA: usize = 0x0080;
    const DST_OBSOLETE_NONE: i32 = 0;
    const DST_OBSOLETE_DEAD: i32 = 2;
    const DST_OBSOLETE_FORCE_CHK: i32 = -1;
    const DST_OBSOLETE_KILL: i32 = -2;
}

/*
struct dst_entry {
    TODO: impl fns
    int			(*input)(struct sk_buff *);
    int			(*output)(struct net *net, struct sock *sk, struct sk_buff *skb);
};
*/
