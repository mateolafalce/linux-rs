/**************************************************************************
 *   types.rs  --  This file is part of linux-rs.                         *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{
    device::Device, mm_types::Page, netdevice::NapiStruct, ptr_ring::PtrRing, refcount::RefCountT,
    types::AtomicT, workqueue::DelayedWork,
};

const PP_ALLOC_CACHE_SIZE: usize = 128;

pub struct PagePool {
    p: PagePoolParams,
    frag_users: i64,
    frag_page: *mut Page,
    frag_offset: u32,
    pages_state_hold_cnt: u32,
    release_dw: DelayedWork,
    //void (*disconnect)(void *pool) TODO use impl
    defer_start: u64,
    defer_warn: u64,
    /// these stats are incremented while in softirq context
    #[cfg(feature = "CONFIG_PAGE_POOL_STATS")]
    alloc_stats: PagePoolAllocStats,
    xdp_mem_id: u32,
    /// Data structure for allocation side
    ///
    /// Drivers allocation side usually already perform some kind
    /// of resource protection.  Piggyback on this protection, and
    /// require driver to protect allocation side.
    ///
    /// For NIC drivers this means, allocate a page_pool per
    /// RX-queue. As the RX-queue is already protected by
    /// Softirq/BH scheduling and napi_schedule. NAPI schedule
    /// guarantee that a single napi_struct will only be scheduled
    /// on a single CPU (see napi_schedule).
    alloc: PpAllocCache,
    /// Data structure for storing recycled pages.
    ///
    /// Returning/freeing pages is more complicated synchronization
    /// wise, because free's can happen on remote CPUs, with no
    /// association with allocation resource.
    ///
    /// Use ptr_ring, as it separates consumer and producer
    /// efficiently, it a way that doesn't bounce cache-lines.
    ///
    /// TODO: Implement bulk return pages into this structure.
    ring: PtrRing,
    /// recycle stats are per-cpu to avoid locking
    #[cfg(feature = "CONFIG_PAGE_POOL_STATS")]
    recycle_stats: *mut PagePoolRecycleStats,
    pages_state_release_cnt: AtomicT,
    /// A page_pool is strictly tied to a single RX-queue being
    /// protected by NAPI, due to above PpAllocCache. This
    /// refcnt serves purpose is to simplify drivers error handling.
    user_cnt: RefCountT,
    destroy_cnt: u64,
}

/// struct page_pool_recycle_stats - recycling (freeing) statistics
/// @cached: recycling placed page in the page pool cache
/// @cache_full: page pool cache was full
/// @ring:   page placed into the ptr ring
/// @ring_full:  page released from page pool because the ptr ring was full
/// @released_refcnt:    page released (and not recycled) because refcnt > 1
#[cfg(all(
    feature = "CONFIG_RT_GROUP_SCHED",
    feature = "CONFIG_WATCH_QUEUE",
    feature = "CONFIG_PAGE_POOL_STATS"
))]
struct PagePoolRecycleStats {
    cached: u64,
    cache_full: u64,
    ring: u64,
    ring_full: u64,
    released_refcnt: u64,
}

#[cfg(all(feature = "CONFIG_RT_GROUP_SCHED", feature = "CONFIG_WATCH_QUEUE"))]
pub const PP_ALLOC_CACHE_SIZE: usize = 128;
#[cfg(all(feature = "CONFIG_RT_GROUP_SCHED", feature = "CONFIG_WATCH_QUEUE"))]
pub const PP_ALLOC_CACHE_REFILL: usize = 64;

/// Fast allocation side cache array/stack
///
/// The cache size and refill watermark is related to the network
/// use-case.  The NAPI budget is 64 packets.  After a NAPI poll the RX
/// ring is usually refilled and the max consumed elements will be 64,
/// thus a natural max size of objects needed in the cache.
///
/// Keeping room for more objects, is due to XDP_DROP use-case.  As
/// XDP_DROP allows the opportunity to recycle objects directly into
/// this array, as it shares the same softirq/NAPI protection.  If
/// cache is already full (or partly full) then the XDP_DROP recycles
/// would have to take a slower code path.
struct PpAllocCache {
    count: u32,
    cache: [Page; PP_ALLOC_CACHE_SIZE],
}

/// struct page_pool_alloc_stats - allocation statistics
/// @fast:   successful fast path allocations
/// @slow:   slow path order-0 allocations
/// @slow_high_order: slow path high order allocations
/// @empty:  ptr ring is empty, so a slow path allocation was forced
/// @refill: an allocation which triggered a refill of the cache
/// @waive:  pages obtained from the ptr ring that cannot be added to
///      the cache due to a NUMA mismatch
#[cfg(all(
    feature = "CONFIG_RT_GROUP_SCHED",
    feature = "CONFIG_WATCH_QUEUE",
    feature = "CONFIG_PAGE_POOL_STATS"
))]
struct PagePoolAllocStats {
    fast: u64,
    slow: u64,
    slow_high_order: u64,
    empty: u64,
    refill: u64,
    waive: u64,
}

/// struct page_pool_params - page pool parameters
/// @flags:  PP_FLAG_DMA_MAP, PP_FLAG_DMA_SYNC_DEV
/// @order:  2^order pages on allocation
/// @pool_size:  size of the ptr_ring
/// @nid:    NUMA node id to allocate from pages from
/// @dev:    device, for DMA pre-mapping purposes
/// @napi:   NAPI which is the sole consumer of pages, otherwise NULL
/// @dma_dir:    DMA mapping direction
/// @max_len:    max DMA sync memory size for PP_FLAG_DMA_SYNC_DEV
/// @offset: DMA sync address offset for PP_FLAG_DMA_SYNC_DEV
#[cfg(all(feature = "CONFIG_RT_GROUP_SCHED", feature = "CONFIG_WATCH_QUEUE"))]
struct PagePoolParams {
    flags: u32,
    order: u32,
    pool_size: u32,
    nid: i32,
    dev: *mut Device,
    napi: *mut NapiStruct,
    max_len: u32,
    // enum dma_data_direction dma_dir; TODO
    offset: u32,
    /// private: used by test code only
    // void (*init_callback)(struct page *page, void *arg); TODO
    init_arg: *mut u32,
}

struct PagePoolParams;
/*
TODO
struct page_pool_params {
    struct_group_tagged(page_pool_params_fast, fast,
        unsigned int    order;
        unsigned int    pool_size;
        int     nid;
        struct device   *dev;
        struct napi_struct *napi;
        enum dma_data_direction dma_dir;
        unsigned int    max_len;
        unsigned int    offset;
    );
    struct_group_tagged(page_pool_params_slow, slow,
        struct net_device *netdev;
        unsigned int queue_idx;
        unsigned int    flags;
/* private: used by test code only */
        void (*init_callback)(netmem_ref netmem, void *arg);
        void *init_arg;
    );
};
*/
