/**************************************************************************
 *   if_inet6.rs  --  This file is part of linux-rs.                      *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::{
    fs::proc::internal::ProcDirEntry,
    include::{
        linux::{
            ipv6::Ipv6Devconf,
            mutex_types::Mutex,
            netdevice::NetDevice,
            refcount::RefCountT,
            rwlock_types::RwlockT,
            skbuff::SkBuffHead,
            spinlock_types::SpinlockT,
            timer::TimerList,
            types::{ListHead, RcuHead},
            workqueue::DelayedWork,
        },
        net::{neighbour::NeighParms, net_trackers::NetdeviceTracker},
        uapi::linux::in6::In6Addr,
    },
};

pub struct Inet6Dev {
    dev: *mut NetDevice,
    dev_tracker: NetdeviceTracker,
    addr_list: ListHead,
    mc_list: *mut Ifmcaddr6,
    mc_tomb: *mut Ifmcaddr6,
    /// Query Robustness Variable
    mc_qrv: u8,
    mc_gq_running: u8,
    mc_ifc_count: u8,
    mc_dad_count: u8,
    /// Max time we stay in MLDv1 mode
    mc_v1_seen: u64,
    /// Query Interval
    mc_qi: u64,
    /// Query Response Interval
    mc_qri: u64,
    mc_maxdelay: u64,
    /// general query work
    mc_gq_work: DelayedWork,
    /// interface change work
    mc_ifc_work: DelayedWork,
    /// dad complete mc work
    mc_dad_work: DelayedWork,
    /// mld query work
    mc_query_work: DelayedWork,
    /// mld report work
    mc_report_work: DelayedWork,
    /// mld query queue
    mc_query_queue: SkBuffHead,
    /// mld report queue
    mc_report_queue: SkBuffHead,
    /// mld query queue lock
    mc_query_lock: SpinlockT,
    /// mld query report lock
    mc_report_lock: SpinlockT,
    /// mld global lock
    mc_lock: Mutex,
    ac_list: *mut Ifmcaddr6,
    lock: RwlockT,
    refcnt: RefCountT,
    if_flags: u32,
    dead: i32,
    desync_factor: u32,
    tempaddr_list: ListHead,
    token: In6Addr,
    nd_parms: *mut NeighParms,
    cnf: Ipv6Devconf,
    stats: Ipv6Devstat,
    rs_timer: TimerList,
    /// in jiffies
    rs_interval: i32,
    rs_probes: u8,
    /// ipv6InterfaceTable update timestamp
    tstamp: u64,
    rcu: RcuHead,
    ra_mtu: u32,
}

struct Ifmcaddr6 {
    mca_addr: In6Addr,
    idev: *mut Inet6Dev,
    next: *mut Ifmcaddr6,
    mca_sources: *mut Ip6SfList,
    mca_tomb: *mut Ip6SfList,
    mca_sfmode: u32,
    mca_crcount: u8,
    mca_sfcount: [u64; 2],
    mca_work: DelayedWork,
    mca_flags: u32,
    mca_users: i32,
    mca_refcnt: RefCountT,
    mca_cstamp: u64,
    mca_tstamp: u64,
    rcu: RcuHead,
}

struct Ip6SfList {
    sf_next: *mut Ip6SfList,
    sf_addr: In6Addr,
    sf_count: u64,
    sf_gsresp: u8,
    sf_oldin: u8,
    sf_crcount: u8,
    rcu: RcuHead,
}

struct Ipv6Devstat {
    proc_dir_entry: *mut ProcDirEntry,
    /*TODO DEFINE_SNMP_STAT(struct ipstats_mib, ipv6);
    DEFINE_SNMP_STAT_ATOMIC(struct icmpv6_mib_device, icmpv6dev);
    DEFINE_SNMP_STAT_ATOMIC(struct icmpv6msg_mib_device, icmpv6msgdev);*/
}
