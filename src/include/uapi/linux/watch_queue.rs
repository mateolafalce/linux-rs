/**************************************************************************
 *   watch_queue.rs  --  This file is part of linux-rs.                   *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

#[cfg(all(feature = "CONFIG_RT_GROUP_SCHED", feature = "CONFIG_WATCH_QUEUE"))]
pub struct WatchNotificationType {}

#[cfg(all(feature = "CONFIG_RT_GROUP_SCHED", feature = "CONFIG_WATCH_QUEUE"))]
impl WatchNotificationType {
    /// Special record
    pub const WATCH_TYPE_META: usize = 0;
    /// Key change event notification
    pub const WATCH_TYPE_KEY_NOTIFY: usize = 1;
    pub const WATCH_TYPE__NR: usize = 2;
}
