/**************************************************************************
 *   ip.rs  --  This file is part of linux-rs.                            *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

/// index values for the variables in ipv4_devconf
const IPV4_DEVCONF_FORWARDING: usize = 1;
const IPV4_DEVCONF_MC_FORWARDING: usize = 2;
const IPV4_DEVCONF_PROXY_ARP: usize = 3;
const IPV4_DEVCONF_ACCEPT_REDIRECTS: usize = 4;
const IPV4_DEVCONF_SECURE_REDIRECTS: usize = 5;
const IPV4_DEVCONF_SEND_REDIRECTS: usize = 6;
const IPV4_DEVCONF_SHARED_MEDIA: usize = 7;
const IPV4_DEVCONF_RP_FILTER: usize = 8;
const IPV4_DEVCONF_ACCEPT_SOURCE_ROUTE: usize = 9;
const IPV4_DEVCONF_BOOTP_RELAY: usize = 10;
const IPV4_DEVCONF_LOG_MARTIANS: usize = 11;
const IPV4_DEVCONF_TAG: usize = 12;
const IPV4_DEVCONF_ARPFILTER: usize = 13;
const IPV4_DEVCONF_MEDIUM_ID: usize = 14;
const IPV4_DEVCONF_NOXFRM: usize = 15;
const IPV4_DEVCONF_NOPOLICY: usize = 16;
const IPV4_DEVCONF_FORCE_IGMP_VERSION: usize = 17;
const IPV4_DEVCONF_ARP_ANNOUNCE: usize = 18;
const IPV4_DEVCONF_ARP_IGNORE: usize = 19;
const IPV4_DEVCONF_PROMOTE_SECONDARIES: usize = 20;
const IPV4_DEVCONF_ARP_ACCEPT: usize = 21;
const IPV4_DEVCONF_ARP_NOTIFY: usize = 22;
const IPV4_DEVCONF_ACCEPT_LOCAL: usize = 23;
const IPV4_DEVCONF_SRC_VMARK: usize = 24;
const IPV4_DEVCONF_PROXY_ARP_PVLAN: usize = 25;
const IPV4_DEVCONF_ROUTE_LOCALNET: usize = 26;
const IPV4_DEVCONF_IGMPV2_UNSOLICITED_REPORT_INTERVAL: usize = 27;
const IPV4_DEVCONF_IGMPV3_UNSOLICITED_REPORT_INTERVAL: usize = 28;
const IPV4_DEVCONF_IGNORE_ROUTES_WITH_LINKDOWN: usize = 29;
const IPV4_DEVCONF_DROP_UNICAST_IN_L2_MULTICAST: usize = 30;
const IPV4_DEVCONF_DROP_GRATUITOUS_ARP: usize = 31;
const IPV4_DEVCONF_BC_FORWARDING: usize = 32;
const IPV4_DEVCONF_ARP_EVICT_NOCARRIER: usize = 33;
const __IPV4_DEVCONF_MAX: usize = 34;

pub const IPV4_DEVCONF_MAX: usize = __IPV4_DEVCONF_MAX - 1;
