/**************************************************************************
 *   limits.rs  --  This file is part of linux-rs.                        *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

const NR_OPEN: usize = 1024;
/// supplemental group IDs are available
const NGROUPS_MAX: usize = 65536;
/// # bytes of args + environ for exec()
const ARG_MAX: usize = 131072;
/// # links a file may have
const LINK_MAX: usize = 127;
/// size of the canonical input queue
const MAX_CANON: usize = 255;
/// size of the type-ahead buffer
const MAX_INPUT: usize = 255;
/// # chars in a file name
const NAME_MAX: usize = 255;
/// # chars in a path name including nul
pub const PATH_MAX: usize = 4096;
/// # bytes in atomic write to a pipe
const PIPE_BUF: usize = 4096;
/// # chars in an extended attribute name
const XATTR_NAME_MAX: usize = 255;
/// size of an extended attribute value (64k)
const XATTR_SIZE_MAX: usize = 65536;
/// size of extended attribute namelist (64k)
const XATTR_LIST_MAX: usize = 65536;
const RTSIG_MAX: usize = 32;
