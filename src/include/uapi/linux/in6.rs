/**************************************************************************
 *   in6.rs  --  This file is part of linux-rs.                           *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

/// IPv6 address structure
pub struct In6Addr {
    u6_addr8: [u8; 16],
    #[cfg(feature = "__UAPI_DEF_IN6_ADDR_ALT")]
    u6_addr16: [u16; 8],
    #[cfg(feature = "__UAPI_DEF_IN6_ADDR_ALT")]
    u6_addr32: [u32; 4],
}

impl In6Addr {
    fn s6_addr(&self) -> [u8; 16] {
        self.u6_addr8
    }
    #[cfg(feature = "__UAPI_DEF_IN6_ADDR_ALT")]
    fn s6_addr16(&self) -> [u16; 8] {
        self.u6_addr16
    }
    #[cfg(feature = "__UAPI_DEF_IN6_ADDR_ALT")]
    fn s6_addr32(&self) -> [u32; 4] {
        self.u6_addr32
    }
}
