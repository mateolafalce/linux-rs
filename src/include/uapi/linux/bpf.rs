/**************************************************************************
 *   bpf.rs  --  This file is part of linux-rs.                           *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

pub const BPF_OBJ_NAME_LEN: usize = 16;

pub struct BpfCgroupStorageKey {
    /// cgroup inode id
    cgroup_inode_id: u64,
    /// program attach type (enum bpf_attach_type)
    attach_type: u32,
}

pub struct BpfFuncInfo {
    insn_off: u32,
    type_id: u32,
}
