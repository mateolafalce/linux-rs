/**************************************************************************
 *   mtd_abi.rs  --  This file is part of linux-rs.                       *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

/// struct mtd_ecc_stats - error correction stats
///
/// @corrected: number of corrected bits
/// @failed: number of uncorrectable errors
/// @badblocks: number of bad blocks in this partition
/// @bbtblocks: number of blocks reserved for bad block tables
pub struct MtdEccStats {
    corrected: u32,
    failed: u32,
    badblocks: u32,
    bbtblocks: u32,
}
