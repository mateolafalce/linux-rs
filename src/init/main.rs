/**************************************************************************
 *   main.rs  --  This file is part of linux-rs.                          *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::{include::linux::sched::TaskStruct, kernel::fork::set_task_stack_end_magic};
use lazy_static::lazy_static;

lazy_static! {
    /// Debug helper: via this flag we know that we are in 'early bootup code'
    /// where only the boot processor is running with IRQ disabled.  This means
    /// two things - IRQ must not be enabled before the flag is cleared and some
    /// operations which are not allowed with IRQ disabled are allowed while the
    /// flag is set.
    pub static ref EARLY_BOOT_IRQS_DISABLED: Mutex<bool> = Mutex::new(false);
}

#[no_mangle]
pub fn start_kernel() {
    let mut command_line: String = String::new();
    let mut after_dashes: String = String::new();

    let init_task: TaskStruct = &mut TaskStruct::init_task();
    set_task_stack_end_magic(init_task);
    smp_setup_processor_id();
    debug_objects_early_init();
    init_vmlinux_build_id();

    cgroup_init_early();

    local_irq_disable();

    *EARLY_BOOT_IRQS_DISABLED.lock().unwrap() = true;

    // Interrupts are still disabled. Do necessary setups, then
    // enable them.

    boot_cpu_init();
    page_address_init();
    pr_notice("{}\n", LINUX_BANNER);
    early_security_init();
    setup_arch(&command_line);
    setup_boot_config();
    setup_command_line(command_line);
    setup_nr_cpu_ids();
    setup_per_cpu_areas();
    smp_prepare_boot_cpu(); // arch-specific boot-cpu hooks
    boot_cpu_hotplug_init();

    pr_notice("Kernel command line: {}\n", saved_command_line);
    // parameters may set static keys
    jump_label_init();
    parse_early_param();
    after_dashes = parse_args(
        "Booting kernel",
        static_command_line,
        __start___param,
        __stop___param - __start___param,
        -1,
        -1,
        NULL,
        &unknown_bootoption,
    );
    print_unknown_bootoptions();
    if !IS_ERR_OR_NULL(after_dashes) {
        parse_args(
            "Setting init args",
            after_dashes,
            NULL,
            0,
            -1,
            -1,
            NULL,
            set_init_arg,
        );
    }
    if extra_init_args {
        parse_args(
            "Setting extra init args",
            extra_init_args,
            NULL,
            0,
            -1,
            -1,
            NULL,
            set_init_arg,
        );
    }

    // Architectural and non-timekeeping rng init, before allocator init
    random_init_early(command_line);

    // These use large bootmem allocations and must precede
    // initalization of page allocator
    setup_log_buf(0);
    vfs_caches_init_early();
    sort_main_extable();
    trap_init();
    mm_core_init();
    poking_init();
    ftrace_init();

    // trace_printk can be enabled here
    early_trace_init();

    // Set up the scheduler prior starting any interrupts (such as the
    // timer interrupt). Full topology setup happens at smp_init()
    // time - but meanwhile we still have a functioning scheduler.
    sched_init();
    if WARN(
        !irqs_disabled(),
        "Interrupts were enabled *very* early, fixing it\n",
    ) {
        local_irq_disable();
    }

    radix_tree_init();
    maple_tree_init();

    /*
     * Set up housekeeping before setting up workqueues to allow the unbound
     * workqueue to take non-housekeeping into account.
     */
    housekeeping_init();

    /*
     * Allow workqueue creation and work item queueing/cancelling
     * early.  Work item execution depends on kthreads and starts after
     * workqueue_init().
     */
    workqueue_init_early();

    rcu_init();

    /* Trace events are available after this */
    trace_init();

    if initcall_debug {
        initcall_debug_enable();
    }

    context_tracking_init();
    /* init some links before init_ISA_irqs() */
    early_irq_init();
    init_IRQ();
    tick_init();
    rcu_init_nohz();
    init_timers();
    srcu_init();
    hrtimers_init();
    softirq_init();
    timekeeping_init();
    time_init();

    // This must be after timekeeping is initialized
    random_init();

    // These make use of the fully initialized rng
    kfence_init();
    boot_init_stack_canary();

    perf_event_init();
    profile_init();
    call_function_init();
    WARN(!irqs_disabled(), "Interrupts were enabled early\n");

    early_boot_irqs_disabled = false;
    local_irq_enable();

    kmem_cache_init_late();

    // HACK ALERT! This is early. We're enabling the console before
    // we've done PCI setups etc, and console_init() must be aware of
    // this. But we do want output early, in case something goes wrong.
    console_init();
    if panic_later {
        panic("Too many boot %s vars at `%s'", panic_later, panic_param);
    }

    lockdep_init();

    /*
     * Need to run this when irqs are enabled, because it wants
     * to self-test [hard/soft]-irqs on/off lock inversion bugs
     * too:
     */
    locking_selftest();

    /*TODO #ifdef CONFIG_BLK_DEV_INITRD
        if (initrd_start && !initrd_below_start_ok &&
            page_to_pfn(virt_to_page((void *)initrd_start)) < min_low_pfn) {
            pr_crit("initrd overwritten (0x%08lx < 0x%08lx) - disabling it.\n",
                page_to_pfn(virt_to_page((void *)initrd_start)),
                min_low_pfn);
            initrd_start = 0;
        }
    #endif*/
    setup_per_cpu_pageset();
    numa_policy_init();
    acpi_early_init();
    if late_time_init {
        late_time_init();
    }
    sched_clock_init();
    calibrate_delay();

    arch_cpu_finalize_init();

    pid_idr_init();
    anon_vma_init();
    /*TODO #ifdef CONFIG_X86
        if (efi_enabled(EFI_RUNTIME_SERVICES))
            efi_enter_virtual_mode();
    #endif*/
    thread_stack_cache_init();
    cred_init();
    fork_init();
    proc_caches_init();
    uts_ns_init();
    key_init();
    security_init();
    dbg_late_init();
    net_ns_init();
    vfs_caches_init();
    pagecache_init();
    signals_init();
    seq_file_init();
    proc_root_init();
    nsfs_init();
    cpuset_init();
    cgroup_init();
    taskstats_init_early();
    delayacct_init();

    acpi_subsystem_init();
    arch_post_acpi_subsys_init();
    kcsan_init();

    // Do the rest non-__init'ed, we're now alive
    arch_call_rest_init();

    // Avoid stack canaries in callers of boot_init_stack_canary for gcc-10
    // and older.
    /*TODO #if !__has_attribute(__no_stack_protector__)
        prevent_tail_call_optimization();
    #endif*/
}
