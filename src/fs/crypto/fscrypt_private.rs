/**************************************************************************
 *   fscrypt_private.rs  --  This file is part of linux-rs.               *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

#[cfg(all(feature = "CONFIG_FS_VERITY", feature = "CONFIG_RT_GROUP_SCHED"))]
use crate::include::linux::{fs::Inode, types::ListHead};

/// fscrypt_inode_info - the "encryption key" for an inode
///
/// When an encrypted file's key is made available, an instance of this struct is
/// allocated and stored in ->i_crypt_info.  Once created, it remains until the
/// inode is evicted.
#[cfg(all(feature = "CONFIG_FS_VERITY", feature = "CONFIG_RT_GROUP_SCHED"))]
pub struct FscryptInodeInfo {
    /// The key in a form prepared for actual encryption/decryption
    ci_enc_key: FscryptPreparedKey,
    /// True if ci_enc_key should be freed when this struct is freed
    ci_owns_key: bool,
    /// True if this inode will use inline encryption (blk-crypto) instead of
    /// the traditional filesystem-layer encryption.
    #[cfg(feature = "CONFIG_FS_ENCRYPTION_INLINE_CRYPT")]
    ci_inlinecrypt: bool,
    /// log2 of the data unit size (granularity of contents encryption) of
    /// this file.  This is computable from ci_policy and ci_inode but is
    /// cached here for efficiency.  Only used for regular files.
    ci_data_unit_bits: u8,
    /// Cached value: log2 of number of data units per FS block
    ci_data_units_per_block_bits: u8,
    /// Encryption mode used for this inode.  It corresponds to either the
    /// contents or filenames encryption mode, depending on the inode type.
    ci_mode: *mut FscryptMode,
    /// Back-pointer to the inode
    ci_inode: *mut Inode,
    /// The master key with which this inode was unlocked (decrypted).  This
    /// will be NULL if the master key was found in a process-subscribed
    /// keyring rather than in the filesystem-level keyring.
    ci_master_key: *mut FscryptMasterKey,
    /// Link in list of inodes that were unlocked with the master key.
    /// Only used when ->ci_master_key is set.
    ci_master_key_link: ListHead,
    /// If non-NULL, then encryption is done using the master key directly
    /// and ci_enc_key will equal ci_direct_key -> dk_key.
    ci_direct_key: *mut FscryptDirectKey,
    /// This inode's hash key for filenames.  This is a 128-bit SipHash-2-4
    /// key.  This is only set for directories that use a keyed dirhash over
    /// the plaintext filenames -- currently just casefolded directories.
    ci_dirhash_key: SiphashKeyT,
    ci_dirhash_key_initialized: bool,
    /// The encryption policy used by this inode
    ci_policy: FscryptPolicy,
    /// This inode's nonce, copied from the fscrypt_context
    ci_nonce: [u8; FSCRYPT_FILE_NONCE_SIZE],
    /// Hashed inode number.  Only set for IV_INO_LBLK_32
    ci_hashed_ino: u32,
}
