/**************************************************************************
 *   kernfs_internal.rs  --  This file is part of linux-rs.               *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{
    idr::Idr,
    kernfs::KernfsNode,
    rwsem::RwSemaphore,
    time64::Timespec64,
    types::{AtomicT, ListHead},
    uidgid_types::{KgidT, KuidT},
    wait::WaitQueueHeadT,
    xattr::SimpleXattrs,
};

pub struct KernfsIattrs {
    ia_uid: KuidT,
    ia_gid: KgidT,
    ia_atime: Timespec64,
    ia_mtime: Timespec64,
    ia_ctime: Timespec64,
    xattrs: SimpleXattrs,
    nr_user_xattrs: AtomicT,
    user_xattr_size: AtomicT,
}

pub struct KernfsRoot {
    /// published fields
    kn: *mut KernfsNode,
    /// KERNFS_ROOT_* flags
    flags: u32,
    /// private fields, do not use outside kernfs proper
    ino_idr: Idr,
    last_id_lowbits: u32,
    id_highbits: u32,
    // TODO impl fns syscall_ops: *mut KernfsSyscallOps,
    /// list of kernfs_super_info of this root, protected by kernfs_rwsem
    supers: ListHead,
    deactivate_waitq: WaitQueueHeadT,
    kernfs_rwsem: RwSemaphore,
    kernfs_iattr_rwsem: RwSemaphore,
    kernfs_supers_rwsem: RwSemaphore,
}
