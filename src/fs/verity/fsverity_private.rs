/**************************************************************************
 *   fsverity_private.rs  --  This file is part of linux-rs.              *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

#[cfg(all(feature = "CONFIG_FS_VERITY", feature = "CONFIG_RT_GROUP_SCHED"))]
use crate::include::linux::{fs::Inode, spinlock_types::SpinlockT};

/// fsverity_info - cached verity metadata for an inode
///
/// When a verity file is first opened, an instance of this struct is allocated
/// and stored in ->i_verity_info; it remains until the inode is evicted.  It
/// caches information about the Merkle tree that's needed to efficiently verify
/// data read from the file.  It also caches the file digest.  The Merkle tree
/// pages themselves are not cached here, but the filesystem may cache them.
#[cfg(all(feature = "CONFIG_FS_VERITY", feature = "CONFIG_RT_GROUP_SCHED"))]
pub struct FsverityInfo {
    tree_params: MerkleTreeParams,
    root_hash: [u8; FS_VERITY_MAX_DIGEST_SIZE],
    file_digest: [u8; FS_VERITY_MAX_DIGEST_SIZE],
    inode: *const Inode,
    hash_block_verified: *mut u64,
    hash_page_init_lock: SpinlockT,
}
