/**************************************************************************
 *   eventfd.rs  --  This file is part of linux-rs.                       *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{kref::Kref, wait::WaitQueueHeadT};

pub struct EventfdCtx {
    kref: Kref,
    wqh: WaitQueueHeadT,
    /// Every time that a write(2) is performed on an eventfd, the
    /// value of the __u64 being written is added to "count" and a
    /// wakeup is performed on "wqh". If EFD_SEMAPHORE flag was not
    /// specified, a read(2) will return the "count" value to userspace,
    /// and will reset "count" to zero. The kernel side eventfd_signal()
    /// also, adds to the "count" counter and issue a wakeup.
    count: u64,
    flags: u32,
    id: i32,
}
