/**************************************************************************
 *   internal.rs  --  This file is part of linux-rs.                      *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{
    completion::Completion,
    rbtree_types::RbNode,
    rbtree_types::RbRoot,
    refcount::RefCountT,
    spinlock_types::SpinlockT,
    types::{AtomicT, ListHead, LoffT, NlinkT, UmodeT},
    uidgid_types::KgidT,
    uidgid_types::KuidT,
};

///  This is not completely implemented yet. The idea is to
///  create an in-memory tree (like the actual /proc filesystem
///  tree) of these proc_dir_entries, so that we can dynamically
///  add new files to /proc.
///
///  parent/subdir are used for the directory structure (every /proc file has a
///  parent, but "subdir" is empty for all non-directory entries).
///  subdir_node is used to build the rb tree "subdir" of the parent.
pub struct ProcDirEntry {
    in_use: AtomicT,
    refcnt: RefCountT,
    pde_openers: ListHead,
    pde_unload_lock: SpinlockT,
    pde_unload_completion: *mut Completion,
    //TODOwrite: ProcWriteT,
    data: *mut u32,
    state_size: u32,
    low_ino: u32,
    nlink: NlinkT,
    uid: KuidT,
    gid: KgidT,
    size: LoffT,
    parent: *mut ProcDirEntry,
    subdir: RbRoot,
    subdir_node: RbNode,
    name: char,
    mode: UmodeT,
    flags: u8,
    namelen: u8,
    //TODO: char inline_name[];
}
/*TODO: const struct inode_operations *proc_iops;
onst struct proc_ops *proc_ops;
const struct file_operations *proc_dir_ops;
const struct dentry_operations *proc_dops;
const struct seq_operations *seq_ops;
int (*single_show)(struct seq_file *, void *);*/
