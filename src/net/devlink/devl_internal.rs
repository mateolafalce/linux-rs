/**************************************************************************
 *   devl_internal.rs  --  This file is part of linux-rs.                 *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::{types::ListHead, xarray::Xarray};

pub struct Devlink {
    index: u32,
    ports: Xarray,
    rate_list: ListHead,
    sb_list: ListHead,
    dpipe_table_list: ListHead,
    resource_list: ListHead,
    params: Xarray,
    region_list: ListHead,
    reporter_list: ListHead,
    dpipe_headers: *mut DevlinkDpipeHeaders,
    trap_list: ListHead,
    trap_group_list: ListHead,
    trap_policer_list: ListHead,
    linecard_list: ListHead,
    ops: *const DevlinkOps,
    snapshot_ids: Xarray,
    stats: DevlinkDevStats,
    dev: *mut Device,
    _net: PossibleNetT,
    /// Serializes access to devlink instance specific objects such as
    /// port, sb, dpipe, resource, params, region, traps and more.
    lock: Mutex,
    lock_key: LockClassKey,
    reload_failed: bool,
    refcount: RefcountT,
    rwork: RcuWork,
    rel: *mut DevlinkRel,
    nested_rels: Xarray,
    //TODOchar priv[] __aligned(NETDEV_ALIGN);
}
