/**************************************************************************
 *   udp_tunnel_nic.rs  --  This file is part of linux-rs.                *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::linux::workqueue::WorkStruct;

///  struct udp_tunnel_nic - UDP tunnel port offload state
///  @work:   async work for talking to hardware from process context
///  @dev:    netdev pointer
///  @need_sync:  at least one port start changed
///  @need_replay: space was freed, we need a replay of all ports
///  @work_pending: @work is currently scheduled
///  @n_tables:   number of tables under @entries
///  @missed: bitmap of tables which overflown
///  @entries:    table of tables of ports currently offloaded
pub struct UdpTunnelNic {
    work: WorkStruct,
    dev: *mut NetDevice,
    need_sync: bool,
    need_replay: bool,
    work_pending: bool,
    n_tables: u32,
    missed: u64,
}
