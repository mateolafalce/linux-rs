/**************************************************************************
 *   gen_estimator.rs  --  This file is part of linux-rs.                 *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::include::{linux::spinlock_types::SpinlockT, net::gen_stats::GnetStatsBasicSync};

///  This code is NOT intended to be used for statistics collection,
///  its purpose is to provide a base for statistical multiplexing
///  for controlled load service.
///  If you need only statistics, run a user level daemon which
///  periodically reads byte counters.
pub struct NetRateEstimator {
    bstats: *mut GnetStatsBasicSync,
    stats_lock: *mut SpinlockT,
    running: bool,
    cpu_bstats: *mut GnetStatsBasicSync,
    ewma_log: u8,
    intvl_log: u8,
    seq: SeqcountT,
    last_packets: u64,
    last_bytes: u64,
    avpps: u64,
    avbps: u64,
    next_jiffies: u64,
    timer: TimerList,
    rcu: RcuHead,
}
