/**************************************************************************
 *   mmu.rs  --  This file is part of linux-rs.                           *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use crate::{
    arch::x86::include::asm::vdso::VdsoImage,
    include::linux::{
        mutex_types::Mutex,
        types::{Atomic64T, AtomicT},
    },
};

/// x86 has arch-specific MMU state beyond what lives in mm_struct.
pub struct MmContextT {
    /// ctx_id uniquely identifies this mm_struct.  A ctx_id will never
    /// be reused, and zero is not a valid ctx_id.
    ctx_id: u64,
    /// Any code that needs to do any sort of TLB flushing for this
    /// mm will first make its changes to the page tables, then
    /// increment tlb_gen, then flush.  This lets the low-level
    /// flushing code keep track of what needs flushing.
    ///
    /// This is not used on Xen PV.
    tlb_gen: Atomic64T,
    #[cfg(feature = "CONFIG_MODIFY_LDT_SYSCALL")]
    ldt_usr_sem: RwSemaphore,
    #[cfg(feature = "CONFIG_MODIFY_LDT_SYSCALL")]
    ldt: *mut LdtStruct,
    flags: u64,
    /// Active LAM mode:  X86_CR3_LAM_U48 or X86_CR3_LAM_U57 or 0 (disabled)
    #[cfg(feature = "CONFIG_ADDRESS_MASKING")]
    lam_cr3_mask: u64,
    /// Significant bits of the virtual address. Excludes tag bits.
    #[cfg(feature = "CONFIG_ADDRESS_MASKING")]
    untag_mask: u64,
    lock: Mutex,
    /// vdso base address
    vdso: *mut u32,
    /// vdso image in use
    vdso_image: *const VdsoImage,
    /// nonzero if rdpmc is allowed
    perf_rdpmc_allowed: AtomicT,
    /// One bit per protection key says whether userspace can
    /// use it or not.  protected by mmap_lock.
    #[cfg(feature = "CONFIG_X86_INTEL_MEMORY_PROTECTION_KEYS")]
    pkey_allocation_map: u16,
    #[cfg(feature = "CONFIG_X86_INTEL_MEMORY_PROTECTION_KEYS")]
    execute_only_pkey: i16,
}
