/**************************************************************************
 *   main.rs  --  This file is part of linux-rs.                          *
 *                                                                        *
 *   Copyright (C) 2025 Mateo Lafalce                                     *
 *                                                                        *
 *   linux-rs is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   linux-rs is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

#![no_main]
#![no_std]

pub mod arch;
pub mod block;
pub mod drivers;
pub mod fs;
pub mod include;
pub mod init;
pub mod kernel;
pub mod mm;
pub mod net;
pub mod panic;
pub mod tools;

use crate::init::main::start_kernel;

#[no_mangle]
#[allow(clippy::empty_loop)]
pub extern "C" fn _start_kernel() -> ! {
    start_kernel();
    loop {}
}
