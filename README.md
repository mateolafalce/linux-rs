<div align="center">

# linux-rs

</div>

A complete reimplementation of the Linux kernel in Rust code, 
with a focus on the x86_64 architecture. Ensuring memory safety, 
secure concurrency and parallelism with powerful abstractions 
without loss of performance improving kernel maintainability 
and readability.

## Contributing

For `linux-rs` development is need rust nightly

```bash
rustup default nightly
```